package np.com.cscpl.AuthorizationServer.config;

public class SecurityConstant {

    public static final String PRIVATE_KEY="-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEpQIBAAKCAQEAyxn2UXLrx7HLnm1R5Kqx9giKPVpoxUbrlyxYuxLTvu4qqlbX\n" +
            "PWCWfwhjvlhCxmttHStWvAqEAzQRY9NUrPvkan9bG3NH4p2uURf9p+iBmeSjjvOp\n" +
            "dTtgl2eypFIiKBNggXJe43hJ7tUdOhQ08F/7isc0HL2lL6WoNRIuCbLVLf1xY1Fg\n" +
            "CUu3bduAXmgX85ikrm73lEe080xH30r0W9fvlqGMRq4UAyGSqeBn5sVwKAKTPvMU\n" +
            "HrP/KRfSDqpYE4paNGaXIHoomJvYiXVjXZ3gOvXtDsTsdWVDKQKnah1Yx3oLrzgt\n" +
            "iaNHnuyktAcaBONz89XJWRcjQkA7ooAPZe5sqQIDAQABAoIBAQC7fQ0A5Vi5MO0q\n" +
            "I/rMX7MGz51lw9ch/jUvibWjhF7KS6xBMQdjVo23WTRSm8GQz8ybd82Kqx5woplH\n" +
            "EOiDA8k5crXVIleC4FLx83aqkR1KxmEibtbJLPYe7pBf1p5HZpPZC46C6CFAA6YG\n" +
            "Xm78U26vDvORcJRT5tXfoBe1xku7Niulk1AObjO3Iv3JloFoBhe+7xq7PpEOGLXu\n" +
            "0ivkLCp//ePo+thazuMYNuMrhmwNdO+pWln/bRporEYB9uDNbyVpdVrPeuvKxwa/\n" +
            "Sy/NCJ2CJZz0Lc1dkurAfpVlz72zBKxTGJDpO3iB8Rqr7JPDxtMEFrYYdSlE8aLJ\n" +
            "PeIx9wjxAoGBAPprUFDLq2kGP65Dy00iR9z5JThVG76fSp7idNWoRDD97jPx9d6Y\n" +
            "iVPp0QMnb3qFzrWzJl38+N1+Hv9nzfm/Mrstq5Re6OFmtZidxXbLKa+AB6sCXSsi\n" +
            "MZQ/soh0TYoCG7E4si4D7g9ygdjC3wBSL1QqKXlEKgwQWe+OEKiDLVOFAoGBAM+g\n" +
            "sSqnPzf8JKVR3rxPVdEIwj+I/zv4n+IffM2cvVdfi1y8iAgqnSDvUTvRnyhH1/7Q\n" +
            "DrmnPfcWZL5mIearpXJ3X5SEuFMzbYCY7rots2Btqu0lET6eDR7VB7Q09jGHxyLM\n" +
            "hSkHmZBIpIuTseu7+fyYUGicYyMgknXKPNaRVePVAoGBAKeIhgoXyEySLqZimq+3\n" +
            "AIb4dAs0/UPRg/W6IVoTodN4/xTniotqV225XukSSAWbYYi0GzYEGWbtisVqaCXu\n" +
            "4XzSeBjiSkEKGvHIk6P/FZthN0AUa8qArNS2rLWXwYxUDrzI5oPcgzb4b6BZ9yFa\n" +
            "2mkfb/G+RbcedVzMGZ7za66VAoGBALdpJzgbl9/3z9NuWAkXOCK4zwdbGMS8Y1hY\n" +
            "ImJrtGLLlwtry+sS4cznO2ZRTXxCws5osqlT0IxF7ua2x/nB5RwR0ZAcEcwFoRS5\n" +
            "hCFZH5zuI9DrLqmW6tYyr2SEaHrmi4X1Dyhoe0IWuaxG4yPv4Ow/DMDsbTLpoheF\n" +
            "eGq+5hWlAoGAA9GtnQoq95fGbq8a1ARyO3wT3c5juwacGIFuXKOG7MM1IgTDdLau\n" +
            "taqgoBw0ohsB6/XZu0gNQWAKKSZPuPv+1T2ukzvIxr2yzCJhTWE9CUGxMjD9Mj0S\n" +
            "nCGLt/eBZMioPI/1xugR38Mbz52ONjIB+X/Yto0+WC0Jf1syJYBsZQA=\n" +
            "-----END RSA PRIVATE KEY-----";


    public static final String PUBLIC_KEY="-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyxn2UXLrx7HLnm1R5Kqx\n" +
            "9giKPVpoxUbrlyxYuxLTvu4qqlbXPWCWfwhjvlhCxmttHStWvAqEAzQRY9NUrPvk\n" +
            "an9bG3NH4p2uURf9p+iBmeSjjvOpdTtgl2eypFIiKBNggXJe43hJ7tUdOhQ08F/7\n" +
            "isc0HL2lL6WoNRIuCbLVLf1xY1FgCUu3bduAXmgX85ikrm73lEe080xH30r0W9fv\n" +
            "lqGMRq4UAyGSqeBn5sVwKAKTPvMUHrP/KRfSDqpYE4paNGaXIHoomJvYiXVjXZ3g\n" +
            "OvXtDsTsdWVDKQKnah1Yx3oLrzgtiaNHnuyktAcaBONz89XJWRcjQkA7ooAPZe5s\n" +
            "qQIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
