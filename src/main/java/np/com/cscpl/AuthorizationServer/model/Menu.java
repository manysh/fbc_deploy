package np.com.cscpl.AuthorizationServer.model;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "MENU")
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NAV_LINK")
    private String navLink;
    @Column(name = "MENU_ORDER")
    private int order;

    @Column(name = "ICON_NAME")
    private String icon;

    @OneToMany(mappedBy = "menu",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JsonBackReference
    private List<SubMenu> subMenus = new ArrayList<>();

    public Menu() {
    }

    public Menu(int id,String name, String navLink, int order, String icon, List<SubMenu> subMenus) {
        this.id=id;
        this.name = name;
        this.navLink=navLink;
        this.order = order;
        this.icon=icon;
        this.subMenus = subMenus;
    }

    public Menu(int id,String name){
        this.id=id;
        this.name=name;
    }

    public Menu(int id,String name,String navLink, int order,String icon){
        this.id=id;
        this.name=name;
        this.navLink=navLink;
        this.order=order;
        this.icon=icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public List<SubMenu> getSubMenus() {
        return subMenus;
    }

    public void setSubMenus(List<SubMenu> subMenus) {
        this.subMenus = subMenus;
    }

    public String getNavLink() {
        return navLink;
    }

    public void setNavLink(String navLink) {
        this.navLink = navLink;
    }

    public void add(SubMenu subMenu){
        if(subMenu==null){
            this.subMenus=new ArrayList<>();
        }
        subMenu.setMenu(this);
        subMenus.add(subMenu);
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", navLink='" + navLink + '\'' +
                ", order=" + order +
                ", icon='" + icon + '\'' +
                ", subMenus=" + subMenus +
                '}';
    }
}
