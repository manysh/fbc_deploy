/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Puzan Sakya < puzan at puzansakya@gmail.com >
 */
@Entity
@Table(name = "final_hr_user_degree")
public class FinalHrUserDegree implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "completion_date")    
    private LocalDate completionDate;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private FinalHrUser finalHrUser;

    @JoinColumn(name = "degree_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Degree degree;

    public FinalHrUserDegree() {
    }

    public FinalHrUserDegree(int id) {
        this.id = id;
    }

    public FinalHrUserDegree(LocalDate completionDate, Degree degree) {
        this.completionDate = completionDate;
        this.degree = degree;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(LocalDate completionDate) {
        this.completionDate = completionDate;
    }

    public FinalHrUser getFinalHrUser() {
        return finalHrUser;
    }

    public void setFinalHrUser(FinalHrUser finalHrUser) {
        this.finalHrUser = finalHrUser;
    }

    public Degree getDegree() {
        return degree;
    }

    public void setDegree(Degree degree) {
        this.degree = degree;
    }

    @Override
    public String toString() {
        return "FinalHrUserDegree{" + "id=" + id + ", completionDate=" + completionDate + ", finalHrUser=" + finalHrUser + ", degree=" + degree + '}';
    }

}
