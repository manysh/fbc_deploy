package np.com.cscpl.AuthorizationServer.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "client")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_id")
    private Integer id;

    @Column(name = "name")
    private String clientName;

    @Column(name = "description")
    private String description;


    @OneToMany(mappedBy = ("client1"), cascade = CascadeType.ALL)
    @JsonBackReference
    private List<FinalProject> projectList;


    @OneToMany(mappedBy = ("client2"), cascade = CascadeType.ALL)
    @JsonBackReference
    private List<FinalProject> projectList2;

    @Column(name = "image_path")
    private String imagePath;



    public Client() {
    }

    public Client(Integer id) {
        this.id = id;
    }

    public Client(String clientName, String description, List<FinalProject> projectList,List<FinalProject> projectList2) {

        this.clientName = clientName;
        this.description = description;

        this.projectList = projectList;
        this.projectList2 = projectList2;

    }

    public Client(Integer clientId, String clientName, String description) {
        this.id = clientId;
        this.clientName = clientName;
        this.description = description;

    }

    public Client(String clientName, String description, String imagePath, List<FinalProject> projectList,List<FinalProject>projectList2) {
        this.clientName = clientName;
        this.description = description;
        this.imagePath = imagePath;
        this.projectList = projectList;
        this.projectList2 = projectList2;

    }

    public Integer getClientId() {
        return id;
    }

    public void setClientId(Integer clientId) {
        this.id = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FinalProject> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<FinalProject> projectList) {
        this.projectList = projectList;
    }


    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<FinalProject> getProjectList2() {
        return projectList2;
    }

    public void setProjectList2(List<FinalProject> projectList2) {
        this.projectList2 = projectList2;
    }
}
