package np.com.cscpl.AuthorizationServer.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "FILE_DOWNLOAD")
public class FileDownload {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FILE_DOWNLOAD_ID")
    private int id;

    @Column(name = "FILE_NAME")
    private String fileName;

    @Column(name = "FILE_DESCRIPTION")
    private String fileDescription;

    @Column(name = "DOWNLOAD_LINK")
    private String downloadLink;

    @Column(name = "CREATE_AT")
    private String createdAt;

    public FileDownload() {
        this.createdAt= LocalDate.now().toString();
    }

    public FileDownload(int id,String fileName, String fileDescription, String downloadLink, String createdAt) {
        this.id=id;
        this.fileName = fileName;
        this.fileDescription = fileDescription;
        this.downloadLink = downloadLink;
        this.createdAt= LocalDate.now().toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileDescription() {
        return fileDescription;
    }

    public void setFileDescription(String fileDescription) {
        this.fileDescription = fileDescription;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
