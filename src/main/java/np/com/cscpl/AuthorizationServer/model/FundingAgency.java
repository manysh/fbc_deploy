package np.com.cscpl.AuthorizationServer.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "FUNDING_AGENCY")
public class FundingAgency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME",unique = true)
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToMany(cascade=CascadeType.ALL,mappedBy = "fundingAgencies")
    private List<FinalProject> finalProjects = new ArrayList<>();

    public FundingAgency() {
    }

    public FundingAgency(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FinalProject> getFinalProjects() {
        return finalProjects;
    }

    public void setFinalProjects(List<FinalProject> finalProjects) {
        this.finalProjects = finalProjects;
    }

    @Override
    public String toString() {
        return "FundingAgency{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", finalProjects=" + finalProjects +
                '}';
    }
}
