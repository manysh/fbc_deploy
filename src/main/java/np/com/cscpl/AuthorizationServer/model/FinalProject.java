package np.com.cscpl.AuthorizationServer.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "FINAL_PROJECT")
public class FinalProject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "TITLE", nullable = false)
    private String title;

    @Column(name = "ASSIGNMENT_TITLE", unique = true, nullable = false)
    private String assignmentTitle;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Column(name = "WORK_DESCRIPTION", nullable = true)
    private String workDescription;

    @Column(name = "INITIATION_DATE", nullable = false)
    private String createdDate;

    @Column(name = "COMPLETETION_DATE")
    private String completetionDate;

    @Column(name = "PROJECT_STATUS")
    private String projectStatus;

    @Column(name = "IMAGE_PATH")
    private String imagePath;

    @Column(name = "FILE_PATH")
    private String filePath;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {
        CascadeType.MERGE,
        CascadeType.DETACH,
        CascadeType.PERSIST,
        CascadeType.REFRESH
    })
    @JsonManagedReference
    @JoinColumn(name = "CLIENT1_ID", nullable = false, referencedColumnName = "client_id")
    @NotFound(action = NotFoundAction.IGNORE)
    private Client client1;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {
        CascadeType.MERGE,
        CascadeType.DETACH,
        CascadeType.PERSIST,
        CascadeType.REFRESH
    })
    @JsonManagedReference
    @JoinColumn(name = "CLIENT2_ID", nullable = true, referencedColumnName = "client_id")
    @NotFound(action = NotFoundAction.IGNORE)
    private Client client2;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonManagedReference
    @JoinColumn(name = "SECTOR1_ID", nullable = false, referencedColumnName = "ID")
    @NotFound(action = NotFoundAction.IGNORE)
    private Sector sector1;

    @ManyToMany(cascade = {
        CascadeType.MERGE,
        CascadeType.DETACH,
        CascadeType.REFRESH
    })
    @JoinTable(
            name = "FINAL_PROJECT_SECTOR2",
            joinColumns = @JoinColumn(name = "PROJECT_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "SECTOR2_ID", referencedColumnName = "ID"))
    @NotFound(action = NotFoundAction.IGNORE)
    private List<Sector2> sector2s = new ArrayList<>();

    @ManyToMany(cascade = {
        CascadeType.MERGE,
        CascadeType.DETACH,
        CascadeType.PERSIST,
        CascadeType.REFRESH
    }, fetch = FetchType.LAZY)
    @JoinTable(
            name = "FINAL_PROJECT_SECTOR3",
            joinColumns = @JoinColumn(name = "PROJECT_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "SECTOR3_ID", referencedColumnName = "ID"))
    @NotFound(action = NotFoundAction.IGNORE)
    private List<Sector3> sector3s = new ArrayList<>();

    @ManyToMany(cascade = {
        CascadeType.MERGE,
        CascadeType.DETACH,
        CascadeType.PERSIST,
        CascadeType.REFRESH
    }, fetch = FetchType.LAZY)
    @JoinTable(
            name = "FINAL_PROJECT_TAG",
            joinColumns = @JoinColumn(name = "PROJECT_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "TAG_ID", referencedColumnName = "ID"))
    @NotFound(action = NotFoundAction.IGNORE)
    private List<Tag> tags = new ArrayList<>();

    @ManyToMany(cascade = {
        CascadeType.MERGE,
        CascadeType.DETACH,
        CascadeType.PERSIST,
        CascadeType.REFRESH
    }, fetch = FetchType.LAZY)
    @JoinTable(
            name = "FINAL_PROJECT_FUNDING_AGENCY",
            joinColumns = @JoinColumn(name = "PROJECT_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "FUNDING_AGENCY_ID", referencedColumnName = "ID"))
    @NotFound(action = NotFoundAction.IGNORE)
    private List<FundingAgency> fundingAgencies = new ArrayList<>();

    @ManyToMany(cascade = {
        CascadeType.MERGE,
        CascadeType.DETACH,
        CascadeType.PERSIST,
        CascadeType.REFRESH
    }, fetch = FetchType.LAZY)
    @JoinTable(
            name = "FINAL_PROJECT_DISTRICT",
            joinColumns = @JoinColumn(name = "PROJECT_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "DISTRICT_ID", referencedColumnName = "ID")
    )
    @NotFound(action = NotFoundAction.IGNORE)
    private List<District> districts = new ArrayList<>();

    public FinalProject() {
    }

    public FinalProject(
            int id,
            String title,
            String assignmentTitle,
            String description,
            String workDescription,
            String createdDate,
            String completetionDate,
            String projectStatus,
            String imagePath,
            String filePath
    ) {
        this.id = id;
        this.title = title;
        this.assignmentTitle = assignmentTitle;
        this.description = description;
        this.workDescription = workDescription;
        this.createdDate = createdDate;
        this.completetionDate = completetionDate;
        this.projectStatus = projectStatus;
        this.imagePath = imagePath;
        this.filePath = filePath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWorkDescription() {
        return workDescription;
    }

    public void setWorkDescription(String workDescription) {
        this.workDescription = workDescription;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCompletetionDate() {
        return completetionDate;
    }

    public void setCompletetionDate(String completetionDate) {
        this.completetionDate = completetionDate;
    }

    public Client getClient1() {
        return client1;
    }

    public void setClient1(Client client1) {
        this.client1 = client1;
    }

    public Client getClient2() {
        return client2;
    }

    public void setClient2(Client client2) {
        this.client2 = client2;
    }

    public Sector getSector1() {
        return sector1;
    }

    public void setSector1(Sector sector1) {
        this.sector1 = sector1;
    }

    public List<Sector2> getSector2s() {
        return sector2s;
    }

    public void setSector2s(List<Sector2> sector2s) {
        this.sector2s = sector2s;
    }

    public List<Sector3> getSector3s() {
        return sector3s;
    }

    public void setSector3s(List<Sector3> sector3s) {
        this.sector3s = sector3s;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    public List<FundingAgency> getFundingAgencies() {
        return fundingAgencies;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getAssignmentTitle() {
        return assignmentTitle;
    }

    public void setAssignmentTitle(String assignmentTitle) {
        this.assignmentTitle = assignmentTitle;
    }

    @Override
    public String toString() {
        return "FinalProject{"
                + "id=" + id
                + ", title='" + title + '\''
                + ", assignmentTitle='" + assignmentTitle + '\''
                + ", description='" + description + '\''
                + ", workDescription='" + workDescription + '\''
                + ", createdDate='" + createdDate + '\''
                + ", completetionDate='" + completetionDate + '\''
                + ", client1=" + client1
                + ", client2=" + client2
                + ", sector1=" + sector1
                + ", sector2s=" + sector2s
                + ", sector3s=" + sector3s
                + ", tags=" + tags
                + ", fundingAgencies=" + fundingAgencies
                + '}';
    }

    public void setFundingAgencies(List<FundingAgency> fundingAgencies) {
        this.fundingAgencies = fundingAgencies;
    }

    public String getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

}
