package np.com.cscpl.AuthorizationServer.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "SECTOR3")
public class Sector3 {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToMany(cascade=CascadeType.ALL,mappedBy = "sector3s")
    private List<FinalProject> finalProjects = new ArrayList<>();

    public Sector3() {
    }

    public Sector3(int id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FinalProject> getFinalProjects() {
        return finalProjects;
    }

    public void setFinalProjects(List<FinalProject> finalProjects) {
        this.finalProjects = finalProjects;
    }

    @Override
    public String toString() {
        return "Sector3{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", projects=" + finalProjects +
                '}';
    }
}
