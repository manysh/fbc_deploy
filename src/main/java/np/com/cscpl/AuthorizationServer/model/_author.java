/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Puzan Sakya < puzan at puzansakya@gmail.com >
 */
@Entity
@Table(name = "authors")
public class _author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;


    @OneToMany(mappedBy = ("author"), cascade = CascadeType.ALL)
    @JsonBackReference
    private List<_book> bookList;

    public _author(int id, String name, List<_book> bookList) {
        this.id = id;
        this.name = name;
        this.bookList = bookList;
    }

    public _author(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<_book> getBookList() {
        return bookList;
    }

    public void setBookList(List<_book> bookList) {
        this.bookList = bookList;
    }
    
    
}
