package np.com.cscpl.AuthorizationServer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "TAG")
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "TAG")
    private String tag;

    @ManyToMany(cascade=CascadeType.ALL,mappedBy = "tags")
    private List<FinalProject> finalProjects= new ArrayList<>();

    public Tag() {
    }

    public Tag(Integer id,String tag) {
        this.id = id;
        this.tag = tag;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<FinalProject> getFinalProjects() {
        return finalProjects;
    }

    public void setFinalProjects(List<FinalProject> finalProjects) {
        this.finalProjects = finalProjects;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", tag='" + tag + '\'' +
                ", projects=" + finalProjects +
                '}';
    }
}
