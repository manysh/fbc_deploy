package np.com.cscpl.AuthorizationServer.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
@Table(name = "SUB_MENUS")
public class SubMenu {

    @Column(name = "SUB_MENU_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "NAME",unique = true)
    private String name;

    @Column(name = "NAV_LINK")
    private String navLink;

    @Column(name = "SUBMENU_ORDER")
    private int order;

    @ManyToOne(fetch = FetchType.EAGER,cascade = {CascadeType.MERGE,CascadeType.DETACH,CascadeType.PERSIST,CascadeType.REFRESH})
    @JsonManagedReference
    @JoinColumn(name = "MENU_ID",nullable = false)
    private Menu menu;

    public SubMenu() {
    }

    public SubMenu(int id,String name,String navLink, int order) {
        this.id=id;
        this.name = name;
        this.navLink=navLink;
        this.order = order;
    }

    public SubMenu(int id,String name,String navLink, int order,Menu menu) {
        this.id=id;
        this.name = name;
        this.navLink=navLink;
        this.order = order;
        this.menu=menu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public String getNavLink() {
        return navLink;
    }

    public void setNavLink(String navLink) {
        this.navLink = navLink;
    }

    @Override
    public String toString() {
        return "SubMenu{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", navLink='" + navLink + '\'' +
                ", order=" + order +
                ", menu=" + menu +
                '}';
    }
}
