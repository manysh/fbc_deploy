package np.com.cscpl.AuthorizationServer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "HRSECTOR")
public class HrSector implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DETAIL")
    private String detail;

    @Column(name = "IS_SUB", columnDefinition = "boolean default false", nullable = false)
    private Boolean isSub = false;

    @Column(name = "IS_OTHER", columnDefinition = "boolean default false", nullable = false)
    private Boolean isOther = false;

    @ManyToMany(
            cascade = CascadeType.ALL,
            mappedBy = "hrSubSectors"
    )
    private List<FinalHrUser> finalHrUsers = new ArrayList<>();

    public HrSector() {
    }

    public HrSector(int id) {
        this.id = id;
    }

    public HrSector(Integer id, String name, String detail, boolean isSub, boolean isOther) {
        this.id = id;
        this.name = name;
        this.detail = detail;
        this.isSub = isSub;
        this.isOther = isOther;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public boolean isIsSub() {
        return isSub;
    }

    public void setIsSub(boolean isSub) {
        this.isSub = isSub;
    }

    public boolean isIsOther() {
        return isOther;
    }

    public void setIsOther(boolean isOther) {
        this.isOther = isOther;
    }

    public List<FinalHrUser> getFinalHrUsers() {
        return finalHrUsers;
    }

    public void setFinalHrUsers(List<FinalHrUser> finalHrUsers) {
        this.finalHrUsers = finalHrUsers;
    }

    @Override
    public String toString() {
        return "HrSector{" + "id=" + id + ", name=" + name + ", detail=" + detail + ", isSub=" + isSub + ", isOther=" + isOther + ", finalHrUsers=" + finalHrUsers + '}';
    }

}
