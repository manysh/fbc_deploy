package np.com.cscpl.AuthorizationServer.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "geography")
public class Geography {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private int geographyId;

    @Column(name = "geographyName")
    private String geographyName;

    @ManyToMany
    @JoinTable(name = "district_geography", joinColumns = @JoinColumn(name = "geography_id"), inverseJoinColumns = @JoinColumn(name = "district_id"))
    private List<District> districtList;

    public Geography() {
    }

    public Geography(int id,String geographyName,List<District> districts) {
        this.geographyId = id;
        this.geographyName = geographyName;
        this.districtList = districts;
    }

    public Geography(int id,String geographyName) {
        this.geographyId = id;
        this.geographyName = geographyName;
    }

    public int getGeographyId() {
        return geographyId;
    }

    public void setGeographyId(int geographyId) {
        this.geographyId = geographyId;
    }

    public String getGeographyName() {
        return geographyName;
    }

    public void setGeographyName(String geographyName) {
        this.geographyName = geographyName;
    }

    public List<District> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<District> districtList) {
        this.districtList = districtList;
    }
}
