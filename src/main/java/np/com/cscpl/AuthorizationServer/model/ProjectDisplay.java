package np.com.cscpl.AuthorizationServer.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
@Table(name = "HOME_PAGE_PROJECT")
public class ProjectDisplay {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "DISPLAY_ID")
    private int displayOrder;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJECT_ID",referencedColumnName = "ID")
    @NotFound(action = NotFoundAction.IGNORE)
    private FinalProject finalProject=new FinalProject();

    public ProjectDisplay() {
    }

    public ProjectDisplay(int id, String status, int displayOrder) {
        this.id = id;
        this.status = status;
        this.displayOrder = displayOrder;
    }

    public ProjectDisplay(int id, String status, FinalProject finalProject) {
        this.id = id;
        this.status = status;
        this.finalProject = finalProject;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public FinalProject getFinalProject() {
        return finalProject;
    }

    public void setFinalProject(FinalProject finalProject) {
        this.finalProject = finalProject;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }
}
