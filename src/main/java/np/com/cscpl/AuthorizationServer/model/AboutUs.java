package np.com.cscpl.AuthorizationServer.model;

import javax.persistence.*;

@Entity
@Table(name = "about_us")
public class AboutUs {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name ="introduction1")
    private String introduction1;

    @Column(name ="introduction2")
    private String introduction2;

    @Column(name ="introduction3")
    private String introduction3;

    @Column(name ="title1")
    private String title1;

    @Column(name = "title1_description")
    private String  title1Description;

    @Column(name = "title2")
    private String title2;

    @Column(name = "title2_description")
    private String title2Description;

    public AboutUs() {
    }

    public AboutUs(String introduction1, String introduction2, String introduction3, String title1, String title1Description, String title2, String title2Description) {
        this.introduction1 = introduction1;
        this.introduction2 = introduction2;
        this.introduction3 = introduction3;
        this.title1 = title1;
        this.title1Description = title1Description;
        this.title2 = title2;
        this.title2Description = title2Description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIntroduction1() {
        return introduction1;
    }

    public void setIntroduction1(String introduction1) {
        this.introduction1 = introduction1;
    }

    public String getIntroduction2() {
        return introduction2;
    }

    public void setIntroduction2(String introduction2) {
        this.introduction2 = introduction2;
    }

    public String getIntroduction3() {
        return introduction3;
    }

    public void setIntroduction3(String introduction3) {
        this.introduction3 = introduction3;
    }

    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    public String getTitle1Description() {
        return title1Description;
    }

    public void setTitle1Description(String title1Description) {
        this.title1Description = title1Description;
    }


    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }

    public String getTitle2Description() {
        return title2Description;
    }

    public void setTitle2Description(String title2Description) {
        this.title2Description = title2Description;
    }
}
