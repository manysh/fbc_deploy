package np.com.cscpl.AuthorizationServer.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author puzansakya
 */
@Entity
@Table(name = "PROJECT")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "CREATED_AT")
    private Date createdAt;

    @Column(name = "DELETED_AT")
    private Date deletedAt;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "ESTIMATED_COMPLETION_DATE")
    private Date estimatedCompletionDate;

    @Column(name = "INITIATED_DATE")
    private Date initiatedDate;

    @Column(name = "IS_DELETE")
    private Short isDelete;

    @Lob
    @Column(name = "BACKDROP", length = 512)
    private String backdrop;

    @Column(name = "NAME")
    private String name;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "UPDATED_AT")
    private Date updatedAt;



    @JoinColumn(name = "SECTOR_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Sector sector;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "PROJECT_DISTRICT",
            joinColumns = {
                @JoinColumn(name = "PROJECT_ID")},
            inverseJoinColumns = {
                @JoinColumn(name = "DISTRICT_ID")})
    private List<District> districtList;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "PROJECT_TAG",
            joinColumns = {
                @JoinColumn(name = "PROJECT_ID")},
            inverseJoinColumns = {
                @JoinColumn(name = "TAG_ID")})
    private List<Tag> tags;

    public Project() {
    }

    public Project(int id) {
        this.id = id;
    }

    public Project(Integer id, Date createdAt, Date deletedAt, String description, Date estimatedCompletionDate, Date initiatedDate, Short isDelete, String name, String status, Date updatedAt, String backdrop, Sector sector) {
        this.id = id;
        this.createdAt = createdAt;
        this.deletedAt = deletedAt;
        this.description = description;
        this.estimatedCompletionDate = estimatedCompletionDate;
        this.initiatedDate = initiatedDate;
        this.isDelete = isDelete;
        this.name = name;
        this.backdrop = backdrop;
        this.status = status;
        this.updatedAt = updatedAt;
        this.sector = sector;
    }

    public Project(int id,Date createdAt, Date deletedAt, String description, Date estimatedCompletionDate, Date initiatedDate, Short isDelete, String name, String status, Date updatedAt) {
        this.id = id;
        this.createdAt = createdAt;
        this.deletedAt = deletedAt;
        this.description = description;
        this.estimatedCompletionDate = estimatedCompletionDate;
        this.initiatedDate = initiatedDate;
        this.isDelete = isDelete;
        this.name = name;
        this.status = status;
        this.updatedAt = updatedAt;
    }

    public Project(Date createdAt, Date deletedAt, String description, Date estimatedCompletionDate, Date initiatedDate, Short isDelete, String name, String status, Date updatedAt, Sector sector) {
        this.createdAt = createdAt;
        this.deletedAt = deletedAt;
        this.description = description;
        this.estimatedCompletionDate = estimatedCompletionDate;
        this.initiatedDate = initiatedDate;
        this.isDelete = isDelete;
        this.name = name;
        this.status = status;
        this.updatedAt = updatedAt;
        this.sector = sector;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEstimatedCompletionDate() {
        return estimatedCompletionDate;
    }

    public void setEstimatedCompletionDate(Date estimatedCompletionDate) {
        this.estimatedCompletionDate = estimatedCompletionDate;
    }

    public Date getInitiatedDate() {
        return initiatedDate;
    }

    public void setInitiatedDate(Date initiatedDate) {
        this.initiatedDate = initiatedDate;
    }

    public Short getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Short isDelete) {
        this.isDelete = isDelete;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public List<District> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<District> districtList) {
        this.districtList = districtList;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

}
