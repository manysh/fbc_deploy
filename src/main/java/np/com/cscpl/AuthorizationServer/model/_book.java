/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 *
 * @author Puzan Sakya < puzan at puzansakya@gmail.com >
 */
@Entity
@Table(name = "books")
public class _book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "book_name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {
        CascadeType.MERGE,
        CascadeType.DETACH,
        CascadeType.PERSIST,
        CascadeType.REFRESH
    })
    @JsonManagedReference
    @JoinColumn(name = "author_id", nullable = false, referencedColumnName = "id")
    @NotFound(action = NotFoundAction.IGNORE)
    private _author author;

    public _book(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public _book(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public _author getAuthor() {
        return author;
    }

    public void setAuthor(_author author) {
        this.author = author;
    }

}
