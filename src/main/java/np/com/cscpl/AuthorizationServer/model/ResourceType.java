package np.com.cscpl.AuthorizationServer.model;

import javax.persistence.*;

@Entity
@Table(name = "resource_type")
public class ResourceType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "resource_type_name")
    private String resourceTypeName;

    public ResourceType() {
    }

    public ResourceType(int id) {
        this.id = id;
    }

    public ResourceType(int id, String resourceTypeName) {
        this.id = id;
        this.resourceTypeName = resourceTypeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResourceTypeName() {
        return resourceTypeName;
    }

    public void setResourceTypeName(String resourceTypeName) {
        this.resourceTypeName = resourceTypeName;
    }
}
