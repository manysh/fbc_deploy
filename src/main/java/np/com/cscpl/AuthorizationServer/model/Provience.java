package np.com.cscpl.AuthorizationServer.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PROVIENCE")
public class Provience {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "NAME",unique = true)
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @OneToMany(mappedBy ="province", cascade = CascadeType.ALL,orphanRemoval = true)
    @JsonIgnore
    private List<District> districtList;


    public Provience() {
    }

    public Provience(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Provience(int id, String name, String description, List<District> districts) {
        this.id=id;
        this.name = name;
        this.description = description;
        this.districtList = districts;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<District> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<District> districtList) {
        this.districtList = districtList;
    }

    @Override
    public String toString() {
        return "Provience{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", districtList=" + districtList +
                '}';
    }
}
