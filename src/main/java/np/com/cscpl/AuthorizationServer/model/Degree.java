package np.com.cscpl.AuthorizationServer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "degree")
public class Degree implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "degree", orphanRemoval = true)
    private List<FinalHrUserDegree> finalHrUserDegrees = new ArrayList<>();

    public Degree() {
    }

    public Degree(int id) {
        this.id = id;
    }

    public Degree(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Degree(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FinalHrUserDegree> getFinalHrUserDegrees() {
        return finalHrUserDegrees;
    }

    public void setFinalHrUserDegrees(List<FinalHrUserDegree> finalHrUserDegrees) {
        this.finalHrUserDegrees = finalHrUserDegrees;
    }

}
