/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 *
 * @author puzansakya
 */
@Entity
@Table(name = "EVENT")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Column(name = "EVENT_DATE", nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date eventDate;

    @Column(name = "IMAGE_PATH", nullable = false)
    private String imagePath;

    @OneToMany(mappedBy = "event")
    private List<EventImage> eventImage = new ArrayList<EventImage>();

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "EVENT_TYPE_ID", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private EventType eventType;

    @Column(name = "IS_SHOW", columnDefinition = "boolean default flase", nullable = false)
    private Boolean isShow = false;

    @Column(name = "EG_IS_SHOW", columnDefinition = "boolean default flase", nullable = false)
    private Boolean egIsShow = false;

    public Event() {
    }

    public Event(int id) {
        this.id = id;
    }

    public Event(int id, String name, String description, Date eventDate, String imagePath, EventType eventType, Boolean isShow, Boolean egIsShow) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.eventDate = eventDate;
        this.imagePath = imagePath;
        this.eventType = eventType;
        this.isShow = isShow;
        this.egIsShow = egIsShow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public List<EventImage> getEventImage() {
        return eventImage;
    }

    public void setEventImage(List<EventImage> eventImage) {
        this.eventImage = eventImage;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public Boolean getIsShow() {
        return isShow;
    }

    public void setIsShow(Boolean isShow) {
        this.isShow = isShow;
    }

    public Boolean getEgIsShow() {
        return egIsShow;
    }

    public void setEgIsShow(Boolean egIsShow) {
        this.egIsShow = egIsShow;
    }

   

    @Override
    public String toString() {
        return "Event{" + "id=" + id + ", name=" + name + ", description=" + description + ", eventDate=" + eventDate + ", imagePath=" + imagePath + ", isShow=" + isShow + ", EGIsShow=" + egIsShow + '}';
    }

}
