package np.com.cscpl.AuthorizationServer.model;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "BATCH_IMAGE")
public class BatchImage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private int id;

    @Column(name = "IMAGE_LINK")
    private String imageLink;

    @Column(name = "CREATED_DATE")
    private String createdDate;

    public BatchImage() {
        this.createdDate=LocalDateTime.now().toString();
    }

    public BatchImage(int id, String imageLink, String createdDate) {
        this.id = id;
        this.imageLink = imageLink;
        this.createdDate = LocalDateTime.now().toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
