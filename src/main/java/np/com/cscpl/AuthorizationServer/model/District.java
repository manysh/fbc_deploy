package np.com.cscpl.AuthorizationServer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "district")
public class District {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "district_name")
    private String districtName;

    @ManyToOne
    @JoinColumn(name = "province_id", referencedColumnName = "id")
    @JsonIgnore
    private Provience province;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "districtList")
    private List<Geography> geographyList;

    @ManyToMany(mappedBy = "districts",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<FinalProject> finalProjects= new ArrayList<>();

    public District() {
    }

    public District(int id) {
        this.id = id;
    }        

    public District(int id, String districtName, Provience provience, List<Geography> geographyList) {
        this.id = id;
        this.districtName = districtName;
    }

    public District(int id, String districtName, List<Geography> geographyList, Provience provience) {
        this.id = id;
        this.districtName = districtName;
        this.geographyList = geographyList;
        this.province = provience;
    }

    public District(int id,String districtName, List<Geography> geographyList) {
        this.id = id;
        this.districtName = districtName;
        this.geographyList = geographyList;
    }

    public District(int id, String districtName) {
        this.id = id;
        this.districtName = districtName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public Provience getProvience() {
        return province;
    }

    public void setProvience(Provience provience) {
        this.province = provience;

    }

    public List<Geography> getGeographyList() {
        return geographyList;
    }

    public void setGeographyList(List<Geography> geographyList) {
        this.geographyList = geographyList;
    }

    @Override
    public String toString() {
        return "district{"
                + "id=" + id
                + ", districtName='" + districtName + '\''
                + ", provience=" + province
                + '}';
    }

    public List<FinalProject> getFinalProjects() {
        return finalProjects;
    }

    public void setFinalProjects(List<FinalProject> finalProjects) {
        this.finalProjects = finalProjects;
    }
}
