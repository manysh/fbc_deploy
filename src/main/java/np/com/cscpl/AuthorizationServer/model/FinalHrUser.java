package np.com.cscpl.AuthorizationServer.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "final_hr_user")
public class FinalHrUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "MIDDLE_NAME", nullable = true)
    private String middleName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "JOIN_DATE")
    private LocalDate joinDate;

    @Column(name = "DOB")
    private LocalDate dob;

    @Column(name = "ADDRESS", nullable = true)
    private String address;

    @Column(name = "IS_CONSULTANT")
    private boolean isConsultant;

    @Column(name = "FILE_PATH")
    private String filePath;

    @Column(name = "email")
    private String email;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONSULTANT_TYPE_ID", nullable = true, referencedColumnName = "id")
    @NotFound(action = NotFoundAction.IGNORE)
    private ConsultantType consultantType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DISTRICT_ID", nullable = false, referencedColumnName = "id")
    @NotFound(action = NotFoundAction.IGNORE)
    private District district;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "FINAL_HR_SECTOR",
            joinColumns = {
                @JoinColumn(name = "USER_ID")},
            inverseJoinColumns = {
                @JoinColumn(name = "SECTOR_ID")}
    )
    private List<HrSector> hrSectors = new ArrayList<>();


    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "FINAL_HR_USER_SUB_SECTOR",
            joinColumns = {
                @JoinColumn(name = "USER_ID")},
            inverseJoinColumns = {
                @JoinColumn(name = "SECTOR_ID")}
    )
    private List<HrSector> hrSubSectors = new ArrayList<>();


    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "FINAL_HR_USER_OTHER_SECTOR",
            joinColumns = {
                @JoinColumn(name = "USER_ID")},
            inverseJoinColumns = {
                @JoinColumn(name = "SECTOR_ID")}
    )
    private List<HrSector> hrOtherSectors = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RESOURCE_TYPE_ID", nullable = true, referencedColumnName = "id")
    @NotFound(action = NotFoundAction.IGNORE)
    private ResourceType resourceType;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "finalHrUser", orphanRemoval = true)
    private List<FinalHrUserDegree> finalHrUserDegrees = new ArrayList<>();

    public FinalHrUser() {
    }

    public FinalHrUser(int id) {
        this.id = id;
    }

    public FinalHrUser(
            int id,
            String firstName,
            String middleName,
            String lastName,
            String phoneNumber,
            String email,
            LocalDate joinDate,
            LocalDate dob,
            String address,
            boolean isConsultant,
            String filePath,
            ConsultantType consultantType,
            District district,
            List<HrSector> hrSectors,
            List<HrSector> hrSubSectors,
            List<HrSector> hrOtherSectors,
            ResourceType resourceType,
            List<FinalHrUserDegree> finalHrUserDegrees
    ) {
        this.id = id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.joinDate = joinDate;
        this.dob = dob;
        this.address = address;
        this.isConsultant = isConsultant;
        this.filePath = filePath;
        this.consultantType = consultantType;
        this.district = district;
        this.hrSectors = hrSectors;
        this.hrSubSectors = hrSubSectors;
        this.hrOtherSectors = hrOtherSectors;
        this.resourceType = resourceType;
        this.finalHrUserDegrees = finalHrUserDegrees;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(LocalDate joinDate) {
        this.joinDate = joinDate;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isIsConsultant() {
        return isConsultant;
    }

    public void setIsConsultant(boolean isConsultant) {
        this.isConsultant = isConsultant;
    }

    public ConsultantType getConsultantType() {
        return consultantType;
    }

    public void setConsultantType(ConsultantType consultantType) {
        this.consultantType = consultantType;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public List<HrSector> getHrSectors() {
        return hrSectors;
    }

    public void setHrSectors(List<HrSector> hrSectors) {
        this.hrSectors = hrSectors;
    }

    public List<HrSector> getHrSubSectors() {
        return hrSubSectors;
    }

    public void setHrSubSectors(List<HrSector> hrSubSectors) {
        this.hrSubSectors = hrSubSectors;
    }

    public List<HrSector> getHrSubSector() {
        return hrSubSectors;
    }

    public void setHrSubSector(List<HrSector> hrSubSectors) {
        this.hrSubSectors = hrSubSectors;
    }

//    public HrSector getHrOtherSector() {
//        return hrOtherSector;
//    }
//
//    public void setHrOtherSector(HrSector hrOtherSector) {
//        this.hrOtherSector = hrOtherSector;
//    }
    public List<HrSector> getHrOtherSectors() {
        return hrOtherSectors;
    }

    public void setHrOtherSectors(List<HrSector> hrOtherSectors) {
        this.hrOtherSectors = hrOtherSectors;
    }

    public ResourceType getResourceType() {
        return resourceType;
    }

    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    public List<FinalHrUserDegree> getFinalHrUserDegrees() {
        return finalHrUserDegrees;
    }

    public void setFinalHrUserDegrees(List<FinalHrUserDegree> finalHrUserDegrees) {
        this.finalHrUserDegrees = finalHrUserDegrees;
    }

    public void addFinalHrUserDegree(FinalHrUserDegree fhud) {
        fhud.setFinalHrUser(this);
        finalHrUserDegrees.add(fhud);
    }

    public void updateFinalHrUserDegree(List<FinalHrUserDegree> fhuds) {
        fhuds.forEach(f -> {
            f.setFinalHrUser(this);
            finalHrUserDegrees.add(f);
        });
    }

    public void removeFinalHrUserDegree(List<FinalHrUserDegree> fhuds) {

        fhuds.forEach(f -> {
            finalHrUserDegrees.remove(f);
            f.setFinalHrUser(this);
            finalHrUserDegrees.add(f);
        });

    }

    public void addSector(HrSector hrSector) {
        hrSectors.add(hrSector);
        hrSector.getFinalHrUsers().add(this);
    }

    public void removeSector(HrSector hrSector) {
        hrSubSectors.remove(hrSector);
        hrSector.getFinalHrUsers().remove(this);
    }

    public void addSubSector(HrSector hrSector) {
        hrSubSectors.add(hrSector);
        hrSector.getFinalHrUsers().add(this);
    }

    public void removeSubSector(HrSector hrSector) {
        hrSubSectors.remove(hrSector);
        hrSector.getFinalHrUsers().remove(this);
    }

    public void addOtherSector(HrSector hrSector) {
        hrOtherSectors.add(hrSector);
        hrSector.getFinalHrUsers().add(this);
    }

    public void removeOtherSector(HrSector hrSector) {
        hrOtherSectors.remove(hrSector);
        hrSector.getFinalHrUsers().remove(this);
    }

}
