package np.com.cscpl.AuthorizationServer.model;

import javax.persistence.*;

@Entity
@Table(name = "consultant_type")
public class ConsultantType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)   
    private int id;

    @Column(name = "consultant_type_name")
    private String consultantTypeName;

    public ConsultantType() {
    }

    public ConsultantType(int id) {
        this.id = id;
    }

    public ConsultantType(int id, String consultantTypeName) {
        this.id = id;
        this.consultantTypeName = consultantTypeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConsultantTypeName() {
        return consultantTypeName;
    }

    public void setConsultantTypeName(String consultantTypeName) {
        this.consultantTypeName = consultantTypeName;
    }
}
