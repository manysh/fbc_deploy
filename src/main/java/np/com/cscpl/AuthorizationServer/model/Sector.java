package np.com.cscpl.AuthorizationServer.model;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "SECTOR")
public class Sector {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DISPLAY_ORDER")
    private Integer displayOrder;

    @Column(name = "DETAIL")
    private String detail;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sector1")
    private List<FinalProject> finalProjects;

    public Sector() {
    }

    public Sector(int id) {
        this.id = id;
    }

    public Sector(Integer id, String name, Integer displayOrder, String detail) {
        this.id = id;
        this.name = name;
        this.displayOrder = displayOrder;
        this.detail = detail;
    }

    public Sector(Integer id, String name,  String detail) {
        this.id = id;
        this.name = name;
        this.detail = detail;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public List<FinalProject> getFinalProjects() {
        return finalProjects;
    }

    public void setFinalProjects(List<FinalProject> finalProjects) {
        this.finalProjects = finalProjects;
    }

    @Override
    public String toString() {
        return "Sector{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", displayOrder=" + displayOrder +
                ", detail='" + detail + '\'' +
                ", finalProjects=" + finalProjects +
                '}';
    }
}
