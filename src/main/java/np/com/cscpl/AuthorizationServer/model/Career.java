package np.com.cscpl.AuthorizationServer.model;


public class Career {


    private int id;


    private String fullName;


    private String email;

    private String contactNo;

    private String cvLink;


    private String subject;

    private String qualification;

    private String message;


    public Career() {
    }

    public Career(int id,String fullName, String email, String contactNo, String cvLink, String subject, String qualification, String message) {
       this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.contactNo = contactNo;
        this.cvLink = cvLink;
        this.subject = subject;
        this.qualification = qualification;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getCvLink() {
        return cvLink;
    }

    public void setCvLink(String cvLink) {
        this.cvLink = cvLink;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
