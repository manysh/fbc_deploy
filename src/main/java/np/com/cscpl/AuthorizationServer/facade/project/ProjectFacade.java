package np.com.cscpl.AuthorizationServer.facade.project;

import np.com.cscpl.AuthorizationServer.dto.project.ProjectParamsDTO;
import np.com.cscpl.AuthorizationServer.dto.project.ProjectResponseDto;
import np.com.cscpl.AuthorizationServer.dto.project.ProjectsResponseDto;
import org.springframework.data.domain.Page;

public interface ProjectFacade {

    Page<?> findAll(int pageNumber, int pageSize) throws Exception;

    Page<?> findAllBySector(String sector, int pageNumber, int pageSize) throws Exception;

    Page<?> findAllByStatus(String status, int pageNumber, int pageSize) throws Exception;

    Page<?> findAllByTag(String tag, int pageNumber, int pageSize) throws Exception;

    ProjectsResponseDto save(ProjectParamsDTO projectParamsDto) throws Exception;

    ProjectsResponseDto findOne(int id) throws Exception;

    void update(ProjectResponseDto projectResponseDto) throws Exception;

    void delete(int id) throws Exception;
}
