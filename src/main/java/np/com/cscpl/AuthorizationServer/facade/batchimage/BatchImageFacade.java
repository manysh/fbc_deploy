package np.com.cscpl.AuthorizationServer.facade.batchimage;

import np.com.cscpl.AuthorizationServer.dto.batchimage.BatchImageResponseDto;
import np.com.cscpl.AuthorizationServer.dto.batchimage.BatchImagesResponseDto;

public interface BatchImageFacade {
    BatchImagesResponseDto save(BatchImageResponseDto batchImageResponseDto) throws Exception;
    BatchImageResponseDto findFirst() throws Exception;
}
