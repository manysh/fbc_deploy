package np.com.cscpl.AuthorizationServer.facade.sector;

import np.com.cscpl.AuthorizationServer.dto.sector.SectorResponseDto;
import np.com.cscpl.AuthorizationServer.dto.sector.SectorsResponseDto;
import np.com.cscpl.AuthorizationServer.model.Sector;
import org.springframework.data.domain.Page;

import java.util.List;

public interface SectorFacade {

    Page<?> findAll(int pageNumber, int pageSize, String search) throws Exception;

    SectorsResponseDto save(SectorResponseDto sectorsResponseDto) throws Exception;

    SectorsResponseDto findOne(int id) throws Exception;

    void update(int id,SectorResponseDto sectorsResponseDto) throws Exception;

    void delete(int id) throws Exception;

    SectorsResponseDto  findByProjectId(int projectId) throws Exception;
}
