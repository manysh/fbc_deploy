package np.com.cscpl.AuthorizationServer.facade.aboutus;


import np.com.cscpl.AuthorizationServer.converter.aboutus.AboutUsConverter;
import np.com.cscpl.AuthorizationServer.dto.aboutus.AboutUsReponsesDto;
import np.com.cscpl.AuthorizationServer.service.aboutus.AboutUsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AboutUsFacadeImpl implements  AboutUsFacade{

    private AboutUsService aboutUsService;

    private AboutUsConverter aboutUsConverter;

    @Autowired
    public AboutUsFacadeImpl(AboutUsService aboutUsService, AboutUsConverter aboutUsConverter) {
        this.aboutUsService = aboutUsService;
        this.aboutUsConverter = aboutUsConverter;
    }

    @Override
    public AboutUsReponsesDto findAll() {

           return new AboutUsReponsesDto(
                   aboutUsConverter.createFromEntities(aboutUsService.getAll()));
    }
}
