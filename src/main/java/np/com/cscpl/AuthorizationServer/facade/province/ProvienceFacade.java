package np.com.cscpl.AuthorizationServer.facade.province;

import np.com.cscpl.AuthorizationServer.dto.province.ProviencesResponseDto;

public interface ProvienceFacade {
    public ProviencesResponseDto findAll() throws Exception;
}
