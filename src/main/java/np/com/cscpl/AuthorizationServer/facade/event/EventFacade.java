package np.com.cscpl.AuthorizationServer.facade.event;

import np.com.cscpl.AuthorizationServer.dto.event.EventResponseDto;
import np.com.cscpl.AuthorizationServer.dto.event.EventsResponseDto;
import org.springframework.data.domain.Page;

public interface EventFacade {

    Page<?> findAll(int pageNumber, int pageSize) throws Exception;

    Page<?> findAllByName(int pageNumber, int pageSize, String name) throws Exception;

    EventsResponseDto save(EventResponseDto sectorsResponseDto) throws Exception;

    EventsResponseDto findOne(int id) throws Exception;

    void update(int id, EventResponseDto sectorsResponseDto) throws Exception;
    
    void updateShowcase(int id, EventResponseDto sectorsResponseDto) throws Exception;

    void delete(int id) throws Exception;

    Page<?> findAllByEventType(String eventType, int pageNumber, int pageSize) throws Exception;

    Page<?> search(String keyword, int pageNumber, int pageSize) throws Exception;
    
    Page<?> findByIsShowTrue(int pageNumber, int pageSize) throws Exception;
    
    Page<?> findByEgIsShowTrue(int pageNumber, int pageSize) throws Exception;
}
