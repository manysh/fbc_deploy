package np.com.cscpl.AuthorizationServer.facade.career;

import np.com.cscpl.AuthorizationServer.converter.career.CareerConverter;
import np.com.cscpl.AuthorizationServer.dto.career.CareerResponseDto;
import np.com.cscpl.AuthorizationServer.service.career.CareerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CareerFacdeImpl implements  CareerFacade {

    @Autowired
    private CareerConverter careerConverter;

    @Autowired
    private CareerService careerService;

    @Override
    public CareerResponseDto sendEmail(CareerResponseDto careerResponseDto) throws Exception {
        try {
            return careerConverter.convertFromEntity(careerService.sendEmail(careerConverter.convertFromDto(careerResponseDto))
            );
        }catch(Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }
}
