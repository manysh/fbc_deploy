package np.com.cscpl.AuthorizationServer.facade.client;


import np.com.cscpl.AuthorizationServer.dto.client.ClientResponseDto;
import np.com.cscpl.AuthorizationServer.dto.client.ClientsResponseDto;
import np.com.cscpl.AuthorizationServer.model.Client;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ClientFacade {
    Page<?> findAll(int pageNumber, int pageSize, String search) throws Exception;
    ClientsResponseDto save(ClientResponseDto clientResponseDto) throws  Exception;
    ClientResponseDto getById(int id);
    void deleteClient(int id);
    ClientResponseDto update(int id, ClientResponseDto clientResponseDto) throws Exception;
    ClientsResponseDto findAll() throws Exception;



}
