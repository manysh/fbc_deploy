package np.com.cscpl.AuthorizationServer.facade.tag;

import np.com.cscpl.AuthorizationServer.facade.tag.*;
import np.com.cscpl.AuthorizationServer.model.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import np.com.cscpl.AuthorizationServer.converter.tag.TagConverter;
import np.com.cscpl.AuthorizationServer.dto.tag.TagResponseDto;
import np.com.cscpl.AuthorizationServer.dto.tag.TagsResponseDto;
import np.com.cscpl.AuthorizationServer.service.tag.TagService;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Service
public class TagFacadeImpl implements TagFacade {

    @Autowired
    private TagService tagService;

    @Autowired
    private TagConverter tagConverter;

    @Override
    public Page<?> findAll(int pageNumber, int pageSize) throws Exception {
        try {
            return tagService.findAll(pageNumber, pageSize)
                    .map(
                            tag -> {
                                return tagConverter.convertFromEntity(tag);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public TagsResponseDto save(TagResponseDto tagResponseDto) throws Exception {
        try {
            return new TagsResponseDto(
                    tagConverter.convertFromEntity(
                            tagService.save(
                                    tagConverter.convertFromDto(tagResponseDto)
                            )
                    )
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public TagsResponseDto findOne(int id) throws Exception {
        try {
            return new TagsResponseDto(
                    tagConverter.convertFromEntity(
                            tagService.findOne(id)
                    )
            );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(TagResponseDto tagResponseDto) throws Exception {
        try {
            tagService.update(tagConverter.convertFromDto(tagResponseDto));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            tagService.delete(id);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public TagsResponseDto findByDepartmentId(int id) throws Exception {
        try{
            return new TagsResponseDto(tagConverter.createFromEntities(
                    tagService.findByProjectId(id)
            ));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new TagsResponseDto();
        }
    }

    @Override
    public TagsResponseDto findAll() throws Exception {
        try {
            return new TagsResponseDto(
                    tagConverter.createFromEntities(
                            tagService.findAll()
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new TagsResponseDto();
        }
    }

    @Override
    public TagsResponseDto findByProjectIds(List<Integer> projectIds) throws Exception {
        try {
            return new TagsResponseDto(
                    tagConverter.createFromEntities(
                            tagService.findByProjectIds(projectIds)
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new TagsResponseDto();
        }
    }

    @Override
    public Page<?> getFilteredResult(String search, int pageNumber, int pageSize) throws Exception {
        try {
            return tagService.getFilteredResult(
                    search,
                    pageNumber,
                    pageSize
            ).map(tag -> new TagResponseDto(tag.getId(),tag.getTag()));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<Object>(new ArrayList<>());
        }
    }

    @Override
    public TagResponseDto updateTag(int id, TagResponseDto tagResponseDto) throws Exception {
        try {
            return tagConverter.convertFromEntity(
                    tagService.updateTag(id,tagConverter.convertFromDto(tagResponseDto))
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }
}
