package np.com.cscpl.AuthorizationServer.facade.agency;

import np.com.cscpl.AuthorizationServer.converter.agency.FundingAgencyConverter;
import np.com.cscpl.AuthorizationServer.dto.agency.FundingAgenciesResponseDto;
import np.com.cscpl.AuthorizationServer.dto.agency.FundingAgencyResponseDto;
import np.com.cscpl.AuthorizationServer.dto.sector3.Sector3ResponseDto;
import np.com.cscpl.AuthorizationServer.model.FundingAgency;
import np.com.cscpl.AuthorizationServer.service.agency.FundingAgencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Null;
import java.util.ArrayList;

@Component
public class FundingAgencyFacadeImpl implements FundingAgencyFacade {

    private FundingAgencyService fundingAgencyService;
    private FundingAgencyConverter fundingAgencyConverter;

    @Autowired
    public FundingAgencyFacadeImpl(FundingAgencyService fundingAgencyService,FundingAgencyConverter fundingAgencyConverter) {
        this.fundingAgencyService = fundingAgencyService;
        this.fundingAgencyConverter = fundingAgencyConverter;
    }

    @Override
    public FundingAgenciesResponseDto findAll() throws Exception {
        try {
            return new FundingAgenciesResponseDto(
                    fundingAgencyConverter.createFromEntities(
                            fundingAgencyService.findAll()
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new FundingAgenciesResponseDto();
        }
    }

    @Override
    public FundingAgenciesResponseDto findByProjectId(int id) throws Exception {
        try {
            return new FundingAgenciesResponseDto(
                    fundingAgencyConverter.createFromEntities(
                            fundingAgencyService.findByProjectId(
                                    id
                            )
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new FundingAgenciesResponseDto();
        }
    }

    @Override
    public FundingAgenciesResponseDto create(FundingAgencyResponseDto fundingAgencyResponseDto) throws Exception {
        try {
            return new FundingAgenciesResponseDto(fundingAgencyConverter.convertFromEntity(fundingAgencyService.create(
                    fundingAgencyConverter.convertFromDto(fundingAgencyResponseDto)
            )));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> filteredResult(int pageNumber, int pageSize, String search) throws Exception {
        try {
            Page<FundingAgency> fundingAgencies=fundingAgencyService.filteredResult(
                    pageNumber,
                    pageSize,
                    search
            );
            return fundingAgencies.map(fundingAgency -> fundingAgencyConverter.convertFromEntity(fundingAgency));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<Object>(new ArrayList<>());
        }
    }

    @Override
    public FundingAgencyResponseDto findById(int id) throws Exception {
        try {
            return  fundingAgencyConverter.convertFromEntity(fundingAgencyService.findById(id));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public FundingAgencyResponseDto update(int id, FundingAgencyResponseDto fundingAgencyResponseDto) throws Exception {
        try {
            return fundingAgencyConverter.convertFromEntity(
                    fundingAgencyService.update(id,fundingAgencyConverter.convertFromDto(fundingAgencyResponseDto))
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }
}
