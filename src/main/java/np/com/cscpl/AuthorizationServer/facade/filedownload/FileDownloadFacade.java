package np.com.cscpl.AuthorizationServer.facade.filedownload;

import np.com.cscpl.AuthorizationServer.dto.filedownload.FileDownloadResponseDto;
import np.com.cscpl.AuthorizationServer.dto.filedownload.FileDownloadsResponseDto;
import np.com.cscpl.AuthorizationServer.model.FileDownload;
import org.springframework.data.domain.Page;

public interface FileDownloadFacade {
    Page<?> fiteredResult(int pageNumber, int pageSize, String search) throws Exception;
    FileDownloadsResponseDto save(FileDownloadResponseDto fileDownloadResponseDto) throws Exception;
    FileDownloadResponseDto findById(int id) throws Exception;
    FileDownloadResponseDto update(int id, FileDownloadResponseDto fileDownloadResponseDto) throws Exception;
}
