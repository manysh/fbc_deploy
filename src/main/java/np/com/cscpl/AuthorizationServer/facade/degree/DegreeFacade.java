package np.com.cscpl.AuthorizationServer.facade.degree;

import np.com.cscpl.AuthorizationServer.dto.degree.DegreeResponseDto;
import org.springframework.data.domain.Page;

public interface DegreeFacade {

    Page<?> findAll(int pageNumber, int pageSize, String search) throws Exception;

    DegreeResponseDto save(DegreeResponseDto degreeResponseDto) throws Exception;

    DegreeResponseDto getById(int id) throws Exception;

    DegreeResponseDto update(int id, DegreeResponseDto degreeResponseDto) throws Exception;
}
