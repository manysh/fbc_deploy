package np.com.cscpl.AuthorizationServer.facade.sector3;

import np.com.cscpl.AuthorizationServer.converter.sector3.Sector3Converter;
import np.com.cscpl.AuthorizationServer.dto.sector3.Sector3ResponseDto;
import np.com.cscpl.AuthorizationServer.dto.sector3.Sector3sResponseDto;
import np.com.cscpl.AuthorizationServer.model.Sector3;
import np.com.cscpl.AuthorizationServer.service.sector3.Sector3Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class Sector3FacadeImpl implements Sector3Facade {

    private Sector3Service sector3Service;
    private Sector3Converter sector3Converter;

    @Autowired
    public Sector3FacadeImpl(Sector3Service sector3Service, Sector3Converter sector3Converter) {
        this.sector3Service = sector3Service;
        this.sector3Converter = sector3Converter;
    }

    @Override
    public Sector3sResponseDto findAll() throws Exception {
        try {
            return new Sector3sResponseDto(
                    sector3Converter.createFromEntities(
                            sector3Service.findAll()
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new Sector3sResponseDto();
        }
    }

    @Override
    public Sector3sResponseDto findByProjectId(int id) throws Exception {
        try {
            return new Sector3sResponseDto(
                    sector3Converter.createFromEntities(
                            sector3Service.findByProjectId(
                                    id
                            )
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new Sector3sResponseDto();
        }
    }

    @Override
    public Sector3sResponseDto create(Sector3ResponseDto sector3ResponseDto) throws Exception {
        try {
            return new Sector3sResponseDto(sector3Converter.convertFromEntity(sector3Service.create(
                    sector3Converter.convertFromDto(
                            sector3ResponseDto
                    )
            )));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> filteredResult(int pageNumber, int pageSize, String search) throws Exception {
        try {
            Page<Sector3> sector3s = sector3Service.filteredResult(pageNumber,pageSize,search);
            return sector3s.map(sector3 -> new Sector3ResponseDto(
                    sector3.getId(),
                    sector3.getTitle(),
                    sector3.getDescription()
            ));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<Object>(new ArrayList<>());
        }
    }

    @Override
    public Sector3ResponseDto findById(int id) throws Exception {
        try {
            return sector3Converter.convertFromEntity(
                    sector3Service.findById(
                            id
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Sector3ResponseDto update(int id, Sector3ResponseDto sector2ResponseDto) throws Exception {
        try {
            return
                    sector3Converter.convertFromEntity(
                            sector3Service.update(id,sector3Converter.convertFromDto(sector2ResponseDto))
                    );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }
}
