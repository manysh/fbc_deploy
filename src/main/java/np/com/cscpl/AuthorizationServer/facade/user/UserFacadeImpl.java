package np.com.cscpl.AuthorizationServer.facade.user;

import np.com.cscpl.AuthorizationServer.converter.user.UserConverter;
import np.com.cscpl.AuthorizationServer.converter.user.UserCreateConverter;
import np.com.cscpl.AuthorizationServer.dto.user.UserCreateDto;
import np.com.cscpl.AuthorizationServer.dto.user.UserResponseDto;
import np.com.cscpl.AuthorizationServer.model.User;
import np.com.cscpl.AuthorizationServer.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserFacadeImpl implements UserFacade {

    private UserConverter userConverter;
    private UserCreateConverter userCreateConverter;
    private UserService userService;
    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(8);

    @Autowired
    public UserFacadeImpl(UserConverter userConverter, UserCreateConverter userCreateConverter, UserService userService) {
        this.userConverter = userConverter;
        this.userCreateConverter = userCreateConverter;
        this.userService = userService;
    }

    @Override
    public UserResponseDto save(UserCreateDto userCreateDto) throws Exception {
        try {
            User user = userService.findByUserName(userCreateDto.getUserName());
            user = user == null ? new User() : user;
            user = userCreateConverter.convertCreateUser(user, userCreateDto);
            user = userService.create(user);
            UserResponseDto userResponseDto = UserConverter.convert(user);
            return userResponseDto;
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public boolean findByUserName(String username) throws Exception {
        try {
            User databaseUser = userService.findByUserName(username);
            return (databaseUser == null || databaseUser == new User()) ? false : true;
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public UserResponseDto findById(long id) throws Exception {
        try {
            return UserConverter.convert(userService.findById(id));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> findAll(int pageNumber, int pageSize, String search) throws Exception {
        try {
            return userService.findAll(pageNumber, pageSize, search)
                    .map(
                            user -> {
                                return UserConverter.convert(user);

                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public int update(Long id, UserCreateDto userCreateDto) throws Exception {
        try {
            User user = userService.findByUserName(userCreateDto.getUserName());
            user = user == null ? new User() : user;

            if (userCreateDto.getPassword() != null && userCreateDto.getNewPassword() != null) {
                String oldPasswordHash = user.getPassword();
                if (passwordEncoder.matches(userCreateDto.getPassword(), oldPasswordHash)) {
                    user = userCreateConverter.convertCreateUser(user, userCreateDto);
                    System.out.println("password match");
                    user.setPassword(passwordEncoder.encode(userCreateDto.getNewPassword()));
                    //update the user model
                      userService.update(id, user);
                    return 1;
                }
                System.out.println("password unmatch");
                return 2;
            }
            user = userCreateConverter.convertCreateUser(user, userCreateDto);
            userService.update(id, user);
            return 3;

        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }
}
