package np.com.cscpl.AuthorizationServer.facade.filedownload;

import np.com.cscpl.AuthorizationServer.converter.filedownload.FileDownloadConverter;
import np.com.cscpl.AuthorizationServer.dto.filedownload.FileDownloadResponseDto;
import np.com.cscpl.AuthorizationServer.dto.filedownload.FileDownloadsResponseDto;
import np.com.cscpl.AuthorizationServer.service.filedownload.FileDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FileDownloadFacadeImpl implements FileDownloadFacade{

    private FileDownloadService fileDownloadService;
    private FileDownloadConverter fileDownloadConverter;

    @Autowired
    public FileDownloadFacadeImpl(
            FileDownloadService fileDownloadService,
            FileDownloadConverter fileDownloadConverter
            ) {
        this.fileDownloadService = fileDownloadService;
        this.fileDownloadConverter=fileDownloadConverter;
    }

    @Override
    public Page<?> fiteredResult(int pageNumber, int pageSize, String search) throws Exception {
        try {
            return fileDownloadService.filteredResult(pageNumber,pageSize,search).map(
                    fileDownload ->{
                        return new FileDownloadResponseDto(
                                fileDownload.getId(),
                                fileDownload.getFileName(),
                                fileDownload.getFileDescription(),
                                fileDownload.getDownloadLink(),
                                fileDownload.getCreatedAt()
                        );
                    }
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public FileDownloadsResponseDto save(FileDownloadResponseDto fileDownloadResponseDto) throws Exception {
        try {
            return new FileDownloadsResponseDto(
                    fileDownloadConverter.convertFromEntity(
                            fileDownloadService.save(
                                    fileDownloadConverter.convertFromDto(
                                            fileDownloadResponseDto
                                    )
                            )
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public FileDownloadResponseDto findById(int id) throws Exception {
        try{
            return fileDownloadConverter.convertFromEntity(fileDownloadService.findById(id));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public FileDownloadResponseDto update(int id, FileDownloadResponseDto fileDownloadResponseDto) throws Exception {
        try {
            return fileDownloadConverter.convertFromEntity(
                    fileDownloadService.update(
                            id,
                            fileDownloadConverter.convertFromDto(
                                    fileDownloadResponseDto
                            )
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }
}
