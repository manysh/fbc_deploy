package np.com.cscpl.AuthorizationServer.facade.sector3;

import np.com.cscpl.AuthorizationServer.dto.sector3.Sector3ResponseDto;
import np.com.cscpl.AuthorizationServer.dto.sector3.Sector3sResponseDto;
import org.springframework.data.domain.Page;

public interface Sector3Facade {
    Sector3sResponseDto findAll() throws Exception;
    Sector3sResponseDto findByProjectId(int id) throws Exception;
    Sector3sResponseDto create(Sector3ResponseDto sector3ResponseDto) throws Exception;
    Page<?> filteredResult(int pageNumber,int pageSize, String search) throws Exception;
    Sector3ResponseDto findById(int id) throws Exception;
    Sector3ResponseDto update(int id, Sector3ResponseDto sector3ResponseDto) throws Exception;
}
