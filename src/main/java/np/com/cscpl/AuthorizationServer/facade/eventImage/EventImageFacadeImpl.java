/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.facade.eventImage;

import np.com.cscpl.AuthorizationServer.converter.eventimage.EventImageConverter;
import np.com.cscpl.AuthorizationServer.dto.eventImage.EventImageParamsDto;
import np.com.cscpl.AuthorizationServer.dto.eventImage.EventImageResponseDto;
import np.com.cscpl.AuthorizationServer.dto.eventImage.EventImagesResponseDto;
import np.com.cscpl.AuthorizationServer.service.eventimage.EventImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

/**
 *
 * @author puzansakya
 */
@Component
public class EventImageFacadeImpl implements EventImageFacade {

    @Autowired
    private EventImageService eventImageService;

    @Autowired
    private EventImageConverter eventImageConverter;

    @Override
    public Page<?> findAll(int eventId, int pageNumber, int pageSize) throws Exception {
        try {
            return eventImageService.findAll(eventId, pageNumber, pageSize)
                    .map(
                            event -> {
                                return eventImageConverter.convertFromEntity(event);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public String save(EventImageParamsDto eventImageParamsDto) throws Exception {

        eventImageParamsDto.getEvents().forEach(event -> {
            event.setEvent(eventImageParamsDto.getEventId());
            try {
                eventImageConverter.convertFromEntity(
                        eventImageService.save(
                                eventImageConverter.convertFromDto(event)
                        )
                );
            } catch (Exception e) {
                System.out.println(e.getLocalizedMessage());
                throw new IllegalArgumentException(e.getLocalizedMessage());
            }
        });

        return "saved";

    }

    @Override
    public EventImagesResponseDto findOne(int id) throws Exception {
        try {
            return new EventImagesResponseDto(
                    eventImageConverter.convertFromEntity(
                            eventImageService.findOne(id)
                    )
            );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(int id, EventImageResponseDto eventImagesResponseDto) throws Exception {
        try {
            eventImageService.update(id, eventImageConverter.convertFromDto(eventImagesResponseDto));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            eventImageService.delete(id);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

}
