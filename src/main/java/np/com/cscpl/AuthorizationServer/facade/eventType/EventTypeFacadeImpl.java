/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.facade.eventType;

import np.com.cscpl.AuthorizationServer.converter.eventtype.EventTypeConverter;
import np.com.cscpl.AuthorizationServer.dto.eventType.EventTypeResponseDto;
import np.com.cscpl.AuthorizationServer.dto.eventType.EventTypesResponseDto;
import np.com.cscpl.AuthorizationServer.facade.eventType.EventTypeFacade;
import np.com.cscpl.AuthorizationServer.service.eventType.EventTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

/**
 *
 * @author puzansakya
 */
@Component
public class EventTypeFacadeImpl implements EventTypeFacade {

    @Autowired
    private EventTypeService eventTypeService;

    @Autowired
    private EventTypeConverter eventTypeConverter;

    @Override
    public Page<?> findAll(int pageNumber, int pageSize) throws Exception {
        try {
            return eventTypeService.findAll(pageNumber, pageSize)
                    .map(
                            eventType -> {
                                return eventTypeConverter.convertFromEntity(eventType);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public EventTypesResponseDto save(EventTypeResponseDto eventTypeResponseDto) throws Exception {
        try {
            return new EventTypesResponseDto(
                    eventTypeConverter.convertFromEntity(
                            eventTypeService.save(
                                    eventTypeConverter.convertFromDto(eventTypeResponseDto)
                            )
                    )
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public EventTypesResponseDto findOne(int id) throws Exception {
        try {
            return new EventTypesResponseDto(
                    eventTypeConverter.convertFromEntity(
                            eventTypeService.findOne(id)
                    )
            );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(int id, EventTypeResponseDto eventTypeResponseDto) throws Exception {
        try {
            eventTypeService.update(id, eventTypeConverter.convertFromDto(eventTypeResponseDto));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            eventTypeService.delete(id);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

}
