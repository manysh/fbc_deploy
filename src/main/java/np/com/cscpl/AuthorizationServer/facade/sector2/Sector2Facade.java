package np.com.cscpl.AuthorizationServer.facade.sector2;

import np.com.cscpl.AuthorizationServer.dto.sector2.Sector2ResponseDto;
import np.com.cscpl.AuthorizationServer.dto.sector2.Sector2sResponseDto;
import org.springframework.data.domain.Page;

public interface Sector2Facade {
    Sector2sResponseDto findAll() throws Exception;
    Sector2sResponseDto findByProjectId(int id) throws Exception;
    Sector2sResponseDto save(Sector2ResponseDto sector2ResponseDto) throws Exception;
    Page<?> filteredResult(int pageNumber, int pageSize, String search) throws Exception;
    Sector2ResponseDto findById(int id) throws Exception;
    Sector2ResponseDto update(int id, Sector2ResponseDto sector2ResponseDto) throws Exception;
 }
