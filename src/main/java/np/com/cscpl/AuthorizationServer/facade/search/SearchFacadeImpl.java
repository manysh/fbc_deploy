/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.facade.search;

import java.util.List;

import np.com.cscpl.AuthorizationServer.converter.aboutus.AboutUsConverter;
import np.com.cscpl.AuthorizationServer.converter.finalproject.FinalProjectConverter;
import np.com.cscpl.AuthorizationServer.converter.sector.SectorConverter;
import np.com.cscpl.AuthorizationServer.dto.aboutus.AboutUsReponseDto;
import np.com.cscpl.AuthorizationServer.dto.finalproject.FinalProjectResponseDto;
import np.com.cscpl.AuthorizationServer.dto.search.SearchResponseDto;
import np.com.cscpl.AuthorizationServer.dto.sector.SectorResponseDto;
import np.com.cscpl.AuthorizationServer.facade.finalproject.Helper;
import np.com.cscpl.AuthorizationServer.service.aboutus.AboutUsService;
import np.com.cscpl.AuthorizationServer.service.finalproject.FinalProjectService;
import np.com.cscpl.AuthorizationServer.service.sector.SectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

/**
 *
 * @author prabhu
 */
@Component
public class SearchFacadeImpl implements SearchFacade {

    @Autowired
    private FinalProjectService finalProjectService;
    @Autowired
    private FinalProjectConverter finalProjectConverter;
    
    @Autowired
    private SectorService sectorService;
    @Autowired
    private SectorConverter sectorConverter;

    @Autowired
    private AboutUsConverter aboutUsConverter;

    @Autowired
    private AboutUsService aboutUsService;

    @Override
    public SearchResponseDto keySearch(String keyword, int pageNumber, int pageSize) throws Exception {
        SearchResponseDto search = new SearchResponseDto();
        
        Page<FinalProjectResponseDto> finalProjects = finalProjectService.keySearch(keyword,pageNumber, pageSize)
                    .map(
                            finalProject -> {
                                try {
                                    return Helper.setLink(finalProjectConverter.convertFromEntity(finalProject));
                                }catch (Exception ex){
                                    return new FinalProjectResponseDto();
                                }
                            }
                    );
//
//         Page<SectorResponseDto> sectors = sectorService.keySearch(keyword,pageNumber, pageSize)
//                    .map(
//                            sector -> {
//                                return sectorConverter.convertFromEntity(sector);
//
//                            }
//                    );

         Page<AboutUsReponseDto> aboutUsReponseDtos = aboutUsService.keySearch(keyword,pageNumber,pageSize)
                 .map(aboutUs -> {
                     return aboutUsConverter.convertFromEntity(aboutUs);
                 }
                 );
        
        search.setFinalProjectDTO(finalProjects);
//        search.setSectorResponseDto(sectors);
        search.setAboutUsReponseDto(aboutUsReponseDtos);

        return search;

    }

}
