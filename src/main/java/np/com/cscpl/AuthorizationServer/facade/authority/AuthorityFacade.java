package np.com.cscpl.AuthorizationServer.facade.authority;

import np.com.cscpl.AuthorizationServer.dto.authority.AuthoritiesResposeDto;

public interface AuthorityFacade {

    AuthoritiesResposeDto findAll() throws Exception;
}
