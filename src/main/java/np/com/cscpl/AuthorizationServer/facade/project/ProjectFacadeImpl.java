package np.com.cscpl.AuthorizationServer.facade.project;

import np.com.cscpl.AuthorizationServer.converter.project.ProjectConverter;
import np.com.cscpl.AuthorizationServer.dto.client.ClientResponseDto;
import np.com.cscpl.AuthorizationServer.dto.project.ProjectParamsDTO;
import np.com.cscpl.AuthorizationServer.dto.project.ProjectResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import np.com.cscpl.AuthorizationServer.dto.project.ProjectsResponseDto;
import np.com.cscpl.AuthorizationServer.dto.sector.SectorResponseDto;
import np.com.cscpl.AuthorizationServer.model.Project;
import np.com.cscpl.AuthorizationServer.service.project.ProjectService;
import org.springframework.data.domain.Page;

@Service
public class ProjectFacadeImpl implements ProjectFacade {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ProjectConverter projectConverter;

    @Override
    public Page<?> findAll(int pageNumber, int pageSize) throws Exception {
        try {
            return projectService.findAll(pageNumber, pageSize)
                    .map(
                            project -> {
                                return projectConverter.convertFromEntity(project);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public ProjectsResponseDto save(ProjectParamsDTO projectParamsDto) throws Exception {
        try {
            return new ProjectsResponseDto(projectConverter.convertFromEntity(projectService.save(
                    projectConverter.convertFromDto(new ProjectResponseDto()
                            .getBuilder()
                            .description(projectParamsDto.getDescription())
                            .estimatedCompletionDate(projectParamsDto.getEstimatedCompletionDate())
                            .initiatedDate(projectParamsDto.getInitiatedDate())
                            .name(projectParamsDto.getName())
                            .status(projectParamsDto.getStatus())
                            .sectorDTO(new SectorResponseDto().getBuilder().id(projectParamsDto.getSectorId()).build())
                            .build()))));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage() + "in projectfacade");
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public ProjectsResponseDto findOne(int id) throws Exception {
        try {
            return new ProjectsResponseDto(
                    projectConverter.convertFromEntity(
                            projectService.findOne(id)
                    )
            );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(ProjectResponseDto projectResponseDto) throws Exception {
        try {
            projectService.update(projectConverter.convertFromDto(projectResponseDto));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            projectService.delete(id);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> findAllBySector(String sector, int pageNumber, int pageSize) throws Exception {
        try {
            return projectService.findAllBySector(sector, pageNumber, pageSize)
                    .map(
                            project -> {
                                return projectConverter.convertFromEntity(project);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> findAllByStatus(String status, int pageNumber, int pageSize) throws Exception {
        try {
            return projectService.findAllByStatus(status, pageNumber, pageSize)
                    .map(
                            project -> {
                                return projectConverter.convertFromEntity(project);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> findAllByTag(String tag, int pageNumber, int pageSize) throws Exception {
        try {
            return projectService.findAllByTag(tag, pageNumber, pageSize)
                    .map(
                            project -> {
                                return projectConverter.convertFromEntity(project);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

}
