package np.com.cscpl.AuthorizationServer.facade.client;

import np.com.cscpl.AuthorizationServer.converter.client.ClientConverter;
import np.com.cscpl.AuthorizationServer.dto.client.ClientResponseDto;
import np.com.cscpl.AuthorizationServer.dto.client.ClientsResponseDto;
import np.com.cscpl.AuthorizationServer.model.Client;
import np.com.cscpl.AuthorizationServer.service.client.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class ClientFacadeImpl implements  ClientFacade{

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientConverter clientConverter;



    @Override
    public Page<?> findAll(int pageNumber, int pageSize,String search) throws Exception {

        try {
            return clientService.findAll(pageNumber, pageSize, search)
                    .map(
                            client -> {
                                return clientConverter.convertFromEntity(client);

                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public ClientsResponseDto save(ClientResponseDto clientResponseDto) throws  Exception {
        try {
            return new ClientsResponseDto(
                    clientConverter.convertFromEntity(
                            clientService.save(
                                    clientConverter.convertFromDto(clientResponseDto))));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }

    }

    @Override
    public ClientResponseDto update(int id, ClientResponseDto clientResponseDto) throws Exception {
        try {
            return  clientConverter.convertFromEntity(
                    clientService.update(
                            id,
                            clientConverter.convertFromDto(
                                    clientResponseDto
                            )
                    )
            );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void deleteClient(int id) {

        clientService.deleteClient(id);
    }

    @Override
    public ClientResponseDto getById(int id) {

        return clientConverter.convertFromEntity(clientService.getById(id));
    }

    @Override
    public ClientsResponseDto findAll() throws Exception {
        try {
            return new ClientsResponseDto(
                    clientConverter.createFromEntities(
                            clientService.findAll()
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ClientsResponseDto();
        }
    }
}
