package np.com.cscpl.AuthorizationServer.facade.resourcetype;

import np.com.cscpl.AuthorizationServer.converter.resourcetype.ResourceTypeConverter;
import np.com.cscpl.AuthorizationServer.dto.resourcetype.ResourceTypeResourceDto;
import np.com.cscpl.AuthorizationServer.dto.resourcetype.ResourceTypesResponseDto;
import np.com.cscpl.AuthorizationServer.service.resourcetype.ResourceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
public class ResourceTypeFacadeImpl implements ResourceTypeFacade {

    @Autowired
    private ResourceTypeConverter resourceTypeConverter;

    @Autowired
    private ResourceTypeService resourceTypeService;

    @Override
    public Page<?> findAll(int pageNumber, int pageSize, String search) throws Exception {
        try {
            return resourceTypeService.findAll(pageNumber, pageSize, search).map(
                    resourceType -> {
                        return resourceTypeConverter.convertFromEntity(resourceType);
                    }
            );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }

    }

    @Override
    public ResourceTypesResponseDto save(ResourceTypeResourceDto resourceTypeResourceDto) throws Exception {
        try {
            return new ResourceTypesResponseDto(
                    resourceTypeConverter.convertFromEntity(resourceTypeService.save(resourceTypeConverter.convertFromDto(resourceTypeResourceDto))));

        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }

    }

    @Override
    public ResourceTypeResourceDto getById(int id) {
        try {
            return resourceTypeConverter.convertFromEntity(resourceTypeService.getById(id));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public ResourceTypeResourceDto update(int id, ResourceTypeResourceDto resourceTypeResourceDto) throws Exception {
        try{
            return new ResourceTypeResourceDto(
                    resourceTypeConverter.convertFromEntity(
                            resourceTypeService.update(id,
                                    resourceTypeConverter.convertFromDto(resourceTypeResourceDto))));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public ResourceTypesResponseDto findAll() throws Exception {
        try {
            return new ResourceTypesResponseDto(
                   resourceTypeConverter.createFromEntities(
                            resourceTypeService.findAll()
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResourceTypesResponseDto();
        }
    }
}
