package np.com.cscpl.AuthorizationServer.facade.batchimage;


import np.com.cscpl.AuthorizationServer.converter.batchimage.BatchImageConverter;
import np.com.cscpl.AuthorizationServer.dto.batchimage.BatchImageResponseDto;
import np.com.cscpl.AuthorizationServer.dto.batchimage.BatchImagesResponseDto;
import np.com.cscpl.AuthorizationServer.service.batchimage.BatchImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BatchImageFacadeImpl  implements BatchImageFacade{
    @Autowired
    private BatchImageService batchImageService;

    @Autowired
    private BatchImageConverter batchImageConverter;

    @Override
    public BatchImagesResponseDto save(BatchImageResponseDto batchImageResponseDto) throws Exception {
        try {
            return new BatchImagesResponseDto(
                    batchImageConverter.convertFromEntity(
                    batchImageService.save(
                            batchImageConverter.convertFromDto(
                                    batchImageResponseDto
                            )
                    )
            ));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public BatchImageResponseDto findFirst() throws Exception {
        try {
                return batchImageConverter.convertFromEntity(batchImageService.findOne());
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return null;
        }
    }
}
