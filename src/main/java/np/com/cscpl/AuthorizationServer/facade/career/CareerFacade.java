package np.com.cscpl.AuthorizationServer.facade.career;

import np.com.cscpl.AuthorizationServer.dto.career.CareerResponseDto;

public interface CareerFacade {
   CareerResponseDto sendEmail(CareerResponseDto careerResponseDto) throws Exception;
}
