package np.com.cscpl.AuthorizationServer.facade.contact;

import np.com.cscpl.AuthorizationServer.converter.contact.ContactConverter;
import np.com.cscpl.AuthorizationServer.dto.contact.ContactReponseDto;
import np.com.cscpl.AuthorizationServer.model.Contact;
import np.com.cscpl.AuthorizationServer.service.contact.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class ContactFacadeImpl implements  ContactFacade {

    private ContactService contactService;

    private ContactConverter contactConverter;


    @Autowired
    public ContactFacadeImpl(ContactService contactService, ContactConverter contactConverter) {
        this.contactService = contactService;
        this.contactConverter = contactConverter;
    }




    @Override
    public Page<?> findAll(int pageNumber, int pageSize) throws Exception {
        return contactService.findAll(pageNumber,pageSize).map(contact -> {
            return new ContactReponseDto(
                    contact.getFullName(),
                    contact.getEmail(),
                    contact.getPhoneNo(),
                    contact.getContactReason(),
                    contact.getMessage()
            );

                }
        );
    }

    @Override
    public ContactReponseDto save(ContactReponseDto contactReponseDto) throws Exception {
        try{
            return  contactConverter.convertFromEntity(
                    contactService.saveContact(
                            contactConverter.convertFromDto(contactReponseDto
                            )
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public ContactReponseDto sendEmail(ContactReponseDto contactReponseDto) throws Exception {

    try {
        return contactConverter.convertFromEntity(contactService.sendEmail(contactConverter.convertFromDto(contactReponseDto))
        );
    }catch(Exception ex){
        System.out.println(ex.getLocalizedMessage());
        throw new IllegalArgumentException(ex.getLocalizedMessage());
    }
    }
}
