package np.com.cscpl.AuthorizationServer.facade.hrSector;

import np.com.cscpl.AuthorizationServer.converter.HrSector.HrSectorConverter;
import org.springframework.stereotype.Service;
import np.com.cscpl.AuthorizationServer.dto.hrSector.HrSectorResponseDto;
import np.com.cscpl.AuthorizationServer.dto.hrSector.HrSectorsResponseDto;
import np.com.cscpl.AuthorizationServer.service.hrSector.HrSectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

@Service
public class HrSectorFacadeImpl implements HrSectorFacade {

    @Autowired
    private HrSectorService hrSectorService;

    @Autowired
    private HrSectorConverter hrSectorConverter;

    @Override
    public Page<?> findAll(int pageNumber, int pageSize) throws Exception {
        try {
            return hrSectorService.findAll(pageNumber, pageSize)
                    .map(
                            hrSector -> {
                                return hrSectorConverter.convertFromEntity(hrSector);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> searchSector(String keyword, int pageNumber, int pageSize) throws Exception {
        try {
            return hrSectorService.searchSector(keyword, pageNumber, pageSize)
                    .map(
                            hrSector -> {
                                return hrSectorConverter.convertFromEntity(hrSector);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }
    
    @Override
    public Page<?> searchSubSector(String keyword, int pageNumber, int pageSize) throws Exception {
        try {
            return hrSectorService.searchSubSector(keyword, pageNumber, pageSize)
                    .map(
                            hrSector -> {
                                return hrSectorConverter.convertFromEntity(hrSector);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }
    
    @Override
    public Page<?> searchOtherSector(String keyword, int pageNumber, int pageSize) throws Exception {
        try {
            return hrSectorService.searchOtherSector(keyword, pageNumber, pageSize)
                    .map(
                            hrSector -> {
                                return hrSectorConverter.convertFromEntity(hrSector);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public HrSectorsResponseDto save(HrSectorResponseDto hrSectorResponseDto) throws Exception {
        try {
            return new HrSectorsResponseDto(
                    hrSectorConverter.convertFromEntity(
                            hrSectorService.save(
                                    hrSectorConverter.convertFromDto(hrSectorResponseDto)
                            )
                    )
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public HrSectorsResponseDto findOne(int id) throws Exception {
        try {
            return new HrSectorsResponseDto(
                    hrSectorConverter.convertFromEntity(
                            hrSectorService.findOne(id)
                    )
            );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(int id, HrSectorResponseDto hrSectorResponseDto) throws Exception {
        try {
            hrSectorService.update(id, hrSectorConverter.convertFromDto(hrSectorResponseDto));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            hrSectorService.delete(id);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> IsOther(int pageNumber, int pageSize) throws Exception {
        try {
            return hrSectorService.IsOther(pageNumber, pageSize)
                    .map(
                            hrSector -> {
                                return hrSectorConverter.convertFromEntity(hrSector);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> IsSub(int pageNumber, int pageSize) throws Exception {
        try {
            return hrSectorService.IsSub(pageNumber, pageSize)
                    .map(
                            hrSector -> {
                                return hrSectorConverter.convertFromEntity(hrSector);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

}
