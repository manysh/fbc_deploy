package np.com.cscpl.AuthorizationServer.facade.finalproject;

import java.util.ArrayList;
import java.util.List;
import np.com.cscpl.AuthorizationServer.converter.finalproject.FinalProjectConverter;
import np.com.cscpl.AuthorizationServer.converter.finalproject.FinalProjectCreateConverter;
import np.com.cscpl.AuthorizationServer.converter.finalproject.FinalProjectDownloadConverter;
import np.com.cscpl.AuthorizationServer.dto.finalproject.*;
import np.com.cscpl.AuthorizationServer.model.FinalProject;
import np.com.cscpl.AuthorizationServer.model.Sector;
import np.com.cscpl.AuthorizationServer.repository.DistrictRepository;
import np.com.cscpl.AuthorizationServer.repository.SectorRepository;
import np.com.cscpl.AuthorizationServer.repository.TagRepository;
import np.com.cscpl.AuthorizationServer.repository.agency.FundingAgencyRepository;
import np.com.cscpl.AuthorizationServer.repository.sector2.Sector2repository;
import np.com.cscpl.AuthorizationServer.repository.sector3.Sector3Repository;
import np.com.cscpl.AuthorizationServer.service.client.ClientService;
import np.com.cscpl.AuthorizationServer.service.finalproject.FinalProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.Optional;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import np.com.cscpl.AuthorizationServer.model.Client;
import np.com.cscpl.AuthorizationServer.model.FundingAgency;
import org.springframework.data.jpa.domain.Specification;

@Component
public class FinalProjectFacadeImpl implements FinalProjectFacade {

    private FinalProjectService finalProjectService;
    private SectorRepository sectorRepository;
    private Sector2repository sector2repository;
    private Sector3Repository sector3Repository;
    private ClientService clientService;
    private TagRepository tagRepository;
    private FinalProjectConverter finalProjectConverter;
    private FinalProjectCreateConverter finalProjectCreateConverter;
    private FundingAgencyRepository fundingAgencyRepository;
    private DistrictRepository districtRepository;
    private FinalProjectDownloadConverter finalProjectDownloadConverter;

    @Autowired
    public FinalProjectFacadeImpl(FinalProjectService finalProjectService,
            SectorRepository sectorRepository,
            Sector2repository sector2repository,
            Sector3Repository sector3Repository,
            ClientService clientService,
            TagRepository tagRepository,
            FinalProjectConverter finalProjectConverter,
            FinalProjectCreateConverter finalProjectCreateConverter,
            FundingAgencyRepository fundingAgencyRepository,
            DistrictRepository districtRepository,
            FinalProjectDownloadConverter finalProjectDownloadConverter
    ) {
        this.finalProjectService = finalProjectService;
        this.sectorRepository = sectorRepository;
        this.sector2repository = sector2repository;
        this.sector3Repository = sector3Repository;
        this.clientService = clientService;
        this.tagRepository = tagRepository;
        this.finalProjectConverter = finalProjectConverter;
        this.finalProjectCreateConverter = finalProjectCreateConverter;
        this.fundingAgencyRepository = fundingAgencyRepository;
        this.districtRepository = districtRepository;
        this.finalProjectDownloadConverter = finalProjectDownloadConverter;
    }

    @Override
    public FinalProjectResponseDto create(FinalProjectCreateResponseDto finalProjectCreateResponseDto) throws Exception {
        try {
            return finalProjectConverter.convertFromEntity(
                    finalProjectService.create(
                            fromCreateToDomain(finalProjectCreateResponseDto)
                    )
            );
        } catch (Exception ex) {
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> extensiveProjectSearch(ProjectSearchDto projectSearchDto) throws Exception {
        try {
            return finalProjectService.extensiveSerch(projectSearchDto);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<Object>(null);
        }
    }

    private FinalProject fromCreateToDomain(FinalProjectCreateResponseDto finalProjectCreateResponseDto) throws Exception {
        if (finalProjectCreateResponseDto == null || finalProjectCreateResponseDto == new FinalProjectCreateResponseDto()) {
            throw new NullPointerException("Data is empty");
        }
        FinalProject finalProject = new FinalProject();
        try {
            finalProject = finalProjectCreateConverter.convertFromDto(finalProjectCreateResponseDto);
            finalProject.setClient1(clientService.getById(finalProjectCreateResponseDto.getClient1Id()));
            if (finalProjectCreateResponseDto.getClient2Id() != 0) {
                finalProject.setClient2(clientService.getById(finalProjectCreateResponseDto.getClient2Id()));
            }
            Optional<Sector> sector = sectorRepository.findById(finalProjectCreateResponseDto.getSector1Id());
            finalProject.setSector1(sector.isPresent() ? sector.get() : null);
            if (finalProjectCreateResponseDto.getSector2Ids() != null && finalProjectCreateResponseDto.getSector2Ids().size() > 0) {
                finalProject.setSector2s(sector2repository.findByIdIn(finalProjectCreateResponseDto.getSector2Ids()));
            }
            if (finalProjectCreateResponseDto.getSector3Ids() != null && finalProjectCreateResponseDto.getSector3Ids().size() > 0) {
                finalProject.setSector3s(sector3Repository.findByIdIn(finalProjectCreateResponseDto.getSector3Ids()));
            }
            finalProject.setTags(tagRepository.findByIdIn(finalProjectCreateResponseDto.getTagIds()));
            finalProject.setFundingAgencies(fundingAgencyRepository.findByIdIn(finalProjectCreateResponseDto.getFundingAgenciesIds()));
            finalProject.setDistricts(districtRepository.findByIdIn(finalProjectCreateResponseDto.getDistrictIds()));
            return finalProject;
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public FinalProjectResponseDto findById(int id) throws Exception {
        try {
            return Helper.setLink(finalProjectConverter.convertFromEntity(finalProjectService.findById(id)));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public FinalProjectResponseDto updateProject(int id, FinalProjectCreateResponseDto finalProjectCreateResponseDto) throws Exception {
        try {
            FinalProject finalProject = finalProjectService.findById(id);
            try {
                finalProject = finalProjectCreateConverter.convertFromDto(finalProjectCreateResponseDto);
                finalProject.setClient1(clientService.getById(finalProjectCreateResponseDto.getClient1Id()));
                if (finalProjectCreateResponseDto.getClient2Id() != 0) {
                    finalProject.setClient2(clientService.getById(finalProjectCreateResponseDto.getClient2Id()));
                } else {
                    finalProject.setClient2(null);
                }
                Optional<Sector> sector = sectorRepository.findById(finalProjectCreateResponseDto.getSector1Id());
                finalProject.setSector1(sector.isPresent() ? sector.get() : null);
                finalProject.setSector2s(sector2repository.findByIdIn(finalProjectCreateResponseDto.getSector2Ids()));
                finalProject.setSector3s(sector3Repository.findByIdIn(finalProjectCreateResponseDto.getSector3Ids()));
                finalProject.setTags(tagRepository.findByIdIn(finalProjectCreateResponseDto.getTagIds()));
                finalProject.setFundingAgencies(fundingAgencyRepository.findByIdIn(finalProjectCreateResponseDto.getFundingAgenciesIds()));
                finalProject.setDistricts(districtRepository.findByIdIn(finalProjectCreateResponseDto.getDistrictIds()));
            } catch (Exception ex) {
                System.out.println(ex.getLocalizedMessage());
                return null;
            }
            return finalProjectConverter.convertFromEntity(finalProjectService.create(Helper.updateProject(
                    finalProject, finalProjectCreateResponseDto)));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public FinalProjectsResponseDto homepageList(String projectStatus) throws Exception {
        try {
            return new FinalProjectsResponseDto(
                    Helper.linkToDto(finalProjectConverter.createFromEntities(
                            finalProjectService.homepageList(projectStatus)
                    )
                    ));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new FinalProjectsResponseDto();
        }
    }

    @Override
    public FinalProjectDownloadsResponseDto downloadProject(String projectStatus) throws Exception {
        try {
            return new FinalProjectDownloadsResponseDto(
                    finalProjectDownloadConverter.createFromEntities(
                            finalProjectService.downloadProject(
                                    projectStatus
                            )
                    )
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new FinalProjectDownloadsResponseDto();
        }
    }

    @Override
    public Page<?> search(ProjectSearchDto psd) throws Exception {
        System.out.println(psd.toString());
        try {
            Specification spec = (Specification) (Root root, CriteriaQuery cq, CriteriaBuilder cb) -> {
                List<Predicate> predicates = new ArrayList<>();

//                if (psd.getTitle() != null) {
//                    predicates.add(cb.or(
//                            cb.like(cb.lower(root.get("title")), "%" + psd.getTitle().toLowerCase() + "%"),
//                            cb.like(cb.lower(root.get("description")), "%" + psd.getTitle().toLowerCase() + "%"),
//                            cb.like(cb.lower(root.get("workDescription")), "%" + psd.getTitle().toLowerCase() + "%")
//                    ));
//                }
//
//                if (psd.getSectorId() != 0) {
//                    Join<FinalProject, Sector> sector = root.join("sector1");
//                    predicates.add(cb.equal(root.get("sector1"), psd.getSectorId()));
//                }

//                if (psd.getClient1Id() != 0) {
//                    Join<FinalProject, Client> client = root.join("client1");
//                    predicates.add(cb.equal(root.get("client1"), psd.getClient1Id()));
//                }
//                if (psd.getAgencyIDs() != null && !psd.getAgencyIDs().isEmpty()) {
//                   // Join<FinalProject, FundingAgency> funding = root.join("fundingAgencies");
//                    predicates.add(cb.equal(root.join("fundingAgencies"), psd.getAgencyIDs().get(0)));
//                }

                if (psd.getAgencyIDs() != null && !psd.getAgencyIDs().isEmpty()) {
                    //Join<FinalProject, FundingAgency> join = root.join("fundingAgencies");
                    In<Integer> in = cb.in(root.join("fundingAgencies"));
                    psd.getAgencyIDs().forEach((f) -> {
                        in.value(f);
                    });
                    predicates.add(in);
                }
                return cb.and(predicates.toArray(new Predicate[0]));
            };

            return finalProjectService.search(psd.getPageNumber(), psd.getPageSize(), spec)
                    .map(fp -> {
                        return finalProjectConverter.convertFromEntity(fp);
                    });
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }
}
