package np.com.cscpl.AuthorizationServer.facade.hrSector;

import np.com.cscpl.AuthorizationServer.dto.hrSector.HrSectorResponseDto;
import np.com.cscpl.AuthorizationServer.dto.hrSector.HrSectorsResponseDto;
import org.springframework.data.domain.Page;

public interface HrSectorFacade {

    Page<?> findAll(int pageNumber, int pageSize) throws Exception;
    
    Page<?> searchSector(String keyword, int pageNumber, int pageSize) throws Exception;
    
    Page<?> searchSubSector(String keyword, int pageNumber, int pageSize) throws Exception;
    
    Page<?> searchOtherSector(String keyword, int pageNumber, int pageSize) throws Exception;

    HrSectorsResponseDto save(HrSectorResponseDto hrSectorResponseDto) throws Exception;

    HrSectorsResponseDto findOne(int id) throws Exception;

    void update(int id,HrSectorResponseDto hrSectorResponseDto) throws Exception;

    void delete(int id) throws Exception;
    
    Page<?> IsOther(int pageNumber, int pageSize) throws Exception;
    
    Page<?> IsSub(int pageNumber, int pageSize) throws Exception;
    
}
