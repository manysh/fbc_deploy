package np.com.cscpl.AuthorizationServer.facade.degree;

import np.com.cscpl.AuthorizationServer.converter.degree.DegreeConverter;
import np.com.cscpl.AuthorizationServer.dto.degree.DegreeResponseDto;
import np.com.cscpl.AuthorizationServer.dto.degree.DegreesReponseDto;
import np.com.cscpl.AuthorizationServer.model.Degree;
import np.com.cscpl.AuthorizationServer.service.degree.DegreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
public class DegreeFacadeImpl implements DegreeFacade {

    @Autowired
    private DegreeConverter degreeConverter;

    @Autowired
    private DegreeService degreeService;

    @Override
    public Page<?> findAll(int pageNumber, int pageSize, String search) throws Exception {
        try {
            return degreeService.findAll(pageNumber, pageSize, search).map(degree -> {
                return degreeConverter.convertFromEntity(degree);
            });
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public DegreeResponseDto save(DegreeResponseDto degreeResponseDto) throws Exception {
        try {
            return degreeConverter.convertFromEntity(
                    degreeService.save(
                            createFromDomain(degreeResponseDto)
                    )
            );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    private Degree createFromDomain(DegreeResponseDto degreeResponseDto) throws Exception {
        if (degreeResponseDto == null || degreeResponseDto == new DegreeResponseDto()) {
            throw new NullPointerException("Data is empty");
        }
        Degree degree = new Degree();
        try {
            degree = degreeConverter.convertFromDto(degreeResponseDto);
//            degree.setLevel(levelService.getById(degreeResponseDto.getLevelId()));
            return degree;
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return null;
        }

    }

    @Override
    public DegreeResponseDto getById(int id) throws Exception {
        try {
            return degreeConverter.convertFromEntity(degreeService.getById(id));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public DegreeResponseDto update(int id, DegreeResponseDto degreeResponseDto) throws Exception {
        try {
            return new DegreeResponseDto(
                    degreeConverter.convertFromEntity(degreeService.update(id, createFromDomain(degreeResponseDto))));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }
}
