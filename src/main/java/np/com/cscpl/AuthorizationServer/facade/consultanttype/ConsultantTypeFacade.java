package np.com.cscpl.AuthorizationServer.facade.consultanttype;

import np.com.cscpl.AuthorizationServer.dto.consultanttype.ConsultantTypeResponseDto;
import np.com.cscpl.AuthorizationServer.dto.consultanttype.ConsultantTypesResponseDto;
import org.springframework.data.domain.Page;

public interface ConsultantTypeFacade {
    Page<?> findAll(int pageNumber,int pageSize,String search)throws Exception;
    ConsultantTypesResponseDto save(ConsultantTypeResponseDto consultantTypeResponseDto)throws Exception;
    ConsultantTypeResponseDto update(int id, ConsultantTypeResponseDto consultantTypeResponseDto)throws Exception;
    ConsultantTypeResponseDto getById(int id)throws  Exception;
    ConsultantTypesResponseDto findAll() throws Exception;
}
