package np.com.cscpl.AuthorizationServer.facade.tag;

import np.com.cscpl.AuthorizationServer.dto.tag.TagResponseDto;
import np.com.cscpl.AuthorizationServer.dto.tag.TagsResponseDto;
import np.com.cscpl.AuthorizationServer.model.Tag;
import org.springframework.data.domain.Page;

import java.util.List;

public interface TagFacade {

    Page<?> findAll(int pageNumber, int pageSize) throws Exception;

    TagsResponseDto save(TagResponseDto tagsResponseDto) throws Exception;

    TagsResponseDto findOne(int id) throws Exception;

    void update(TagResponseDto tagsResponseDto) throws Exception;

    void delete(int id) throws Exception;

    TagsResponseDto findByDepartmentId(int id) throws Exception;

    TagsResponseDto findAll() throws Exception;

    TagsResponseDto findByProjectIds(List<Integer> projectIds) throws Exception;

    Page<?> getFilteredResult(String search,int pageNumber, int pageSize) throws Exception;

    TagResponseDto updateTag(int id, TagResponseDto tagResponseDto)throws Exception;
}
