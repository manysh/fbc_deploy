package np.com.cscpl.AuthorizationServer.facade.authority;

import np.com.cscpl.AuthorizationServer.converter.authority.AuthorityConverter;
import np.com.cscpl.AuthorizationServer.dto.authority.AuthoritiesResposeDto;
import np.com.cscpl.AuthorizationServer.service.authority.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuthorityFacadeImpl implements AuthorityFacade {

    private AuthorityService authorityService;
    private AuthorityConverter authorityConverter;

    @Autowired
    public AuthorityFacadeImpl(AuthorityService authorityService, AuthorityConverter authorityConverter) {
        this.authorityService = authorityService;
        this.authorityConverter = authorityConverter;
    }

    @Override
    public AuthoritiesResposeDto findAll() throws Exception {
        try {
            return new AuthoritiesResposeDto(authorityConverter.createFromEntities(authorityService.findAll()));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new AuthoritiesResposeDto();
        }
    }
}
