package np.com.cscpl.AuthorizationServer.facade.sector2;

import np.com.cscpl.AuthorizationServer.converter.sector2.Sector2Converter;
import np.com.cscpl.AuthorizationServer.dto.sector2.Sector2ResponseDto;
import np.com.cscpl.AuthorizationServer.dto.sector2.Sector2sResponseDto;
import np.com.cscpl.AuthorizationServer.model.Sector2;
import np.com.cscpl.AuthorizationServer.service.Sector2.Sector2Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class Sector2FacadeImpl implements Sector2Facade {

    private Sector2Service sector2Service;
    private Sector2Converter sector2Converter;

    @Autowired
    public Sector2FacadeImpl(Sector2Service sector2Service, Sector2Converter sector2Converter) {
        this.sector2Service = sector2Service;
        this.sector2Converter = sector2Converter;
    }

    @Override
    public Sector2sResponseDto findAll() throws Exception {
        try {
            return new Sector2sResponseDto(
                    sector2Converter.createFromEntities(
                            sector2Service.findAll()
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new Sector2sResponseDto();
        }
    }

    @Override
    public Sector2sResponseDto findByProjectId(int id) throws Exception {
        try {
            return new Sector2sResponseDto(
                    sector2Converter.createFromEntities(
                            sector2Service.findByProjectId(id)
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new Sector2sResponseDto();
        }
    }

    @Override
    public Sector2sResponseDto save(Sector2ResponseDto sector2ResponseDto) throws Exception {
        try {
            return new Sector2sResponseDto(sector2Converter.convertFromEntity(sector2Service.create(
                    sector2Converter.convertFromDto(
                            sector2ResponseDto
                    )
            )
            ));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> filteredResult(int pageNumber, int pageSize, String search) throws Exception {
        try{
            Page<Sector2> sector2s = sector2Service.filteredResult(
                    pageNumber,
                    pageSize,
                    search
            );
            return  sector2s.map(sector2 -> new Sector2ResponseDto(
                    sector2.getId(),
                    sector2.getTitle(),
                    sector2.getDescription()
            ));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<Object>(new ArrayList<>());
        }
    }

    @Override
    public Sector2ResponseDto findById(int id) throws Exception {
        try {
            return sector2Converter.convertFromEntity(sector2Service.findById(id));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Sector2ResponseDto update(int id, Sector2ResponseDto sector2ResponseDto) throws Exception {
        try {
            return
                    sector2Converter.convertFromEntity(
                            sector2Service.update(id,sector2Converter.convertFromDto(sector2ResponseDto))
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }
}
