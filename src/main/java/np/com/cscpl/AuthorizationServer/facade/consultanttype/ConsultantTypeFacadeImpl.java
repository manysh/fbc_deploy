package np.com.cscpl.AuthorizationServer.facade.consultanttype;


import np.com.cscpl.AuthorizationServer.converter.consultanttype.ConsultantTypeConverter;
import np.com.cscpl.AuthorizationServer.dto.consultanttype.ConsultantTypeResponseDto;
import np.com.cscpl.AuthorizationServer.dto.consultanttype.ConsultantTypesResponseDto;
import np.com.cscpl.AuthorizationServer.service.consultanttype.ConsultantTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
public class ConsultantTypeFacadeImpl implements  ConsultantTypeFacade{

    @Autowired
    private ConsultantTypeService consultantTypeService;

    @Autowired
    private ConsultantTypeConverter consultantTypeConverter;



    @Override
    public Page<?> findAll(int pageNumber, int pageSize, String search) throws Exception {
        try {
            return consultantTypeService.findAll(pageNumber, pageSize, search).map(
                    resourceType -> {
                        return consultantTypeConverter.convertFromEntity(resourceType);
                    }
            );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }

    }

    @Override
    public ConsultantTypesResponseDto save(ConsultantTypeResponseDto consultantTypeResponseDto) throws Exception {
        try {
            return new ConsultantTypesResponseDto(
                    consultantTypeConverter.convertFromEntity(
                            consultantTypeService.save(
                                    consultantTypeConverter.convertFromDto(consultantTypeResponseDto))));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public ConsultantTypeResponseDto update(int id, ConsultantTypeResponseDto consultantTypeResponseDto) throws Exception {
        try{
            return new ConsultantTypeResponseDto(
                    consultantTypeConverter.convertFromEntity(
                           consultantTypeService.update(id,
                                    consultantTypeConverter.convertFromDto(consultantTypeResponseDto))));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public ConsultantTypeResponseDto getById(int id) throws Exception {
       try{
           return consultantTypeConverter.convertFromEntity(consultantTypeService.getById(id));
       }catch (Exception ex){
           System.out.println(ex.getLocalizedMessage());
           throw new IllegalArgumentException(ex.getLocalizedMessage());
       }
    }

    @Override
    public ConsultantTypesResponseDto findAll() throws Exception {
        try {
            return new ConsultantTypesResponseDto(
                    consultantTypeConverter.createFromEntities(
                            consultantTypeService.findAll()
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ConsultantTypesResponseDto();
        }
    }
}
