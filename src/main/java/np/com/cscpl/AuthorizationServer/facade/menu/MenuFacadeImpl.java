package np.com.cscpl.AuthorizationServer.facade.menu;

import np.com.cscpl.AuthorizationServer.converter.menu.MenuConverter;
import np.com.cscpl.AuthorizationServer.dto.menu.MenuResponseDto;
import np.com.cscpl.AuthorizationServer.dto.menu.MenuResponsesDto;
import np.com.cscpl.AuthorizationServer.model.Menu;
import np.com.cscpl.AuthorizationServer.service.menu.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MenuFacadeImpl implements MenuFacade {
    @Autowired
    private MenuService menuService;

    @Autowired
    private MenuConverter menuConverter;
    @Override
    public MenuResponsesDto findAll() throws Exception {
        try {
            return new MenuResponsesDto(menuConverter.createFromEntities(menuService.findAll()));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new MenuResponsesDto();
        }
    }

    @Override
    public MenuResponsesDto save(MenuResponseDto menuResponseDto) throws Exception {
        try {
            return new MenuResponsesDto(
                    menuConverter.convertFromEntity(
                            menuService.save(
                                    menuConverter.convertFromDto(menuResponseDto)
                            )
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<MenuResponseDto> menuSearch(int pageNumber, int pageSize, String search) throws Exception {
        try {
            Page<Menu> menus = menuService.menuSearch(
                    pageNumber,
                    pageSize,
                    search
            );
            return menus.map(menu -> menuConverter.convertFromEntity(menu));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<MenuResponseDto>(new ArrayList<>());
        }
    }
}
