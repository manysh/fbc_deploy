package np.com.cscpl.AuthorizationServer.facade.aboutus;

import np.com.cscpl.AuthorizationServer.dto.aboutus.AboutUsReponsesDto;

import java.util.List;

public interface AboutUsFacade {
     AboutUsReponsesDto findAll()  throws Exception;
}
