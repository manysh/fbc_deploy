package np.com.cscpl.AuthorizationServer.facade.finalproject;

import np.com.cscpl.AuthorizationServer.controller.tag.ProjectDisplayCreateDto;
import np.com.cscpl.AuthorizationServer.converter.finalproject.ProjectDisplayConverter;
import np.com.cscpl.AuthorizationServer.dto.finalproject.ProjectDisplaysResponseDto;
import np.com.cscpl.AuthorizationServer.model.FinalProject;
import np.com.cscpl.AuthorizationServer.model.ProjectDisplay;
import np.com.cscpl.AuthorizationServer.service.finalproject.FinalProjectService;
import np.com.cscpl.AuthorizationServer.service.finalproject.ProjectDisplayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class ProjectDisplayFacadeImpl implements ProjectDisplayFacade {

    private ProjectDisplayService projectDisplayService;
    private ProjectDisplayConverter projectDisplayConverter;
    private FinalProjectService finalProjectService;

    @Autowired
    public ProjectDisplayFacadeImpl(
            ProjectDisplayService projectDisplayService,
            ProjectDisplayConverter projectDisplayConverter,
            FinalProjectService finalProjectService
    ) {
        this.projectDisplayService = projectDisplayService;
        this.projectDisplayConverter = projectDisplayConverter;
        this.finalProjectService = finalProjectService;
    }

    @Override
    public ProjectDisplaysResponseDto findByStatus(String status) throws Exception {
        try{
            return new ProjectDisplaysResponseDto(
                    projectDisplayConverter.createFromEntities(
                            projectDisplayService.findByStatus(
                                    status
                            )
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ProjectDisplaysResponseDto();
        }
    }

    @Override
    public ProjectDisplaysResponseDto update(int id,ProjectDisplayCreateDto projectDisplayCreateDto) throws Exception {
        try{
            ProjectDisplay databaseDisplayData=projectDisplayService.findById(id);
            FinalProject databaseProject= finalProjectService.findById(projectDisplayCreateDto.getDepartmentId());
            databaseDisplayData.setFinalProject(databaseProject);
            return new ProjectDisplaysResponseDto(
                    projectDisplayConverter.convertFromEntity(
                            projectDisplayService.update(databaseDisplayData)
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }
}
