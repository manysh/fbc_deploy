package np.com.cscpl.AuthorizationServer.facade.province;

import np.com.cscpl.AuthorizationServer.converter.provience.ProvienceConverter;
import np.com.cscpl.AuthorizationServer.dto.province.ProviencesResponseDto;
import np.com.cscpl.AuthorizationServer.service.provience.ProvienceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProvienceFacadeImpl implements ProvienceFacade{


    @Autowired
    private ProvienceService provienceService;

    @Autowired
    private ProvienceConverter provienceConverter;

    @Override
    public ProviencesResponseDto findAll() throws Exception {
        try {
            return new ProviencesResponseDto(provienceConverter.createFromEntities(provienceService.findAll()));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ProviencesResponseDto();
        }
    }
}
