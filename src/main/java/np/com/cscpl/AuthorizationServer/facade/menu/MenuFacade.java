package np.com.cscpl.AuthorizationServer.facade.menu;

import np.com.cscpl.AuthorizationServer.dto.menu.MenuResponseDto;
import np.com.cscpl.AuthorizationServer.dto.menu.MenuResponsesDto;
import np.com.cscpl.AuthorizationServer.model.Menu;
import org.springframework.data.domain.Page;

import java.util.List;

public interface MenuFacade {
    MenuResponsesDto findAll() throws Exception;
    MenuResponsesDto save(MenuResponseDto menuResponseDto) throws Exception;
    Page<MenuResponseDto> menuSearch(int pageNumber,int pageSize, String search) throws Exception;
}
