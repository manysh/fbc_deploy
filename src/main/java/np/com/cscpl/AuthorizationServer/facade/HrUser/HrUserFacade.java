package np.com.cscpl.AuthorizationServer.facade.HrUser;

import java.util.List;
import np.com.cscpl.AuthorizationServer.dto.finalHrUser.FinalHrUserResponseDto;
import org.springframework.data.domain.Page;

public interface HrUserFacade {

    List<?> findAll() throws Exception;

    Page<?> search(int pageNumber, int pageSize, FinalHrUserResponseDto fhurd, int minXp, int maxXp, int degree) throws Exception;

    FinalHrUserResponseDto findOne(int id) throws Exception;

    FinalHrUserResponseDto save(FinalHrUserResponseDto fhurd) throws Exception;

    void update(int id, FinalHrUserResponseDto fhurd) throws Exception;

    void delete(int id) throws Exception;

}
