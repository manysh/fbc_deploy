package np.com.cscpl.AuthorizationServer.facade.district;

import np.com.cscpl.AuthorizationServer.converter.district.DistrictConverter;
import np.com.cscpl.AuthorizationServer.dto.district.DistrictsResponseDto;
import np.com.cscpl.AuthorizationServer.service.district.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DistrictFacadeImpl implements DistrictFacade {

    private DistrictService districtService;
    private DistrictConverter districtConverter;

    @Autowired
    public DistrictFacadeImpl(DistrictService districtService, DistrictConverter districtConverter) {
        this.districtService = districtService;
        this.districtConverter = districtConverter;
    }

    @Override
    public DistrictsResponseDto findByProjectId(int projectId) throws Exception {
        try {
            return new DistrictsResponseDto(
                    districtConverter.createFromEntities(
                            districtService.findByProjectId(projectId)
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return  new DistrictsResponseDto();
        }
    }

    @Override
    public DistrictsResponseDto findAll() throws Exception {
        try {
            return new DistrictsResponseDto(
                    districtConverter.createFromEntities(
                            districtService.findAll()
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return  new DistrictsResponseDto();
        }
    }
}
