package np.com.cscpl.AuthorizationServer.facade.department;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import np.com.cscpl.AuthorizationServer.converter.department.DepartmentConverter;
import np.com.cscpl.AuthorizationServer.dto.department.DepartmentResponseDto;
import np.com.cscpl.AuthorizationServer.dto.department.DepartmentsResponseDto;
import np.com.cscpl.AuthorizationServer.service.department.DepartmentService;

@Service
public class DepartmentFacadeImpl implements DepartmentFacade {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private DepartmentConverter departmentConverter;

    @Override
    public DepartmentsResponseDto findAll() throws Exception {
        try {
            return new DepartmentsResponseDto(departmentConverter.createFromEntities(departmentService.findAll()));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return new DepartmentsResponseDto();
        }
    }

    @Override
    public DepartmentsResponseDto save(DepartmentResponseDto departmentResponseDto) throws Exception {
        try {
            return new DepartmentsResponseDto(
                    departmentConverter.convertFromEntity(
                            departmentService.save(
                                    departmentConverter.convertFromDto(departmentResponseDto)
                            )
                    )
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public DepartmentsResponseDto findOne(int id) throws Exception {
        try {
            return new DepartmentsResponseDto(
                    departmentConverter.convertFromEntity(
                            departmentService.findOne(id)
                    )
            );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(DepartmentResponseDto departmentResponseDto) throws Exception {
        try {
            departmentService.update(departmentConverter.convertFromDto(departmentResponseDto));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            departmentService.delete(id);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

}
