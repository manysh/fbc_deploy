package np.com.cscpl.AuthorizationServer.facade.department;

import np.com.cscpl.AuthorizationServer.dto.department.DepartmentResponseDto;
import np.com.cscpl.AuthorizationServer.dto.department.DepartmentsResponseDto;

public interface DepartmentFacade {

    DepartmentsResponseDto findAll() throws Exception;

    DepartmentsResponseDto save(DepartmentResponseDto departmentResponseDto) throws Exception;

    DepartmentsResponseDto findOne(int id) throws Exception;

    void update(DepartmentResponseDto sectorsResponseDto) throws Exception;

    void delete(int id) throws Exception;
}
