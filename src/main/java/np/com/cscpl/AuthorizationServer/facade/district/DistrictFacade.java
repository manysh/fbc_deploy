package np.com.cscpl.AuthorizationServer.facade.district;

import np.com.cscpl.AuthorizationServer.dto.district.DistrictsResponseDto;

public interface DistrictFacade {
    DistrictsResponseDto findByProjectId(int projectId) throws Exception;
    DistrictsResponseDto findAll() throws Exception;
}
