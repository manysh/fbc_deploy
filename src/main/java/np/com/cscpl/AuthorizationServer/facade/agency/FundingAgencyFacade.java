package np.com.cscpl.AuthorizationServer.facade.agency;

import np.com.cscpl.AuthorizationServer.dto.agency.FundingAgenciesResponseDto;
import np.com.cscpl.AuthorizationServer.dto.agency.FundingAgencyResponseDto;
import np.com.cscpl.AuthorizationServer.model.FundingAgency;
import org.springframework.data.domain.Page;

public interface FundingAgencyFacade {
    FundingAgenciesResponseDto findAll() throws Exception;
    FundingAgenciesResponseDto findByProjectId(int id) throws Exception;
    FundingAgenciesResponseDto create(FundingAgencyResponseDto fundingAgencyResponseDto) throws Exception;
    Page<?> filteredResult(int pageNumber, int pageSize, String search) throws Exception;
    FundingAgencyResponseDto findById(int id) throws Exception;
    FundingAgencyResponseDto update(int id, FundingAgencyResponseDto fundingAgencyResponseDto) throws Exception;
}
