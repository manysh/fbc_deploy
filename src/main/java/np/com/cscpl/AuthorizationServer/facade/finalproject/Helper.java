package np.com.cscpl.AuthorizationServer.facade.finalproject;

import np.com.cscpl.AuthorizationServer.controller.agency.FundingAgencyController;
import np.com.cscpl.AuthorizationServer.controller.district.DistrictController;
import np.com.cscpl.AuthorizationServer.controller.sector2.Sector2Controller;
import np.com.cscpl.AuthorizationServer.controller.sector3.Sector3Controller;
import np.com.cscpl.AuthorizationServer.controller.tag.TagController;
import np.com.cscpl.AuthorizationServer.dto.finalproject.FinalProjectCreateResponseDto;
import np.com.cscpl.AuthorizationServer.dto.finalproject.FinalProjectResponseDto;
import np.com.cscpl.AuthorizationServer.model.FinalProject;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

public class Helper {

    public static FinalProjectResponseDto setLink(FinalProjectResponseDto finalProjectResponseDto)
            throws Exception{
        try {
            int projectId=finalProjectResponseDto.getProjectId();
            finalProjectResponseDto.add(
                    Arrays.asList(

                            /*SECTOR 2 IS ADDED TO LINK*/
                            linkTo(
                                    ControllerLinkBuilder.methodOn(
                                            Sector2Controller.class
                                    )
                                            .findByProjectId(projectId)
                            ).withRel("sector2"),

                            /*SECTOR3 IS ADDED TO LINK*/
                            linkTo(
                                    ControllerLinkBuilder.methodOn(
                                            Sector3Controller.class
                                    )
                                            .findByProjectId(projectId)
                            ).withRel("sector3"),

                            /*TAGS IS ADDED TO LINK*/
                            linkTo(
                                    ControllerLinkBuilder.methodOn(
                                            TagController.class
                                    ).findByProjectId(projectId)
                            ).withRel("tags"),

                            /*FUNDING AGENCY IS ADDED*/
                            linkTo(
                                    ControllerLinkBuilder.methodOn(
                                            FundingAgencyController.class
                                    ).findByProjectId(projectId)
                            ).withRel("agency"),

                            /*LOCATION IS ADDED*/
                            linkTo(
                                    ControllerLinkBuilder.methodOn(
                                            DistrictController.class
                                    ).findByProjectId(projectId)
                            ).withRel("location")
                    )
            );

        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new Exception(ex.getLocalizedMessage());
        }
        return finalProjectResponseDto;
    }

    /**
     *
     * @param finalProjectResponseDtos
     * @return
     * @throws Exception
     */
    public static Page<?> setLinks(Page<FinalProjectResponseDto> finalProjectResponseDtos) throws Exception{
        finalProjectResponseDtos.map(finalProjectResponseDto -> {
            try {
                return setLink(finalProjectResponseDto);
            }catch (Exception ex){
                System.out.println(ex.getLocalizedMessage());
                return finalProjectResponseDto;
            }
        });
        return finalProjectResponseDtos;
    }

    public static FinalProject updateProject(
            FinalProject databaseProject,
            FinalProjectCreateResponseDto finalProjectCreateResponseDto
    ) throws Exception{
        try {
            databaseProject.setTitle(finalProjectCreateResponseDto.getTitle());
            databaseProject.setDescription(finalProjectCreateResponseDto.getDescription());
            databaseProject.setWorkDescription(finalProjectCreateResponseDto.getWorkDescription());
            databaseProject.setCreatedDate(finalProjectCreateResponseDto.getCreatedDate());
            databaseProject.setCompletetionDate(finalProjectCreateResponseDto.getEstimatedCompletionDate());
            databaseProject.setProjectStatus(finalProjectCreateResponseDto.getProjectStatus());
            databaseProject.setImagePath(finalProjectCreateResponseDto.getImagePath());
            return databaseProject;
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    public static List<FinalProjectResponseDto> linkToDto(
            List<FinalProjectResponseDto> finalProjectResponseDtos
    ) throws Exception{
        try {
            List<FinalProjectResponseDto> finalProjectResponseDtos1=finalProjectResponseDtos.stream().map(finalProjectResponseDto -> {
                try {
                    return setLink(finalProjectResponseDto);
                }catch (Exception ex){
                    System.out.println(ex.getLocalizedMessage());
                    return finalProjectResponseDto;
                }
            }).collect(Collectors.toList());
            return finalProjectResponseDtos1;
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }
}
