package np.com.cscpl.AuthorizationServer.facade.event;

import np.com.cscpl.AuthorizationServer.converter.event.EventConverter;
import org.springframework.beans.factory.annotation.Autowired;

import np.com.cscpl.AuthorizationServer.dto.event.EventResponseDto;
import np.com.cscpl.AuthorizationServer.dto.event.EventsResponseDto;
import np.com.cscpl.AuthorizationServer.service.event.EventService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
public class EventFacadeImpl implements EventFacade {

    @Autowired
    private EventService eventService;

    @Autowired
    private EventConverter eventConverter;

    @Override
    public Page<?> findAll(int pageNumber, int pageSize) throws Exception {
        try {
            return eventService.findAll(pageNumber, pageSize)
                    .map(
                            event -> {
                                return eventConverter.convertFromEntity(event);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> findAllByName(int pageNumber, int pageSize, String name) throws Exception {
        try {
            return eventService.findAllByName(pageNumber, pageSize, name)
                    .map(
                            event -> {
                                return eventConverter.convertFromEntity(event);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public EventsResponseDto save(EventResponseDto eventResponseDto) throws Exception {
        try {
            return new EventsResponseDto(
                    eventConverter.convertFromEntity(
                            eventService.save(
                                    eventConverter.convertFromDto(eventResponseDto)
                            )
                    )
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public EventsResponseDto findOne(int id) throws Exception {
        try {
            return new EventsResponseDto(
                    eventConverter.convertFromEntity(
                            eventService.findOne(id)
                    )
            );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(int id, EventResponseDto eventResponseDto) throws Exception {
        try {
            eventService.update(id, eventConverter.convertFromDto(eventResponseDto));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            eventService.delete(id);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> findAllByEventType(String eventType, int pageNumber, int pageSize) throws Exception {
        try {
            return eventService.findAllByEventType(eventType, pageNumber, pageSize)
                    .map(
                            event -> {
                                return eventConverter.convertFromEntity(event);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> search(String keyword, int pageNumber, int pageSize) throws Exception {
        try {
            return eventService.search(keyword, pageNumber, pageSize).map(event -> {
                return eventConverter.convertFromEntity(event);
            });
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> findByIsShowTrue(int pageNumber, int pageSize) throws Exception {
        try {
            return eventService.findByIsShowTrue(pageNumber, pageSize).map(event -> {
                return eventConverter.convertFromEntity(event);
            });
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public void updateShowcase(int id, EventResponseDto eventResponseDto) throws Exception {
        try {
            eventService.update(id, eventConverter.convertFromDto(eventResponseDto));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> findByEgIsShowTrue(int pageNumber, int pageSize) throws Exception {
        try {
            return eventService.findByEgIsShowTrue(pageNumber, pageSize).map(event -> {
                return eventConverter.convertFromEntity(event);
            });
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

}
