package np.com.cscpl.AuthorizationServer.facade.resourcetype;

import np.com.cscpl.AuthorizationServer.dto.resourcetype.ResourceTypeResourceDto;
import np.com.cscpl.AuthorizationServer.dto.resourcetype.ResourceTypesResponseDto;
import org.springframework.data.domain.Page;

public interface ResourceTypeFacade {
    Page<?> findAll(int pageNumber, int pageSize, String search) throws Exception;
    ResourceTypesResponseDto save(ResourceTypeResourceDto resourceTypeResourceDto) throws  Exception;
    ResourceTypeResourceDto getById(int id);
    ResourceTypeResourceDto update(int id, ResourceTypeResourceDto resourceTypeResourceDto) throws Exception;
    ResourceTypesResponseDto findAll() throws Exception;


}
