package np.com.cscpl.AuthorizationServer.facade.finalproject;

import np.com.cscpl.AuthorizationServer.controller.tag.ProjectDisplayCreateDto;
import np.com.cscpl.AuthorizationServer.dto.finalproject.ProjectDisplaysResponseDto;

import java.util.List;

public interface ProjectDisplayFacade {

    ProjectDisplaysResponseDto findByStatus(String status) throws Exception;
    ProjectDisplaysResponseDto update(int id,ProjectDisplayCreateDto projectDisplayCreateDto) throws Exception;
}
