/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.facade.eventImage;

import np.com.cscpl.AuthorizationServer.dto.event.EventResponseDto;
import np.com.cscpl.AuthorizationServer.dto.eventImage.EventImageParamsDto;
import np.com.cscpl.AuthorizationServer.dto.eventImage.EventImageResponseDto;
import np.com.cscpl.AuthorizationServer.dto.eventImage.EventImagesResponseDto;
import org.springframework.data.domain.Page;

/**
 *
 * @author puzansakya
 */
public interface EventImageFacade {

    Page<?> findAll(int eventId, int pageNumber, int pageSize) throws Exception;

    String save(EventImageParamsDto eventImageParamsDto) throws Exception;

    EventImagesResponseDto findOne(int id) throws Exception;

    void update(int id, EventImageResponseDto eventImagesResponseDto) throws Exception;

    void delete(int id) throws Exception;
}
