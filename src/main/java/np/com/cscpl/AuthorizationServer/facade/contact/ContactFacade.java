package np.com.cscpl.AuthorizationServer.facade.contact;


import np.com.cscpl.AuthorizationServer.dto.contact.ContactReponseDto;
import np.com.cscpl.AuthorizationServer.model.Contact;
import org.springframework.data.domain.Page;

public interface ContactFacade {
    Page<?> findAll(int pageNumber, int pageSize) throws Exception;
    ContactReponseDto save(ContactReponseDto contactReponseDto) throws  Exception;
   ContactReponseDto sendEmail(ContactReponseDto contactReponseDto) throws Exception;
}
