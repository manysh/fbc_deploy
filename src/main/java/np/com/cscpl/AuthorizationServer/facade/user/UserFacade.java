package np.com.cscpl.AuthorizationServer.facade.user;

import np.com.cscpl.AuthorizationServer.dto.user.UserCreateDto;
import np.com.cscpl.AuthorizationServer.dto.user.UserResponseDto;
import org.springframework.data.domain.Page;

public interface UserFacade {

    UserResponseDto save(UserCreateDto userCreateDto) throws Exception;

    boolean findByUserName(String username) throws Exception;

    UserResponseDto findById(long id) throws Exception;

    Page<?> findAll(int pageNumber, int pageSize, String search) throws Exception;

    int update(Long id, UserCreateDto userCreateDto) throws Exception;

}
