/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.facade.search;

import np.com.cscpl.AuthorizationServer.dto.search.SearchResponseDto;
import org.springframework.data.domain.Page;

/**
 *
 * @author prabhu
 */
public interface SearchFacade {
    
     SearchResponseDto keySearch(String keyword, int pageNumber, int pageSize) throws Exception;
    
}
