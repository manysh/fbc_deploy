/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.facade.eventType;

import np.com.cscpl.AuthorizationServer.dto.eventType.EventTypeResponseDto;
import np.com.cscpl.AuthorizationServer.dto.eventType.EventTypesResponseDto;
import org.springframework.data.domain.Page;

/**
 *
 * @author puzansakya
 */
public interface EventTypeFacade {

    Page<?> findAll(int pageNumber, int pageSize) throws Exception;

    EventTypesResponseDto save(EventTypeResponseDto eventTypeResponseDto) throws Exception;

    EventTypesResponseDto findOne(int id) throws Exception;

    void update(int id, EventTypeResponseDto eventTypeResponseDto) throws Exception;

    void delete(int id) throws Exception;
}
