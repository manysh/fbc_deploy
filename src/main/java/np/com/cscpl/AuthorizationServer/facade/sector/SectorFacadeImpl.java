package np.com.cscpl.AuthorizationServer.facade.sector;

import np.com.cscpl.AuthorizationServer.model.Sector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import np.com.cscpl.AuthorizationServer.converter.sector.SectorConverter;
import np.com.cscpl.AuthorizationServer.dto.sector.SectorResponseDto;
import np.com.cscpl.AuthorizationServer.dto.sector.SectorsResponseDto;
import np.com.cscpl.AuthorizationServer.service.sector.SectorService;
import org.springframework.data.domain.Page;

import java.util.List;

@Service
public class SectorFacadeImpl implements SectorFacade {

    @Autowired
    private SectorService sectorService;

    @Autowired
    private SectorConverter sectorConverter;

    @Override
    public Page<?> findAll(int pageNumber, int pageSize, String search) throws Exception {
        try {
            return sectorService.findAll(pageNumber, pageSize, search)
                    .map(
                            sector -> {
                                return sectorConverter.convertFromEntity(sector);
                            }
                    );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public SectorsResponseDto save(SectorResponseDto sectorResponseDto) throws Exception {
        try {
            return new SectorsResponseDto(
                    sectorConverter.convertFromEntity(
                            sectorService.save(
                                    sectorConverter.convertFromDto(sectorResponseDto)
                            )
                    )
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public SectorsResponseDto findOne(int id) throws Exception {
        try {
            return new SectorsResponseDto(
                    sectorConverter.convertFromEntity(
                            sectorService.findOne(id)
                    )
            );
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(int id,SectorResponseDto sectorResponseDto) throws Exception {
        try {
            sectorService.update(id,sectorConverter.convertFromDto(sectorResponseDto));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            sectorService.delete(id);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public SectorsResponseDto findByProjectId(int projectId) throws Exception {
        try {
            return new SectorsResponseDto(
                    sectorConverter.createFromEntities(
                            sectorService.findByProjectId(projectId)
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new SectorsResponseDto();
        }
    }
}
