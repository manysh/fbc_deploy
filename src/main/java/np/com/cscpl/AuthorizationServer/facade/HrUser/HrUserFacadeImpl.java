package np.com.cscpl.AuthorizationServer.facade.HrUser;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import np.com.cscpl.AuthorizationServer.converter.finalHrUserConverter.FinalHrUserConverter;
import np.com.cscpl.AuthorizationServer.dto.finalHrUser.FinalHrUserResponseDto;
import np.com.cscpl.AuthorizationServer.model.FinalHrUser;
import np.com.cscpl.AuthorizationServer.model.FinalHrUserDegree;
import np.com.cscpl.AuthorizationServer.model.HrSector;
import np.com.cscpl.AuthorizationServer.model.ResourceType;
import np.com.cscpl.AuthorizationServer.service.hrUser.HrUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class HrUserFacadeImpl implements HrUserFacade {

    @Autowired
    private HrUserService hus;

    @Autowired
    private FinalHrUserConverter fhuc;

    @Override
    public List<?> findAll() throws Exception {
        try {
            return hus.findAll().stream().map(event -> {
                return fhuc.convertFromEntity(event);
            }).collect(Collectors.toList());
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<?> search(int pageNumber, int pageSize, FinalHrUserResponseDto fhurd, int minXp, int maxXp, int degree) throws Exception {

        try {
            Specification spec = (Specification) (Root root, CriteriaQuery cq, CriteriaBuilder cb) -> {
                List<Predicate> predicates = new ArrayList<>();

                if (fhurd.getFirstName() != null) {
                    predicates.add(cb.like(
                            cb.lower(root.get("firstName")), 
                            fhurd.getFirstName().toLowerCase() + "%")
                    );
                }

                if (fhurd.getMiddleName() != null) {
                    predicates.add(cb.like(cb.lower(root.get("middleName")), fhurd.getMiddleName().toLowerCase() + "%"));
                }

                if (fhurd.getLastName() != null) {
                    predicates.add(cb.like(cb.lower(root.get("lastName")), fhurd.getLastName().toLowerCase() + "%"));
                }

                if (fhurd.getPhoneNumber() != null) {
                    predicates.add(cb.like(cb.lower(root.get("phoneNumber")), fhurd.getPhoneNumber().toLowerCase() + "%"));
                }

                if (fhurd.getAddress() != null) {
                    predicates.add(cb.like(cb.lower(root.get("address")), fhurd.getAddress().toLowerCase() + "%"));
                }

                if (!fhurd.getSectorId().isEmpty()) {
                    Join<FinalHrUser, HrSector> userSector = root.join("hrSectors");
                    predicates.add(userSector.in(fhurd.getSectorId()));
                    cq.groupBy(root.get("id"));
                }

                if (!fhurd.getHrSubSectorId().isEmpty()) {
                    Join<FinalHrUser, HrSector> userSector = root.join("hrSubSectors");
                    predicates.add(userSector.in(fhurd.getHrSubSectorId()));
                    cq.groupBy(root.get("id"));
                }

                if (!fhurd.getHrOtherSectorId().isEmpty()) {
                    Join<FinalHrUser, HrSector> userSector = root.join("hrOtherSectors");
                    predicates.add(userSector.in(fhurd.getHrOtherSectorId()));
                    cq.groupBy(root.get("id"));
                }

                if (fhurd.getConsutlanTypeId() != 0) {
                    Join<FinalHrUser, ResourceType> userConsultantType = root.join("consultantType");
                    predicates.add(cb.equal(cb.lower(userConsultantType.get("id")), fhurd.getConsutlanTypeId()));
                }

                if (fhurd.getResourceTypeId() != 0) {
                    Join<FinalHrUser, ResourceType> userResource = root.join("resourceType");
                    predicates.add(cb.equal(cb.lower(userResource.get("id")), fhurd.getResourceTypeId()));
                }

                if (degree != 0) {
                    Join<FinalHrUser, ResourceType> userdegreejoin = root.join("finalHrUserDegrees");
                    predicates.add(cb.equal(cb.lower(userdegreejoin.get("degree").get("id")), degree));
                }

                if (degree != 0 && maxXp != 0) {
                    System.out.println(LocalDate.now().minusYears(maxXp));
                    System.out.println(LocalDate.now().minusYears(minXp));
                    Join<FinalHrUser, FinalHrUserDegree> userdegreejoin = root.join("finalHrUserDegrees");
                    predicates.add(cb.between(userdegreejoin.<LocalDate>get("completionDate"), LocalDate.now().minusYears(maxXp), LocalDate.now().minusYears(minXp)));
                }

                return cb.and(predicates.toArray(new Predicate[0]));
            };

            return hus.search(pageNumber, pageSize, spec)
                    .map(user -> {
                        return fhuc.convertFromEntity(user);
                    });
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public FinalHrUserResponseDto findOne(int id) throws Exception {
        try {
            return fhuc.convertFromEntity(hus.findOne(id));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public FinalHrUserResponseDto save(FinalHrUserResponseDto fhurd) throws Exception {
        try {
            return fhuc.convertFromEntity(hus.save(fhuc.convertFromDto(fhurd)));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public void update(int id, FinalHrUserResponseDto fhurd) throws Exception {
        try {
            hus.update(id, fhuc.convertFromDto(fhurd));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            hus.delete(id);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

}
