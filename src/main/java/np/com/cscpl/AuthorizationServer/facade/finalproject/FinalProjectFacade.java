package np.com.cscpl.AuthorizationServer.facade.finalproject;

import np.com.cscpl.AuthorizationServer.dto.finalproject.*;
import org.springframework.data.domain.Page;


public interface FinalProjectFacade {

    FinalProjectResponseDto create(FinalProjectCreateResponseDto finalProjectCreateResponseDto) throws Exception;

    Page<?> extensiveProjectSearch(ProjectSearchDto projectSearchDto) throws Exception;
    
    Page<?> search(ProjectSearchDto psd) throws Exception;

    FinalProjectResponseDto findById(int id) throws Exception;

    FinalProjectResponseDto updateProject(int id, FinalProjectCreateResponseDto finalProjectCreateResponseDto) throws Exception;

    FinalProjectsResponseDto homepageList(String projectStatus) throws Exception;

    FinalProjectDownloadsResponseDto downloadProject(String projectStatus) throws Exception;
}
