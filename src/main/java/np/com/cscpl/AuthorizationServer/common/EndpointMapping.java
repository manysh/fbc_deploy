package np.com.cscpl.AuthorizationServer.common;

public class EndpointMapping {

    public static final String BASE = "/api";

    public static final String PROVIENCE = "provience";
    public static final String MENU = "/menu";
    public static final String MENU_SEARCH_URL = "/search/{pageNumber}/{pageSize}";
    public static final String SLASH_PROJECT = "/project";

    /*TAG SECTION*/
    public static final String TAG = "/tag";
    public static final String GET_TAG_BY_PROJECT_ID = SLASH_PROJECT + "/{projectId}";
    public static final String GET_TAG_BY_PROJECT_IDS = "/project/get/tag";
    public static final String BATCH = "/batch";
    public static final String IMAGE = "/image";
    public static final String GET = "/get";
    public static final String GET_FILE_BY_ID = "/get/{id}";
    public static final String UPDATE = "/update";
    public static final String UPDATE_BY_ID = "/{id}";
    public static final String DELETE = "/delete";
    public static final String UPLOAD_BATCH_IMAGE = "/upload/image";
    public static final String GET_BATCH_IMAGE = "/get/{imageName}";
    public static final String DELETE_BATCH_IMAGE = "/delete/{imageName}";
    public static final String SECTOR = "/sector";
    public static final String GET_SECTOR_BY_PROJECT = "/sector/project/{projectId}";

    public static final String DISTRICT = "district";
    public static final String CLIENT = "/client";
    public static final String GET_ALL_CLIENT = "/get/all";
    public static final String DEPARTMENT = "department";
    public static final String FILE_DOWNLOAD = "/downlaodFile";
    public static final String FILE_DOWNLOAD_GET = "/{pageNumber}/{pageSize}";
    public static final String FILE = "/file";
    public static final String SLASH = "/";
    public static final String FILE_NAME = "/{fileName}";
    public static final String SAVE = "/save";
    public static final String UPLOAD = "/upload";
    public static final String FILE_INFORMATION = "/file-information";

    public static final String PROJECT = "project";
    public static final String PROJECT_GET = "/{pageNumber}/{pageSize}";
    public static final String PROJECT_GET_BY_SECTOR = "/sector";
    public static final String PROJECT_GET_BY_STATUS = "/status";
    public static final String PROJECT_HOMEPAGE_DISPLAY = "/project/homepage/display/{status}";
    public static final String PROJECT_HOMEPAGE_DISPLAY_UPDATE = "/project/homepage/display/{id}";

    /*FUNDING AGENCY SECTION*/
    public static final String AGENCY = "/agency";
    public static final String AGENCY_SEARCH_URL = "/search/{pageNumber}/{pageSize}";
    public static final String PAGE_NUMBER = "pageNumber";
    public static final String PAGE_SIZE = "pageSize";
    public static final String SEARCH = "search";
    public static final String FIND_BY_ID_URL = "/single/{id}";
    public static final String AGENCY_UPDATE = "/update/{id}";

    /*NEW SECTION SECTION*/
    public static final String SECTOR2 = "/sector2";
    public static final String SECTOR2_BY_ID = "/{id}";
    public static final String SECTOR2_FIND_BY_PROJECT_ID = SLASH_PROJECT + "/{id}";
    public static final String SECTOR2_FILTERED = "/search/{pageNumber}/{pageSize}";
    public static final String SECTOR_UPDATE_ID = "/{id}";
    public static final String SECTOR3 = "/sector3";
    public static final String SECTOR3_FIND_BY_PROJECT_ID = SLASH_PROJECT + "/{id}";
    public static final String SECTOR3_FILTERED = "/search/{pageNumber}/{pageSize}";
    public static final String SECTOR3_FIND_BY_ID = "/{id}";

    /*FINAL PROJECT SECTION*/
    public static final String FINAL_PROJECT = "/finalProject";
    public static final String PROJECT_GET_BY_TAG = "/tag";
    public static final String TAG_SLASH_FIND_ALL = "/tag/findAll";
    public static final String PROJECT_SEARCH = "/search";
    public static final String TAG_FILTERED_RESULT = "/filter/{pageNumber}/{pageSize}";
    public static final String TAG_FILTERED_RESULT_OPTIONAL = "/filter";
    public static final String FINAL_PROJECT_UPDATE_URL = "/{id}";
    public static final String FINAL_PROJECT_HOMEPAGE_LIST_URL = "/homepage/{projectStatus}";
    public static final String FINAL_PROJECT_DOWNLOAD_BY_STATUS = "/download/{projectStatus}";

    /*
    * DISTRICT SECTION
    * */
    public static final String SLASH_DISTRICT = "/district";
    public static final String DISTRICT_BY_PROJECT_ID = "/project/{projectId}";

    /*EVENT SECTION*/
    public static final String EVENT = "/event";

    /*EVENT SECTION*/
    public static final String EVENT_IMAGE = "api/event/image";

    /*EVENT TYPE SECTION*/
    public static final String EVENT_TYPE = "/event/type";


    /*CONTACT SECTION */
    public static final String CONTACT = "/contact";

    /* About Us Page */
    public static final String ABOUT = "/about";

    /**
     * Career *
     */
    public static final String CAREER = "/career";


    /**
     * Level
     */

    public static final String LEVEL = "/level";
    public static final String GET_ALL_LEVEL = "/get/all";

    /**
     * Degree
     */

    public static final String DEGREE = "/degree";
    public static final String DEGREE_BY_LEVEL="/level/{levelId}";

    /**
     * Resource Type
     */
    public static final String RESOURCE_TYPE = "/resourcetype";

    /*HR SECTOR SECTION */
    public static final String HR_SECTOR = "/hrsector";

    /*HR SUB SECTOR SECTION */
    public static final String HR_SUB_SECTOR = "/hrsubsector";

    /*HR OTHER SECTOR SECTION */
    public static final String HR_OTHER_SECTOR = "/hrothersector";

    /*HR USER SECTION */
    public static final String HR_USER = "/hruser";
    /*
    * USER SECTION
    * */

    public static final String USER="/user";
    public static final String USER_STATUS="/user-status";
    public static final String GET_USER_BY_ID="/{userId}";

    /**
     * AUTHORITY SECTION
     */
    public static final String AUTHORITY="/authority";

    /**Consultant Section
     */

    public static final String CONSULTANT_TYPE = "/consultanttype";
}
