package np.com.cscpl.AuthorizationServer.common;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class GetPageable {
    public static Pageable createPageRequest(int pageNumber, int pageSize){
        return new Pageable() {
            @Override
            public int getPageNumber() {
                return pageNumber;
            }

            @Override
            public int getPageSize() {
                return pageSize;
            }

            @Override
            public long getOffset() {
                return (pageSize*pageNumber-pageSize);
            }

            @Override
            public Sort getSort() {
                return new Sort(
                        new Sort.Order(Sort.Direction.ASC,"id")
                );
            }

            @Override
            public Pageable next() {
                return null;
            }

            @Override
            public Pageable previousOrFirst() {
                return null;
            }

            @Override
            public Pageable first() {
                return null;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }
        };
    }
}
