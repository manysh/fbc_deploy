package np.com.cscpl.AuthorizationServer.common;

import np.com.cscpl.AuthorizationServer.othermodel.FileUpload;
import np.com.cscpl.AuthorizationServer.othermodel.ImageUpload;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.UUID;
import static np.com.cscpl.AuthorizationServer.common.ConstantData.*;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;

public class ImageUploadHelper {
    public static String TOMCAT_BASE_PATH="/opt/fbc_content";
    public static final String SLASH="/";
    public static final String IMAGE="image";
    public static final String PDF="pdf";
    public static final String ONLY_IMAGE_FILE_IS_ALLOWED="Only image file is allowed.";
    public static final String ONLY_PDF_FILE_IS_ALLOWED="Only pdf file is allowed.";
    public static final String APPLICATION_PDF="application/pdf";

    public static ImageUpload saveImageToServer(MultipartFile image) throws Exception{
        ImageUpload imageUpload = new ImageUpload();

        try {
            String getExtension = image.getContentType();
            String [] str=getExtension.split(SLASH);
            if(!str[0].equalsIgnoreCase(IMAGE)){
                throw new EntityNotFoundException(ONLY_IMAGE_FILE_IS_ALLOWED);
            }
            String fileName=UUID.randomUUID().toString()
                            +image.getOriginalFilename()
                            .replaceAll(WHITE_SPACE,EMPTY_STRING);

            Path path= getImagePath(fileName);

            if(path!=null){
                try {
                    Files.copy(image.getInputStream(),path);
                    imageUpload.setImagePath(BATCH+GET+SLASH+fileName);
                    imageUpload.setDeletePath(BATCH+DELETE+SLASH+fileName);

                }catch (Exception ex){
                    throw new NullPointerException(ex.getLocalizedMessage());
                }
            }

        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
        }
        return imageUpload;
    }


    public static FileUpload saveFileToServer(MultipartFile file) throws Exception{
        FileUpload fileUpload = new FileUpload();
        try {
            String getExtension = file.getContentType();
            if(!getExtension.equalsIgnoreCase(APPLICATION_PDF)){
                throw new EntityNotFoundException(ONLY_PDF_FILE_IS_ALLOWED);
            }
            String fileName=UUID.randomUUID().toString()
                    +file.getOriginalFilename()
                    .replaceAll(WHITE_SPACE,EMPTY_STRING);

            Path path= getFilePath(fileName);

            if(path!=null){
                try {
                    Files.copy(file.getInputStream(),path);
                    fileUpload.setFilePath(FILE+GET+SLASH+fileName);
                    fileUpload.setDeletePath(FILE+DELETE+SLASH+fileName);
                }catch (Exception ex){
                    throw new NullPointerException(ex.getLocalizedMessage());
                }
            }

        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
        }
        return fileUpload;
    }

    public static Path getImagePath(String fileName) throws Exception{
        try {

            Path path= Paths.get(
                            getTomcatBasePath()+
                                    File.separator+
                                    "data"+
                                    File.separator+
                                    "images"+
                                    File.separator+
                            fileName);
            System.out.println(path.toString());
            return path;
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return null;
        }
    }

    public static Path getFilePath(String fileName) throws Exception{
        try {

            Path path= Paths.get(
                    getTomcatBasePath()+
                            File.separator+
                            "data"+
                            File.separator+
                            "pdfs"+
                            File.separator+
                            fileName);
            return path;
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return null;
        }
    }


    public static String getImageLocation(String fileName) throws Exception{
        return getTomcatBasePath()+
                File.separator+
                "data"+
                File.separator+
                "images"+
                File.separator+
                fileName;

    }

    public static String getfileLocation(String fileName) throws Exception{
        return getTomcatBasePath()+
                File.separator+
                "data"+
                File.separator+
                "pdfs"+
                File.separator+
                fileName;
    }

    public static boolean deleFile(String fileName,String type) throws Exception{
        String location="";
        if(type==IMAGE_FILE){
            location=getImageLocation(fileName);
        }
        if(type==PDF_FILE){
            location=getfileLocation(fileName);
        }
        File getImage=new File(location);
        try{
            if(getImage.exists()){
                getImage.delete();
                return true;
            }
            else{
                return false;
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public static String getTomcatBasePath(){
        File file = new File("");
        String path=file.getAbsolutePath();
        path=path+File.separator+"fbc_content";
        return path;
    }
}
