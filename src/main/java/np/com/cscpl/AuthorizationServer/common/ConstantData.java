package np.com.cscpl.AuthorizationServer.common;

public class ConstantData {
    public static final String WHITE_SPACE="\\s+";
    public static final String EMPTY_STRING="";
    public static final String SERVER_IMAGE_PATH="data/images";
    public static final String IMAGE_FILE="imageFile";
    public static final String PDF_FILE="pdfFile";
}
