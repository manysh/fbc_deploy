package np.com.cscpl.AuthorizationServer.service.client;

import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.Client;
import np.com.cscpl.AuthorizationServer.model.FileDownload;
import np.com.cscpl.AuthorizationServer.model.FinalProject;
import np.com.cscpl.AuthorizationServer.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
@Import(GetPageable.class)
@Transactional
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private Environment env;

    Path path;


    @Override
    public Page<Client> findAll(int pageNumber, int pageSize, String name)throws  Exception {
        try {

           return  clientRepository.findByClientNameIgnoreCaseContaining(name,GetPageable.createPageRequest(pageNumber,pageSize));
        }catch(Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());

        }
    }

    @Override
    public Client save(Client client) throws Exception {
        try {

            for(int i =0; i < client.getProjectList().size(); i++)
                client.getProjectList().get(i).setClient1(client);
            for(int j =0; j <client.getProjectList().size();j++)

                client.getProjectList().get(j).setClient2(client);
                return clientRepository.save(client);


        }catch (Exception ex){
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Client update(int id, Client client) throws Exception {
        try{
            Client updateClient = clientRepository.getOne(id);


            if(updateClient==null || updateClient==new Client()){
                throw new NullPointerException("Data not found.");
            }
           client.setClientId(updateClient.getClientId());
            return clientRepository.save(client);
        }catch(Exception ex){
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public void deleteClient(int id) {
        clientRepository.deleteById(id);
    }

    @Override
    public Client getById(int id) {
        return clientRepository.getOne(id);
    }

    @Override
    public List<Client> findAll() throws Exception {
        try {
            return clientRepository.findAll();
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }
}
