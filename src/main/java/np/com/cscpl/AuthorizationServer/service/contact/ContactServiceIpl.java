package np.com.cscpl.AuthorizationServer.service.contact;


import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.Contact;
import np.com.cscpl.AuthorizationServer.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Service
public class ContactServiceIpl implements ContactService {


    private ContactRepository contactRepository;


    @Autowired
    private JavaMailSender sender;

    @Autowired
    public ContactServiceIpl(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @Override
    public Page<Contact> findAll(int pageNumber, int pageSize) throws Exception {
        try{
            return  contactRepository.findAll(GetPageable.createPageRequest(pageNumber,pageSize));
        }catch (Exception ex){
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Contact saveContact(Contact contact) throws Exception {
       try {
           return contactRepository.save(contact);
       }catch (Exception ex){
           ex.getLocalizedMessage();
           throw new IllegalArgumentException(ex.getLocalizedMessage());
       }
    }

    @Override
    public Contact sendEmail(Contact contact) throws Exception {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        try {
           helper.setFrom(contact.getEmail());
            helper.setTo("feedback@fbc.com.np");
            helper.setText("This Message is sent By "+ contact.getFullName()+ " whose email address is " + contact.getEmail() + " and  mobile number is " +contact.getPhoneNo()+" and his/her message is :"+System.lineSeparator()+
                    contact.getMessage());
            helper.setSubject(contact.getContactReason());
        } catch (MessagingException e) {
            e.printStackTrace();
            throw new IllegalArgumentException(e.getLocalizedMessage());

        }
        sender.send(message);
        return contact;


    }
}
