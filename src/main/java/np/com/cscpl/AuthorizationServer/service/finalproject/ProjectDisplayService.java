package np.com.cscpl.AuthorizationServer.service.finalproject;

import np.com.cscpl.AuthorizationServer.model.ProjectDisplay;

import java.util.List;

public interface ProjectDisplayService {
    List<ProjectDisplay> findByStatus(String status) throws Exception;
    ProjectDisplay update(ProjectDisplay projectDisplay) throws Exception;
    ProjectDisplay findById(int id) throws Exception;
}
