package np.com.cscpl.AuthorizationServer.service.provience;

import np.com.cscpl.AuthorizationServer.model.Provience;
import np.com.cscpl.AuthorizationServer.repository.ProvienceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProvienceServiceImpl implements ProvienceService {

    @Autowired
    private ProvienceRepository provienceRepository;


    @Override
    public List<Provience> findAll() throws Exception {
        try {
            return provienceRepository.findAll();
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }
}
