package np.com.cscpl.AuthorizationServer.service.consultanttype;

import np.com.cscpl.AuthorizationServer.model.ConsultantType;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ConsultantTypeService {
    Page<ConsultantType> findAll(int pageNumber,int pageSize,String name)throws Exception;
    ConsultantType save(ConsultantType consultantType)throws Exception;
    ConsultantType update(int id, ConsultantType consultantType)throws Exception;
    ConsultantType getById(int id)throws Exception;
    List<ConsultantType> findAll() throws Exception;
}
