package np.com.cscpl.AuthorizationServer.service.resourcetype;

import np.com.cscpl.AuthorizationServer.model.ResourceType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ResourceTypeService {
    Page<ResourceType> findAll(int pageNumber,int pageSize, String name)throws  Exception;
    ResourceType save(ResourceType resourceType)throws Exception;
    ResourceType getById(int id) throws Exception;
    ResourceType update(int id,ResourceType resourceType);
    List<ResourceType> findAll() throws Exception;
}
