package np.com.cscpl.AuthorizationServer.service.sector;

import java.util.List;
import java.util.Optional;
import np.com.cscpl.AuthorizationServer.model.Sector;
import org.springframework.data.domain.Page;

public interface SectorService {

    Page<Sector> findAll(int pageNumber, int pageSize, String name) throws Exception;

    Sector findOne(int id) throws Exception;

    Sector save(Sector sector) throws Exception;

    void update(int id, Sector sector) throws Exception;

    void delete(int id) throws Exception;

    List<Sector> findByIdIn(List<Integer> ids) throws Exception;

    List<Sector> findByProjectId(int projectId) throws Exception;

    Page<Sector> keySearch(String keyword, int pageNumber, int pageSize) throws Exception;
}
