package np.com.cscpl.AuthorizationServer.service.client;

import np.com.cscpl.AuthorizationServer.model.Client;
import org.springframework.data.domain.Page;

import java.util.List;


public interface ClientService {
     Page<Client> findAll(int pageNumber, int pageSize, String name) throws Exception;
     Client save(Client client) throws Exception ;
     Client getById(int id);
     void deleteClient(int id);
     Client update(int id,Client client) throws Exception;
     List<Client> findAll() throws Exception;

}
