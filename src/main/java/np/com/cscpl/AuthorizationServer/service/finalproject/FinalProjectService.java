package np.com.cscpl.AuthorizationServer.service.finalproject;

import np.com.cscpl.AuthorizationServer.dto.finalproject.ProjectSearchDto;
import np.com.cscpl.AuthorizationServer.model.FinalProject;
import org.springframework.data.domain.Page;

import java.util.List;
import org.springframework.data.jpa.domain.Specification;

public interface FinalProjectService {

    FinalProject create(FinalProject finalProject) throws Exception;

    Page<?> extensiveSerch(ProjectSearchDto projectSearchDto);
    
    Page<FinalProject> search(int pageNumber, int pageSize,Specification spec) throws Exception;

    Page<FinalProject> keySearch(String keyword, int pageNumber, int pageSize) throws Exception;

    FinalProject findById(int id) throws Exception;

    List<FinalProject> homepageList(String projectStatus) throws Exception;

    List<FinalProject> downloadProject(String projectStatus) throws Exception;
}
