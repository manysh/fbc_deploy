package np.com.cscpl.AuthorizationServer.service.hrUser;

import java.util.List;
import np.com.cscpl.AuthorizationServer.model.FinalHrUser;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

public interface HrUserService {

    List<FinalHrUser> findAll() throws Exception;

    Page<FinalHrUser> search(int pageNumber, int pageSize,Specification spec) throws Exception;

    FinalHrUser save(FinalHrUser fhu) throws Exception;

    void update(int id, FinalHrUser fhu) throws Exception;

    void delete(int id) throws Exception;

    FinalHrUser findOne(int id) throws Exception;

}
