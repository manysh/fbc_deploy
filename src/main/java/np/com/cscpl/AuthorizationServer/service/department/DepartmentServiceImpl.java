package np.com.cscpl.AuthorizationServer.service.department;

import np.com.cscpl.AuthorizationServer.service.department.*;
import np.com.cscpl.AuthorizationServer.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import np.com.cscpl.AuthorizationServer.repository.DepartmentRepository;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public List<Department> findAll() throws Exception {
        try {
            return departmentRepository.findAll();
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public Department findOne(int id) throws Exception {
        try {
            Department department = departmentRepository.findById(id).get();
            if (department != null) {
                return department;
            }
            return null;
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Department save(Department department) throws Exception {
        try {
            return departmentRepository.save(department);
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public void update(Department department) throws Exception {
        try {
            Department departmentUpdate = departmentRepository.findById(department.getId()).get();
            if (departmentUpdate != null) {
                departmentUpdate.setDepartment(department.getDepartment());
                departmentRepository.save(departmentUpdate);
            }
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            Department department = departmentRepository.findById(id).get();
            if (department != null) {
                departmentRepository.delete(department);
            }
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

}
