package np.com.cscpl.AuthorizationServer.service.aboutus;

import np.com.cscpl.AuthorizationServer.model.AboutUs;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AboutUsService {
    List<AboutUs> getAll();
    Page<AboutUs> keySearch(String keyword , int pageNumber, int pageSize)throws Exception;
}
