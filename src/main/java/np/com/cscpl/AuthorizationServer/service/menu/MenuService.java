package np.com.cscpl.AuthorizationServer.service.menu;

import np.com.cscpl.AuthorizationServer.model.Menu;
import org.springframework.data.domain.Page;

import java.util.List;

public interface MenuService {
    List<Menu> findAll() throws Exception;
    Menu save(Menu menu) throws Exception;
    Page<Menu> menuSearch(int pageNumber, int pageSize, String search) throws Exception;
}
