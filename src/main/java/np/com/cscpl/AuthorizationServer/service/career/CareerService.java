package np.com.cscpl.AuthorizationServer.service.career;

import np.com.cscpl.AuthorizationServer.model.Career;

public interface CareerService {
    Career sendEmail(Career career) throws Exception;
}
