package np.com.cscpl.AuthorizationServer.service.hrUser;

import java.util.ArrayList;
import java.util.List;
import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.FinalHrUser;
import np.com.cscpl.AuthorizationServer.model.FinalHrUserDegree;
import np.com.cscpl.AuthorizationServer.model.HrSector;
import np.com.cscpl.AuthorizationServer.repository.HrRepository;
import np.com.cscpl.AuthorizationServer.repository.HrSectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class HrUserServiceImpl implements HrUserService {

    @Autowired
    private HrRepository hr;

    @Autowired
    private HrSectorRepository hrSectorRepository;

    @Override
    public List<FinalHrUser> findAll() throws Exception {
        try {
            return hr.findAll();

        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<FinalHrUser> search(int pageNumber, int pageSize, Specification spec) throws Exception {
        try {
            return hr.findAll(spec, GetPageable.createPageRequest(pageNumber, pageSize));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public FinalHrUser save(FinalHrUser fhu) throws Exception {
        try {
            // for mapping user degree many to many
            List<FinalHrUserDegree> fhudrds = fhu.getFinalHrUserDegrees();
            FinalHrUser finalHrUser = fhu;
            finalHrUser.setFinalHrUserDegrees(new ArrayList<>());
            fhudrds.forEach(fhud -> {
                finalHrUser.addFinalHrUserDegree(fhud);
            });

            fhu.setFinalHrUserDegrees(finalHrUser.getFinalHrUserDegrees());
            // end mapping

            // for mapping main sector many to many
            List<HrSector> hrSectors = fhu.getHrSectors();
            finalHrUser.setHrSectors(new ArrayList<>());
            hrSectors.forEach(Sector -> {
                finalHrUser.addSector(Sector);
            });

            fhu.setHrSectors(finalHrUser.getHrSectors());
            // end mapping   

            // for mapping sub sector many to many
            List<HrSector> hrSubSectors = fhu.getHrSubSector();
            finalHrUser.setHrSubSector(new ArrayList<>());
            hrSubSectors.forEach(subSector -> {
                finalHrUser.addSubSector(subSector);
            });

            fhu.setHrSubSector(finalHrUser.getHrSubSector());
            // end mapping    

            // for mapping other sector many to many
            List<HrSector> hrOtherSectors = fhu.getHrOtherSectors();
            finalHrUser.setHrOtherSectors(new ArrayList<>());
            hrOtherSectors.forEach(otherSector -> {
                finalHrUser.addOtherSector(otherSector);
            });

            fhu.setHrOtherSectors(finalHrUser.getHrOtherSectors());
            // end mapping  

            return hr.save(fhu);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(int id, FinalHrUser fhu) throws Exception {
        try {
            FinalHrUser _fhuToBeUpdated = hr.findById(id).get();
            if (_fhuToBeUpdated != null) {

                _fhuToBeUpdated.setFirstName(fhu.getFirstName());
                _fhuToBeUpdated.setMiddleName(fhu.getMiddleName());
                _fhuToBeUpdated.setLastName(fhu.getLastName());
                _fhuToBeUpdated.setPhoneNumber(fhu.getPhoneNumber());
                _fhuToBeUpdated.setEmail(fhu.getEmail());
                _fhuToBeUpdated.setJoinDate(fhu.getJoinDate());
                _fhuToBeUpdated.setDob(fhu.getDob());
                _fhuToBeUpdated.setAddress(fhu.getAddress());
                _fhuToBeUpdated.setIsConsultant(fhu.isIsConsultant());
                _fhuToBeUpdated.setConsultantType(fhu.getConsultantType());
                _fhuToBeUpdated.setFilePath(fhu.getFilePath());
                _fhuToBeUpdated.setDistrict(fhu.getDistrict());
                _fhuToBeUpdated.setHrSectors(fhu.getHrSectors());
                _fhuToBeUpdated.setResourceType(fhu.getResourceType());

                //related to degree
                if (_fhuToBeUpdated.getFinalHrUserDegrees() != null) {
                    _fhuToBeUpdated.getFinalHrUserDegrees().clear();
                    fhu.getFinalHrUserDegrees().forEach(newFhud -> {
                        _fhuToBeUpdated.addFinalHrUserDegree(newFhud);
                    });
                }
                //end

                 //related to sector
                List<HrSector> _hrSectors = new ArrayList<>();
                fhu.getHrSectors().forEach(sector -> {
                    _hrSectors.add(hrSectorRepository.findById(sector.getId()).get());
                });

                _fhuToBeUpdated.setHrSectors(_hrSectors);
                //end
                
                //related to sub sector
                List<HrSector> _hrSubSectors = new ArrayList<>();
                fhu.getHrSubSector().forEach(subSector -> {
                    _hrSubSectors.add(hrSectorRepository.findById(subSector.getId()).get());
                });

                _fhuToBeUpdated.setHrSubSector(_hrSubSectors);
                //end

                //related to other sector                
                List<HrSector> _hrOtherSectors = new ArrayList<>();
                fhu.getHrOtherSectors().forEach(otherSector -> {
                    _hrOtherSectors.add(hrSectorRepository.findById(otherSector.getId()).get());
                });

                _fhuToBeUpdated.setHrOtherSectors(_hrOtherSectors);
                //end

                hr.save(_fhuToBeUpdated);
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            FinalHrUser fhu = hr.findById(id).get();
            if (fhu != null) {
                hr.delete(fhu);
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public FinalHrUser findOne(int id) throws Exception {
        try {
            FinalHrUser fhu = hr.findById(id).get();
            if (fhu != null) {
                return fhu;
            }
            return null;
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

}
