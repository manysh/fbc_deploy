package np.com.cscpl.AuthorizationServer.service.department;

import java.util.List;
import np.com.cscpl.AuthorizationServer.model.Department;

public interface DepartmentService {

    public List<Department> findAll() throws Exception;

    Department findOne(int id) throws Exception;

    Department save(Department department) throws Exception;

    void update(Department department) throws Exception;

    void delete(int id) throws Exception;
}
