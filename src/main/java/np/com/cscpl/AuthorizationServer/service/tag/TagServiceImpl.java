package np.com.cscpl.AuthorizationServer.service.tag;

import np.com.cscpl.AuthorizationServer.service.tag.*;
import np.com.cscpl.AuthorizationServer.model.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import javax.transaction.Transactional;
import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.repository.TagRepository;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@Import(GetPageable.class)
@Transactional
public class TagServiceImpl implements TagService {

    @Autowired
    private TagRepository tagRepository;

    @Override
    public Page<Tag> findAll(int pageNumber, int pageSize) throws Exception {
        try {
            return tagRepository.findAll(GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Tag findOne(int id) throws Exception {
        try {
            Tag tag = tagRepository.findById(id).get();
            if (tag != null) {
                return tag;
            }
            return null;
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Tag save(Tag tag) throws Exception {
        try {
            return tagRepository.save(tag);
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public void update(Tag tag) throws Exception {
        try {
            Tag tagToBeUpdated = tagRepository.findById(tag.getId()).get();
            if (tagToBeUpdated != null) {

                tagToBeUpdated.setTag(tag.getTag());

                tagRepository.save(tagToBeUpdated);
            }
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            Tag tag = tagRepository.findById(id).get();
            if (tag != null) {
                tagRepository.delete(tag);
            }
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public List<Tag> findByProjectId(int id) throws Exception {
        try {
            return tagRepository.findByFinalProjects_id(id);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<Tag> findByIdIn(List<Integer> ids) throws Exception {
        try {
            return tagRepository.findByIdIn(ids);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<Tag> findAll() throws Exception {
        try {
            return tagRepository.findAll();
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<Tag> findByProjectIds(List<Integer> projectIds) throws Exception {
        try {
            return tagRepository.getTagByProjectIds(projectIds);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public Page<Tag> getFilteredResult(String search, int pageNumber, int pageSize) throws Exception {
        try {
            return tagRepository.findByTagIgnoreCaseContaining(search,GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<Tag>(new ArrayList<>());
        }
    }

    @Override
    public Tag updateTag(int id, Tag tag) throws Exception {
        try {
            Tag databaseTag = this.findOne(id);
            if(databaseTag==null || databaseTag==new Tag()){
                throw new NullPointerException("Not Found");
            }
            databaseTag.setTag(tag.getTag());
            return tagRepository.save(databaseTag);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }
}
