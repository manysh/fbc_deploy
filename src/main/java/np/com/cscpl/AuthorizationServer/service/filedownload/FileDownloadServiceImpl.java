package np.com.cscpl.AuthorizationServer.service.filedownload;

import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.FileDownload;
import np.com.cscpl.AuthorizationServer.repository.filedownlaod.FileDownloadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Import(GetPageable.class)
@Transactional
public class FileDownloadServiceImpl implements FileDownloadService {

    private FileDownloadRepository fileDownloadRepository;

    @Autowired
    public FileDownloadServiceImpl(FileDownloadRepository fileDownloadRepository) {
        this.fileDownloadRepository = fileDownloadRepository;
    }

    @Override
    public Page<FileDownload> filteredResult(int pageNumber, int pageSize, String fileName) throws Exception {
        try {
            return fileDownloadRepository.findByFileNameIgnoreCaseContainingOrderByCreatedAtDesc(
                    fileName,GetPageable.createPageRequest(
                            pageNumber,
                            pageSize
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public FileDownload save(FileDownload fileDownload) throws Exception {
        try {
            return fileDownloadRepository.save(fileDownload);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public FileDownload findById(int id) throws Exception {
        try {
            return fileDownloadRepository.findById(id);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public FileDownload update(int id, FileDownload fileDownload) throws Exception {
        try {
            FileDownload toBeUpdated=fileDownloadRepository.findById(id);
            if(toBeUpdated==null || toBeUpdated==new FileDownload()){
                throw new NullPointerException("Data not found.");
            }
            fileDownload.setId(toBeUpdated.getId());
            return fileDownloadRepository.save(fileDownload);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }
}
