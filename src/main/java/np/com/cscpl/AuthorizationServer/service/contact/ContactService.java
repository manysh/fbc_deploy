package np.com.cscpl.AuthorizationServer.service.contact;

import np.com.cscpl.AuthorizationServer.model.Contact;
import org.springframework.data.domain.Page;

public interface ContactService {
    Page<Contact> findAll(int pageNumber, int pageSize) throws Exception;
    Contact saveContact(Contact contact) throws  Exception;
   Contact sendEmail(Contact contact) throws Exception;
}
