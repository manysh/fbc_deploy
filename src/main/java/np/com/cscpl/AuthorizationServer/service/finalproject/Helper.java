package np.com.cscpl.AuthorizationServer.service.finalproject;

import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.dto.finalproject.ProjectSearchDto;
import np.com.cscpl.AuthorizationServer.model.FinalProject;
import np.com.cscpl.AuthorizationServer.repository.finalproject.FinalProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Helper {
    private FinalProjectRepository finalProjectRepository;

    @Autowired
    public Helper(FinalProjectRepository finalProjectRepository) {
        this.finalProjectRepository = finalProjectRepository;
    }

    public Page<FinalProject> searchProject(ProjectSearchDto projectSearchDto) throws Exception{
        try {
            List<Integer> sector2Ids=projectSearchDto.getSector2Ids();
            List<Integer> sector3Ids=projectSearchDto.getSector3Ids();

            boolean isSec2Null=(sector2Ids==null || sector2Ids.size()<=0)?true:false;
            boolean isSec3Null = (sector3Ids==null || sector3Ids.size()<=0)?true:false;

            if(!isSec2Null && !isSec3Null){
                return finalProjectRepository.extensiveSearch(
                        projectSearchDto.getTitle(),
                        projectSearchDto.getTagIds(),
                        projectSearchDto.getAgencyIDs(),
                        projectSearchDto.getClient1Id(),
                        projectSearchDto.getClient2Id(),
                        projectSearchDto.getSectorId(),
                        projectSearchDto.getSector2Ids(),
                        projectSearchDto.getSector3Ids(),
                        projectSearchDto.getDistrictIds(),
                        projectSearchDto.getFromDate(),
                        projectSearchDto.getToDate(),
                        projectSearchDto.getProjectStatus(),
                        projectSearchDto.getShortListed(),
                        GetPageable.createPageRequest(
                                projectSearchDto.getPageNumber(),
                                projectSearchDto.getPageSize()
                        )
                );
            }

            else if(isSec2Null && !isSec3Null){
                return finalProjectRepository.extensiveSearchSec2IsNull(
                        projectSearchDto.getTitle(),
                        projectSearchDto.getTagIds(),
                        projectSearchDto.getAgencyIDs(),
                        projectSearchDto.getClient1Id(),
                        projectSearchDto.getClient2Id(),
                        projectSearchDto.getSectorId(),
                        projectSearchDto.getSector3Ids(),
                        projectSearchDto.getDistrictIds(),
                        projectSearchDto.getFromDate(),
                        projectSearchDto.getToDate(),
                        projectSearchDto.getProjectStatus(),
                        projectSearchDto.getShortListed(),
                        GetPageable.createPageRequest(
                                projectSearchDto.getPageNumber(),
                                projectSearchDto.getPageSize()
                        )
                );
            }

            else if(!isSec2Null && isSec3Null){
                return finalProjectRepository.extensiveSearchSec3IsNull(
                        projectSearchDto.getTitle(),
                        projectSearchDto.getTagIds(),
                        projectSearchDto.getAgencyIDs(),
                        projectSearchDto.getClient1Id(),
                        projectSearchDto.getClient2Id(),
                        projectSearchDto.getSectorId(),
                        projectSearchDto.getSector2Ids(),
                        projectSearchDto.getDistrictIds(),
                        projectSearchDto.getFromDate(),
                        projectSearchDto.getToDate(),
                        projectSearchDto.getProjectStatus(),
                        projectSearchDto.getShortListed(),
                        GetPageable.createPageRequest(
                                projectSearchDto.getPageNumber(),
                                projectSearchDto.getPageSize()
                        )
                );
            }

            else{
                return finalProjectRepository.extensiveSearchSec3AndSec2IsNull(
                        projectSearchDto.getTitle(),
                        projectSearchDto.getTagIds(),
                        projectSearchDto.getAgencyIDs(),
                        projectSearchDto.getClient1Id(),
                        projectSearchDto.getClient2Id(),
                        projectSearchDto.getSectorId(),
                        projectSearchDto.getDistrictIds(),
                        projectSearchDto.getFromDate(),
                        projectSearchDto.getToDate(),
                        projectSearchDto.getProjectStatus(),
                        projectSearchDto.getShortListed(),
                        GetPageable.createPageRequest(
                                projectSearchDto.getPageNumber(),
                                projectSearchDto.getPageSize()
                        )
                );
            }

        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<FinalProject>(new ArrayList<>());
        }
    }
}
