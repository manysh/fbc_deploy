package np.com.cscpl.AuthorizationServer.service.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.Client;
import np.com.cscpl.AuthorizationServer.model.Project;
import np.com.cscpl.AuthorizationServer.model.Sector;
import np.com.cscpl.AuthorizationServer.repository.ProjectRepository;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;

@Service
@Import(GetPageable.class)
@Transactional
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Page<Project> findAll(int pageNumber, int pageSize) throws Exception {
        try {
            return projectRepository.findAll(GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Project findOne(int id) throws Exception {
        try {
            Project project = projectRepository.findById(id).get();
            if (project != null) {
                return project;
            }
            return null;
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Project save(Project project) throws Exception {
        try {
            return projectRepository.save(project);
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public void update(Project project) throws Exception {
        try {
            Project projectUpdate = projectRepository.findById(project.getId()).get();
            if (projectUpdate != null) {

                projectUpdate.setCreatedAt(project.getCreatedAt());
                projectUpdate.setDeletedAt(project.getDeletedAt());
                projectUpdate.setDescription(project.getDescription());
                projectUpdate.setEstimatedCompletionDate(project.getEstimatedCompletionDate());
                projectUpdate.setInitiatedDate(project.getInitiatedDate());
                projectUpdate.setName(project.getName());
                projectUpdate.setStatus(project.getStatus());
                projectUpdate.setUpdatedAt(new Date());
                projectUpdate.setSector(new Sector(project.getSector().getId()));

                projectRepository.save(projectUpdate);
            }
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            Project project = projectRepository.findById(id).get();
            if (project != null) {
                projectRepository.delete(project);
            }
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<Project> findAllBySector(String sector, int pageNumber, int pageSize) throws Exception {
        try {
            return projectRepository.findBySector_NameIgnoreCaseContainingOrderByCreatedAtDesc(sector, GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<Project> findAllByStatus(String status, int pageNumber, int pageSize) throws Exception {
        try {
            return projectRepository.findByStatusIgnoreCaseContainingOrderByCreatedAtDesc(status, GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<Project> findAllByTag(String tag, int pageNumber, int pageSize) throws Exception {
        try {
            return projectRepository.findByTags_TagIgnoreCaseContainingOrderByCreatedAtDesc(tag, GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

}
