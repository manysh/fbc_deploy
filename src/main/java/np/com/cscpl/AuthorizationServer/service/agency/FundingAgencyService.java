package np.com.cscpl.AuthorizationServer.service.agency;

import np.com.cscpl.AuthorizationServer.model.FundingAgency;
import org.springframework.data.domain.Page;

import java.util.List;

public interface FundingAgencyService {
    List<FundingAgency> findAll() throws Exception;
    List<FundingAgency> findByProjectId(int id) throws Exception;
    FundingAgency create(FundingAgency fundingAgency) throws Exception;
    Page<FundingAgency> filteredResult(int pageNumber, int pageSize, String search) throws Exception;
    FundingAgency findById(int id) throws Exception;
    FundingAgency update(int id, FundingAgency fundingAgency) throws Exception;
}
