package np.com.cscpl.AuthorizationServer.service.eventType;

import javax.transaction.Transactional;
import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.EventType;
import np.com.cscpl.AuthorizationServer.repository.EventTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@Import(GetPageable.class)
@Transactional
public class EventTypeServiceImpl implements EventTypeService {

    private final EventTypeRepository eventTypeRepository;

    @Autowired
    public EventTypeServiceImpl(EventTypeRepository eventTypeRepository) {
        this.eventTypeRepository = eventTypeRepository;
    }

    @Override
    public Page<EventType> findAll(int pageNumber, int pageSize) throws Exception {
        try {
            return eventTypeRepository.findAll(GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public EventType findOne(int eventTypeId) throws Exception {
        try {
            EventType eventType = eventTypeRepository.findById(eventTypeId).get();
            if (eventType != null) {
                return eventType;
            }
            return null;
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public EventType save(EventType eventType) throws Exception {
        try {
            return eventTypeRepository.save(eventType);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(int id, EventType eventType) throws Exception {
        try {
            EventType eventTypeUpdate = eventTypeRepository.findById(id).get();
            if (eventTypeUpdate != null) {                
                eventTypeUpdate.setType(eventType.getType());

                eventTypeRepository.save(eventTypeUpdate);
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            EventType eventType = eventTypeRepository.findById(id).get();
            if (eventType != null) {
                eventTypeRepository.delete(eventType);
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

}
