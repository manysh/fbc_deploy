package np.com.cscpl.AuthorizationServer.service.sector;

import np.com.cscpl.AuthorizationServer.service.sector.*;
import np.com.cscpl.AuthorizationServer.model.Sector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.repository.SectorRepository;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.data.domain.PageImpl;

@Service
@Import(GetPageable.class)
@Transactional
public class SectorServiceImpl implements SectorService {

    @Autowired
    private SectorRepository sectorRepository;

    @Override
    public Page<Sector> findAll(int pageNumber, int pageSize,String name) throws Exception {
        try {
            return sectorRepository.findByNameIgnoreCaseContaining(name,GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Sector findOne(int id) throws Exception {
        try {
            Sector sector = sectorRepository.findById(id).get();
            if (sector != null) {
                return sector;
            }
            return null;
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Sector save(Sector sector) throws Exception {
        try {
            sectorRepository.updateDisplayOrder(sector.getDisplayOrder());
            return sectorRepository.save(sector);
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public void update(int id,Sector sector) throws Exception {
        try {
            Sector sectorUpdate = sectorRepository.getOne(id);
            if (sectorUpdate != null) {

                sectorUpdate.setName(sector.getName());
                sectorUpdate.setDisplayOrder(sector.getDisplayOrder());
                sectorUpdate.setDetail(sector.getDetail());

                sectorRepository.save(sectorUpdate);
            }
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            Sector sector = sectorRepository.findById(id).get();
            if (sector != null) {
                sectorRepository.delete(sector);
            }
        } catch (Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public List<Sector> findByIdIn(List<Integer> ids) throws Exception {
        try {
            return sectorRepository.findByIdIn(ids);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<Sector> findByProjectId(int projectId) throws Exception {
        try {
            return sectorRepository.findByFinalProjects_id(projectId);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public Page<Sector> keySearch(String keyword, int pageNumber, int pageSize) throws Exception {
        try {

            Page<Sector> sectors = sectorRepository.keySearch(keyword,
                    GetPageable.createPageRequest(
                            pageNumber,
                            pageSize
                    ));

            return sectors;

        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<Sector>(Arrays.asList(new Sector()));
        }
    }
}
