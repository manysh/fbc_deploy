package np.com.cscpl.AuthorizationServer.service.district;

import np.com.cscpl.AuthorizationServer.model.District;
import np.com.cscpl.AuthorizationServer.repository.DistrictRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class DistrictServiceImpl implements DistrictService {

    private DistrictRepository districtRepository;

    @Autowired
    public DistrictServiceImpl(DistrictRepository districtRepository) {
        this.districtRepository = districtRepository;
    }

    @Override
    public List<District> findByProjectId(int projectId) throws Exception {
        try {
            return districtRepository.findByFinalProjects_id(projectId);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<District> findAll() throws Exception {
        try {
            return districtRepository.findAllByOrderByDistrictNameAsc();
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }
}
