package np.com.cscpl.AuthorizationServer.service.degree;

import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.Degree;
import np.com.cscpl.AuthorizationServer.repository.Degree.DegreeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class DegreeServiceImpl implements  DegreeService {

    private final DegreeRepository degreeRepository; 

    @Autowired
    public DegreeServiceImpl(DegreeRepository degreeRepository) {
        this.degreeRepository = degreeRepository;
    }

    @Override
    public Page<Degree> findAll(int pageNumber, int pageSize, String name) throws Exception {
      try {
          return degreeRepository.findByNameIgnoreCaseContaining(name, GetPageable.createPageRequest(pageNumber,pageSize));
      }catch (Exception ex){
          ex.getLocalizedMessage();
          throw new IllegalArgumentException(ex.getLocalizedMessage());
      }
    }

    @Override
    public Degree save(Degree degree) throws Exception {
       try {
           return  degreeRepository.save(degree);
       }catch (Exception ex){
           ex.getLocalizedMessage();
           throw new IllegalArgumentException(ex.getLocalizedMessage());
       }
    }

    @Override
    public Degree getById(int id) throws Exception {
        try {
            return degreeRepository.getOne(id);
        }catch (Exception ex){
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Degree update(int id, Degree degree) {
        try{
            Degree degreeUpdate = degreeRepository.getOne(id);

            if(degreeUpdate == null || degreeUpdate==new Degree()){
                throw new NullPointerException("Data not found.");
            }
            degree.setId(degreeUpdate.getId());
            return degreeRepository.save(degree);
        }catch(NullPointerException ex){
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }
  
}
