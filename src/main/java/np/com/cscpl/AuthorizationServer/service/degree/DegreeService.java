package np.com.cscpl.AuthorizationServer.service.degree;

import np.com.cscpl.AuthorizationServer.model.Degree;
import org.springframework.data.domain.Page;


public interface DegreeService {

    Page<Degree> findAll(int pageNumber, int pageSize, String name) throws Exception;

    Degree save(Degree degree) throws Exception;

    Degree getById(int id) throws Exception;

    Degree update(int id, Degree degree);   
}
