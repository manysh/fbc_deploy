package np.com.cscpl.AuthorizationServer.service.finalproject;

import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.converter.finalproject.FinalProjectPageListMap;
import np.com.cscpl.AuthorizationServer.dto.finalproject.ProjectSearchDto;
import np.com.cscpl.AuthorizationServer.model.FinalProject;
import np.com.cscpl.AuthorizationServer.repository.DistrictRepository;
import np.com.cscpl.AuthorizationServer.repository.SectorRepository;
import np.com.cscpl.AuthorizationServer.repository.TagRepository;
import np.com.cscpl.AuthorizationServer.repository.agency.FundingAgencyRepository;
import np.com.cscpl.AuthorizationServer.repository.finalproject.FinalProjectRepository;
import np.com.cscpl.AuthorizationServer.repository.finalproject.ProjectDisplayRepository;
import np.com.cscpl.AuthorizationServer.repository.sector2.Sector2repository;
import np.com.cscpl.AuthorizationServer.repository.sector3.Sector3Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;

@Service
public class FinalProjectServiceImpl implements FinalProjectService {

    private FinalProjectRepository finalProjectRepository;
    private TagRepository tagRepository;
    private SectorRepository sectorRepository;
    private Sector2repository sector2repository;
    private Sector3Repository sector3Repository;
    private DistrictRepository districtRepository;
    private FundingAgencyRepository fundingAgencyRepository;
    private ProjectDisplayRepository projectDisplayRepository;
    private Helper helper;

    @Autowired
    public FinalProjectServiceImpl(
            FinalProjectRepository finalProjectRepository,
            ProjectDisplayRepository projectDisplayRepository,
            TagRepository tagRepository,
            SectorRepository sectorRepository,
            Sector2repository sector2repository,
            Sector3Repository sector3Repository,
            DistrictRepository districtRepository,
            FundingAgencyRepository fundingAgencyRepository,
            Helper helper
    ) {
        this.finalProjectRepository = finalProjectRepository;
        this.tagRepository = tagRepository;
        this.sectorRepository = sectorRepository;
        this.sector2repository = sector2repository;
        this.sector3Repository = sector3Repository;
        this.districtRepository = districtRepository;
        this.fundingAgencyRepository = fundingAgencyRepository;
        this.projectDisplayRepository=projectDisplayRepository;
        this.helper = helper;
    }

    @Override
    public FinalProject create(FinalProject finalProject) throws Exception {
        try {
            return finalProjectRepository.save(finalProject);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    /*
    MASTER SEARH OF PROJECT LIST
     */
    @Override
    public Page<?> extensiveSerch(
            ProjectSearchDto projectSearchDto) {
        try {

            Page<FinalProject> finalProjects = helper.searchProject(validate(projectSearchDto));

            return FinalProjectPageListMap.unboundedMap(finalProjects);

        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<FinalProject>(Arrays.asList(new FinalProject()));
        }
    }


    private ProjectSearchDto validate(ProjectSearchDto projectSearchDto) throws Exception{
        projectSearchDto.setTitle(projectSearchDto.getTitle() == null ? "" : projectSearchDto.getTitle());
        projectSearchDto.setTagIds(projectSearchDto.getTagIds() == null ? tagRepository.getAllTagIds() : projectSearchDto.getTagIds());
        projectSearchDto.setAgencyIDs(projectSearchDto.getAgencyIDs() == null ? fundingAgencyRepository.fidAllIds() : projectSearchDto.getAgencyIDs());
        projectSearchDto.setDistrictIds(projectSearchDto.getDistrictIds() == null ? districtRepository.fidAllIds() : projectSearchDto.getDistrictIds());
        projectSearchDto.setProjectStatus(projectSearchDto.getProjectStatus() == null ? "" : projectSearchDto.getProjectStatus());
        return projectSearchDto;
    }

    @Override
    public FinalProject findById(int id) throws Exception {
        try {
            return finalProjectRepository.findById(id);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public List<FinalProject> homepageList(String projectStatus) throws Exception {
        try {
            return  projectDisplayRepository.findByStatus(
                    projectStatus,
                    GetPageable.createPageRequest(
                            1,
                            4
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return  new ArrayList<>();
        }
    }

    @Override
    public Page<FinalProject> keySearch(String keyword, int pageNumber, int pageSize) throws Exception {
        try {

            Page<FinalProject> finalProjects = finalProjectRepository.keySearch(keyword,
                    GetPageable.createPageRequest(
                            pageNumber,
                            pageSize
                    ));

            return finalProjects;

        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<FinalProject>(Arrays.asList(new FinalProject()));
        }
    }

    @Override
    public List<FinalProject> downloadProject(String projectStatus) throws Exception {
        try {
            return finalProjectRepository.findAllByStatus(projectStatus);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public Page<FinalProject> search(int pageNumber, int pageSize, Specification spec) throws Exception {
        try {
            return finalProjectRepository.findAll(spec, GetPageable.createPageRequest(pageNumber, pageSize));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }
}
