package np.com.cscpl.AuthorizationServer.service.eventimage;

import javax.transaction.Transactional;
import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.EventImage;
import np.com.cscpl.AuthorizationServer.repository.EventImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@Import(GetPageable.class)
@Transactional
public class EventImageServiceImpl implements EventImageService {

    private final EventImageRepository eventImageRepository;

    @Autowired
    public EventImageServiceImpl(EventImageRepository eventImageRepository) {
        this.eventImageRepository = eventImageRepository;
    }

    @Override
    public Page<EventImage> findAll(int eventid, int pageNumber, int pageSize) throws Exception {
        try {
            return eventImageRepository.findAll(eventid, GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public EventImage findOne(int id) throws Exception {
        try {
            EventImage eventImage = eventImageRepository.findById(id).get();
            if (eventImage != null) {
                return eventImage;
            }
            return null;
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public EventImage save(EventImage eventImage) throws Exception {
        try {
            return eventImageRepository.save(eventImage);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(int id, EventImage eventImage) throws Exception {
        try {
            EventImage eventImageUpdate = eventImageRepository.findById(id).get();
            if (eventImage != null) {
                eventImageUpdate.setImagePath(eventImage.getImagePath());
                eventImageUpdate.setCaption(eventImage.getCaption());

                eventImageRepository.save(eventImageUpdate);
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            EventImage eventImage = eventImageRepository.findById(id).get();
            if (eventImage != null) {
                eventImageRepository.delete(eventImage);
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

}
