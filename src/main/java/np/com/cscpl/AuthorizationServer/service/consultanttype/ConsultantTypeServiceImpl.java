package np.com.cscpl.AuthorizationServer.service.consultanttype;

import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.ConsultantType;
import np.com.cscpl.AuthorizationServer.repository.consultanttype.ConsultantTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Import(GetPageable.class)
@Transactional
public class ConsultantTypeServiceImpl implements  ConsultantTypeService {

    @Autowired
    private ConsultantTypeRepository consultantTypeRepository;


    @Override
    public Page<ConsultantType> findAll(int pageNumber,int pageSize, String name) {
        try{
            return consultantTypeRepository.findByConsultantTypeNameIgnoreCaseContaining(name,GetPageable.createPageRequest(pageNumber,pageSize));
        }catch(Exception ex){
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public ConsultantType save(ConsultantType consultantType) throws Exception {
        try {
            return consultantTypeRepository.save(consultantType);
        }catch(Exception ex){
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public ConsultantType update(int id, ConsultantType consultantType) throws Exception {
        try{
            ConsultantType consultantType1 = consultantTypeRepository.getOne(id);


            if(consultantType1==null || consultantType1==new ConsultantType()){
                throw new NullPointerException("Data not found.");
            }
           consultantType.setId(consultantType1.getId());
            return consultantTypeRepository.save(consultantType);
        }catch(Exception ex){
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public ConsultantType getById(int id) throws Exception {
        try {
            return consultantTypeRepository.getOne(id);

        }catch(Exception ex) {
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public List<ConsultantType> findAll() throws Exception {
        try {
            return consultantTypeRepository.findAll();
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }
}
