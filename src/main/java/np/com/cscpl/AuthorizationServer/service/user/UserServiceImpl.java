package np.com.cscpl.AuthorizationServer.service.user;

import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.User;
import np.com.cscpl.AuthorizationServer.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(User user) throws Exception {
        try {
            return userRepository.save(user);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public User findByUserName(String username) throws Exception {
        try {
            return userRepository.findByUsername(username);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public User findById(long id) throws Exception {
        try {
            return userRepository.findById(id);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<User> findAll(int pageNumber, int pageSize, String name) throws Exception {
        try {
            return userRepository.findByFullNameIgnoreCaseContaining(name, GetPageable.createPageRequest(pageNumber, pageSize));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public User update(Long id, User user) throws Exception {
        try {
            User fhuUpdate = userRepository.findById(id).get();
            if (fhuUpdate != null)

                fhuUpdate.setFullName(user.getFullName());
                fhuUpdate.setUsername(user.getUsername());
                fhuUpdate.setPassword(user.getPassword());
                fhuUpdate.setAuthorities(user.getAuthorities());

                return userRepository.save(fhuUpdate);

        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

}
