package np.com.cscpl.AuthorizationServer.service.hrSector;

import np.com.cscpl.AuthorizationServer.service.sector.*;
import java.util.List;
import java.util.Optional;
import np.com.cscpl.AuthorizationServer.model.HrSector;
import np.com.cscpl.AuthorizationServer.model.Sector;
import org.springframework.data.domain.Page;

public interface HrSectorService {

    Page<HrSector> findAll(int pageNumber, int pageSize) throws Exception;
    
    Page<HrSector> searchSector(String keyword, int pageNumber, int pageSize) throws Exception;
    
    Page<HrSector> searchSubSector(String keyword, int pageNumber, int pageSize) throws Exception;
    
    Page<HrSector> searchOtherSector(String keyword, int pageNumber, int pageSize) throws Exception;

    HrSector findOne(int id) throws Exception;

    HrSector save(HrSector hrSector) throws Exception;

    void update(int id, HrSector hrSector) throws Exception;

    void delete(int id) throws Exception;          
    
    Page<HrSector> IsOther(int pageNumber, int pageSize) throws Exception;
    
    Page<HrSector> IsSub(int pageNumber, int pageSize) throws Exception;
}
