package np.com.cscpl.AuthorizationServer.service.user;

import np.com.cscpl.AuthorizationServer.model.User;
import org.springframework.data.domain.Page;

public interface UserService {

    User create(User user) throws Exception;

    User findByUserName(String username) throws Exception;

    User findById(long id) throws Exception;

    Page<User> findAll(int pageNumber, int pageSize, String name) throws Exception;

    User update(Long id, User user) throws Exception;
       
}
