package np.com.cscpl.AuthorizationServer.service.district;

import np.com.cscpl.AuthorizationServer.model.District;

import java.util.List;

public interface DistrictService {
    List<District> findByProjectId(int projectId) throws Exception;
    List<District> findAll() throws Exception;
}
