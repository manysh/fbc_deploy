package np.com.cscpl.AuthorizationServer.service.hrSector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import np.com.cscpl.AuthorizationServer.common.GetPageable;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;

import np.com.cscpl.AuthorizationServer.model.HrSector;
import np.com.cscpl.AuthorizationServer.repository.HrSectorRepository;

@Service
@Import(GetPageable.class)
@Transactional
public class HrSectorServiceImpl implements HrSectorService {

    @Autowired
    private HrSectorRepository hrSectorRepository;

    @Override
    public Page<HrSector> findAll(int pageNumber, int pageSize) throws Exception {
        try {
            return hrSectorRepository.findByIsOtherFalseAndIsSubFalse(GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }
    
    
    @Override
    public Page<HrSector> searchSector(String keyword, int pageNumber, int pageSize) throws Exception {
        try {
            return hrSectorRepository.searchSector(keyword,GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    
    
    @Override
    public Page<HrSector> searchSubSector(String keyword, int pageNumber, int pageSize) throws Exception {
        try {
            return hrSectorRepository.searchSubSector(keyword,GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    
    
    @Override
    public Page<HrSector> searchOtherSector(String keyword, int pageNumber, int pageSize) throws Exception {
        try {
            return hrSectorRepository.searchOtherSector(keyword,GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }


    @Override
    public HrSector findOne(int id) throws Exception {
        try {
            HrSector hrSector = hrSectorRepository.findById(id).get();
            if (hrSector != null) {
                return hrSector;
            }
            return null;
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public HrSector save(HrSector hrSector) throws Exception {
        try {
            return hrSectorRepository.save(hrSector);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(int id, HrSector hrSector) throws Exception {
        try {
            HrSector hrSectorUpdate = hrSectorRepository.findById(id).get();
            if (hrSectorUpdate != null) {

                hrSectorUpdate.setName(hrSector.getName());
                hrSectorUpdate.setDetail(hrSector.getDetail());
                hrSectorUpdate.setIsOther(hrSector.isIsOther());
                hrSectorUpdate.setIsSub(hrSector.isIsSub());

                hrSectorRepository.save(hrSectorUpdate);
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            HrSector hrSector = hrSectorRepository.findById(id).get();
            if (hrSector != null) {
                hrSectorRepository.delete(hrSector);
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<HrSector> IsOther(int pageNumber, int pageSize) throws Exception {
        try {
            return hrSectorRepository.findByIsOtherTrue(GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<HrSector> IsSub(int pageNumber, int pageSize) throws Exception {
        try {
            return hrSectorRepository.findByIsSubTrue(GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

}
