package np.com.cscpl.AuthorizationServer.service.career;

import np.com.cscpl.AuthorizationServer.model.Career;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.UrlResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;



@Service
public class CareerServiceImpl implements CareerService {

    private JavaMailSender sender;

    @Autowired
    private Environment env;


    @Autowired
    public void setMailSender(JavaMailSender mailSender) {
        this.sender = mailSender;
    }
    @Override
    public Career sendEmail(Career career) throws Exception {
        MimeMessage message = sender.createMimeMessage();


        try {


            MimeMessageHelper helper = new MimeMessageHelper(message,true);
            helper.setFrom(career.getEmail());
            helper.setTo("vacancy@fbc.com.np");
            helper.setText("Cover Letter is : " + System.lineSeparator()+
                    career.getMessage());
            helper.setSubject("This Message is sent By "+ career.getFullName()+ " whose email address is " + career.getEmail() + " and contact No is " + career.getContactNo() + " . This candidate's qualification is " +career.getQualification() + " and the subject is  " +career.getSubject());

         helper.addAttachment("CV.pdf",new UrlResource(env.getProperty("file_path")+career.getCvLink()));

        } catch (MessagingException e) {
            e.printStackTrace();
            throw new IllegalArgumentException(e.getLocalizedMessage());

        }
        sender.send(message);
        return career;
    }
}
