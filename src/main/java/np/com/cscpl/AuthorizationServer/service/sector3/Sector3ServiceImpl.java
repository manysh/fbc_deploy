package np.com.cscpl.AuthorizationServer.service.sector3;

import javassist.NotFoundException;
import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.Sector3;
import np.com.cscpl.AuthorizationServer.repository.sector3.Sector3Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class Sector3ServiceImpl implements Sector3Service {

    private Sector3Repository sector3Repository;

    @Autowired
    public Sector3ServiceImpl(Sector3Repository sector3Repository) {
        this.sector3Repository = sector3Repository;
    }

    @Override
    public List<Sector3> findAll() throws Exception {
        try {
            return sector3Repository.findAllByOrderByTitleAsc();
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<Sector3> findByProjectId(int id) throws Exception {
        try {
            return sector3Repository.findByFinalProjects_id(id);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<Sector3> findByIdIn(List<Integer> ids) throws Exception {
        try {
            return sector3Repository.findByIdIn(ids);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public Sector3 create(Sector3 sector3) throws Exception {
        try {
            return sector3Repository.save(sector3);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<Sector3> filteredResult(int pageNumber, int pageSize, String search) throws Exception {
        try {
            return sector3Repository.findByTitleIgnoreCaseContaining(search, GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<Sector3>(new ArrayList<>());
        }
    }

    @Override
    public Sector3 findById(int id) throws Exception {
        try {
            return sector3Repository.findById(id);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Sector3 update(int id, Sector3 sector3) throws Exception {
        try {
            Sector3 databaseSector = sector3Repository.findById(id);
            if (databaseSector == null || databaseSector == new Sector3()) {
                throw new NotFoundException("Not found.");
            }
            sector3.setId(id);
            return sector3Repository.save(sector3);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NotFoundException(ex.getLocalizedMessage());
        }
    }
}
