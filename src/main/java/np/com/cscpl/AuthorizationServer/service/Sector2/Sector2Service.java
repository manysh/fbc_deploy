package np.com.cscpl.AuthorizationServer.service.Sector2;

import np.com.cscpl.AuthorizationServer.model.Sector2;
import org.springframework.data.domain.Page;

import java.util.List;

public interface Sector2Service {
    List<Sector2> findAll() throws Exception;
    List<Sector2> findByProjectId(int id) throws Exception;
    List<Sector2> findByIdIn(List<Integer> ids) throws Exception;
    Sector2 create(Sector2 sector2) throws Exception;
    Page<Sector2> filteredResult(int pageNumber, int pageSize, String search) throws Exception;
    Sector2 findById(int id) throws Exception;
    Sector2 update(int id, Sector2 sector2) throws Exception;
}
