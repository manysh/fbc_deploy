package np.com.cscpl.AuthorizationServer.service.geography;

import np.com.cscpl.AuthorizationServer.model.Geography;

import java.util.List;

public interface GeographyService {
    List<Geography> findAllGeography();
}
