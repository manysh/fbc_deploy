package np.com.cscpl.AuthorizationServer.service.batchimage;

import np.com.cscpl.AuthorizationServer.model.BatchImage;

public interface BatchImageService {
    BatchImage save(BatchImage batchImage) throws Exception;
    BatchImage findOne() throws Exception;
}
