package np.com.cscpl.AuthorizationServer.service.finalproject;

import np.com.cscpl.AuthorizationServer.model.ProjectDisplay;
import np.com.cscpl.AuthorizationServer.repository.finalproject.ProjectDisplayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectDisplayServiceImpl implements ProjectDisplayService {

    private ProjectDisplayRepository projectDisplayRepository;

    @Autowired
    public ProjectDisplayServiceImpl(ProjectDisplayRepository projectDisplayRepository) {
        this.projectDisplayRepository = projectDisplayRepository;
    }

    @Override
    public List<ProjectDisplay> findByStatus(String status) throws Exception {
        try {
            return projectDisplayRepository.searchByStatus(status);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public ProjectDisplay update(ProjectDisplay projectDisplay) throws Exception {
        try {
            return projectDisplayRepository.save(projectDisplay);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public ProjectDisplay findById(int id) throws Exception {
        try {
            return projectDisplayRepository.findById(id);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }
}
