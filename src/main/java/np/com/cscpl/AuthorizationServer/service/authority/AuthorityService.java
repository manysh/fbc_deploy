package np.com.cscpl.AuthorizationServer.service.authority;

import np.com.cscpl.AuthorizationServer.model.Authority;

import java.util.List;

public interface AuthorityService {
    List<Authority> findAll()throws Exception;
}
