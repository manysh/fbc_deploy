package np.com.cscpl.AuthorizationServer.service.project;

import java.util.List;
import np.com.cscpl.AuthorizationServer.model.Project;
import org.springframework.data.domain.Page;

public interface ProjectService {

    Page<Project> findAll(int pageNumber, int pageSize) throws Exception;

    Page<Project> findAllBySector(String sector, int pageNumber, int pageSize) throws Exception;

    Page<Project> findAllByStatus(String status, int pageNumber, int pageSize) throws Exception;

    Page<Project> findAllByTag(String tag, int pageNumber, int pageSize) throws Exception;

    Project findOne(int id) throws Exception;

    Project save(Project project) throws Exception;

    void update(Project project) throws Exception;

    void delete(int id) throws Exception;
}
