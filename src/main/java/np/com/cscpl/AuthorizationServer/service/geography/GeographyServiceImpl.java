package np.com.cscpl.AuthorizationServer.service.geography;

import np.com.cscpl.AuthorizationServer.model.Geography;
import np.com.cscpl.AuthorizationServer.repository.GeographyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GeographyServiceImpl implements GeographyService {

    private GeographyRepository geographyRepository;

    @Autowired
    public GeographyServiceImpl(GeographyRepository geographyRepository) {
        this.geographyRepository = geographyRepository;
    }

    @Override
    public List<Geography> findAllGeography() {
        try {
            return geographyRepository.findAll();
        }catch(Exception e){
            System.out.println(e.getMessage());
            return new ArrayList<>();
        }

    }
}
