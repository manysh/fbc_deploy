package np.com.cscpl.AuthorizationServer.service.resourcetype;

import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.ResourceType;
import np.com.cscpl.AuthorizationServer.repository.resourcetype.ResourceTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ResourceTypeServiceImpl implements  ResourceTypeService {

    @Autowired
    private ResourceTypeRepository resourceTypeRepository;


    @Override
    public Page<ResourceType> findAll(int pageNumber,int pageSize, String name) {
        try{
            return resourceTypeRepository.findByResourceTypeNameIgnoreCaseContaining(name,GetPageable.createPageRequest(pageNumber,pageSize));
        }catch(Exception ex){
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public ResourceType save(ResourceType resourceType) throws Exception {
        try{
            return resourceTypeRepository.save(resourceType);
        }catch(Exception ex){
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public ResourceType getById(int id) throws Exception {
        try{
            return resourceTypeRepository.getOne(id);
        }catch(Exception ex){
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public ResourceType update(int id, ResourceType resourceType) {
        try{
            ResourceType resourceType1 = resourceTypeRepository.getOne(id);

            if(resourceType1 == null || resourceType1==new ResourceType()){
                throw new NullPointerException("Data not found.");
            }
            resourceType.setId(resourceType1.getId());
            return resourceTypeRepository.save(resourceType);
        }catch(Exception ex){
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public List<ResourceType> findAll() throws Exception {
        try {
            return resourceTypeRepository.findAll();
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }
}
