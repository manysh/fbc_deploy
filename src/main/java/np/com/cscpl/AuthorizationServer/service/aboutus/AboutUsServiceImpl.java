package np.com.cscpl.AuthorizationServer.service.aboutus;

import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.AboutUs;
import np.com.cscpl.AuthorizationServer.repository.aboutus.AboutUsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class AboutUsServiceImpl implements  AboutUsService {

    @Autowired
    private AboutUsRepository aboutUsRepository;

    public AboutUsServiceImpl(AboutUsRepository aboutUsRepository) {
              this.aboutUsRepository =aboutUsRepository;
    }

    @Override
    public List<AboutUs> getAll() {
      try {
          return aboutUsRepository.findAll();
      }catch (Exception ex){
          ex.getLocalizedMessage();
          throw new IllegalArgumentException(ex.getLocalizedMessage());
      }
    }

    @Override
    public Page<AboutUs> keySearch(String keyword, int pageNumber,int pageSize) throws Exception {
       try {
           return aboutUsRepository.keySearch(keyword, GetPageable.createPageRequest(pageNumber,pageSize));
       }catch(Exception ex){
           System.out.println(ex.getLocalizedMessage());
           return new PageImpl<AboutUs>(Arrays.asList(new AboutUs()));
       }
    }
}
