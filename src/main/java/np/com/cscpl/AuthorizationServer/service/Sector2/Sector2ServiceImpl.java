package np.com.cscpl.AuthorizationServer.service.Sector2;

import javassist.NotFoundException;
import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.Sector2;
import np.com.cscpl.AuthorizationServer.repository.sector2.Sector2repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class Sector2ServiceImpl implements Sector2Service{
    private Sector2repository sector2repository;

    @Autowired
    public Sector2ServiceImpl(Sector2repository sector2repository) {
        this.sector2repository = sector2repository;
    }

    @Override
    public List<Sector2> findAll() throws Exception {
        try {
            return sector2repository.findAllByOrderByTitleAsc();
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<Sector2> findByProjectId(int id) throws Exception {
        try {
            return sector2repository.findByFinalProjects_id(id);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<Sector2> findByIdIn(List<Integer> ids) throws Exception {
        try {
            return sector2repository.findByIdIn(ids);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public Sector2 create(Sector2 sector2) throws Exception {
        try {
            return sector2repository.save(sector2);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<Sector2> filteredResult(int pageNumber, int pageSize, String search) throws Exception {
        try {
            return sector2repository.findByTitleIgnoreCaseContaining(search, GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<Sector2>(null);
        }
    }

    @Override
    public Sector2 findById(int id) throws Exception {
        try {
            return sector2repository.findById(id);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Sector2 update(int id, Sector2 sector2) throws Exception {
        try {
            Sector2 databaseSector = sector2repository.findById(id);
            if(databaseSector ==null || databaseSector == new Sector2()){
                throw new NotFoundException("Not found.");
            }
            sector2.setId(id);
            return sector2repository.save(sector2);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NotFoundException(ex.getLocalizedMessage());
        }
    }
}
