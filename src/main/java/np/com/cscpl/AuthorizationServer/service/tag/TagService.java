package np.com.cscpl.AuthorizationServer.service.tag;

import np.com.cscpl.AuthorizationServer.model.Tag;
import org.springframework.data.domain.Page;

import java.util.List;

public interface TagService {

    Page<Tag> findAll(int pageNumber, int pageSize) throws Exception;

    Tag findOne(int id) throws Exception;

    Tag save(Tag tag) throws Exception;

    void update(Tag tag) throws Exception;

    void delete(int id) throws Exception;

    List<Tag> findByProjectId(int id) throws Exception;


    List<Tag> findByIdIn(List<Integer> ids) throws Exception;

    List<Tag> findAll() throws Exception;

    List<Tag> findByProjectIds(List<Integer> projectIds) throws Exception;

    Page<Tag> getFilteredResult(String search, int pageNumber,int pageSize) throws Exception;

    Tag updateTag(int id, Tag tag) throws Exception;
}
