package np.com.cscpl.AuthorizationServer.service.agency;

import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.FundingAgency;
import np.com.cscpl.AuthorizationServer.repository.agency.FundingAgencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FundingAgencyServiceImpl implements FundingAgencyService{

    private FundingAgencyRepository fundingAgencyRepository;

    @Autowired
    public FundingAgencyServiceImpl(FundingAgencyRepository fundingAgencyRepository) {
        this.fundingAgencyRepository = fundingAgencyRepository;
    }

    @Override
    public List<FundingAgency> findAll() throws Exception {
        try {
            return fundingAgencyRepository.getAll();
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<FundingAgency> findByProjectId(int id) throws Exception {
        try {
            return fundingAgencyRepository.findByFinalProjects_id(id);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public FundingAgency create(FundingAgency fundingAgency) throws Exception {
        try {
            return fundingAgencyRepository.save(fundingAgency);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<FundingAgency> filteredResult(int pageNumber, int pageSize, String search) throws Exception {
        try {
            return fundingAgencyRepository.findByNameIgnoreCaseContaining(
                    search,
                    GetPageable.createPageRequest(
                            pageNumber,
                            pageSize
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<FundingAgency>(new ArrayList<>());
        }
    }

    @Override
    public FundingAgency findById(int id) throws Exception {
        try {
            return fundingAgencyRepository.findById(id);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public FundingAgency update(int id, FundingAgency fundingAgency) throws Exception {
        try {
            FundingAgency databaseAgency = fundingAgencyRepository.findById(id);
            fundingAgency.setId(databaseAgency.getId());
            return fundingAgencyRepository.save(fundingAgency);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }
}
