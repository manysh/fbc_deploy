package np.com.cscpl.AuthorizationServer.service.menu;

import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.Menu;
import np.com.cscpl.AuthorizationServer.model.SubMenu;
import np.com.cscpl.AuthorizationServer.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuRepository menuRepository;

    @Override
    public List<Menu> findAll() throws Exception {
        try {
            return menuRepository.findAllByOrderByOrderAsc();
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public Menu save(Menu menu) throws Exception {
        try {
            List<SubMenu> subMenus= menu.getSubMenus();
            Menu menu1= menu;
            menu1.setSubMenus(new ArrayList<>());
            subMenus.forEach(subMenu -> {
                menu1.add(subMenu);
            });
            menu.setSubMenus(menu1.getSubMenus());
            Menu savedMenu=menuRepository.save(menu);
            return savedMenu;
        }catch (Exception ex){
            ex.getLocalizedMessage();
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<Menu> menuSearch(int pageNumber, int pageSize, String search) throws Exception {
        try {
            return menuRepository.menuSearch(
                    search,
                    GetPageable.createPageRequest(
                            pageNumber,
                            pageSize
                    )
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new PageImpl<Menu>(new ArrayList<>());
        }
    }
}
