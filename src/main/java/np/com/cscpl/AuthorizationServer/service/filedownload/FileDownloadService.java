package np.com.cscpl.AuthorizationServer.service.filedownload;

import np.com.cscpl.AuthorizationServer.model.FileDownload;
import org.springframework.data.domain.Page;

public interface FileDownloadService {
    Page<FileDownload> filteredResult(int pageNumber, int pageSize, String fileName) throws Exception;
    FileDownload save(FileDownload fileDownload) throws Exception;
    FileDownload findById(int id) throws Exception;
    FileDownload update(int id,FileDownload fileDownload) throws Exception;
}
