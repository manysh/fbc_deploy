package np.com.cscpl.AuthorizationServer.service.event;

import np.com.cscpl.AuthorizationServer.model.Event;
import org.springframework.data.domain.Page;

public interface EventService {

    Page<Event> findAll(int pageNumber, int pageSize) throws Exception;

    Page<Event> findAllByName(int pageNumber, int pageSize, String name) throws Exception;

    Event findOne(int id) throws Exception;

    Event save(Event sector) throws Exception;

    void update(int id,Event sector) throws Exception;

    void delete(int id) throws Exception;
    
    Page<Event> findAllByEventType(String eventType, int pageNumber, int pageSize) throws Exception;
    
    Page<Event> search(String keyword, int pageNumber, int pageSize) throws Exception;
    
    Page<Event> findByIsShowTrue(int pageNumber, int pageSize) throws Exception;
    
    Page<Event> findByEgIsShowTrue(int pageNumber, int pageSize) throws Exception;

}
