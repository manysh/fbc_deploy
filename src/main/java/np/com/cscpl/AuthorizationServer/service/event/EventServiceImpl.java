package np.com.cscpl.AuthorizationServer.service.event;

import javax.transaction.Transactional;
import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.model.Event;
import np.com.cscpl.AuthorizationServer.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@Import(GetPageable.class)
@Transactional
public class EventServiceImpl implements EventService {

    private EventRepository eventRepository;

    @Autowired
    public EventServiceImpl(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Override
    public Page<Event> findAll(int pageNumber, int pageSize) throws Exception {
        try {
            return eventRepository.findAllByOrderByEventDateDesc(GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<Event> findAllByName(int pageNumber, int pageSize, String name) throws Exception {
        try {
            return eventRepository.findByNameIgnoreCaseContaining(name, GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public Event findOne(int id) throws Exception {
        try {
            Event event = eventRepository.findById(id).get();
            if (event != null) {
                return event;
            }
            return null;
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public Event save(Event event) throws Exception {
        try {
            return eventRepository.save(event);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(int id, Event event) throws Exception {
        try {
            Event eventUpdate = eventRepository.findById(id).get();
            if (eventUpdate != null) {

                eventUpdate.setName(event.getName());
                eventUpdate.setDescription(event.getDescription());
                eventUpdate.setEventDate(event.getEventDate());
                eventUpdate.setImagePath(event.getImagePath());
                eventUpdate.setIsShow(event.getIsShow());
                eventUpdate.setEgIsShow(event.getEgIsShow());

                eventRepository.save(eventUpdate);
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            Event event = eventRepository.findById(id).get();
            if (event != null) {
                eventRepository.delete(event);
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<Event> findAllByEventType(String eventType, int pageNumber, int pageSize) throws Exception {
        try {
            return eventRepository.findByEventType_TypeIgnoreCaseContainingOrderByEventDateAsc(eventType, GetPageable.createPageRequest(
                    pageNumber,
                    pageSize
            ));
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @Override
    public Page<Event> search(String keyword, int pageNumber, int pageSize) throws Exception {
        try {
            return eventRepository.search(keyword, GetPageable.createPageRequest(pageNumber, pageSize));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<Event> findByIsShowTrue(int pageNumber, int pageSize) throws Exception {
        try {
            return eventRepository.findByIsShowTrue(GetPageable.createPageRequest(pageNumber, pageSize));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

    @Override
    public Page<Event> findByEgIsShowTrue(int pageNumber, int pageSize) throws Exception {
        try {
            return eventRepository.findByEgIsShowTrueOrderByEventDateDesc(GetPageable.createPageRequest(pageNumber, pageSize));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            throw new NullPointerException(e.getLocalizedMessage());
        }
    }

}
