package np.com.cscpl.AuthorizationServer.service.sector3;

import np.com.cscpl.AuthorizationServer.model.Sector3;
import org.springframework.data.domain.Page;

import java.util.List;

public interface Sector3Service {
    List<Sector3> findAll() throws Exception;
    List<Sector3> findByProjectId(int id) throws Exception;
    List<Sector3> findByIdIn(List<Integer> ids) throws Exception;
    Sector3 create(Sector3 sector3) throws Exception;
    Page<Sector3> filteredResult(int pageNumber, int pageSize, String search) throws Exception;
    Sector3 findById(int id) throws Exception;
    Sector3 update(int id, Sector3 sector3) throws Exception;
}
