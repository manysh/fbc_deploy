package np.com.cscpl.AuthorizationServer.service.batchimage;

import np.com.cscpl.AuthorizationServer.common.ImageUploadHelper;
import np.com.cscpl.AuthorizationServer.model.BatchImage;
import np.com.cscpl.AuthorizationServer.repository.BatchImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static np.com.cscpl.AuthorizationServer.common.ConstantData.IMAGE_FILE;

@Service
public class BatchImageServiceImpl implements BatchImageService {
    @Autowired
    private BatchImageRepository batchImageRepository;

    @Override
    public BatchImage save(BatchImage batchImage) throws Exception {
        try {
            List<BatchImage> dataBaseImages=batchImageRepository.findAll();
            BatchImage databaseImage=new BatchImage();
            if(dataBaseImages !=null && dataBaseImages.size()>0){
                databaseImage = dataBaseImages.get(0);
                if(databaseImage==null || databaseImage==new BatchImage()){
                    databaseImage= new BatchImage();
                }
                else{
                    String imagePath=databaseImage.getImageLink();
                    if(imagePath !=null && imagePath.contains("/")){
                        ImageUploadHelper.deleFile(imagePath.substring(imagePath.lastIndexOf("/")+1),IMAGE_FILE);
                    }
                }
            }
            databaseImage.setImageLink(batchImage.getImageLink());
            databaseImage.setCreatedDate(LocalDateTime.now().toString());
            return batchImageRepository.save(databaseImage);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @Override
    public BatchImage findOne() throws Exception {
        try {
            BatchImage batchImage = batchImageRepository.findFirst();
            return batchImageRepository.findFirst();
        }catch (Exception Ex){
            System.out.println(Ex.getLocalizedMessage());
            return null;
        }
    }
}
