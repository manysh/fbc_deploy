package np.com.cscpl.AuthorizationServer.service.provience;

import np.com.cscpl.AuthorizationServer.model.Provience;

import java.util.List;

public interface ProvienceService {
    public List<Provience> findAll() throws Exception;
}
