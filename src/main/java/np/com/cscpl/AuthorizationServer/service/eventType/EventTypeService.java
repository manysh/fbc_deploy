/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.service.eventType;

import np.com.cscpl.AuthorizationServer.model.EventType;
import org.springframework.data.domain.Page;

/**
 *
 * @author puzansakya
 */
public interface EventTypeService {

    Page<EventType> findAll( int pageNumber, int pageSize) throws Exception;

    EventType findOne(int eventTypeId) throws Exception;

    EventType save(EventType eventType) throws Exception;

    void update(int id, EventType eventType) throws Exception;

    void delete(int id) throws Exception;

}
