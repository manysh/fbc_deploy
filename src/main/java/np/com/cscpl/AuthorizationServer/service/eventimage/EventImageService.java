package np.com.cscpl.AuthorizationServer.service.eventimage;

import java.util.List;
import np.com.cscpl.AuthorizationServer.model.EventImage;
import org.springframework.data.domain.Page;

/**
 *
 * @author puzansakya
 */

public interface EventImageService {

    Page<EventImage> findAll(int id, int pageNumber, int pageSize) throws Exception;

    EventImage findOne(int eventId) throws Exception;

    EventImage save(EventImage sector) throws Exception;

    void update(int id, EventImage eventImage) throws Exception;

    void delete(int id) throws Exception;

}
