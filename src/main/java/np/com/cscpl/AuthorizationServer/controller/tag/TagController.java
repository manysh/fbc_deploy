package np.com.cscpl.AuthorizationServer.controller.tag;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import javax.websocket.server.PathParam;

import io.swagger.annotations.ApiOperation;
import np.com.cscpl.AuthorizationServer.dto.tag.TagProjectListDto;
import np.com.cscpl.AuthorizationServer.facade.tag.TagFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;
import np.com.cscpl.AuthorizationServer.dto.tag.TagResponseDto;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import static np.com.cscpl.AuthorizationServer.controller.tag.Documentation.*;

@RestController
@RequestMapping(BASE+TAG)
@CrossOrigin
public class TagController {

    @Autowired
    private TagFacade tagFacade;

    @GetMapping
    public ResponseEntity<?> findAll(
            @RequestParam("pageNumber") Optional<Integer> pageNumber,
            @RequestParam("pageSize") Optional<Integer> pageSize
    ) throws Exception {
        try {
            int newPageNumber = pageNumber.isPresent() ? pageNumber.get() : 1;
            int newPageSize = pageSize.isPresent() ? pageSize.get() : 10;
            newPageNumber = newPageNumber < 0 ? 1 : newPageNumber;
            newPageSize = newPageSize > 100 ? 100 : (newPageSize < 10 ? 10 : newPageSize);
            return new ResponseEntity<Object>(
                    tagFacade.findAll(
                            newPageNumber,
                            newPageSize
                    ),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> save(@Valid @RequestBody TagResponseDto tagResponseDto) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    tagFacade.save(
                            tagResponseDto
                    ),
                    HttpStatus.CREATED
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findOne(@PathVariable int id) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    tagFacade.findOne(id),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) throws Exception {
        try {
            tagFacade.delete(id);
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @PutMapping
    public ResponseEntity<?> update(@Valid @RequestBody TagResponseDto tagResponseDto) throws Exception {
        try {
            tagFacade.update(tagResponseDto);
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @PutMapping("/update/{id}")
    @ApiOperation("This endpoint update the tag for given tagId.")
    public ResponseEntity<?> update(@PathVariable("id")int id, @Valid @RequestBody TagResponseDto tagResponseDto)throws Exception{
        try {
            return new ResponseEntity<Object>(tagFacade.updateTag(id,tagResponseDto),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping(GET_TAG_BY_PROJECT_ID)
    @ApiOperation(Documentation.TAG_GET_BY_PROJECT_ID)
    public  ResponseEntity<?> findByProjectId(@PathVariable int projectId) throws Exception{
        try {
            return new ResponseEntity<Object>(tagFacade.findByDepartmentId(projectId),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(TAG_SLASH_FIND_ALL)
    @ApiOperation(Documentation.TAG_GET_BY_PROJECT_ID)
    public ResponseEntity<?> findAllTag() throws Exception{
        try {
            return new ResponseEntity<Object>(tagFacade.findAll(),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(GET_TAG_BY_PROJECT_IDS)
    @ApiOperation(TAG_GET_FILTERED_RESULT_DOC)
    public ResponseEntity<?> getTagsByProjectIds(@RequestBody TagProjectListDto tagProjectListDto) throws Exception{
        List<Integer> deparmentIds=new ArrayList<>();
        deparmentIds=tagProjectListDto!=null?tagProjectListDto.getProjectIds():new ArrayList<>();
        try {
            return new ResponseEntity<Object>(tagFacade.findByProjectIds(deparmentIds),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = TAG_FILTERED_RESULT)
    public ResponseEntity<?> filtredResult(
            @PathVariable("pageNumber") int pageNumber,
            @PathVariable("pageSize") int pageSize,
            @RequestParam("search") Optional<String> search) throws Exception{
        try {

            pageNumber=pageNumber==0?1:pageNumber;
            pageSize=pageSize<10?10:(pageSize>100?100:pageSize);

            return new ResponseEntity<Object>(
                    tagFacade.getFilteredResult(search.isPresent()?search.get():"",pageNumber,pageSize),
                    HttpStatus.OK
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }


}


