package np.com.cscpl.AuthorizationServer.controller;

import javax.validation.Valid;
import np.com.cscpl.AuthorizationServer.facade.department.DepartmentFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.DEPARTMENT;
import np.com.cscpl.AuthorizationServer.dto.department.DepartmentResponseDto;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping(DEPARTMENT)
public class DepartmentController {

    @Autowired
    private DepartmentFacade departmentFacade;

    @GetMapping
    public ResponseEntity<?> findAll() throws Exception {
        try {
            return new ResponseEntity<Object>(departmentFacade.findAll(), HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<?> save(@Valid @RequestBody DepartmentResponseDto departmentResponseDto) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    departmentFacade.save(
                            departmentResponseDto
                    ),
                    HttpStatus.CREATED
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findOne(@PathVariable int id) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    departmentFacade.findOne(id),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> delete(@PathVariable int id) throws Exception {
        try {
            departmentFacade.delete(id);
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @PutMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(@Valid @RequestBody DepartmentResponseDto departmentResponseDto) throws Exception {
        try {
            departmentFacade.update(departmentResponseDto);
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }
}
