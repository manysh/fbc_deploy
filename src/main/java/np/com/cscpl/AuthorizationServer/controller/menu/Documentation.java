package np.com.cscpl.AuthorizationServer.controller.menu;

public class Documentation {
    public static final String MENU_SEARCH_DOCS="This endpoint returns pageable menu with search feature.";
    public static final String MENU_CREATE_DOCS="This endpoint is to create new menu.";
    public static final String MENU_FIND_ALL_DOCS="This endpoint returns all the menu items.";
}
