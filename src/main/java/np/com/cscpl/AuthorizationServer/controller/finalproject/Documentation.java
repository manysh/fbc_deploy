package np.com.cscpl.AuthorizationServer.controller.finalproject;

public class Documentation {
    public static final String CREATE_FINAL_PROJECT_DOC="This end point create new final project for given model.";
    public static final String PROJECT_EXTENSIVE_SEARCH="This end point returns paged list of fianl project";
    public static final String PROJECT_GET_BY_ID="This end point returns project for specific id.";
    public static final String FINAL_PROJECT_UPDATE_ENDPOINT="This endpoint update project specific to given id.";
    public static final String FINAL_PROJECT_HOMEPAGE_DOC="This method returns only 4 project list of either completed" +
            " or ongoing project depending on status ('Ongoing' or 'Completed')";

    /**
     * SECTION FOR PROJECT DIS_LAY CONTROLER
     *
     */
    public static final String HOMEPAGE_PROJECT_DISPLAY_DOCS="This endpoint returns project to display in homepage " +
            "for given status \"Completed\" or \"Ongoing\" or \"shortListed\" ";

    public static  final String FINAL_PROJECT_DOWNLOAD_BY_STATUS_DOCS="This endpoint returns all the project" +
            " based on given status \"Completed\" or \"Ongoing\" or \"shortListed\"";
}
