package np.com.cscpl.AuthorizationServer.controller.tag;

public class ProjectDisplayCreateDto {
    private int projectDisplayId;
    private int departmentId;

    public int getProjectDisplayId() {
        return projectDisplayId;
    }

    public void setProjectDisplayId(int projectDisplayId) {
        this.projectDisplayId = projectDisplayId;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }
}
