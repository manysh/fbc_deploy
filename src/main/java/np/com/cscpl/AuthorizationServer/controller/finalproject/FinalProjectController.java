package np.com.cscpl.AuthorizationServer.controller.finalproject;

import io.swagger.annotations.ApiOperation;
import np.com.cscpl.AuthorizationServer.dto.finalproject.FinalProjectCreateResponseDto;
import np.com.cscpl.AuthorizationServer.dto.finalproject.ProjectSearchDto;
import np.com.cscpl.AuthorizationServer.facade.finalproject.FinalProjectFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.print.Doc;
import javax.validation.Valid;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;

@RestController
@RequestMapping(BASE + FINAL_PROJECT)
@CrossOrigin
public class FinalProjectController {

    private FinalProjectFacade finalProjectFacade;

    @Autowired
    public FinalProjectController(FinalProjectFacade finalProjectFacade) {
        this.finalProjectFacade = finalProjectFacade;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    @ApiOperation(Documentation.CREATE_FINAL_PROJECT_DOC)
    public ResponseEntity<?> createProject(
            @RequestBody FinalProjectCreateResponseDto finalProjectCreateResponseDto
    ) throws Exception {
        try {
            return new ResponseEntity<Object>(finalProjectFacade.create(finalProjectCreateResponseDto), HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(PROJECT_SEARCH)
    @ApiOperation(Documentation.PROJECT_EXTENSIVE_SEARCH)
    public ResponseEntity<?> extensiveSearh(@RequestBody ProjectSearchDto projectSearchDto) throws Exception {
        try {
            return new ResponseEntity<>(
//                    finalProjectFacade.search(projectSearchDto),
                    finalProjectFacade.extensiveProjectSearch(projectSearchDto),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    @ApiOperation(Documentation.PROJECT_GET_BY_ID)
    public ResponseEntity<?> findById(@PathVariable("id") int id) throws Exception {
        try {
            return new ResponseEntity<Object>(finalProjectFacade.findById(id), HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(FINAL_PROJECT_UPDATE_URL)
    @PreAuthorize("hasAuthority('ADMIN')")
    @ApiOperation(Documentation.FINAL_PROJECT_UPDATE_ENDPOINT)
    public ResponseEntity<?> updateProject(@PathVariable("id") int id, @Valid @RequestBody FinalProjectCreateResponseDto finalProjectCreateResponseDto) {
        try {
            finalProjectCreateResponseDto.setProjectId(id);
            return new ResponseEntity<Object>(finalProjectFacade.updateProject(id, finalProjectCreateResponseDto), HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping(FINAL_PROJECT_HOMEPAGE_LIST_URL)
    @ApiOperation(Documentation.FINAL_PROJECT_HOMEPAGE_DOC)
    public ResponseEntity<?> homepageList(@PathVariable("projectStatus") String projectStatus) throws Exception {
        try {
            return new ResponseEntity<Object>(finalProjectFacade.homepageList(projectStatus), HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(FINAL_PROJECT_DOWNLOAD_BY_STATUS)
    @ApiOperation(Documentation.FINAL_PROJECT_DOWNLOAD_BY_STATUS_DOCS)
    public ResponseEntity<?> downloadProject(@PathVariable("projectStatus") String projectStatus) throws Exception {
        try {
            projectStatus
                    = projectStatus.equalsIgnoreCase("Ongoing") ? "Ongoing"
                    : (projectStatus.equalsIgnoreCase("Completed") ? "Completed" : "shortListed");
            return new ResponseEntity<Object>(
                    finalProjectFacade.downloadProject(
                            projectStatus
                    ), HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
