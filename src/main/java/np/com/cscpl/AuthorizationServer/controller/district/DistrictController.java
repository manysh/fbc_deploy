package np.com.cscpl.AuthorizationServer.controller.district;


import io.swagger.annotations.ApiOperation;
import np.com.cscpl.AuthorizationServer.facade.district.DistrictFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;

@RestController
@RequestMapping(BASE+SLASH_DISTRICT)
@CrossOrigin
public class DistrictController {

    private DistrictFacade districtFacade;

    @Autowired
    public DistrictController(DistrictFacade districtFacade) {
        this.districtFacade = districtFacade;
    }

    @GetMapping(DISTRICT_BY_PROJECT_ID)
    @ApiOperation(Documentation.FIND_BY_PROJECT_ID)
    public ResponseEntity<?> findByProjectId(@PathVariable int projectId) throws Exception{
        try {
            return new ResponseEntity<Object>(districtFacade.findByProjectId(projectId),HttpStatus.OK

            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    @ApiOperation(Documentation.FIND_ALL_DOC)
    public ResponseEntity<?> findAll() throws Exception{
        try {
            return new ResponseEntity<Object>(districtFacade.findAll(),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }
}
