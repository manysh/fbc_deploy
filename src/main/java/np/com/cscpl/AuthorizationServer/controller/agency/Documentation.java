package np.com.cscpl.AuthorizationServer.controller.agency;

public class Documentation {
    public static final String GET_MAP_DOC="This endpoint returns all funding agency.";
    public static final String FIND_BY_ID_DOC="This endpoint returns agency for given id.";
    public static final String UPDATE_FUNDING_AGENCY = "This endpoint update existing agency.";
}
