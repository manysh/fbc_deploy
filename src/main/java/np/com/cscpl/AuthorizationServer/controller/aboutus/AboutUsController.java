package np.com.cscpl.AuthorizationServer.controller.aboutus;

import np.com.cscpl.AuthorizationServer.facade.aboutus.AboutUsFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;

@RestController
@RequestMapping(ABOUT)
@CrossOrigin(origins = "http://localhost:3000")
public class AboutUsController {

    private AboutUsFacade aboutUsFacade;


    @Autowired
    public AboutUsController(AboutUsFacade aboutUsFacade) {
        this.aboutUsFacade = aboutUsFacade;
    }

    @GetMapping("/findAll")
    public ResponseEntity<?> findAll()throws  Exception{
        try{
        return new ResponseEntity<Object>(aboutUsFacade.findAll(),HttpStatus.OK);
        }catch(Exception ex){
            System.out.println(ex.getLocalizedMessage());

            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
