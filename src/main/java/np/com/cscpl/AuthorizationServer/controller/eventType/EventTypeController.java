package np.com.cscpl.AuthorizationServer.controller.eventType;

import javax.validation.Valid;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.BASE;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.EVENT_TYPE;

import np.com.cscpl.AuthorizationServer.facade.event.EventFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import np.com.cscpl.AuthorizationServer.dto.event.EventResponseDto;
import np.com.cscpl.AuthorizationServer.dto.eventType.EventTypeResponseDto;
import np.com.cscpl.AuthorizationServer.facade.eventType.EventTypeFacade;

@RestController
@RequestMapping(BASE+EVENT_TYPE)
@CrossOrigin
public class EventTypeController {

    @Autowired
    private EventTypeFacade eventTypeFacade;

    @GetMapping
    public ResponseEntity<?> findAll(
            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "search", required = false) String search
    ) throws Exception {
        try {          
            int newPageNumber = (pageNumber == null || pageNumber <= 0) ? 1 : pageNumber;
            int newPageSize = (pageSize == null || pageSize <= 0) ? 10 : (pageSize > 100 ? 100 : (pageSize < 10 ? 10 : pageSize));
            
            return new ResponseEntity<>(
                    eventTypeFacade.findAll(
                            newPageNumber,
                            newPageSize
                    ),
                    HttpStatus.OK
            );

        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> save(@Valid @RequestBody EventTypeResponseDto eventTypeResponseDto) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    eventTypeFacade.save(
                            eventTypeResponseDto
                    ),
                    HttpStatus.CREATED
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findOne(@PathVariable int id) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    eventTypeFacade.findOne(id),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> delete(@PathVariable int id) throws Exception {
        try {
            eventTypeFacade.delete(id);
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @PutMapping(value="/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(@PathVariable("id") int id,@Valid @RequestBody EventTypeResponseDto eventTypeResponseDto) throws Exception {
        try {
            eventTypeFacade.update(id,eventTypeResponseDto);
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }
}
