package np.com.cscpl.AuthorizationServer.controller.sector2;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import np.com.cscpl.AuthorizationServer.dto.sector2.Sector2ResponseDto;
import np.com.cscpl.AuthorizationServer.facade.sector2.Sector2Facade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.print.Doc;
import javax.validation.Valid;

import java.util.Optional;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;

@RestController
@RequestMapping(BASE+SECTOR2)
@CrossOrigin
public class Sector2Controller {
    private Sector2Facade sector2Facade;

    @Autowired
    public Sector2Controller(Sector2Facade sector2Facade) {
        this.sector2Facade = sector2Facade;
    }

    @GetMapping
    @ApiOperation(Documentation.GET_MAP_DOC)
    public ResponseEntity<?> findAll() throws Exception{
        try {
            return new ResponseEntity<Object>(sector2Facade.findAll(),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(SECTOR2_FIND_BY_PROJECT_ID)
    @ApiOperation(Documentation.GET_SEC2_BY_PROJECT_ID_DOC)
    public ResponseEntity<?> findByProjectId(@PathVariable int id) throws Exception{
        try {
            return  new ResponseEntity<Object>(sector2Facade.findByProjectId(id),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @ApiOperation(Documentation.CREATE_SEC2_DOCS)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> create(@Valid @RequestBody Sector2ResponseDto sector2ResponseDto) throws Exception{
        try {
            return new ResponseEntity<Object>(sector2Facade.save(sector2ResponseDto),HttpStatus.CREATED);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping(SECTOR2_FILTERED)
    @ApiOperation(Documentation.FILTERED_RESULT_DOCS)
    public ResponseEntity<?> filteredResult(
            @PathVariable("pageNumber") int pageNumber,
            @PathVariable("pageSize") int pageSize,
            @RequestParam("search")Optional<String> search)throws Exception{
        try {
            pageNumber=pageNumber<0?1:pageNumber;
            pageSize=pageSize<10?10:(pageSize>100?100:pageSize);
            String _search=search.isPresent()?search.get():"";
            return new ResponseEntity<Object>(sector2Facade.filteredResult(pageNumber,pageSize,_search),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(SECTOR2_BY_ID)
    @ApiOperation(Documentation.FIND_BY_ID)
    public ResponseEntity<?> findById(@PathVariable("id") int id) throws Exception{
        try {
            return new ResponseEntity<Object>(sector2Facade.findById(id),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(SECTOR_UPDATE_ID)
    @ApiOperation(Documentation.UPDATE_SEC2_DOCS)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(
            @PathVariable("id") int id,
            @Valid @RequestBody Sector2ResponseDto sector2ResponseDto
    ) throws Exception{
        try {
            return new ResponseEntity<Object>(sector2Facade.update(id,sector2ResponseDto),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NotFoundException(ex.getLocalizedMessage());
        }
    }
}
