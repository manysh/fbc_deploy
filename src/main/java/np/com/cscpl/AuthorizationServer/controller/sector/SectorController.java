package np.com.cscpl.AuthorizationServer.controller.sector;

import java.util.Optional;
import javax.validation.Valid;
import javax.websocket.server.PathParam;

import io.swagger.annotations.ApiOperation;
import np.com.cscpl.AuthorizationServer.facade.sector.SectorFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;
import np.com.cscpl.AuthorizationServer.dto.sector.SectorResponseDto;

@RestController
@RequestMapping(BASE+SECTOR)
@CrossOrigin
public class SectorController {

    @Autowired
    private SectorFacade sectorFacade;

    @GetMapping(value={"/findAll/{pageNumber}/{pageSize}","/findAll/{pageNumber}","/findAll"})
    public ResponseEntity<?> findAll(
            @PathVariable("pageNumber") Optional<Integer> pageNumber,
            @PathVariable("pageSize") Optional<Integer> pageSize,
            @PathParam("search")String search
    ) throws Exception {
        try {
            int newPageNumber = pageNumber.isPresent() ? pageNumber.get() : 1;
            int newPageSize = pageSize.isPresent() ? pageSize.get() : 10;
            newPageNumber = newPageNumber < 0 ? 1 : newPageNumber;
            newPageSize = newPageSize > 100 ? 100 : (newPageSize < 10 ? 10 : newPageSize);
            search=search==null?"":search;
            return new ResponseEntity<Object>(
                    sectorFacade.findAll(
                            newPageNumber,
                            newPageSize,
                            search
                    ),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value="/create")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> save(@Valid @RequestBody SectorResponseDto sectorResponseDto) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    sectorFacade.save(
                            sectorResponseDto
                    ),
                    HttpStatus.CREATED
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findOne(@PathVariable int id) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    sectorFacade.findOne(id),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> delete(@PathVariable int id) throws Exception {
        try {
            sectorFacade.delete(id);
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @PutMapping(value="/update/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(@PathVariable("id") int id,@Valid @RequestBody SectorResponseDto sectorResponseDto) throws Exception {
        try {
            sectorFacade.update(id,sectorResponseDto);
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping(GET_SECTOR_BY_PROJECT)
    @ApiOperation(Documentation.GET_SECTOR_BY_DEPARTMENT_ID)
    public ResponseEntity<?> getSectorByDepartment(@PathVariable int projectId) throws Exception{
        try {
            return new ResponseEntity<Object>(sectorFacade.findByProjectId(projectId),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }
}
