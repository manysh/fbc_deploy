package np.com.cscpl.AuthorizationServer.controller.finalproject;


import io.swagger.annotations.ApiOperation;
import np.com.cscpl.AuthorizationServer.controller.tag.ProjectDisplayCreateDto;
import np.com.cscpl.AuthorizationServer.facade.finalproject.ProjectDisplayFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.BASE;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.PROJECT_HOMEPAGE_DISPLAY;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.PROJECT_HOMEPAGE_DISPLAY_UPDATE;
import static np.com.cscpl.AuthorizationServer.controller.finalproject.Documentation.HOMEPAGE_PROJECT_DISPLAY_DOCS;

@RestController
@CrossOrigin
public class ProjectDisplayController {

    private ProjectDisplayFacade projectDisplayFacade;

    @Autowired
    public ProjectDisplayController(ProjectDisplayFacade projectDisplayFacade) {
        this.projectDisplayFacade = projectDisplayFacade;
    }

    @GetMapping(BASE+PROJECT_HOMEPAGE_DISPLAY)
    @ApiOperation(HOMEPAGE_PROJECT_DISPLAY_DOCS)
    public ResponseEntity<?> findByStatus(@PathVariable("status")String status)throws Exception{
        try {
            return new ResponseEntity<Object>(projectDisplayFacade.findByStatus(
                    status
            ),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(BASE+PROJECT_HOMEPAGE_DISPLAY_UPDATE)
    public ResponseEntity<?> update(
            @PathVariable("id") int id,
            @Valid @RequestBody ProjectDisplayCreateDto ProjectDisplayCreateDto
    ) throws Exception{
        try {
            return new ResponseEntity<Object>(projectDisplayFacade.update(
                    id,
                    ProjectDisplayCreateDto
            ),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return  new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }
}
