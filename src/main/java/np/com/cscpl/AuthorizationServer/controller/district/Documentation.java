package np.com.cscpl.AuthorizationServer.controller.district;

public class Documentation {

    public static final String FIND_ALL_DOC="This endpoint returns all the district list";
    public static final String FIND_BY_PROJECT_ID="This end point returns district list specific to project id";
}
