/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.controller.search;

import np.com.cscpl.AuthorizationServer.facade.search.SearchFacade;
import np.com.cscpl.AuthorizationServer.service.finalproject.FinalProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.BASE;

/**
 *
 * @author prabhu
 */
@RestController
@RequestMapping(BASE+"/search")
@CrossOrigin
public class SearchController {
    @Autowired
    private SearchFacade searchFacade;

    @GetMapping
    public ResponseEntity<?> findAll(@RequestParam(value = "keyword", required = false) String keyword) throws Exception {
        try {
            return new ResponseEntity<>(
                    searchFacade.keySearch(keyword,1,10),
                    HttpStatus.OK
            );

        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
