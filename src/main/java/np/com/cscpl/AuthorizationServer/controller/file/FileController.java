package np.com.cscpl.AuthorizationServer.controller.file;

import np.com.cscpl.AuthorizationServer.common.ImageUploadHelper;
import np.com.cscpl.AuthorizationServer.facade.filedownload.FileDownloadFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;

import static np.com.cscpl.AuthorizationServer.common.ConstantData.PDF_FILE;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;
import static np.com.cscpl.AuthorizationServer.common.ImageUploadHelper.saveFileToServer;

@RequestMapping(BASE+FILE)
@RestController
@CrossOrigin
public class FileController {

    private FileDownloadFacade fileDownloadFacade;

    @Autowired
    public FileController(FileDownloadFacade fileDownloadFacade) {
        this.fileDownloadFacade = fileDownloadFacade;
    }

    @GetMapping(value = GET+FILE_NAME,produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> downloadFile(@PathVariable("fileName") String fileName) throws Exception{
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename="+fileName);
        File file= new File(ImageUploadHelper.getfileLocation(fileName));
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(resource);
    }

    @PostMapping(value = UPLOAD,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> uploadFile(@RequestParam("file")MultipartFile file){
        try {
            return new ResponseEntity<Object>(saveFileToServer(file),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = DELETE+FILE_NAME)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteFile(@PathVariable("fileName")String fileName) throws Exception{
        try {
            if(ImageUploadHelper.deleFile(fileName,PDF_FILE)){
                return new ResponseEntity<Object>(null,HttpStatus.OK);
            }
            else{
                return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
            }
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }
}
