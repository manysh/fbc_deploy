package np.com.cscpl.AuthorizationServer.controller.sector3;


import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import np.com.cscpl.AuthorizationServer.dto.sector3.Sector3ResponseDto;
import np.com.cscpl.AuthorizationServer.facade.sector3.Sector3Facade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.Optional;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;

@RestController
@RequestMapping(BASE+SECTOR3)
@CrossOrigin
public class Sector3Controller {
    private Sector3Facade sector3Facade;

    @Autowired
    public Sector3Controller(Sector3Facade sector3Facade) {
        this.sector3Facade = sector3Facade;
    }

    @GetMapping
    @ApiOperation(Documentation.GET_MAP_DOC)
    public ResponseEntity<?> findAll() throws Exception{
        try {
            return new ResponseEntity<Object>(sector3Facade.findAll(),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(SECTOR3_FIND_BY_PROJECT_ID)
    @ApiOperation(Documentation.GET_SEC3_BY_PROJECT_ID_DOC)
    public ResponseEntity<?> findByProjectId(@PathVariable int id) throws Exception{
        try {
            return  new ResponseEntity<Object>(sector3Facade.findByProjectId(id),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    @ApiOperation(Documentation.SEC3_CREATE_DOCS)
    public ResponseEntity<?> create(@Valid @RequestBody Sector3ResponseDto sector3ResponseDto)throws Exception{
        try {
            return new ResponseEntity<Object>(sector3Facade.create(sector3ResponseDto),HttpStatus.CREATED);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping(SECTOR3_FILTERED)
    @ApiOperation(Documentation.SEC3_FILTER_DOCS)
    public ResponseEntity<?> filteredResuly(
            @PathVariable("pageNumber") int pageNumber,
            @PathVariable("pageSize") int pageSize,
            @RequestParam("search")Optional<String> search
            ) throws Exception{
        try {
            pageNumber=pageNumber<=0?1:pageNumber;
            pageSize=pageSize<10?10:(pageSize>100?100:pageSize);
            return new ResponseEntity<Object>(sector3Facade.filteredResult(
                    pageNumber,
                    pageSize,
                    search.isPresent()?search.get():""
            ),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(SECTOR3_FIND_BY_ID)
    @ApiOperation(Documentation.FIND_BY_ID_DOCS)
    public ResponseEntity<?> findById(
            @PathVariable("id") int id
    ) throws Exception{
        try {
            return new ResponseEntity<Object>(
                    sector3Facade.findById(id),
                    HttpStatus.OK
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(SECTOR_UPDATE_ID)
    @PreAuthorize("hasAuthority('ADMIN')")
    @ApiOperation(Documentation.UPDATE_SEC3_DOCS)
    public ResponseEntity<?> update(
            @PathVariable("id") int id,
            @Valid @RequestBody Sector3ResponseDto sector3ResponseDto
    ) throws Exception{
        try {
            return new ResponseEntity<Object>(sector3Facade.update(id,sector3ResponseDto),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NotFoundException(ex.getLocalizedMessage());
        }
    }

}
