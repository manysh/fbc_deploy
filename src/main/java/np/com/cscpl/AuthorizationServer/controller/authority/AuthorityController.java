package np.com.cscpl.AuthorizationServer.controller.authority;


import io.swagger.annotations.ApiOperation;
import np.com.cscpl.AuthorizationServer.facade.authority.AuthorityFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;

@RestController
@RequestMapping(BASE+AUTHORITY)
public class AuthorityController {

    private AuthorityFacade authorityFacade;

    @Autowired
    public AuthorityController(AuthorityFacade authorityFacade) {
        this.authorityFacade = authorityFacade;
    }

    @GetMapping
    @ApiOperation(Documentation.FIND_ALL_DOCS)
    public ResponseEntity<?> findAll() throws Exception{
        try {
            return new ResponseEntity<Object>(authorityFacade.findAll(),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
        }
    }
}
