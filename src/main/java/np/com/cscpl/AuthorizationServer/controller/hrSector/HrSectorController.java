package np.com.cscpl.AuthorizationServer.controller.hrSector;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;
import np.com.cscpl.AuthorizationServer.dto.hrSector.HrSectorResponseDto;
import np.com.cscpl.AuthorizationServer.facade.hrSector.HrSectorFacade;

@RestController
@RequestMapping(BASE + HR_SECTOR)
@CrossOrigin
public class HrSectorController {

    @Autowired
    private HrSectorFacade hrSectorFacade;

    @GetMapping()
    public ResponseEntity<?> findAll(
            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "keyword", required = false) String keyword
    ) throws Exception {
        try {
            int newPageNumber = (pageNumber == null || pageNumber <= 0) ? 1 : pageNumber;
            int newPageSize=pageSize < 10 ? 10 : pageSize;
            if (keyword != null) {
                return new ResponseEntity<>(
                        hrSectorFacade.searchSector(
                                keyword,
                                newPageNumber,
                                pageSize),
                        HttpStatus.OK
                );
            }

            return new ResponseEntity<>(
                    hrSectorFacade.findAll(
                            newPageNumber,
                            newPageSize
                    ),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/other")
    public ResponseEntity<?> findAllOther(
            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "keyword", required = false) String keyword
    ) throws Exception {
        try {
            int newPageNumber = (pageNumber == null || pageNumber <= 0) ? 1 : pageNumber;
            int newPageSize=pageSize < 10 ? 10 : pageSize;

            if (keyword != null) {
                return new ResponseEntity<>(
                        hrSectorFacade.searchOtherSector(
                                keyword,
                                newPageNumber,
                                pageSize),
                        HttpStatus.OK
                );
            }

            return new ResponseEntity<>(
                    hrSectorFacade.IsOther(
                            newPageNumber,
                            newPageSize
                    ),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/sub")
    public ResponseEntity<?> findAllSub(
            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "keyword", required = false) String keyword
    ) throws Exception {
        try {
            int newPageNumber = (pageNumber == null || pageNumber <= 0) ? 1 : pageNumber;
            int newPageSize=pageSize < 10 ? 10 : pageSize;

            if (keyword != null) {
                return new ResponseEntity<>(
                        hrSectorFacade.searchSubSector(
                                keyword,
                                newPageNumber,
                                pageSize),
                        HttpStatus.OK
                );
            }

            return new ResponseEntity<>(
                    hrSectorFacade.IsSub(
                            newPageNumber,
                            newPageSize
                    ),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping()
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> save(@Valid @RequestBody HrSectorResponseDto hrSectorResponseDto) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    hrSectorFacade.save(
                            hrSectorResponseDto
                    ),
                    HttpStatus.CREATED
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findOne(@PathVariable int id) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    hrSectorFacade.findOne(id),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> delete(@PathVariable int id) throws Exception {
        try {
            hrSectorFacade.delete(id);
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @PutMapping(value = "/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(@PathVariable("id") int id, @Valid @RequestBody HrSectorResponseDto hrSectorResponseDto) throws Exception {
        try {
            hrSectorFacade.update(id, hrSectorResponseDto);
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

}
