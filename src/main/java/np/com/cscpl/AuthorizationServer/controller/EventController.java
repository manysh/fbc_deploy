package np.com.cscpl.AuthorizationServer.controller;

import javax.validation.Valid;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.EVENT;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.BASE;

import np.com.cscpl.AuthorizationServer.facade.event.EventFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import np.com.cscpl.AuthorizationServer.dto.event.EventResponseDto;

@RestController
@RequestMapping(BASE + EVENT)
@CrossOrigin
public class EventController {

    @Autowired
    private EventFacade eventFacade;

    @GetMapping
    public ResponseEntity<?> findAll(
            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "eventType", required = false) String eventType,
            @RequestParam(value = "keyword", required = false) String keyword
    ) throws Exception {
        try {
            int newPageNumber = (pageNumber == null || pageNumber <= 0) ? 1 : pageNumber;
            int newPageSize = (pageSize == null || pageSize <= 0) ? 10 : (pageSize > 100 ? 100 : (pageSize < 10 ? 10 : pageSize));
            
            if (eventType != null) {
                return new ResponseEntity<>(
                        eventFacade.findAllByEventType(
                                eventType,
                                newPageNumber,
                                pageSize),
                        HttpStatus.OK
                );
            }
            if (keyword != null) {
                System.out.println(keyword);
                return new ResponseEntity<>(
                        eventFacade.search(
                                keyword,
                                newPageNumber,
                                pageSize),
                        HttpStatus.OK
                );
            }
            return new ResponseEntity<>(
                    eventFacade.findAll(
                            newPageNumber,
                            newPageSize
                    ),
                    HttpStatus.OK
            );

        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
    
    @GetMapping("/show")
    public ResponseEntity<?> findByIsShow(
            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", required = false) Integer pageSize         
    ) throws Exception {
        try {
            int newPageNumber = (pageNumber == null || pageNumber <= 0) ? 1 : pageNumber;
            int newPageSize = (pageSize == null || pageSize <= 0) ? 10 : (pageSize > 100 ? 100 : (pageSize < 10 ? 10 : pageSize));
            
        
            return new ResponseEntity<>(
                    eventFacade.findByIsShowTrue(
                            newPageNumber,
                            newPageSize
                    ),
                    HttpStatus.OK
            );

        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
    
    @GetMapping("/eventshow")
    public ResponseEntity<?> findByEgIsShow(
            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", required = false) Integer pageSize         
    ) throws Exception {
        try {
            int newPageNumber = (pageNumber == null || pageNumber <= 0) ? 1 : pageNumber;
            int newPageSize = (pageSize == null || pageSize <= 0) ? 10 : (pageSize > 100 ? 100 : (pageSize < 10 ? 10 : pageSize));
            
        
            return new ResponseEntity<>(
                    eventFacade.findByEgIsShowTrue(
                            newPageNumber,
                            newPageSize
                    ),
                    HttpStatus.OK
            );

        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> save(@Valid @RequestBody EventResponseDto eventResponseDto) throws Exception {
        try {
            return new ResponseEntity<>(
                    eventFacade.save(
                            eventResponseDto
                    ),
                    HttpStatus.CREATED
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findOne(@PathVariable int id) throws Exception {
        try {
            return new ResponseEntity<>(
                    eventFacade.findOne(id),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> delete(@PathVariable int id) throws Exception {
        try {
            eventFacade.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @PutMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(@PathVariable("id") int id, @Valid @RequestBody EventResponseDto eventResponseDto) throws Exception {
        try {
            eventFacade.update(id, eventResponseDto);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }
    
     @PutMapping(value = "/showcase/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> updateShowcase(@PathVariable("id") int id, @Valid @RequestBody EventResponseDto eventResponseDto) throws Exception {
        try {
            eventFacade.update(id, eventResponseDto);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

}
