package np.com.cscpl.AuthorizationServer.controller.tag;

public class Documentation {
    public static final String TAG_GET_BY_PROJECT_ID="This end point returns tags list for specific project.";
    public static final String TAG_GET_ALL="This Endpoint returns all the tag";
    public static final String TAG_GET_FILTERED_RESULT_DOC="This endpoint returns paged filtered result.";
}
