/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.controller.image;

import static np.com.cscpl.AuthorizationServer.common.ConstantData.IMAGE_FILE;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.DELETE;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.FILE_NAME;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.IMAGE;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.UPLOAD;
import np.com.cscpl.AuthorizationServer.common.ImageUploadHelper;
import static np.com.cscpl.AuthorizationServer.common.ImageUploadHelper.saveImageToServer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author puzansakya
 */
@RequestMapping(IMAGE)
@RestController
@CrossOrigin
public class ImageController {

    @PostMapping(value = UPLOAD, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> uploadImage(@RequestParam("image") MultipartFile image) {
        try {
            return new ResponseEntity<Object>(saveImageToServer(image), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = DELETE + FILE_NAME)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteFile(@PathVariable("fileName") String fileName) throws Exception {
        try {
            if (ImageUploadHelper.deleFile(fileName, IMAGE_FILE)) {
                return new ResponseEntity<Object>(null, HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
