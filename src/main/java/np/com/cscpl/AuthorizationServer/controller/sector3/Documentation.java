package np.com.cscpl.AuthorizationServer.controller.sector3;

public class Documentation {
    public static final String GET_MAP_DOC="This end point returns all sector3";
    public static final String GET_SEC3_BY_PROJECT_ID_DOC="This end point returns sec3 of specfic project";
    public static final String SEC3_CREATE_DOCS="This endpoint creates new sector3.";
    public static final String SEC3_FILTER_DOCS = "This end point returns filtered sector 3 list. Sample url" +
            " /sector3/{pageNumber}/{pageSize} or /sector3/{pageNumber}/{pageSize}?search={search_parameter}";
    public static final String FIND_BY_ID_DOCS="This end point returns specific sec3 for given id.";
    public static final String UPDATE_SEC3_DOCS="This end point update sec3 for specific id.";
}
