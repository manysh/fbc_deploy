package np.com.cscpl.AuthorizationServer.controller;

import np.com.cscpl.AuthorizationServer.dto.career.CareerResponseDto;
import np.com.cscpl.AuthorizationServer.facade.career.CareerFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.BASE;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.CAREER;


@RestController
@RequestMapping(BASE + CAREER)
@CrossOrigin
public class CareerController {

    @Autowired
    private CareerFacade careerFacade;


    @PostMapping(value = "/send")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> send(@Valid @RequestBody CareerResponseDto careerResponseDto) throws Exception {


        try {
            return new ResponseEntity<Object>(
                    careerFacade.sendEmail(careerResponseDto), HttpStatus.OK);

        } catch (MessagingException e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());

        }
    }
}
