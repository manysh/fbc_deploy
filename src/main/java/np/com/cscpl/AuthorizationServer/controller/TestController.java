package np.com.cscpl.AuthorizationServer.controller;

import np.com.cscpl.AuthorizationServer.common.GetPageable;
import np.com.cscpl.AuthorizationServer.converter.finalproject.FinalProjectConverter;
import np.com.cscpl.AuthorizationServer.dto.finalproject.FinalProjectCreateResponseDto;
import np.com.cscpl.AuthorizationServer.dto.finalproject.FinalProjectResponseDto;
import np.com.cscpl.AuthorizationServer.model.FinalProject;
import np.com.cscpl.AuthorizationServer.repository.finalproject.FinalProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class TestController implements ErrorController {

    @Autowired
    private FinalProjectRepository finalProjectRepository;

    @Autowired
    private FinalProjectConverter finalProjectConverter;

    @GetMapping("/test")
    public String test() {
        return "not secured controller";
    }

    @GetMapping("/test/check")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String secured() {

        return "secured end point";
    }

    @PostMapping("/new/check")
    public ResponseEntity<?> testme(@RequestBody TestClass integers) throws Exception {
        Page<FinalProject> finalProjectPage = finalProjectRepository.searchProject(integers.integers, GetPageable.createPageRequest(
                1,
                1
        ));
        Page<FinalProjectResponseDto> finalProjectResponseDtos = finalProjectPage.map(finalProject -> new FinalProjectResponseDto(
                finalProject.getId(),
                finalProject.getTitle(),
                finalProject.getAssignmentTitle(),
                finalProject.getDescription(),
                finalProject.getWorkDescription(),
                finalProject.getCreatedDate(),
                finalProject.getCompletetionDate(),
                1,
                "",
                1,
                "",
                1,
                "",
                "Completed",
                "",
                ""
        )
        );
        return new ResponseEntity<Object>(finalProjectResponseDtos, HttpStatus.OK);
    }

    @PostMapping("/check/test/test")
    public ResponseEntity<?> testmeagain(@RequestBody TestClass testClass) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    finalProjectConverter.createFromEntities(
                            finalProjectRepository
                                    .extensiveSearch(testClass.integers, testClass.integerList)), HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    /*@RequestMapping("/")
    public String index(){
        return "index.html";
    }*/
    private static final String PATH = "/error";

    @Override
    public String getErrorPath() {
        return PATH;
    }

    @RequestMapping(value = PATH)
    public String error() {
        return "index.html";
    }
}

class TestClass {

    List<Integer> integers = new ArrayList<>();
    List<Integer> integerList = new ArrayList<>();

    public List<Integer> getIntegers() {
        return integers;
    }

    public void setIntegers(List<Integer> integers) {
        this.integers = integers;
    }

    public List<Integer> getIntegerList() {
        return integerList;
    }

    public void setIntegerList(List<Integer> integerList) {
        this.integerList = integerList;
    }
}
