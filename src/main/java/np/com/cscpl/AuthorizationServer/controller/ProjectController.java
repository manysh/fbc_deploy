package np.com.cscpl.AuthorizationServer.controller;

import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;
import np.com.cscpl.AuthorizationServer.dto.project.ProjectParamsDTO;
import np.com.cscpl.AuthorizationServer.dto.project.ProjectResponseDto;
import np.com.cscpl.AuthorizationServer.facade.project.ProjectFacade;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@CrossOrigin
@RequestMapping(PROJECT)
public class ProjectController {

    @Autowired
    private ProjectFacade projectFacade;

    @GetMapping(PROJECT_GET)
    public ResponseEntity<?> findAll(
            @PathVariable("pageNumber") Optional<Integer> pageNumber,
            @PathVariable("pageSize") Optional<Integer> pageSize
    ) throws Exception {
        try {
            int newPageNumber = pageNumber.isPresent() ? pageNumber.get() : 1;
            int newPageSize = pageSize.isPresent() ? pageSize.get() : 10;
            newPageNumber = newPageNumber < 0 ? 1 : newPageNumber;
            newPageSize = newPageSize > 100 ? 100 : (newPageSize < 10 ? 10 : newPageSize);
            return new ResponseEntity<Object>(
                    projectFacade.findAll(
                            newPageNumber,
                            newPageSize
                    ),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(PROJECT_GET_BY_SECTOR + SLASH + PROJECT_GET)
    public ResponseEntity<?> findAllBySector(
            @PathVariable("pageNumber") Optional<Integer> pageNumber,
            @PathVariable("pageSize") Optional<Integer> pageSize,
            @RequestParam("search") String search
    ) throws Exception {
        System.out.println(search);
        try {
            int newPageNumber = pageNumber.isPresent() ? pageNumber.get() : 1;
            int newPageSize = pageSize.isPresent() ? pageSize.get() : 10;
            newPageNumber = newPageNumber < 0 ? 1 : newPageNumber;
            newPageSize = newPageSize > 100 ? 100 : (newPageSize < 10 ? 10 : newPageSize);
            return new ResponseEntity<Object>(
                    projectFacade.findAllBySector(
                            search,
                            newPageNumber,
                            newPageSize
                    ),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(PROJECT_GET_BY_STATUS + SLASH + PROJECT_GET)
    public ResponseEntity<?> findAllByStatus(
            @PathVariable("pageNumber") Optional<Integer> pageNumber,
            @PathVariable("pageSize") Optional<Integer> pageSize,
            @RequestParam("status") String status
    ) throws Exception {
        try {
            int newPageNumber = pageNumber.isPresent() ? pageNumber.get() : 1;
            int newPageSize = pageSize.isPresent() ? pageSize.get() : 10;
            newPageNumber = newPageNumber < 0 ? 1 : newPageNumber;
            newPageSize = newPageSize > 100 ? 100 : (newPageSize < 10 ? 10 : newPageSize);
            return new ResponseEntity<Object>(
                    projectFacade.findAllByStatus(
                            status,
                            newPageNumber,
                            newPageSize
                    ),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(PROJECT_GET_BY_TAG + SLASH + PROJECT_GET)
    public ResponseEntity<?> findAllByTag(
            @PathVariable("pageNumber") Optional<Integer> pageNumber,
            @PathVariable("pageSize") Optional<Integer> pageSize,
            @RequestParam("tag") String tag
    ) throws Exception {
        try {
            int newPageNumber = pageNumber.isPresent() ? pageNumber.get() : 1;
            int newPageSize = pageSize.isPresent() ? pageSize.get() : 10;
            newPageNumber = newPageNumber < 0 ? 1 : newPageNumber;
            newPageSize = newPageSize > 100 ? 100 : (newPageSize < 10 ? 10 : newPageSize);
            return new ResponseEntity<Object>(
                    projectFacade.findAllByTag(
                            tag,
                            newPageNumber,
                            newPageSize
                    ),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> save(@Valid @RequestBody ProjectParamsDTO projectParamsDto) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    projectFacade.save(
                            projectParamsDto
                    ),
                    HttpStatus.CREATED
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findOne(@PathVariable int id) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    projectFacade.findOne(id),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> delete(@PathVariable int id) throws Exception {
        try {
            projectFacade.delete(id);
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @PutMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(@Valid @RequestBody ProjectResponseDto projectResponseDto) throws Exception {
        try {
            projectFacade.update(projectResponseDto);
            return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }
}
