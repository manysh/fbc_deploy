package np.com.cscpl.AuthorizationServer.controller.contact;

import np.com.cscpl.AuthorizationServer.dto.contact.ContactReponseDto;
import np.com.cscpl.AuthorizationServer.facade.contact.ContactFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import java.util.Optional;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;

@RestController
@RequestMapping(BASE + CONTACT)
@CrossOrigin
public class ContactController {

    private ContactFacade contactFacade;


    @Autowired
    private JavaMailSender sender;

    @Autowired
    public ContactController(ContactFacade contactFacade) {
        this.contactFacade = contactFacade;

    }


    @GetMapping(value = {"/findAll/{pageNumber}/{pageSize}", "/findAll/{pageNumber}", "/findAll"})
    public ResponseEntity<?> findAll(@PathVariable("pageNumber") Optional<Integer> pageNumber,
                                     @PathVariable("pageSize") Optional<Integer> pageSize) throws Exception {
        try {
            int newPageNumber = pageNumber.isPresent() ? pageNumber.get() : 1;
            int newPageSize = pageSize.isPresent() ? pageSize.get() : 10;
            newPageNumber = newPageNumber < 0 ? 1 : newPageNumber;
            newPageSize = newPageSize > 100 ? 100 : (newPageSize < 10 ? 10 : newPageSize);
//            search=search==null?"":search;
            return new ResponseEntity<Object>(contactFacade.findAll(newPageNumber,
                    newPageSize),
                    HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());

            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> save(@Valid @RequestBody ContactReponseDto contactReponseDto) throws Exception {
        try {

            return new ResponseEntity<Object>(
                    contactFacade.save(contactReponseDto), HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @PostMapping(value = "/send")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> send(@Valid @RequestBody ContactReponseDto contactReponseDto) throws Exception {


        try {
            return new ResponseEntity<Object>(
                    contactFacade.sendEmail(contactReponseDto), HttpStatus.OK);

        } catch (MessagingException e) {
            System.out.println(e.getLocalizedMessage());
            throw new IllegalArgumentException(e.getLocalizedMessage());

        }
    }


}
