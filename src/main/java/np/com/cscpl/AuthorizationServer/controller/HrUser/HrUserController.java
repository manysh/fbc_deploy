package np.com.cscpl.AuthorizationServer.controller.HrUser;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import np.com.cscpl.AuthorizationServer.facade.HrUser.HrUserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;
import np.com.cscpl.AuthorizationServer.dto.finalHrUser.FinalHrUserResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

@RestController
@RequestMapping(BASE + HR_USER)
public class HrUserController {

    @Autowired
    private HrUserFacade huf;

    @GetMapping()
    public ResponseEntity<?> findAll(
            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", required = false) Integer pageSize
    ) throws Exception {
        try {
            int newPageNumber = (pageNumber == null || pageNumber <= 0) ? 1 : pageNumber;
            int newPageSize = (pageSize == null || pageSize <= 0) ? 10 : (pageSize > 100 ? 100 : (pageSize < 10 ? 10 : pageSize));

            return new ResponseEntity<>(
                    huf.findAll(),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * USER SEARCH SECTION
     *
     * @param pageNumber
     * @param pageSize
     * @param firstName
     * @param middleName
     * @param phoneNumber
     * @param lastName
     * @param address
     * @param primarySector
     * @param secondarySector
     * @param otherSector
     * @param resourceType
     * @param consultantType
     * @param degree
     * @param minXp
     * @param maxXp
     * @param export
     * @return
     * @throws java.lang.Exception
     */
    @GetMapping("/search")
    public ResponseEntity<?> search(
            @RequestParam(value = "pageNumber"      , required = false) Integer pageNumber,
            @RequestParam(value = "pageSize"        , required = false) Integer pageSize,
            @RequestParam(value = "firstName"       , required = false) String firstName,
            @RequestParam(value = "middleName"      , required = false) String middleName,
            @RequestParam(value = "lastName"        , required = false) String lastName,
            @RequestParam(value = "phoneNumber"     , required = false) String phoneNumber,
            @RequestParam(value = "address"         , required = false) String address,
            @RequestParam(value = "primarySector"   , required = false) List<Integer> primarySector,
            @RequestParam(value = "secondarySector" , required = false) List<Integer> secondarySector,
            @RequestParam(value = "otherSector"     , required = false) List<Integer> otherSector,            
            @RequestParam(value = "resourceType"    , required = false) Integer resourceType,
            @RequestParam(value = "consultantType"  , required = false) Integer consultantType,
            @RequestParam(value = "degree"          , required = false) Integer degree,
            @RequestParam(value = "minXp"           , required = false) Integer minXp,
            @RequestParam(value = "maxXp"           , required = false) Integer maxXp,
            @RequestParam(value = "export"          , required = false) boolean export
    ) throws Exception {
        try {
            int newPageNumber           = (pageNumber       == null || pageNumber  <= 0) ? 1   : pageNumber;
            int newPageSize             = (pageSize         == null || pageSize    <= 0) ? 10  : (pageSize > 100 ? 100 : (pageSize < 10 ? 10 : pageSize));
//            int newPrimarySector        = (primarySector    == null) ? 0 : primarySector;
//            int newSecondarySector      = (secondarySector  == null) ? 0 : secondarySector;                      
            int newResourceType         = (resourceType     == null) ? 0 : resourceType;
            int newConsultantType       = (consultantType   == null) ? 0 : consultantType;
            int newDegree               = (degree           == null) ? 0 : degree;

            int newMinXp = (minXp == null || minXp <= 0) ? 0  : minXp;
            int newMaxXp = (maxXp == null || maxXp <= 0) ? 30 : maxXp;    
            
            List<Integer> newPrimarySector= (primarySector == null) ? null: primarySector;             
            List<Integer> newSecondarySector= (secondarySector == null) ? null: secondarySector;                         
            List<Integer> newOtherSector= (otherSector == null) ? null: otherSector; 

            FinalHrUserResponseDto fhurd = new FinalHrUserResponseDto();
            fhurd.setFirstName(firstName);
            fhurd.setMiddleName(middleName);
            fhurd.setLastName(lastName);
            fhurd.setPhoneNumber(phoneNumber);
            fhurd.setAddress(address);
            fhurd.setSectorId(newPrimarySector);
            fhurd.setHrSubSectorId(newSecondarySector);
            fhurd.setHrOtherSectorId(newOtherSector);            
            fhurd.setResourceTypeId(newResourceType);
            fhurd.setConsutlanTypeId(newConsultantType);

            Page<?> page = new PageImpl<>(huf.findAll());

            if (export) {
                return new ResponseEntity<>(
                        page,
                        HttpStatus.OK
                );
            }

            return new ResponseEntity<>(
                    huf.search(
                            newPageNumber,
                            newPageSize,
                            fhurd,
                            newMinXp,
                            newMaxXp,
                            newDegree
                    ),
                    HttpStatus.OK
            );

        } catch (Exception ex) {
            System.out.println("error 3");
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findOne(@PathVariable int id) throws Exception {
        try {
            return new ResponseEntity<>(
                    huf.findOne(id),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @PostMapping
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> save(@Valid @RequestBody FinalHrUserResponseDto fhurd) throws Exception {
        try {
            return new ResponseEntity<>(
                    huf.save(fhurd),
                    HttpStatus.CREATED
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @PutMapping(value = "/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(@PathVariable("id") int id, @Valid @RequestBody FinalHrUserResponseDto fhurd) throws Exception {
        try {
            huf.update(id, fhurd);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> delete(@PathVariable int id) throws Exception {
        try {
            huf.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

}
