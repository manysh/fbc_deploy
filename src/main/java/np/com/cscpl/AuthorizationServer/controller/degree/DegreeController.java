package np.com.cscpl.AuthorizationServer.controller.degree;

import np.com.cscpl.AuthorizationServer.dto.degree.DegreeResponseDto;
import np.com.cscpl.AuthorizationServer.facade.degree.DegreeFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.Optional;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.BASE;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.DEGREE;

@RestController
@RequestMapping(BASE + DEGREE)
@CrossOrigin
public class DegreeController {

    @Autowired
    private DegreeFacade degreeFacade;

    @GetMapping(value = {"/findAll/{pageNumber}/{pageSize}", "/findAll/{pageNumber}", "/findAll"})
    public ResponseEntity<?> findAll(@PathVariable("pageNumber") Optional<Integer> pageNumber,
            @PathVariable("pageSize") Optional<Integer> pageSize,
            @PathParam("search") String search) throws Exception {
        try {
            int newPageNumber = pageNumber.isPresent() ? pageNumber.get() : 1;
            int newPageSize = pageSize.isPresent() ? pageSize.get() : 10;
            newPageNumber = newPageNumber < 0 ? 1 : newPageNumber;
            newPageSize = newPageSize > 1000 ? 1000 : (newPageSize < 10 ? 10 : newPageSize);
            search = search == null ? "" : search;

            return new ResponseEntity<>(
                    degreeFacade.findAll(
                            newPageNumber,
                            newPageSize,
                            search
                    ),
                    HttpStatus.OK);

        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/create")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> save(@Valid @RequestBody DegreeResponseDto degreeResponseDto) throws Exception {
        try {

            return new ResponseEntity<>(
                    degreeFacade.save(degreeResponseDto), HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @PutMapping(value = "/update/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(@PathVariable("id") int id, @Valid @RequestBody DegreeResponseDto degreeResponseDto) throws Exception {
        try {
            return new ResponseEntity<>(degreeFacade.update(id, degreeResponseDto), HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping(value = "/findById/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(degreeFacade.getById(id), HttpStatus.OK);
        } catch (Exception ex) {
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

}
