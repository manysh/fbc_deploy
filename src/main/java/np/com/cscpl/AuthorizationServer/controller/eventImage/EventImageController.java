/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.controller.eventImage;

import javax.validation.Valid;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.EVENT_IMAGE;
import np.com.cscpl.AuthorizationServer.dto.eventImage.EventImageParamsDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import np.com.cscpl.AuthorizationServer.dto.eventImage.EventImageResponseDto;
import np.com.cscpl.AuthorizationServer.facade.eventImage.EventImageFacade;

@RestController
@RequestMapping(EVENT_IMAGE)
@CrossOrigin
public class EventImageController {

    @Autowired
    private EventImageFacade eventImageFacade;

    @GetMapping("/event/{eventId}")
    public ResponseEntity<?> findAll(
            @PathVariable int eventId,
            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "search", required = false) String search
    ) throws Exception {
        try {
            System.out.println(pageNumber);
            System.out.println(pageSize);
            int newPageNumber = (pageNumber == null || pageNumber <= 0) ? 1 : pageNumber;
            int newPageSize = (pageSize == null || pageSize <= 0) ? 10 : (pageSize > 100 ? 100 : (pageSize < 10 ? 10 : pageSize));
          
            return new ResponseEntity<>(
                    eventImageFacade.findAll(
                            eventId,
                            newPageNumber,
                            newPageSize
                    ),
                    HttpStatus.OK
            );

        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> save(@Valid @RequestBody EventImageParamsDto eventImageParamsDto) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    eventImageFacade.save(
                            eventImageParamsDto
                    ),
                    HttpStatus.CREATED
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findOne(@PathVariable int id) throws Exception {
        try {
            return new ResponseEntity<Object>(
                    eventImageFacade.findOne(id),
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> delete(@PathVariable int id) throws Exception {
        try {
            eventImageFacade.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @PutMapping(value="/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(@PathVariable("id") int id,@Valid @RequestBody EventImageResponseDto eventImageResponseDto) throws Exception {
        try {
            eventImageFacade.update(id,eventImageResponseDto);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }
}
