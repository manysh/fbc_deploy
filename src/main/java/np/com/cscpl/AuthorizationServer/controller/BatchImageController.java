package np.com.cscpl.AuthorizationServer.controller;

import io.swagger.annotations.ApiOperation;
import np.com.cscpl.AuthorizationServer.common.ImageUploadHelper;
import np.com.cscpl.AuthorizationServer.dto.batchimage.BatchImageResponseDto;
import np.com.cscpl.AuthorizationServer.dto.batchimage.BatchImagesResponseDto;
import np.com.cscpl.AuthorizationServer.facade.batchimage.BatchImageFacade;
import np.com.cscpl.AuthorizationServer.model.BatchImage;
import np.com.cscpl.AuthorizationServer.othermodel.ImageUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.persistence.EntityNotFoundException;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;

import static np.com.cscpl.AuthorizationServer.common.ConstantData.IMAGE_FILE;
import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;
import static np.com.cscpl.AuthorizationServer.common.ImageUploadHelper.*;

@RestController
@RequestMapping(BASE+BATCH)
@CrossOrigin
public class BatchImageController {

    public static final String IMAGE_FILE_NOT_FOUND="Image file not found.";

    @Autowired
    private BatchImageFacade batchImageFacade;

    @PostMapping(value = UPLOAD_BATCH_IMAGE,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> uploadFile(@RequestParam("image") MultipartFile image)throws Exception {
        try {
            ImageUpload imageUpload=saveImageToServer(image);
            BatchImagesResponseDto dataBaseInformation = new BatchImagesResponseDto();
            if(imageUpload !=null && imageUpload != new ImageUpload()){
                    return new ResponseEntity<Object>(imageUpload, HttpStatus.CREATED);
            }
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @PutMapping("/{imagePath}")
    public ResponseEntity<?> updateBatchImage(@PathVariable("imagePath") String imagePath) throws Exception{
        try {
                BatchImageResponseDto batchImageResponseDto= new BatchImageResponseDto();
                batchImageResponseDto.setImageLink("/batch/get/"+imagePath);
            BatchImagesResponseDto dataBaseInformation=batchImageFacade.save(batchImageResponseDto);
            if(dataBaseInformation!=null){
                return new ResponseEntity<Object>(HttpStatus.OK);
            }
            return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping(value = "/image/new/upload",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> uploadImage(@RequestParam("image") MultipartFile image)throws Exception {
        try {
            ImageUpload imageUpload=saveImageToServer(image);
            if(imageUpload!=null && imageUpload != new ImageUpload()){
                return new ResponseEntity<Object>(imageUpload,HttpStatus.OK);
            }
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @GetMapping(GET+"/{imageName}")
    public ResponseEntity<byte[]> uploadFile(@PathVariable("imageName") String imageName)throws Exception{
        File imageFile = new File(getImageLocation(imageName));
        if((!imageFile.exists()) || imageFile.isDirectory()){
            throw new EntityNotFoundException("Image not found.");
        }

        String extension = imageName.substring(imageName.lastIndexOf(".") + 1, imageName.length());

        BufferedImage imagefile = ImageIO.read(imageFile);
        ByteArrayOutputStream BAOS = new ByteArrayOutputStream();
        ImageIO.write(imagefile, extension, BAOS);
        byte[] imageByteArray=BAOS.toByteArray();


        return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(imageByteArray);
    }


    @DeleteMapping(DELETE+"{imageName}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteImage(@PathVariable("imageName") String imageName) throws Exception{
        try {
            if(deleFile(imageName,IMAGE_FILE)){
                return new ResponseEntity<Object>(null,HttpStatus.OK);
            }
            else{
                return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
            }
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/get/batch-image")
    @ApiOperation("This endpoint return image path of batch image")
    public ResponseEntity<?> getBatchImage() throws Exception{
        try {
            return new ResponseEntity<Object>(batchImageFacade.findFirst(),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }

}
