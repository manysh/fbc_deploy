package np.com.cscpl.AuthorizationServer.controller.resourcetype;

import io.swagger.annotations.ApiOperation;
import np.com.cscpl.AuthorizationServer.dto.resourcetype.ResourceTypeResourceDto;
import np.com.cscpl.AuthorizationServer.facade.resourcetype.ResourceTypeFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import java.util.Optional;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;
import static np.com.cscpl.AuthorizationServer.controller.client.Documentation.FIND_ALL_DOCS;

@RestController
@RequestMapping(BASE+RESOURCE_TYPE)
@CrossOrigin
public class ResourceTypeController {

    @Autowired
    private ResourceTypeFacade resourceTypeFacade;

    @GetMapping(value={"/findAll/{pageNumber}/{pageSize}","/findAll/{pageNumber}","/findAll"})
    public ResponseEntity<?> findAll(@PathVariable("pageNumber") Optional<Integer> pageNumber,
                                     @PathVariable("pageSize") Optional<Integer> pageSize,
                                     @PathParam("search") String search)throws Exception{
        try {
            int newPageNumber = pageNumber.isPresent() ? pageNumber.get() : 1;
            int newPageSize = pageSize.isPresent() ? pageSize.get() : 10;
            newPageNumber = newPageNumber < 0 ? 1 : newPageNumber;
            newPageSize = newPageSize > 100 ? 100 : (newPageSize < 10 ? 10 : newPageSize);
            search=search==null?"":search;
            return new ResponseEntity<Object>(resourceTypeFacade.findAll(newPageNumber,
                    newPageSize, search),
                    HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());

            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping(value="/create")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> save(@Valid @RequestBody ResourceTypeResourceDto resourceTypeResourceDto ) throws Exception{
        try {

            return new ResponseEntity<Object>(
                    resourceTypeFacade.save(resourceTypeResourceDto), HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }
    @PutMapping(value ="/update/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?>  update(@PathVariable("id") int id, @Valid @RequestBody ResourceTypeResourceDto resourceTypeResourceDto )throws Exception {
        try {

            return new ResponseEntity<Object>(resourceTypeFacade.update(id,resourceTypeResourceDto),HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping(value ="/findById/{id}")
    public ResponseEntity<?> getById(@PathVariable("id")int id){
        try {
            return new ResponseEntity<Object>(resourceTypeFacade.getById(id),HttpStatus.OK);
        }catch (Exception ex){
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }


    @GetMapping
    public ResponseEntity<?> getAll() throws Exception{
        try {
            return new ResponseEntity<Object>(resourceTypeFacade.findAll(),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }




}
