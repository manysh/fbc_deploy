package np.com.cscpl.AuthorizationServer.controller;


import np.com.cscpl.AuthorizationServer.facade.province.ProvienceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;

@RestController
@RequestMapping(PROVIENCE)
public class ProvienceController {

    @Autowired
    private ProvienceFacade provienceFacade;

    @GetMapping
    public ResponseEntity<?> findAll() throws Exception{
        try {
            return new ResponseEntity<Object>(provienceFacade.findAll(),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
