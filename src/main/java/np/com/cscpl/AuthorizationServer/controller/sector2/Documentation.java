package np.com.cscpl.AuthorizationServer.controller.sector2;

public class Documentation {
    public static final String GET_MAP_DOC="This end point returns all sector2";
    public static final String GET_SEC2_BY_PROJECT_ID_DOC="This end point returns sec2 of specfic project";
    public static final String CREATE_SEC2_DOCS="This end point create new sec2 ";
    public static final String FILTERED_RESULT_DOCS="This end point returns filtered result by title and paged result.";
    public static final String FIND_BY_ID="This end point returns specific sector 2 by id.";
    public static final String UPDATE_SEC2_DOCS="This end point update sec2 for specific id.";
}
