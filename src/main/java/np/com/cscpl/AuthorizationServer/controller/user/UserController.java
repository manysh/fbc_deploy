package np.com.cscpl.AuthorizationServer.controller.user;

import io.swagger.annotations.ApiOperation;
import np.com.cscpl.AuthorizationServer.dto.user.UserCreateDto;
import np.com.cscpl.AuthorizationServer.facade.user.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import java.util.Optional;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;
import np.com.cscpl.AuthorizationServer.model.User;

@RestController
@RequestMapping(BASE + USER)
public class UserController {

    private UserFacade userFacade;

    @Autowired
    public UserController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @PostMapping
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> create(@Valid @RequestBody UserCreateDto userCreateDto) throws Exception {
        try {
//            userCreateDto.setPassword("P@ssw0rd");
            return new ResponseEntity<Object>(userFacade.save(userCreateDto), HttpStatus.CREATED);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping(USER_STATUS)
    public ResponseEntity<?> findUserStatus(@PathParam("userName") String userName) throws Exception {
        try {
            boolean status = userFacade.findByUserName(userName);
            if (status) {
                return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<Object>(HttpStatus.OK);
            }
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(GET_USER_BY_ID)
    @ApiOperation("This endpoint returns specific user to given user id")
    public ResponseEntity<?> findById(@PathVariable long userId) throws Exception {
        try {
            return new ResponseEntity<Object>(userFacade.findById(userId), HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);

        }
    }

    @GetMapping(value = {"/findAll/{pageNumber}/{pageSize}", "/findAll/{pageNumber}", "/findAll"})
    public ResponseEntity<?> findAll(@PathVariable("pageNumber") Optional<Integer> pageNumber,
            @PathVariable("pageSize") Optional<Integer> pageSize,
            @PathParam("search") String search) throws Exception {
        try {
            int newPageNumber = pageNumber.isPresent() ? pageNumber.get() : 1;
            int newPageSize = pageSize.isPresent() ? pageSize.get() : 10;
            newPageNumber = newPageNumber < 0 ? 1 : newPageNumber;
            newPageSize = newPageSize > 100 ? 100 : (newPageSize < 10 ? 10 : newPageSize);
            search = search == null ? "" : search;
            return new ResponseEntity<Object>(userFacade.findAll(newPageNumber,
                    newPageSize, search),
                    HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());

            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/{id}")
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(@PathVariable("id") Long id, @Valid @RequestBody UserCreateDto userCreateDto) throws Exception {
        try {
            int updateStatus = userFacade.update(id, userCreateDto);                   
            if (updateStatus == 1) {
                return new ResponseEntity<>("{\"status\":1}",HttpStatus.OK);
            }else if(updateStatus==2){
                return new ResponseEntity<>("{\"status\":2}",HttpStatus.OK);
            }
            return new ResponseEntity<>("{\"status\":3}",HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

}
