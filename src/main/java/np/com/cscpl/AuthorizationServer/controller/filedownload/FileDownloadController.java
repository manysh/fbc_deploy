package np.com.cscpl.AuthorizationServer.controller.filedownload;

import io.swagger.annotations.ApiOperation;
import np.com.cscpl.AuthorizationServer.common.ImageUploadHelper;
import np.com.cscpl.AuthorizationServer.dto.batchimage.BatchImageResponseDto;
import np.com.cscpl.AuthorizationServer.dto.batchimage.BatchImagesResponseDto;
import np.com.cscpl.AuthorizationServer.dto.filedownload.FileDownloadResponseDto;
import np.com.cscpl.AuthorizationServer.facade.filedownload.FileDownloadFacade;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.websocket.server.PathParam;

import java.util.Optional;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;

@RestController
@RequestMapping(BASE+FILE_INFORMATION)
@CrossOrigin
public class FileDownloadController {

    private FileDownloadFacade fileDownloadFacade;

    @Autowired
    public FileDownloadController(FileDownloadFacade fileDownloadFacade) {
        this.fileDownloadFacade = fileDownloadFacade;
    }

    @GetMapping(FILE_DOWNLOAD_GET)
    @ApiOperation("This endpoint return list of filtered paged FileDownload Result")
    public ResponseEntity<?> filteredResult(
            @PathVariable("pageNumber")Optional<Integer> pageNumber,
            @PathVariable("pageSize") Optional<Integer> pageSize,
            @PathParam("search") String search)
        throws Exception{
        int newPageNumber=pageNumber.isPresent()?pageNumber.get():1;
        int newPageSize=pageSize.isPresent()?pageSize.get():10;
        newPageNumber=newPageNumber<0?1:newPageNumber;
        newPageSize=newPageSize>100?100:(newPageSize<10?10:newPageSize);
        search=search==null?"":search;
        try {
            return new ResponseEntity<Object>( fileDownloadFacade.fiteredResult(
                    newPageNumber,
                    newPageSize,
                    search
            ),HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> saveFIleInformation(@Valid @RequestBody FileDownloadResponseDto fileDownloadResponseDto) throws Exception{
        try {
            return new ResponseEntity<Object>(fileDownloadFacade.save(fileDownloadResponseDto),HttpStatus.CREATED);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }

    @GetMapping(GET_FILE_BY_ID)
    public ResponseEntity<?> findById(@PathVariable("id") int id) throws Exception{
        try {
            return new ResponseEntity<Object>(fileDownloadFacade.findById(id),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }
    @PutMapping(UPDATE+UPDATE_BY_ID)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> updateById(@PathVariable("id") int id,@Valid @RequestBody FileDownloadResponseDto fileDownloadResponseDto) throws Exception{
        try {
            return new ResponseEntity<Object>(fileDownloadFacade.update(id,fileDownloadResponseDto),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new NullPointerException(ex.getLocalizedMessage());
        }
    }
}
