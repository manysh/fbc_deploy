package np.com.cscpl.AuthorizationServer.controller.agency;


import io.swagger.annotations.ApiOperation;
import np.com.cscpl.AuthorizationServer.dto.agency.FundingAgencyResponseDto;
import np.com.cscpl.AuthorizationServer.facade.agency.FundingAgencyFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.print.Doc;
import javax.validation.Valid;

import java.util.Optional;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;


@RestController
@RequestMapping(BASE+AGENCY)
@CrossOrigin
public class FundingAgencyController {
    private FundingAgencyFacade fundingAgencyFacade;

    @Autowired
    public FundingAgencyController(FundingAgencyFacade fundingAgencyFacade) {
        this.fundingAgencyFacade = fundingAgencyFacade;
    }

    @GetMapping
    @ApiOperation(Documentation.GET_MAP_DOC)
    public ResponseEntity<?> findAll() throws Exception{
        try {
            return new ResponseEntity<Object>(fundingAgencyFacade.findAll(),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> findByProjectId(@PathVariable int id)throws Exception{
        try {
            return new ResponseEntity<Object>(fundingAgencyFacade.findByProjectId(id),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> create(@Valid @RequestBody FundingAgencyResponseDto fundingAgencyResponseDto)throws Exception{
        try {
            return new ResponseEntity<Object>(fundingAgencyFacade.create(
                    fundingAgencyResponseDto
            ),HttpStatus.CREATED);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping(AGENCY_SEARCH_URL)
    public ResponseEntity<?> search(
            @PathVariable(PAGE_NUMBER) int pageNumber,
            @PathVariable(PAGE_SIZE) int pageSize,
            @RequestParam(SEARCH)Optional<String> search
            )throws Exception{
        try {
            return new ResponseEntity<Object>(fundingAgencyFacade.filteredResult(
                    pageNumber<0?1:pageNumber,
                    pageSize<10?10:(pageSize>100?100:pageSize),
                    search.isPresent()?search.get():""
            ),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(FIND_BY_ID_URL)
    @ApiOperation(Documentation.FIND_BY_ID_DOC)
    public ResponseEntity<?> findById(@PathVariable("id") int id) throws Exception{
        try {
            return new ResponseEntity<Object>(fundingAgencyFacade.findById(id),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(AGENCY_UPDATE)
    @ApiOperation(Documentation.UPDATE_FUNDING_AGENCY)
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> update(
            @PathVariable("id") int id,
            @Valid @RequestBody FundingAgencyResponseDto fundingAgencyResponseDto
    ) throws Exception{
        try {
            return new ResponseEntity<Object>(fundingAgencyFacade.update(id,fundingAgencyResponseDto),HttpStatus.OK);
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
