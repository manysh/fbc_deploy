package np.com.cscpl.AuthorizationServer.controller.menu;

import io.swagger.annotations.ApiOperation;
import np.com.cscpl.AuthorizationServer.dto.menu.MenuResponseDto;
import np.com.cscpl.AuthorizationServer.facade.menu.MenuFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.print.Doc;
import javax.validation.Valid;

import java.util.Optional;

import static np.com.cscpl.AuthorizationServer.common.EndpointMapping.*;

@RestController
@RequestMapping(BASE+MENU)
@CrossOrigin
public class MenuController {
    @Autowired
    private MenuFacade menuFacade;
    @GetMapping
    @ApiOperation(Documentation.MENU_FIND_ALL_DOCS)
    public ResponseEntity<?> findAll() throws Exception{
        try {
            return new ResponseEntity<Object>(menuFacade.findAll(),HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    @ApiOperation(Documentation.MENU_CREATE_DOCS)
    public ResponseEntity<?> save(@Valid @RequestBody MenuResponseDto menuResponseDto) throws Exception{
        try {
            return new ResponseEntity<Object>(
                    menuFacade.save(
                            menuResponseDto
                    ),
                    HttpStatus.OK
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            throw new IllegalArgumentException(ex.getLocalizedMessage());
        }
    }

    @GetMapping(MENU_SEARCH_URL)
    @ApiOperation(Documentation.MENU_SEARCH_DOCS)
    public ResponseEntity<?> search(
            @PathVariable("pageNumber") int pageNumber,
            @PathVariable("pageSize") int pageSize,
            @RequestParam("search")Optional<String> search
            ) throws Exception{
        try {
            return new ResponseEntity<Object>(
                    menuFacade.menuSearch(
                            pageNumber==0?1:pageNumber,
                            pageSize<=0?10:(pageSize>100?100:pageSize),
                            search.isPresent()?search.get():""
                    ),
                    HttpStatus.OK
            );
        }catch (Exception ex){
            System.out.println(ex.getLocalizedMessage());
            return new ResponseEntity<Object>(null,HttpStatus.BAD_REQUEST);
        }
    }
}
