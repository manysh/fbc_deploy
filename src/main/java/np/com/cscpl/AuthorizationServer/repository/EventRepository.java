package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.model.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {

    Page<Event> findAllByOrderByEventDateDesc(Pageable pageable) throws Exception;

    Page<Event> findByEventType_TypeIgnoreCaseContainingOrderByEventDateAsc(String eventType, Pageable pageable) throws Exception;

    @Query("SELECT E FROM Event E JOIN E.eventType ET where E.name LIKE CONCAT('%',:keyword,'%') OR E.eventDate LIKE CONCAT('%',:keyword,'%') OR E.eventType.type LIKE CONCAT('%',:keyword,'%') order by E.eventDate Desc")
    Page<Event> search(@Param("keyword") String keyword, Pageable pageable) throws Exception;

    Page<Event> findByNameIgnoreCaseContaining(String name, Pageable pageable) throws Exception;

    Page<Event> findByIsShowTrue(Pageable pageable) throws Exception;
    
    Page<Event> findByEgIsShowTrueOrderByEventDateDesc(Pageable pageable) throws Exception;
}
