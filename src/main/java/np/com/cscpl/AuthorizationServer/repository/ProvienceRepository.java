package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.model.Provience;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProvienceRepository extends JpaRepository<Provience,Integer> {
    Provience findById(int id) throws Exception;
}
