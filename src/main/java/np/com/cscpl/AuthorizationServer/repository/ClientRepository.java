package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.model.Client;
import np.com.cscpl.AuthorizationServer.model.Sector;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;



public interface ClientRepository extends JpaRepository<Client, Integer> {
    Page<Client> findByClientNameIgnoreCaseContaining(String name, Pageable pageable) throws Exception;


}
