package np.com.cscpl.AuthorizationServer.repository.Degree;

import np.com.cscpl.AuthorizationServer.model.Degree;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DegreeRepository extends JpaRepository<Degree, Integer> {

    Page<Degree> findByNameIgnoreCaseContaining(String name, Pageable pageable) throws Exception;

    Degree findById(int id) throws Exception;

}
