/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.model.HrSector;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Puzan Sakya < puzan at puzansakya@gmail.com >
 */
public interface HrSectorRepository extends JpaRepository<HrSector, Integer> {

    Page<HrSector> findByIsOtherFalseAndIsSubFalse(Pageable pageable) throws Exception;

    Page<HrSector> findByIsOtherTrue(Pageable pageable) throws Exception;

    Page<HrSector> findByIsSubTrue(Pageable pageable) throws Exception;

    @Query("SELECT H FROM HrSector H where H.name LIKE CONCAT('%',:keyword,'%') AND H.isOther=false AND isSub=false")
    Page<HrSector> searchSector(@Param("keyword") String keyword, Pageable pageable) throws Exception;

    @Query("SELECT H FROM HrSector H where H.name LIKE CONCAT('%',:keyword,'%') AND H.isOther=false AND isSub=true")
    Page<HrSector> searchSubSector(@Param("keyword") String keyword, Pageable pageable) throws Exception;

    @Query("SELECT H FROM HrSector H where H.name LIKE CONCAT('%',:keyword,'%') AND H.isOther=true AND isSub=false")
    Page<HrSector> searchOtherSector(@Param("keyword") String keyword, Pageable pageable) throws Exception;

    @Query("SELECT HR FROM HrSector HR WHERE HR.id=?1")
    HrSector findBySectorId(int id) throws Exception;

}
