package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.model.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Integer> {
    List<Tag> findByFinalProjects_id(int id) throws Exception;
    List<Tag> findByIdIn(List<Integer> ids) throws Exception;
    @Query("SELECT T.id FROM Tag T")
    List<Integer> getAllTagIds() throws Exception;

    @Query("SELECT T FROM Tag T JOIN T.finalProjects FP WHERE FP.id IN (:projectIds) GROUP BY T.id")
    List<Tag> getTagByProjectIds(@Param("projectIds") List<Integer> projectIds) throws Exception;

    Page<Tag> findByTagIgnoreCaseContaining(String tag, Pageable pageable) throws Exception;
}
