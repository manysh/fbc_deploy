package np.com.cscpl.AuthorizationServer.repository.finalproject;

import np.com.cscpl.AuthorizationServer.model.FinalProject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import org.springframework.data.repository.query.Param;

@Repository
@Transactional
public interface FinalProjectRepository extends JpaRepository<FinalProject, Integer>, JpaSpecificationExecutor<FinalProject> {

    @Query("SELECT FP FROM FinalProject FP JOIN FP.districts D where D.id in ?1")
    Page<FinalProject> searchProject(List<Integer> ids, Pageable pageable) throws Exception;

    /*EXTENSIVE SEARCH
    * PARAMEtER INCLUDES ARE
    * (Location, tags, funding agencies, sector, sector2, sector3, client1, client2,title, from to range)
    * */
    @Query(
            "SELECT FP FROM FinalProject FP"
            + " JOIN FP.districts D JOIN FP.fundingAgencies FA "
            + "WHERE D.id in ?1 AND FA.id in ?2"
    )
    List<FinalProject> extensiveSearch(List<Integer> districtIds, List<Integer> agencyIds) throws Exception;

    /*
    * MASTER SEARCH OF THE PROJECT
    * THIS METHOD RETURNS PAGED LIST OF PROJECT WITH SECTOR
    * METHOD TO USE WHEN SECTOR 3 AND SECTOR 2 BOTH ARE PRESENT
    * */
    @Query("SELECT FP FROM FinalProject  FP JOIN FP.districts D "
            + "JOIN FP.tags T "
            + "JOIN FP.fundingAgencies FA "
            + "JOIN FP.client1 C1 "
            + "LEFT JOIN FP.client2 C2 "
            + "JOIN FP.sector1 S1 "
            + "LEFT JOIN FP.sector2s S2 "
            + "LEFT JOIN FP.sector3s S3 "
            + "LEFT JOIN D.province P "
            + "WHERE (FP.title LIKE CONCAT('%',?1,'%') "
            + "OR FP.assignmentTitle LIKE CONCAT('%',?1,'%') "
            + "OR D.districtName LIKE CONCAT('%',?1,'%') "
            + "OR FP.description LIKE CONCAT('%',?1,'%') "
            + " OR P.name LIKE  CONCAT('%',?1,'%') "
            + "OR FP.workDescription LIKE CONCAT('%',?1,'%') "
            + "OR FA.name LIKE CONCAT('%',?1,'%') "
            + "OR T.tag LIKE CONCAT('%',?1,'%') "
            + ") AND (T.id IN ?2)"
            + " AND (FA.id IN ?3) AND (?4 =0 OR C1.id =?4 ) "
            + "AND (?5 =0 OR C2.id = ?5) AND (?6 = 0 OR S1.id=?6) "
            + "AND (S2.id IN ?7) AND (S3.id IN ?8) "
            + "AND (D.id IN ?9) AND (?10 IS NULL OR ?10 <= FP.createdDate) "
            + "AND FP.projectStatus LIKE CONCAT('%',?12,'%') "
            + "AND FP.projectStatus <> ?13 "
            + "AND (?11 IS NULL OR ?11 >= FP.createdDate) "
            + "GROUP BY FP.id ORDER BY FP.projectStatus DESC, CASE WHEN FP.projectStatus='Ongoing' THEN FP.createdDate  ELSE FP.completetionDate END DESC")
    Page<FinalProject> extensiveSearch(
            String title,
            List<Integer> tagIds,
            List<Integer> agencyIDs,
            int client1Id,
            int client2Id,
            int sectorId,
            List<Integer> sector2Ids,
            List<Integer> sector3Ids,
            List<Integer> districtIds,
            String from,
            String to,
            String projectStatus,
            String shortListed,
            Pageable pageable
    ) throws Exception;

    /**
     * METHOD TO USE WHEN SECTOR 2 IS NULL
     */
    @Query("SELECT FP FROM FinalProject  FP JOIN FP.districts D "
            + "JOIN FP.tags T "
            + "JOIN FP.fundingAgencies FA "
            + "JOIN FP.client1 C1 "
            + "LEFT JOIN FP.client2 C2 "
            + "JOIN FP.sector1 S1 "
            + "LEFT JOIN FP.sector2s S2 "
            + "LEFT JOIN FP.sector3s S3 "
            + "LEFT JOIN D.province P "
            + "WHERE (FP.title LIKE CONCAT('%',?1,'%') "
            + "OR FP.assignmentTitle LIKE CONCAT('%',?1,'%') "
            + "OR D.districtName LIKE CONCAT('%',?1,'%') "
            + "OR FP.description LIKE CONCAT('%',?1,'%') "
            + " OR P.name LIKE  CONCAT('%',?1,'%') "
            + "OR FP.workDescription LIKE CONCAT('%',?1,'%') "
            + "OR FA.name LIKE CONCAT('%',?1,'%') "
            + "OR T.tag LIKE CONCAT('%',?1,'%') "
            + ") "
            + "AND (T.id IN ?2)"
            + " AND (FA.id IN ?3) AND (?4 =0 OR C1.id =?4 ) "
            + "AND (?5 =0 OR C2.id = ?5) AND (?6 = 0 OR S1.id=?6) "
            + "AND (S3.id IN ?7) "
            + "AND (D.id IN ?8) AND (?9 IS NULL OR ?9 <= FP.createdDate) "
            + "AND FP.projectStatus LIKE CONCAT('%',?11,'%') "
            + "AND FP.projectStatus <> ?12 "
            + "AND (?10 IS NULL OR ?10 >= FP.createdDate) "
            + "GROUP BY FP.id ORDER BY FP.projectStatus DESC, CASE WHEN FP.projectStatus='Ongoing' THEN FP.createdDate  ELSE FP.completetionDate END DESC ")
    Page<FinalProject> extensiveSearchSec2IsNull(
            String title,
            List<Integer> tagIds,
            List<Integer> agencyIDs,
            int client1Id,
            int client2Id,
            int sectorId,
            List<Integer> sector3Ids,
            List<Integer> districtIds,
            String from,
            String to,
            String projectStatus,
            String shortListed,
            Pageable pageable
    ) throws Exception;

    /*
    METHOD TO USE WHEN SECTOR 3 IS NULL
     */
    @Query("SELECT FP FROM FinalProject  FP JOIN FP.districts D "
            + "JOIN FP.tags T "
            + "JOIN FP.fundingAgencies FA "
            + "JOIN FP.client1 C1 "
            + "LEFT JOIN FP.client2 C2 "
            + "JOIN FP.sector1 S1 "
            + "LEFT JOIN FP.sector2s S2 "
            + "LEFT JOIN FP.sector3s S3 "
            + "LEFT JOIN D.province P "
            + "WHERE (FP.title LIKE CONCAT('%',?1,'%') "
            + "OR FP.assignmentTitle LIKE CONCAT('%',?1,'%') "
            + "OR D.districtName LIKE CONCAT('%',?1,'%') "
            + " OR P.name LIKE  CONCAT('%',?1,'%') "
            + "OR FP.description LIKE CONCAT('%',?1,'%') "
            + "OR FP.workDescription LIKE CONCAT('%',?1,'%') "
            + "OR FA.name LIKE CONCAT('%',?1,'%') "
            + "OR T.tag LIKE CONCAT('%',?1,'%') "
            + ") "
            + "AND (T.id IN ?2)"
            + " AND (FA.id IN ?3) AND (?4 =0 OR C1.id =?4 ) "
            + "AND (?5 =0 OR C2.id = ?5) AND (?6 = 0 OR S1.id=?6) "
            + "AND (S2.id IN ?7) "
            + "AND (D.id IN ?8) AND (?9 IS NULL OR ?9 <= FP.createdDate) "
            + "AND FP.projectStatus LIKE CONCAT('%',?11,'%') "
            + "AND FP.projectStatus <> ?12 "
            + "AND (?10 IS NULL OR ?10 >= FP.createdDate) "
            + "GROUP BY FP.id ORDER BY FP.projectStatus DESC, CASE WHEN FP.projectStatus='Ongoing' THEN FP.createdDate  ELSE FP.completetionDate END DESC ")
    Page<FinalProject> extensiveSearchSec3IsNull(
            String title,
            List<Integer> tagIds,
            List<Integer> agencyIDs,
            int client1Id,
            int client2Id,
            int sectorId,
            List<Integer> sector2Ids,
            List<Integer> districtIds,
            String from,
            String to,
            String projectStatus,
            String shortListed,
            Pageable pageable
    ) throws Exception;


    /*
    METHOD TO USE WHEN BOTH SECTOR3 AND SECTOR 2 IS NULL
     */
    @Query("SELECT FP FROM FinalProject  FP JOIN FP.districts D "
            + "JOIN FP.tags T "
            + "JOIN FP.fundingAgencies FA "
            + "JOIN FP.client1 C1 "
            + "LEFT JOIN FP.client2 C2 "
            + "JOIN FP.sector1 S1 "
            + "LEFT JOIN FP.sector2s S2 "
            + "LEFT JOIN FP.sector3s S3 "
            + "LEFT JOIN D.province P "
            + "WHERE (FP.title LIKE CONCAT('%',?1,'%') "
            + "OR FP.assignmentTitle LIKE CONCAT('%',?1,'%') "
            + "OR D.districtName LIKE CONCAT('%',?1,'%') "
            + " OR P.name LIKE  CONCAT('%',?1,'%') "
            + "OR FP.description LIKE CONCAT('%',?1,'%') "
            + "OR FP.workDescription LIKE CONCAT('%',?1,'%') "
            + "OR FA.name LIKE CONCAT('%',?1,'%') "
            + "OR T.tag LIKE CONCAT('%',?1,'%') "
            + ") "
            + "AND (T.id IN ?2)"
            + " AND (FA.id IN ?3) AND (?4 =0 OR C1.id =?4 ) "
            + "AND (?5 =0 OR C2.id = ?5) AND (?6 = 0 OR S1.id=?6) "
            + "AND (D.id IN ?7) AND (?8 IS NULL OR ?8 <= FP.createdDate) "
            + "AND FP.projectStatus LIKE CONCAT('%',?10,'%') "
            + "AND FP.projectStatus <> ?11 "
            + "AND (?9 IS NULL OR ?9 >= FP.createdDate) "
            + "GROUP BY FP.id ORDER BY FP.projectStatus DESC, CASE WHEN FP.projectStatus='Ongoing' THEN FP.createdDate  ELSE FP.completetionDate END DESC ")
    Page<FinalProject> extensiveSearchSec3AndSec2IsNull(
            String title,
            List<Integer> tagIds,
            List<Integer> agencyIDs,
            int client1Id,
            int client2Id,
            int sectorId,
            List<Integer> districtIds,
            String from,
            String to,
            String projectStatus,
            String shortListed,
            Pageable pageable
    ) throws Exception;

    FinalProject findById(int id) throws Exception;

    /*
    * METHOD TO GET ONGOING OR COMPLETED PROJECT BASED ON VALUE
    * PROJECT STATUS VALUE ARE (Ongoing or Completed)
    * PROJECT HOME PAGE PURPOSE
    * */
    List<FinalProject> findByProjectStatusIgnoreCaseContainingOrderByCompletetionDate(
            String projectStatus,
            Pageable pageable
    ) throws Exception;

    @Query("SELECT FP FROM FinalProject FP JOIN  FP.tags t  where FP.title LIKE CONCAT('%',:keyword,'%') OR FP.assignmentTitle LIKE CONCAT('%',:keyword,'%') OR FP.description LIKE CONCAT('%',:keyword,'%')OR FP.workDescription LIKE CONCAT('%',:keyword,'%') OR FP.createdDate LIKE  CONCAT('%',:keyword,'%')  OR  t.tag LIKE CONCAT('%',:keyword,'%') GROUP BY FP.id ORDER BY FP.projectStatus DESC,FP.createdDate DESC, FP.completetionDate DESC")
    Page<FinalProject> keySearch(@Param("keyword") String keyword, Pageable pageable) throws Exception;

    @Query("SELECT FP FROM FinalProject FP WHERE FP.projectStatus LIKE CONCAT('%',?1,'%') "
            + "GROUP BY FP.id ORDER BY FP.projectStatus DESC, CASE WHEN FP.projectStatus='Ongoing' THEN FP.createdDate  ELSE FP.completetionDate END DESC")
    List<FinalProject> findAllByStatus(String projectStatus) throws Exception;

}
