package np.com.cscpl.AuthorizationServer.repository.filedownlaod;

import np.com.cscpl.AuthorizationServer.model.FileDownload;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileDownloadRepository extends JpaRepository<FileDownload,Integer> {
    Page<FileDownload> findByFileNameIgnoreCaseContainingOrderByCreatedAtDesc(String fileName, Pageable pageable) throws Exception;
    FileDownload findById(int id) throws Exception;
}
