/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.model.EventType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author puzansakya
 */
@Repository
public interface EventTypeRepository extends JpaRepository<EventType, Integer> {

}
