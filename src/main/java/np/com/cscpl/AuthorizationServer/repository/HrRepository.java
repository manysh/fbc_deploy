package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.model.FinalHrUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface HrRepository extends JpaRepository<FinalHrUser, Integer>, JpaSpecificationExecutor<FinalHrUser> {

//    Page<HrUser> findAllByFullNameIgnoreCaseContaining(String name, Pageable pageable) throws Exception;

}
