package np.com.cscpl.AuthorizationServer.repository.sector3;

import np.com.cscpl.AuthorizationServer.model.Sector3;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface Sector3Repository  extends JpaRepository<Sector3,Integer> {
    List<Sector3> findAllByOrderByTitleAsc() throws Exception;
    List<Sector3> findByFinalProjects_id(int id) throws Exception;
    List<Sector3> findByIdIn(List<Integer> ids) throws Exception;


    @Query("SELECT S.id FROM Sector3 S")
    List<Integer> fidAllIds() throws Exception;

    Page<Sector3> findByTitleIgnoreCaseContaining(String title,Pageable pageable)throws Exception;

    Sector3 findById(int id) throws Exception;
}
