package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long>{

    @Query("SELECT DISTINCT user FROM User user "
            + "INNER JOIN FETCH user.authorities AS authorities "
            + "WHERE user.username = :username")
    User findByUsername(@Param("username") String username);

    User findById(long id) throws Exception;

    Page<User> findByFullNameIgnoreCaseContaining(String name, Pageable pageable) throws Exception;

}
