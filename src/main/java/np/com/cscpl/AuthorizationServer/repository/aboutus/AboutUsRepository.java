package np.com.cscpl.AuthorizationServer.repository.aboutus;

import np.com.cscpl.AuthorizationServer.model.AboutUs;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AboutUsRepository extends JpaRepository<AboutUs,Integer> {
  @Query("select A from AboutUs A where A.introduction1 LIKE  CONCAT('%',:keyword,'%') OR" +
          " A.introduction2 LIKE  CONCAT('%',:keyword,'%') OR" +
          " A.introduction3 LIKE  CONCAT('%',:keyword,'%') OR " +
          "A.title1 LIKE  CONCAT('%',:keyword,'%') OR " +
          "A.title1Description LIKE  CONCAT('%',:keyword,'%') OR" +
          " A.title2 LIKE  CONCAT('%',:keyword,'%') OR" +
          " A.title2Description LIKE  CONCAT('%',:keyword,'%')")
      Page<AboutUs> keySearch(@Param("keyword")String keyword, Pageable pageable);
}
