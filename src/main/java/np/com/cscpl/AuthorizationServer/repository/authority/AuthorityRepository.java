package np.com.cscpl.AuthorizationServer.repository.authority;

import np.com.cscpl.AuthorizationServer.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AuthorityRepository extends JpaRepository<Authority,Long> {

    List<Authority> findByIdIn(List<Long> ids) throws Exception;
}
