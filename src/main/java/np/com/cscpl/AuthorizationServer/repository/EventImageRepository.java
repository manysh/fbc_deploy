package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.model.EventImage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EventImageRepository extends JpaRepository<EventImage, Integer> {

    //  Page<EventImage> findAllByOrderByEventDateDesc(Pageable pageable) throws Exception;
    @Query("SELECT EI FROM EventImage EI where EI.event.id = ?1")
    Page<EventImage> findAll(int eventId, Pageable pageable) throws Exception;
   
}
