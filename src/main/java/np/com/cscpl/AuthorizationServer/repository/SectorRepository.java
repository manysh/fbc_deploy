package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.model.Sector;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import org.springframework.data.repository.query.Param;

public interface SectorRepository extends JpaRepository<Sector, Integer> {

    Page<Sector> findAllByOrderByNameAsc(Pageable pageable) throws Exception;

    @Query("select S from Sector S where S.name like CONCAT('%',:name,'%') ORDER BY S.name ASC ")
    Page<Sector> findByNameIgnoreCaseContaining(@Param("name") String name, Pageable pageable) throws Exception;

    List<Sector> findByIdIn(List<Integer> ids) throws Exception;

    List<Sector> findByFinalProjects_id(int projectId) throws Exception;

    @Query("SELECT S.id FROM Sector S")
    List<Integer> fidAllIds() throws Exception;

    @Modifying
    @Query("UPDATE Sector S SET S.displayOrder=S.displayOrder+1 WHERE S.displayOrder>=?1")
    void updateDisplayOrder(int id) throws Exception;

    @Query("SELECT S FROM Sector S where S.name like CONCAT('%',:keyword,'%') OR S.detail like CONCAT('%',:keyword,'%')")
    Page<Sector> keySearch(@Param("keyword") String keyword, Pageable pageable) throws Exception;
}
