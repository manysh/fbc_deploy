package np.com.cscpl.AuthorizationServer.repository.resourcetype;

import np.com.cscpl.AuthorizationServer.model.ResourceType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResourceTypeRepository extends JpaRepository<ResourceType,Integer> {
      Page<ResourceType> findByResourceTypeNameIgnoreCaseContaining(String name, Pageable pageable);
}
