/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.repository.userSearch;

import np.com.cscpl.AuthorizationServer.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author Puzan Sakya < puzan at puzansakya@gmail.com >
 */
public interface UserSearchRepository {
    
      Page<User> search(String name, Pageable pageable) throws Exception;
    
}
