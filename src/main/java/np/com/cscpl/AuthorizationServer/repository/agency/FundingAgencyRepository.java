package np.com.cscpl.AuthorizationServer.repository.agency;

import np.com.cscpl.AuthorizationServer.model.FinalProject;
import np.com.cscpl.AuthorizationServer.model.FundingAgency;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FundingAgencyRepository extends JpaRepository<FundingAgency,Integer>{
    List<FundingAgency> findByFinalProjects_id(int id) throws Exception;
    List<FundingAgency> findByIdIn(List<Integer> ids) throws Exception;


    @Query("SELECT FA.id FROM FundingAgency FA")
    List<Integer> fidAllIds() throws Exception;

    Page<FundingAgency> findByNameIgnoreCaseContaining(String name,Pageable pageable) throws Exception;

    FundingAgency findById(int id) throws Exception;

    @Query("SELECT FA FROM FundingAgency FA ORDER BY FA.name ASC ")
    List<FundingAgency> getAll() throws Exception;
}
