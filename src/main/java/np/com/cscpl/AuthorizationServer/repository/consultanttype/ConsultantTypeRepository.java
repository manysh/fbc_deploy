package np.com.cscpl.AuthorizationServer.repository.consultanttype;

import np.com.cscpl.AuthorizationServer.model.ConsultantType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsultantTypeRepository extends JpaRepository<ConsultantType,Integer> {
    Page<ConsultantType> findByConsultantTypeNameIgnoreCaseContaining(String name, Pageable pageable);
}
