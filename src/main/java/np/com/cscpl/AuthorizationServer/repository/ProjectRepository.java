package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.dto.project.ProjectResponseDto;
import np.com.cscpl.AuthorizationServer.model.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Integer> {

    Page<Project> findBySector_NameIgnoreCaseContainingOrderByCreatedAtDesc(String sector, Pageable pageable) throws Exception;

    Page<Project> findByStatusIgnoreCaseContainingOrderByCreatedAtDesc(String status, Pageable pageable) throws Exception;

    Page<Project> findByTags_TagIgnoreCaseContainingOrderByCreatedAtDesc(String tag, Pageable pageable) throws Exception;
}
