package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.model.Geography;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GeographyRepository extends JpaRepository<Geography,Integer> {
}
