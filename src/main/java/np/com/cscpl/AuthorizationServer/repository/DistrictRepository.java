package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.model.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DistrictRepository extends JpaRepository<District, Integer> {

    List<District> findAllByOrderByDistrictNameAsc() throws Exception;

    List<District> findByFinalProjects_id(int projectId) throws Exception;

    List<District> findByIdIn(List<Integer> districtIds) throws Exception;

    @Query("SELECT D.id FROM District D")
    List<Integer> fidAllIds() throws Exception;

    District findById(int id) throws Exception;
}
