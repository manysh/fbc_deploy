package np.com.cscpl.AuthorizationServer.repository.sector2;

import np.com.cscpl.AuthorizationServer.model.Sector2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Sector2repository extends JpaRepository<Sector2, Integer> {

    List<Sector2> findAllByOrderByTitleAsc() throws Exception;

    List<Sector2> findByFinalProjects_id(int id) throws Exception;

    List<Sector2> findByIdIn(List<Integer> ids) throws Exception;

    @Query("SELECT S.id FROM Sector2 S")
    List<Integer> fidAllIds() throws Exception;

    Page<Sector2> findByTitleIgnoreCaseContaining(String title, Pageable pageable) throws Exception;

    Sector2 findById(int id) throws Exception;
}
