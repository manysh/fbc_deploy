package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.model.Menu;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MenuRepository extends JpaRepository<Menu,Integer>{
    List<Menu> findAllByOrderByOrderAsc() throws Exception;

    @Query("SELECT M FROM Menu M " +
            "WHERE " +
            "M.name LIKE CONCAT('%',?1,'%') " +
            "OR M.navLink LIKE CONCAT('%',?1,'%') " +
            "OR M.icon LIKE CONCAT('%',?1,'?')")
    Page<Menu> menuSearch(String search,Pageable pageable) throws Exception;
}
