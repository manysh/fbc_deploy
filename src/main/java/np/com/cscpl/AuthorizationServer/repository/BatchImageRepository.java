package np.com.cscpl.AuthorizationServer.repository;

import np.com.cscpl.AuthorizationServer.model.BatchImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BatchImageRepository extends JpaRepository<BatchImage,Integer>{

    @Query("SELECT BI FROM BatchImage BI ")
    public BatchImage findFirst() throws Exception;
}
