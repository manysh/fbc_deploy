package np.com.cscpl.AuthorizationServer.repository.finalproject;

import np.com.cscpl.AuthorizationServer.model.FinalProject;
import np.com.cscpl.AuthorizationServer.model.ProjectDisplay;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProjectDisplayRepository extends JpaRepository<ProjectDisplay,Integer> {

    @Query("SELECT PD FROM ProjectDisplay PD WHERE PD.status=?1")
    List<ProjectDisplay> searchByStatus(String status) throws Exception;

    ProjectDisplay findById(int id) throws Exception;

    @Query("SELECT PD.finalProject FROM ProjectDisplay PD JOIN PD.finalProject FP WHERE PD.status=?1 " +
            "ORDER BY FP.createdDate DESC")
    List<FinalProject> findByStatus(String status,Pageable pageable)throws Exception;
}
