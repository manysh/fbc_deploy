package np.com.cscpl.AuthorizationServer.othermodel;

public class FileUpload {
    private String filePath;
    private String deletePath;

    public FileUpload() {
    }

    public FileUpload(String filePath, String deletePath) {
        this.filePath = filePath;
        this.deletePath = deletePath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getDeletePath() {
        return deletePath;
    }

    public void setDeletePath(String deletePath) {
        this.deletePath = deletePath;
    }
}
