package np.com.cscpl.AuthorizationServer.othermodel;



public class ImageUpload {
    private String imagePath;
    private String deletePath;


    public ImageUpload() {
    }

    public ImageUpload(String imagePath, String deletePath) {
        this.imagePath = imagePath;
        this.deletePath = deletePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getDeletePath() {
        return deletePath;
    }

    public void setDeletePath(String deletePath) {
        this.deletePath = deletePath;
    }
}
