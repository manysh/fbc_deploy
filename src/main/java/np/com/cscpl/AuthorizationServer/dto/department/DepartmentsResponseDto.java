package np.com.cscpl.AuthorizationServer.dto.department;

import np.com.cscpl.AuthorizationServer.dto.department.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentsResponseDto {

    private List<DepartmentResponseDto> departmentResponseDtos = new ArrayList<>();

    public DepartmentsResponseDto() {
    }

    public DepartmentsResponseDto(DepartmentResponseDto departmentResponseDto) {
        this.getDepartmentResponseDtos().add(departmentResponseDto);
    }

    public DepartmentsResponseDto(List<DepartmentResponseDto> departmentResponseDtos) {
        this.departmentResponseDtos = departmentResponseDtos;
    }

    public List<DepartmentResponseDto> getDepartmentResponseDtos() {
        return departmentResponseDtos;
    }

    public void setDepartmentResponseDtos(List<DepartmentResponseDto> departmentResponseDtos) {
        this.departmentResponseDtos = departmentResponseDtos;
    }
}
