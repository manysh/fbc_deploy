package np.com.cscpl.AuthorizationServer.dto.finalproject;

import java.util.ArrayList;
import java.util.List;

public class FinalProjectsResponseDto {

    List<FinalProjectResponseDto> finalProjectResponseDtos = new ArrayList<>();

    public FinalProjectsResponseDto() {
    }

    public FinalProjectsResponseDto(FinalProjectResponseDto finalProjectResponseDto) {
        this.getFinalProjectResponseDtos().add(finalProjectResponseDto);
    }

    public FinalProjectsResponseDto(List<FinalProjectResponseDto> finalProjectResponseDtos) {
        this.finalProjectResponseDtos = finalProjectResponseDtos;
    }

    public List<FinalProjectResponseDto> getFinalProjectResponseDtos() {
        return finalProjectResponseDtos;
    }

    public void setFinalProjectResponseDtos(List<FinalProjectResponseDto> finalProjectResponseDtos) {
        this.finalProjectResponseDtos = finalProjectResponseDtos;
    }
}
