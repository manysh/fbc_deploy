package np.com.cscpl.AuthorizationServer.dto.project;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;
import np.com.cscpl.AuthorizationServer.dto.client.ClientResponseDto;
import np.com.cscpl.AuthorizationServer.dto.district.DistrictResponseDto;
import np.com.cscpl.AuthorizationServer.dto.sector.SectorResponseDto;
import np.com.cscpl.AuthorizationServer.dto.tag.TagResponseDto;

@JsonInclude(Include.NON_NULL)
public class ProjectResponseDto {

    private Integer id;
    private Date createdAt;
    private Date deletedAt;
    private String description;
    private Date estimatedCompletionDate;
    private Date initiatedDate;
    private Short isDelete;
    private String name;
    private String status;
    private Date updateAt;
    private String backdrop;
    private SectorResponseDto sectorDTO;
    private List<DistrictResponseDto> districtDTOList;
    private List<TagResponseDto> tagDTOList;

    public ProjectResponseDto() {
    }

    private ProjectResponseDto(Builder builder) {
        this.id = builder.id;
        this.createdAt = builder.createdAt;
        this.deletedAt = builder.deletedAt;
        this.description = builder.description;
        this.estimatedCompletionDate = builder.estimatedCompletionDate;
        this.initiatedDate = builder.initiatedDate;
        this.isDelete = builder.isDelete;
        this.name = builder.name;
        this.status = builder.status;
        this.updateAt = builder.updateAt;
//        this.clientDTO = builder.clientDTO;
        this.sectorDTO = builder.sectorDTO;
        this.districtDTOList = builder.districtDTOList;
        this.tagDTOList = builder.tagDTOList;
        this.backdrop = builder.backdrop;
    }


    public ProjectResponseDto(Integer id, Date createdAt, Date deletedAt, String description, Date estimatedCompletionDate, Date initiatedDate, Short isDelete, String name, String status, Date updateAt) {
        this.id = id;
        this.createdAt = createdAt;
        this.deletedAt = deletedAt;
        this.description = description;
        this.estimatedCompletionDate = estimatedCompletionDate;
        this.initiatedDate = initiatedDate;
        this.isDelete = isDelete;
        this.name = name;
        this.status = status;
        this.updateAt = updateAt;
    }

    public ProjectResponseDto(Integer id, Date createdAt, Date deletedAt, String description, Date estimatedCompletionDate, Date initiatedDate, Short isDelete, String name, String status, Date updateAt, String clientDTO, SectorResponseDto sectorDTO) {
        this.id = id;
        this.createdAt = createdAt;
        this.deletedAt = deletedAt;
        this.description = description;
        this.estimatedCompletionDate = estimatedCompletionDate;
        this.initiatedDate = initiatedDate;
        this.isDelete = isDelete;
        this.name = name;
        this.status = status;
        this.updateAt = updateAt;
        this.sectorDTO = sectorDTO;
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public Integer getId() {
        return id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public String getDescription() {
        return description;
    }

    public Date getEstimatedCompletionDate() {
        return estimatedCompletionDate;
    }

    public Date getInitiatedDate() {
        return initiatedDate;
    }

    public Short getIsDelete() {
        return isDelete;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public Date getUpdateAt() {
        return updateAt;
    }


    public List<TagResponseDto> getTagDTOList() {
        return tagDTOList;
    }

    public SectorResponseDto getSectorDTO() {
        return sectorDTO;
    }

    public List<DistrictResponseDto> getDistrictDTOList() {
        return districtDTOList;
    }

    public static class Builder {

        private Integer id;
        private Date createdAt;
        private Date deletedAt;
        private String description;
        private Date estimatedCompletionDate;
        private Date initiatedDate;
        private Short isDelete;
        private String name;
        private String status;
        private String backdrop;
        private Date updateAt;

        private SectorResponseDto sectorDTO;
        private List<DistrictResponseDto> districtDTOList;
        private List<TagResponseDto> tagDTOList;

        public Builder() {
        }

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder createdAt(Date createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder deletedAt(Date deletedAt) {
            this.deletedAt = deletedAt;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder estimatedCompletionDate(Date estimatedCompletionDate) {
            this.estimatedCompletionDate = estimatedCompletionDate;
            return this;
        }

        public Builder initiatedDate(Date initiatedDate) {
            this.initiatedDate = initiatedDate;
            return this;
        }

        public Builder isDelete(Short isDelete) {
            this.isDelete = isDelete;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public Builder updateAt(Date updateAt) {
            this.updateAt = updateAt;
            return this;
        }


        public Builder sectorDTO(SectorResponseDto sectorDTO) {
            this.sectorDTO = sectorDTO;
            return this;
        }

        public Builder districtDTOList(List<DistrictResponseDto> districtDTOList) {
            this.districtDTOList = districtDTOList;
            return this;
        }

        public Builder tagDTOList(List<TagResponseDto> tagDTOList) {
            this.tagDTOList = tagDTOList;
            return this;
        }

        public Builder backdrop(String backdrop) {
            this.backdrop = backdrop;
            return this;
        }

        public ProjectResponseDto build() {
            ProjectResponseDto build = new ProjectResponseDto(this);
            return build;
        }
    }
}
