package np.com.cscpl.AuthorizationServer.dto.client;

import java.util.List;

public class ClientsResponseDto {
    private List<ClientResponseDto> clientResponseDtoList;

    public ClientsResponseDto() {
    }

    public ClientsResponseDto(ClientResponseDto clientResponseDto) {
    }

    public ClientsResponseDto(List<ClientResponseDto> clientResponseDtoList) {
        this.clientResponseDtoList = clientResponseDtoList;
    }

    public List<ClientResponseDto> getClientResponseDtoList() {
        return clientResponseDtoList;
    }

    public void setClientResponseDtoList(List<ClientResponseDto> clientResponseDtoList) {
        this.clientResponseDtoList = clientResponseDtoList;
    }
}
