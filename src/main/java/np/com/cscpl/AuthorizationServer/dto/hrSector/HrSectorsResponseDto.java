package np.com.cscpl.AuthorizationServer.dto.hrSector;

import java.util.ArrayList;
import java.util.List;

public class HrSectorsResponseDto {

    private List<HrSectorResponseDto> hrSectorResponseDtos = new ArrayList<>();

    public HrSectorsResponseDto() {
    }

    public HrSectorsResponseDto(HrSectorResponseDto hrSectorResponseDto ) {
        this.getHrSectorResponseDtos().add(hrSectorResponseDto);
    }

    public HrSectorsResponseDto(List<HrSectorResponseDto> hrSectorResponseDtos) {
        this.hrSectorResponseDtos = hrSectorResponseDtos;
    }

    public List<HrSectorResponseDto> getHrSectorResponseDtos() {
        return hrSectorResponseDtos;
    }

    public void setHrSectorResponseDtos(List<HrSectorResponseDto> hrSectorResponseDtos) {
        this.hrSectorResponseDtos = hrSectorResponseDtos;
    }
}
