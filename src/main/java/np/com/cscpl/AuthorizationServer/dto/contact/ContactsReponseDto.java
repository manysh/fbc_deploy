package np.com.cscpl.AuthorizationServer.dto.contact;

import java.util.List;

public class ContactsReponseDto {
    private List<ContactReponseDto> contactReponseDtoList;


    public ContactsReponseDto(List<ContactReponseDto> contactReponseDtoList) {
        this.contactReponseDtoList = contactReponseDtoList;
    }

    public List<ContactReponseDto> getContactReponseDtoList() {
        return contactReponseDtoList;
    }

    public void setContactReponseDtoList(List<ContactReponseDto> contactReponseDtoList) {
        this.contactReponseDtoList = contactReponseDtoList;
    }
}
