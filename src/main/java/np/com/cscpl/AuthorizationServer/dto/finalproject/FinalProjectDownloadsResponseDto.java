package np.com.cscpl.AuthorizationServer.dto.finalproject;

import java.util.ArrayList;
import java.util.List;

public class FinalProjectDownloadsResponseDto {

    private List<FinalProjectDownloadResponseDto> finalProjectDownloadResponseDtos = new ArrayList<>();

    public FinalProjectDownloadsResponseDto() {
    }

    public FinalProjectDownloadsResponseDto(List<FinalProjectDownloadResponseDto> finalProjectDownloadResponseDtos) {
        this.finalProjectDownloadResponseDtos = finalProjectDownloadResponseDtos;
    }

    public FinalProjectDownloadsResponseDto(FinalProjectDownloadResponseDto finalProjectDownloadResponseDtos) {
        this.getFinalProjectDownloadResponseDtos().add(finalProjectDownloadResponseDtos);
    }

    public List<FinalProjectDownloadResponseDto> getFinalProjectDownloadResponseDtos() {
        return finalProjectDownloadResponseDtos;
    }

    public void setFinalProjectDownloadResponseDtos(List<FinalProjectDownloadResponseDto> finalProjectDownloadResponseDtos) {
        this.finalProjectDownloadResponseDtos = finalProjectDownloadResponseDtos;
    }
}
