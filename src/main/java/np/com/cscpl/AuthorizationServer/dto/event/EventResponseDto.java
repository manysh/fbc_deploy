/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.dto.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import np.com.cscpl.AuthorizationServer.model.EventType;

/**
 *
 * @author puzansakya
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventResponseDto {

    @ApiModelProperty(hidden = true)
    private int id;
    private String name;
    private String description;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date eventDate;

    private String imagePath;

    private int eventTypeId;

    @ApiModelProperty(hidden = true)
    private EventType eventType;

    private Boolean isShow;

    private Boolean egIsShow;

    public EventResponseDto() {
    }

    public EventResponseDto(int id, String name, String description, Date eventDate, String imagePath, EventType eventType, Boolean isShow, Boolean egIsShow) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.eventDate = eventDate;
        this.imagePath = imagePath;
        this.eventType = eventType;
        this.isShow = isShow;
        this.egIsShow = egIsShow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(int eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public Boolean getIsShow() {
        return isShow;
    }

    public void setIsShow(Boolean isShow) {
        this.isShow = isShow;
    }

    public Boolean getEgIsShow() {
        return egIsShow;
    }

    public void setEgIsShow(Boolean egIsShow) {
        this.egIsShow = egIsShow;
    }

}
