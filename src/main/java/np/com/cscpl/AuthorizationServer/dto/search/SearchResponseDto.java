/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.dto.search;

import java.util.ArrayList;
import java.util.List;

import np.com.cscpl.AuthorizationServer.dto.aboutus.AboutUsReponseDto;
import np.com.cscpl.AuthorizationServer.dto.finalproject.FinalProjectResponseDto;
import np.com.cscpl.AuthorizationServer.dto.sector.SectorResponseDto;
import org.springframework.data.domain.Page;

/**
 *
 * @author prabhu
 */
public class SearchResponseDto {

    private Page<FinalProjectResponseDto> finalProjectDTO;
//    private Page<SectorResponseDto> sectorResponseDto;
    private Page<AboutUsReponseDto> aboutUsReponseDto;

    public Page<AboutUsReponseDto> getAboutUsReponseDto() {
        return aboutUsReponseDto;
    }

    public void setAboutUsReponseDto(Page<AboutUsReponseDto> aboutUsReponseDto) {
        this.aboutUsReponseDto = aboutUsReponseDto;
    }

//    public Page<SectorResponseDto> getSectorResponseDto() {
//        return sectorResponseDto;
//    }
//
//    public void setSectorResponseDto(Page<SectorResponseDto> sectorResponseDto) {
//        this.sectorResponseDto = sectorResponseDto;
//    }

    public Page<FinalProjectResponseDto> getFinalProjectDTO() {
        return finalProjectDTO;
    }

    public void setFinalProjectDTO(Page<FinalProjectResponseDto> finalProjectDTO) {
        this.finalProjectDTO = finalProjectDTO;
    }

}
