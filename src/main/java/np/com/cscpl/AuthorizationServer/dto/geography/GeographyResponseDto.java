package np.com.cscpl.AuthorizationServer.dto.geography;

import io.swagger.annotations.ApiModelProperty;
import np.com.cscpl.AuthorizationServer.dto.province.DistrictResponseDto;
import np.com.cscpl.AuthorizationServer.model.District;

import java.util.List;

public class GeographyResponseDto {
    private int geographyId;
    private String geographyName;

    public GeographyResponseDto() {
    }


    public GeographyResponseDto(int geographyId, String geographyName) {
        this.geographyId = geographyId;
        this.geographyName = geographyName;

    }

    public int getGeographyId() {
        return geographyId;
    }

    public void setGeographyId(int geographyId) {
        this.geographyId = geographyId;
    }

    public String getGeographyName() {
        return geographyName;
    }

    public void setGeographyName(String geographyName) {
        this.geographyName = geographyName;
    }


}
