package np.com.cscpl.AuthorizationServer.dto.career;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CareerResponseDto {

    public static final String ID = "id";
    public static final String FULL_NAME = "full_name";
    public static final String EMAIL = "email";
    public static final String CONTACT_NO = "contact_no";
    public static final String SUBJECT = "subject";
    public static final String QUALIFICATION = "qualification";
    public static final String MESSAGE = "message";
    public static final String CV_LINK = "cv_link";
    public static final String IS_REQUIRED = " is required";

    @ApiModelProperty(hidden = true)
    private int id;
    @ApiModelProperty(notes = FULL_NAME + IS_REQUIRED, required = true)
    @NotNull(message = FULL_NAME + IS_REQUIRED)
    private String fullName;

    @ApiModelProperty(notes = EMAIL + IS_REQUIRED, required = true)
    @NotNull(message = EMAIL + IS_REQUIRED)
    private String email;

    @ApiModelProperty(notes = CONTACT_NO + IS_REQUIRED, required = true)
    @NotNull(message = CONTACT_NO + IS_REQUIRED)
    private String contactNo;

    @ApiModelProperty(notes = SUBJECT + IS_REQUIRED, required = true)
    @NotNull(message = SUBJECT + IS_REQUIRED)
    private String subject;

    @ApiModelProperty(notes = QUALIFICATION + IS_REQUIRED, required = true)
    @NotNull(message = QUALIFICATION + IS_REQUIRED)
    private String qualification;
    @ApiModelProperty(notes = MESSAGE + IS_REQUIRED, required = true)
    @NotNull(message = MESSAGE + IS_REQUIRED)
    private String message;

    @ApiModelProperty(notes = CV_LINK + IS_REQUIRED, required = true)
    @NotNull(message = CV_LINK + IS_REQUIRED)
    private String cvLink;

    public CareerResponseDto() {
    }

    public CareerResponseDto(int id,@NotNull(message = FULL_NAME + IS_REQUIRED) String fullName, @NotNull(message = EMAIL + IS_REQUIRED) String email, @NotNull(message = CONTACT_NO + IS_REQUIRED) String contactNo, @NotNull(message = SUBJECT + IS_REQUIRED) String subject, @NotNull(message = QUALIFICATION + IS_REQUIRED) String qualification, @NotNull(message = MESSAGE + IS_REQUIRED) String message, @NotNull(message = CV_LINK + IS_REQUIRED) String cvLink) {

        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.contactNo = contactNo;
        this.subject = subject;
        this.qualification = qualification;
        this.message = message;
        this.cvLink = cvLink;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCvLink() {
        return cvLink;
    }

    public void setCvLink(String cvLink) {
        this.cvLink = cvLink;
    }
}
