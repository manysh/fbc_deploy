package np.com.cscpl.AuthorizationServer.dto.project;

import np.com.cscpl.AuthorizationServer.dto.project.*;
import java.util.ArrayList;
import java.util.List;

public class ProjectsResponseDto {

    private List<ProjectResponseDto> projectResponseDtos = new ArrayList<>();

    public ProjectsResponseDto() {
    }

    public ProjectsResponseDto(ProjectResponseDto projectResponseDto) {
        this.getProjectResponseDtos().add(projectResponseDto);
    }

    public ProjectsResponseDto(List<ProjectResponseDto> projectResponseDtos) {
        this.projectResponseDtos = projectResponseDtos;
    }

    public List<ProjectResponseDto> getProjectResponseDtos() {
        return projectResponseDtos;
    }

    public void setProjectResponseDtos(List<ProjectResponseDto> projectResponseDtos) {
        this.projectResponseDtos = projectResponseDtos;
    }
}
