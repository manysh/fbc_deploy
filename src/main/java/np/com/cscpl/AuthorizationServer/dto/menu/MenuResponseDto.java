package np.com.cscpl.AuthorizationServer.dto.menu;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class MenuResponseDto {
    public static final String NAME="name";
    public static final String NAV_LINK="navLink";
    public static final String MENU_ORDER="menuOrder";
    public static final String MENU_ICON="menuIcon";
    public static final String IS_REQUIRED=" is required";

    @ApiModelProperty(hidden = true)
    private int menuId;

    @ApiModelProperty(notes = NAME+IS_REQUIRED,required = true)
    @NotNull(message = NAME+IS_REQUIRED)
    private String name;

    @ApiModelProperty(notes = NAV_LINK+IS_REQUIRED,required = true)
    @NotNull(message = NAV_LINK+IS_REQUIRED)
    private String navLink;

    @ApiModelProperty(notes = MENU_ORDER+IS_REQUIRED,required = true)
    @NotNull(message = MENU_ORDER+IS_REQUIRED)
    private int menuOrder;

    @ApiModelProperty(notes = MENU_ICON+IS_REQUIRED,required = true)
    @NotNull(message = MENU_ICON+IS_REQUIRED)
    private String menuIcon;

    @ApiModelProperty(notes = "OPTIONAL FIELD")
    private List<SubmenuDto> submenuDtos= new ArrayList<>();

    public MenuResponseDto() {
    }

    public MenuResponseDto(int menuId, String name,String navLink, int menuOrder,String menuIcon, List<SubmenuDto> submenuDtos) {
        this.menuId = menuId;
        this.name = name;
        this.navLink=navLink;
        this.menuOrder = menuOrder;
        this.menuIcon=menuIcon;
        this.submenuDtos = submenuDtos;
    }

    public MenuResponseDto(int menuId, String name,String navLink, int menuOrder,String menuIcon){
        this.menuId=menuId;
        this.name=name;
        this.navLink=navLink;
        this.menuIcon=menuIcon;
        this.menuOrder=menuOrder;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMenuOrder() {
        return menuOrder;
    }

    public void setMenuOrder(int menuOrder) {
        this.menuOrder = menuOrder;
    }

    public List<SubmenuDto> getSubmenuDtos() {
        return submenuDtos;
    }

    public void setSubmenuDtos(List<SubmenuDto> submenuDtos) {
        this.submenuDtos = submenuDtos;
    }

    public String getNavLink() {
        return navLink;
    }

    public void setNavLink(String navLink) {
        this.navLink = navLink;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    @Override
    public String toString() {
        return "MenuResponseDto{" +
                "menuId=" + menuId +
                ", name='" + name + '\'' +
                ", navLink='" + navLink + '\'' +
                ", menuOrder=" + menuOrder +
                ", menuIcon='" + menuIcon + '\'' +
                ", submenuDtos=" + submenuDtos +
                '}';
    }
}
