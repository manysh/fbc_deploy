package np.com.cscpl.AuthorizationServer.dto.province;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class ProvienceResponseDto {
    public static final String NAME="name";
    public static final String DESC="description";
    public static final String DISTRICT ="district";
    public static final String GEOGRAPHY="geography";
    public static final String IS_REQUIRED=" is required";



    @ApiModelProperty(hidden = true)
    private int provienceId;

    @ApiModelProperty(notes = NAME+IS_REQUIRED,required = true)
    @NotNull(message = NAME+IS_REQUIRED)
    private String name;

    @ApiModelProperty(notes = DESC+IS_REQUIRED,required = true)
    @NotNull(message = DESC+IS_REQUIRED)
    private String description;


    @ApiModelProperty(notes = "OPTIONAL FIELD")
    private List<DistrictResponseDto> districtList = new ArrayList<>();

    public ProvienceResponseDto() {
    }

    public ProvienceResponseDto(int provienceId, String name, String description,List<DistrictResponseDto> districts) {
        this.provienceId = provienceId;
        this.name = name;
        this.description = description;
        this.districtList = districts;
    }

    public int getProvienceId() {
        return provienceId;
    }

    public void setProvienceId(int provienceId) {
        this.provienceId = provienceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<DistrictResponseDto> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<DistrictResponseDto> districtList) {
        this.districtList = districtList;
    }
}
