package np.com.cscpl.AuthorizationServer.dto.province;

import java.util.ArrayList;
import java.util.List;

public class ProviencesResponseDto {
    private List<ProvienceResponseDto> provienceResponseDtos = new ArrayList<>();

    public ProviencesResponseDto(ProvienceResponseDto provienceResponseDto) {
        this.getProvienceResponseDtos().add(provienceResponseDto);
    }

    public ProviencesResponseDto() {
    }

    public ProviencesResponseDto(List<ProvienceResponseDto> provienceResponseDtos) {
        this.provienceResponseDtos = provienceResponseDtos;
    }

    public List<ProvienceResponseDto> getProvienceResponseDtos() {
        return provienceResponseDtos;
    }

    public void setProvienceResponseDtos(List<ProvienceResponseDto> provienceResponseDtos) {
        this.provienceResponseDtos = provienceResponseDtos;
    }
}
