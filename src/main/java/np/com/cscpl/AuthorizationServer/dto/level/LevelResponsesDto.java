package np.com.cscpl.AuthorizationServer.dto.level;

import java.util.List;

public class LevelResponsesDto {
    private List<LevelResponseDto> levelResponseDtoList;

    public LevelResponsesDto() {
    }

    public LevelResponsesDto(LevelResponseDto levelResponseDto) {
    }

    public LevelResponsesDto(List<LevelResponseDto> levelResponseDtoList) {
        this.levelResponseDtoList = levelResponseDtoList;
    }

    public List<LevelResponseDto> getLevelResponseDtoList() {
        return levelResponseDtoList;
    }

    public void setLevelResponseDtoList(List<LevelResponseDto> levelResponseDtoList) {
        this.levelResponseDtoList = levelResponseDtoList;
    }
}
