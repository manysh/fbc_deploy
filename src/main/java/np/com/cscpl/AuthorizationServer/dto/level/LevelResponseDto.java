package np.com.cscpl.AuthorizationServer.dto.level;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LevelResponseDto {

    private int id;
    private String name;

    public LevelResponseDto() {
    }

    public LevelResponseDto(LevelResponseDto levelResponseDto) {
    }

    public LevelResponseDto(Integer id,  String name) {
        this.id = id;
        this.name = name;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
