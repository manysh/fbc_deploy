package np.com.cscpl.AuthorizationServer.dto.district;

import np.com.cscpl.AuthorizationServer.model.Geography;
import np.com.cscpl.AuthorizationServer.model.Provience;

import java.util.List;

public class DistrictResponseDto {

    private int districtId;
    private String name;
    private Provience provience;
    private List<Geography> geographyList;

    public DistrictResponseDto() {
    }

    public DistrictResponseDto(int districtId, String name, Provience provience, List<Geography> geographyList) {
        this.districtId = districtId;
        this.name = name;
        this.provience = provience;
        this.geographyList = geographyList;
    }

    public DistrictResponseDto(int districtId, String name) {
        this.districtId = districtId;
        this.name = name;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Provience getProvience() {
        return provience;
    }

    public void setProvience(Provience provience) {
        this.provience = provience;
    }

    public List<Geography> getGeographyList() {
        return geographyList;
    }

    public void setGeographyList(List<Geography> geographyList) {
        this.geographyList = geographyList;
    }
}
