package np.com.cscpl.AuthorizationServer.dto.sector2;

import java.util.ArrayList;
import java.util.List;

public class Sector2sResponseDto {
    List<Sector2ResponseDto> sector2ResponseDtos = new ArrayList<>();


    public Sector2sResponseDto() {
    }

    public Sector2sResponseDto(Sector2ResponseDto sector2ResponseDto) {
        this.getSector2ResponseDtos().add(sector2ResponseDto);
    }

    public Sector2sResponseDto(List<Sector2ResponseDto> sector2ResponseDtos) {
        this.sector2ResponseDtos = sector2ResponseDtos;
    }

    public List<Sector2ResponseDto> getSector2ResponseDtos() {
        return sector2ResponseDtos;
    }

    public void setSector2ResponseDtos(List<Sector2ResponseDto> sector2ResponseDtos) {
        this.sector2ResponseDtos = sector2ResponseDtos;
    }
}
