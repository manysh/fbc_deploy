/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.dto.eventImage;

import java.util.List;

/**
 *
 * @author prabhu
 */
public class EventImageParamsDto {             
    
    private int eventId;
    
    private List<EventImageResponseDto> events;   

    public EventImageParamsDto() {
    }
    
    public EventImageParamsDto(int eventId, List<EventImageResponseDto> events) {
        this.eventId = eventId;
        this.events = events;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public List<EventImageResponseDto> getEvents() {
        return events;
    }

    public void setEvents(List<EventImageResponseDto> events) {
        this.events = events;
    }
    
    
}
