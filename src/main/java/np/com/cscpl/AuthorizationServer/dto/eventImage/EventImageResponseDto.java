/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.dto.eventImage;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import np.com.cscpl.AuthorizationServer.dto.event.EventResponseDto;

/**
 *
 * @author puzansakya
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventImageResponseDto {
    
    private int id;

    private String imagePath;
    
    private String caption;
    
    private int event;

    public EventImageResponseDto() {
    }

    public EventImageResponseDto(int id, String imagePath, String caption, int event) {
        this.id = id;
        this.imagePath = imagePath;
        this.caption = caption;
        this.event = event;
    }
   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
    
    public int getEvent() {
        return event;
    }

    public void setEvent(int event) {
        this.event = event;
    }

    @Override
    public String toString() {
        return "EventImageResponseDTO{" + "id=" + id + ", imagePath=" + imagePath + ", event=" + event + '}';
    }
            
}
