package np.com.cscpl.AuthorizationServer.dto.authority;

public class AuthorityResposeDto {
    private long authorityId;

    private String authorityName;

    public AuthorityResposeDto() {
    }

    public AuthorityResposeDto(long authorityId, String authorityName) {
        this.authorityId = authorityId;
        this.authorityName = authorityName;
    }

    public long getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(long authorityId) {
        this.authorityId = authorityId;
    }

    public void setAuthorityId(int authorityId) {
        this.authorityId = authorityId;
    }

    public String getAuthorityName() {
        return authorityName;
    }

    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName;
    }
}
