package np.com.cscpl.AuthorizationServer.dto.sector;

import java.util.ArrayList;
import java.util.List;

public class SectorsResponseDto {

    private List<SectorResponseDto> sectorResponseDtos = new ArrayList<>();

    public SectorsResponseDto() {
    }

    public SectorsResponseDto(SectorResponseDto sectorResponseDto) {
        this.getSectorResponseDtos().add(sectorResponseDto);
    }

    public SectorsResponseDto(List<SectorResponseDto> sectorResponseDtos) {
        this.sectorResponseDtos = sectorResponseDtos;
    }

    public List<SectorResponseDto> getSectorResponseDtos() {
        return sectorResponseDtos;
    }

    public void setSectorResponseDtos(List<SectorResponseDto> sectorResponseDtos) {
        this.sectorResponseDtos = sectorResponseDtos;
    }
}
