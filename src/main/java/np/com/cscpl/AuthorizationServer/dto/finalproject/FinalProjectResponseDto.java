package np.com.cscpl.AuthorizationServer.dto.finalproject;

import org.springframework.hateoas.ResourceSupport;

public class FinalProjectResponseDto extends ResourceSupport {

    public static final String PROJECT_ID = "projectId";
    public static final String TITLE = "title";
    public static final String ASSIGNMENT_TITLE = "assignmentTitle";
    public static final String DESCRIPTION = "description";
    public static final String WORK_DESCRIPTION = "workDescription";
    public static final String CREATED_DATE = "createdDate";
    public static final String ESTIMATED_COMPLETION_DATE = "estimatedCompletionDate";
    public static final String CLIENT1_ID = "client1Id";
    public static final String CLIENT1_NAME = "client1Name";
    public static final String CLIENT2_ID = "client2Id";
    public static final String CLIENT2_NAME = "client2Name";
    public static final String SECTOR1_ID = "sector1Id";
    public static final String SECTOR1_NAME = "sector1Name";
    public static final String SECTOR2_IDS = "sector2Ids";
    public static final String SECTOR3_IDS = "sector3Ids";
    public static final String TAG_IDS = "tagIds";
    public static final String IMAGE_PATH = "imagePath";
    public static final String FILE_PATH = "filePath";
    public static final String PROJECT_STATUS = "projectStatus";
    public static final String IS_REQUIRED = " is required";
    public static final String IS_OPTIONAL = " is optional";

    private int projectId;
    private String title;
    private String assignmentTitle;
    private String description;
    private String workDescription;
    private String createdDate;
    private String estimatedCompletionDate;
    private int client1Id;
    private String client1Name;
    private int client2Id;
    private String client2Name;
    private int sector1Id;
    private String sector1Name;
    private String projectStatus;
    private String imagePath;
    private String filePath;

    public FinalProjectResponseDto() {
    }

    public FinalProjectResponseDto(
            int projectId,
            String title,
            String assignmentTitle,
            String description,
            String workDescription,
            String createdDate,
            String estimatedCompletionDate,
            int client1Id,
            String client1Name,
            int client2Id,
            String client2Name,
            int sector1Id,
            String sector1Name,
            String projectStatus,
            String imagePath,
            String filePath) {
        this.projectId = projectId;
        this.title = title;
        this.assignmentTitle = assignmentTitle;
        this.description = description;
        this.workDescription = workDescription;
        this.createdDate = createdDate;
        this.estimatedCompletionDate = estimatedCompletionDate;
        this.client1Id = client1Id;
        this.client1Name = client1Name;
        this.client2Id = client2Id;
        this.client2Name = client2Name;
        this.sector1Id = sector1Id;
        this.sector1Name = sector1Name;
        this.projectStatus = projectStatus;
        this.imagePath = imagePath;
        this.filePath = filePath;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWorkDescription() {
        return workDescription;
    }

    public void setWorkDescription(String workDescription) {
        this.workDescription = workDescription;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getEstimatedCompletionDate() {
        return estimatedCompletionDate;
    }

    public void setEstimatedCompletionDate(String estimatedCompletionDate) {
        this.estimatedCompletionDate = estimatedCompletionDate;
    }

    public int getClient1Id() {
        return client1Id;
    }

    public void setClient1Id(int client1Id) {
        this.client1Id = client1Id;
    }

    public String getClient1Name() {
        return client1Name;
    }

    public void setClient1Name(String client1Name) {
        this.client1Name = client1Name;
    }

    public int getClient2Id() {
        return client2Id;
    }

    public void setClient2Id(int client2Id) {
        this.client2Id = client2Id;
    }

    public String getClient2Name() {
        return client2Name;
    }

    public void setClient2Name(String client2Name) {
        this.client2Name = client2Name;
    }

    public int getSector1Id() {
        return sector1Id;
    }

    public void setSector1Id(int sector1Id) {
        this.sector1Id = sector1Id;
    }

    public String getSector1Name() {
        return sector1Name;
    }

    public void setSector1Name(String sector1Name) {
        this.sector1Name = sector1Name;
    }

    public String getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getAssignmentTitle() {
        return assignmentTitle;
    }

    public void setAssignmentTitle(String assignmentTitle) {
        this.assignmentTitle = assignmentTitle;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    
    

}
