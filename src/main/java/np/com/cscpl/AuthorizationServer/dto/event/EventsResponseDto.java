package np.com.cscpl.AuthorizationServer.dto.event;

import java.util.ArrayList;
import java.util.List;

public class EventsResponseDto {

    private List<EventResponseDto> sectorResponseDtos = new ArrayList<>();

    public EventsResponseDto() {
    }

    public EventsResponseDto(EventResponseDto sectorResponseDto) {
        this.getEventResponseDtos().add(sectorResponseDto);
    }

    public EventsResponseDto(List<EventResponseDto> sectorResponseDtos) {
        this.sectorResponseDtos = sectorResponseDtos;
    }

    public List<EventResponseDto> getEventResponseDtos() {
        return sectorResponseDtos;
    }

    public void setEventResponseDtos(List<EventResponseDto> sectorResponseDtos) {
        this.sectorResponseDtos = sectorResponseDtos;
    }
}
