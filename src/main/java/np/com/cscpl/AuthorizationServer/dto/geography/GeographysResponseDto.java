package np.com.cscpl.AuthorizationServer.dto.geography;

import java.util.List;

public class GeographysResponseDto {
    private List<GeographyResponseDto> geographyResponseDtoList;


    public GeographysResponseDto() {
    }

    public GeographysResponseDto(List<GeographyResponseDto> geographyResponseDtoList) {
        this.geographyResponseDtoList = geographyResponseDtoList;
    }

    public List<GeographyResponseDto> getGeographyResponseDtoList() {
        return geographyResponseDtoList;
    }

    public void setGeographyResponseDtoList(List<GeographyResponseDto> geographyResponseDtoList) {
        this.geographyResponseDtoList = geographyResponseDtoList;
    }
}
