package np.com.cscpl.AuthorizationServer.dto.filedownload;

import java.util.ArrayList;
import java.util.List;

public class FileDownloadsResponseDto {
    List<FileDownloadResponseDto> fileDownloadResponseDtos = new ArrayList<>();

    public FileDownloadsResponseDto() {
    }

    public FileDownloadsResponseDto(FileDownloadResponseDto fileDownloadResponseDto) {
        this.getFileDownloadResponseDtos().add(fileDownloadResponseDto);
    }

    public FileDownloadsResponseDto(List<FileDownloadResponseDto> fileDownloadResponseDtos) {
        this.fileDownloadResponseDtos = fileDownloadResponseDtos;
    }


    public List<FileDownloadResponseDto> getFileDownloadResponseDtos() {
        return fileDownloadResponseDtos;
    }

    public void setFileDownloadResponseDtos(List<FileDownloadResponseDto> fileDownloadResponseDtos) {
        this.fileDownloadResponseDtos = fileDownloadResponseDtos;
    }
}
