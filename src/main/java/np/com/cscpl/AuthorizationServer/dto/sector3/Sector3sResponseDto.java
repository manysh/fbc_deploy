package np.com.cscpl.AuthorizationServer.dto.sector3;

import java.util.ArrayList;
import java.util.List;

public class Sector3sResponseDto {
    List<Sector3ResponseDto> sector3ResponseDtos = new ArrayList<>();


    public Sector3sResponseDto() {
    }

    public Sector3sResponseDto(Sector3ResponseDto sector3ResponseDto) {
        this.getSector3ResponseDtos().add(sector3ResponseDto);
    }

    public Sector3sResponseDto(List<Sector3ResponseDto> sector2ResponseDtos) {
        this.sector3ResponseDtos = sector2ResponseDtos;
    }

    public List<Sector3ResponseDto> getSector3ResponseDtos() {
        return sector3ResponseDtos;
    }

    public void setSector3ResponseDtos(List<Sector3ResponseDto> sector3ResponseDtos) {
        this.sector3ResponseDtos = sector3ResponseDtos;
    }
}
