package np.com.cscpl.AuthorizationServer.dto.sector;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SectorResponseDto {

    private Integer id;
    private String name;
    private Integer displayOrder;
    private String detail;

    public SectorResponseDto() {
    }

    public SectorResponseDto(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.displayOrder = builder.displayOrder;
        this.detail = builder.detail;
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public String getDetail() {
        return detail;
    }

    public static class Builder {

        private Integer id;
        private String name;
        private Integer displayOrder;
        private String detail;

        public Builder() {
        }

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }


        public Builder displayOrder(int displayOrder) {
            this.displayOrder = displayOrder;
            return this;
        }

        public Builder detail(String detail) {
            this.detail = detail;
            return this;
        }

        public SectorResponseDto build() {
            SectorResponseDto build = new SectorResponseDto(this);
            return build;
        }
    }
}
