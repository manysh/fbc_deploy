package np.com.cscpl.AuthorizationServer.dto.tag;

import java.util.ArrayList;
import java.util.List;

public class TagProjectListDto {

    private List<Integer> projectIds= new ArrayList<>();

    public TagProjectListDto() {
    }

    public TagProjectListDto(List<Integer> projectIds) {
        this.projectIds = projectIds;
    }

    public List<Integer> getProjectIds() {
        return projectIds;
    }

    public void setProjectIds(List<Integer> projectIds) {
        this.projectIds = projectIds;
    }
}
