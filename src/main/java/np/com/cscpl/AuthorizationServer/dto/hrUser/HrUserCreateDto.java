package np.com.cscpl.AuthorizationServer.dto.hrUser;

import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDate;
import java.util.List;

public class HrUserCreateDto {
    public static final String USER_ID="hrUserId";
    public static final String FULL_NAME="fullName";
    public static final String SECTOR="sector";
    public static final String SUB_SECTOR="subSector";
    public static final String OTHER_SECTOR="otherSector";
    public static final String QUALIFICATION_LEVEL="qualificationLevel";
    public static final String MASTER_DEGREE="masterDegree";
    public static final String MASTER_GRADUATION_YEAR="masterGraduationYear";
    public static final String BACHELOR_DEGREE="bachelorDegree";
    public static final String BACHELOR_GRADUATION_YEAR="bachelorGraduationYear";
    public static final String OTHER_DEGREE="otherDegree";
    public static final String OTHER_GRADUATION_YEAR="otherGraduationYear";
    public static final String IS_CONSULTANT="isConsultant";
    public static final String CONSULTANT_TYPE="consultantType";
    public static final String YEAR_OF_EXPERIENCE="yearOfExperience";
    public static final String PHONE_NUMBER="phoneNumber";
    public static final String JOIN_DATE="joinDate";
    public static final String PROVIENCE="provience";
    public static final String DISTRICT="district";
    public static final String ADDRESS="address";

    public static final String IS_REQUIRED=" is required.";
    public static final String IS_OPTIONAL=" is optional.";

    @ApiModelProperty(hidden = true)
    private int hrUserId;

    @ApiModelProperty(value = FULL_NAME+IS_REQUIRED,required = true)
    private String fullName;

    @ApiModelProperty(value = SECTOR+IS_REQUIRED, required = true)
    private int sector;

    @ApiModelProperty(SUB_SECTOR+IS_OPTIONAL)
    private int subSector;

    @ApiModelProperty(OTHER_SECTOR+IS_OPTIONAL)
    private int otherSector;

    @ApiModelProperty(value = QUALIFICATION_LEVEL+IS_REQUIRED,required = true)
    private int qualificationLevel;

    @ApiModelProperty(MASTER_DEGREE+IS_OPTIONAL)
    private int masterDegree;

    @ApiModelProperty(MASTER_GRADUATION_YEAR+IS_OPTIONAL)
    private LocalDate masterGraduationYear;

    @ApiModelProperty(BACHELOR_DEGREE+IS_OPTIONAL)
    private int bachelorDegree;

    @ApiModelProperty(BACHELOR_GRADUATION_YEAR+IS_OPTIONAL)
    private LocalDate bachelorGraduationYear;

    @ApiModelProperty(OTHER_DEGREE+IS_OPTIONAL)
    private int otherDegree;

    @ApiModelProperty(OTHER_GRADUATION_YEAR+IS_OPTIONAL)
    private LocalDate otherGraduationYear;

    @ApiModelProperty(value = IS_CONSULTANT+IS_REQUIRED,required = true)
    private boolean isConsultant;

    @ApiModelProperty(value = CONSULTANT_TYPE+IS_OPTIONAL,notes = "WIll depends upon isConsultant. if True then required.")
    private String consultantType;

    @ApiModelProperty(value = YEAR_OF_EXPERIENCE+IS_REQUIRED,required = true)
    private int yearOfExperience;

    @ApiModelProperty(value = PHONE_NUMBER+IS_REQUIRED,required = true)
    private String phoneNumber;

    @ApiModelProperty(value = JOIN_DATE+IS_REQUIRED,required = true)
    private LocalDate joinDate;

    @ApiModelProperty(value = PROVIENCE+IS_REQUIRED,required = true)
    private int provience;

    @ApiModelProperty(value = DISTRICT+IS_REQUIRED,required = true)
    private int district;

    @ApiModelProperty(value = ADDRESS+IS_REQUIRED,required = true)
    private String address;

    @ApiModelProperty(value = "filePath "+IS_REQUIRED,required = true)
    private String filePath;

    public HrUserCreateDto() {
    }

    public HrUserCreateDto(String fullName, int sector, int subSector, int otherSector, int qualificationLevel, int masterDegree, LocalDate masterGraduationYear, int bachelorDegree, LocalDate bachelorGraduationYear, int otherDegree, LocalDate otherGraduationYear, boolean isConsultant, String consultantType, int yearOfExperience, String phoneNumber, LocalDate joinDate, int provience, int district, String address) {
        this.fullName = fullName;
        this.sector = sector;
        this.subSector = subSector;
        this.otherSector = otherSector;
        this.qualificationLevel = qualificationLevel;
        this.masterDegree = masterDegree;
        this.masterGraduationYear = masterGraduationYear;
        this.bachelorDegree = bachelorDegree;
        this.bachelorGraduationYear = bachelorGraduationYear;
        this.otherDegree = otherDegree;
        this.otherGraduationYear = otherGraduationYear;
        this.isConsultant = isConsultant;
        this.consultantType = consultantType;
        this.yearOfExperience = yearOfExperience;
        this.phoneNumber = phoneNumber;
        this.joinDate = joinDate;
        this.provience = provience;
        this.district = district;
        this.address = address;

    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getSector() {
        return sector;
    }

    public void setSector(int sector) {
        this.sector = sector;
    }

    public int getSubSector() {
        return subSector;
    }

    public void setSubSector(int subSector) {
        this.subSector = subSector;
    }

    public int getOtherSector() {
        return otherSector;
    }

    public void setOtherSector(int otherSector) {
        this.otherSector = otherSector;
    }

    public int getQualificationLevel() {
        return qualificationLevel;
    }

    public void setQualificationLevel(int qualificationLevel) {
        this.qualificationLevel = qualificationLevel;
    }

    public int getMasterDegree() {
        return masterDegree;
    }

    public void setMasterDegree(int masterDegree) {
        this.masterDegree = masterDegree;
    }

    public LocalDate getMasterGraduationYear() {
        return masterGraduationYear;
    }

    public void setMasterGraduationYear(LocalDate masterGraduationYear) {
        this.masterGraduationYear = masterGraduationYear;
    }

    public int getBachelorDegree() {
        return bachelorDegree;
    }

    public void setBachelorDegree(int bachelorDegree) {
        this.bachelorDegree = bachelorDegree;
    }

    public LocalDate getBachelorGraduationYear() {
        return bachelorGraduationYear;
    }

    public void setBachelorGraduationYear(LocalDate bachelorGraduationYear) {
        this.bachelorGraduationYear = bachelorGraduationYear;
    }

    public int getOtherDegree() {
        return otherDegree;
    }

    public void setOtherDegree(int otherDegree) {
        this.otherDegree = otherDegree;
    }

    public LocalDate getOtherGraduationYear() {
        return otherGraduationYear;
    }

    public void setOtherGraduationYear(LocalDate otherGraduationYear) {
        this.otherGraduationYear = otherGraduationYear;
    }

    public boolean isConsultant() {
        return isConsultant;
    }

    public void setConsultant(boolean consultant) {
        isConsultant = consultant;
    }

    public String getConsultantType() {
        return consultantType;
    }

    public void setConsultantType(String consultantType) {
        this.consultantType = consultantType;
    }

    public int getYearOfExperience() {
        return yearOfExperience;
    }

    public void setYearOfExperience(int yearOfExperience) {
        this.yearOfExperience = yearOfExperience;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public LocalDate getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(LocalDate joinDate) {
        this.joinDate = joinDate;
    }

    public int getProvience() {
        return provience;
    }

    public void setProvience(int provience) {
        this.provience = provience;
    }

    public int getDistrict() {
        return district;
    }

    public void setDistrict(int district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
