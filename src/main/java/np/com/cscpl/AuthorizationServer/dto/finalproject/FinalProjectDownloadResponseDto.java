package np.com.cscpl.AuthorizationServer.dto.finalproject;

public class FinalProjectDownloadResponseDto {

    private int projectId;
    private String projectTitle;
    private String projectAssignmentTitle;
    private String projectDescription;
    private String projectWorkDescription;
    private String startDate;
    private String endDate;
    private String projectStatus;
    private String client1;
    private String client2;
    private String sector1;
    private String sector2;
    private String secotor3;
    private String location;
    private String fundingAgency;

    public FinalProjectDownloadResponseDto() {
    }

    public FinalProjectDownloadResponseDto(int projectId, String projectTitle, String projectAssignmentTitle, String projectDescription, String projectWorkDescription, String startDate, String endDate, String projectStatus, String client1, String client2, String sector1, String sector2, String secotor3, String location, String fundingAgency) {
        this.projectId = projectId;
        this.projectTitle = projectTitle;
        this.projectAssignmentTitle = projectAssignmentTitle;
        this.projectDescription = projectDescription;
        this.projectWorkDescription = projectWorkDescription;
        this.startDate = startDate;
        this.endDate = endDate;
        this.projectStatus = projectStatus;
        this.client1 = client1;
        this.client2 = client2;
        this.sector1 = sector1;
        this.sector2 = sector2;
        this.secotor3 = secotor3;
        this.location = location;
        this.fundingAgency = fundingAgency;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getProjectWorkDescription() {
        return projectWorkDescription;
    }

    public void setProjectWorkDescription(String projectWorkDescription) {
        this.projectWorkDescription = projectWorkDescription;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getClient1() {
        return client1;
    }

    public void setClient1(String client1) {
        this.client1 = client1;
    }

    public String getClient2() {
        return client2;
    }

    public void setClient2(String client2) {
        this.client2 = client2;
    }

    public String getSector1() {
        return sector1;
    }

    public void setSector1(String sector1) {
        this.sector1 = sector1;
    }

    public String getSector2() {
        return sector2;
    }

    public void setSector2(String sector2) {
        this.sector2 = sector2;
    }

    public String getSecotor3() {
        return secotor3;
    }

    public void setSecotor3(String secotor3) {
        this.secotor3 = secotor3;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFundingAgency() {
        return fundingAgency;
    }

    public void setFundingAgency(String fundingAgency) {
        this.fundingAgency = fundingAgency;
    }

    public String getProjectAssignmentTitle() {
        return projectAssignmentTitle;
    }

    public void setProjectAssignmentTitle(String projectAssignmentTitle) {
        this.projectAssignmentTitle = projectAssignmentTitle;
    }

}
