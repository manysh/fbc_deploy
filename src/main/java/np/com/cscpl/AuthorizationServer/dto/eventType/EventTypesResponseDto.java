/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.dto.eventType;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author prabhu
 */
public class EventTypesResponseDto {

    private List<EventTypeResponseDto> eventTypeResponseDtos = new ArrayList<>();

    public EventTypesResponseDto() {
    }

    public EventTypesResponseDto(EventTypeResponseDto eventTypeResponseDto) {
        this.getEventTypeResponseDtos().add(eventTypeResponseDto);
    }

    public EventTypesResponseDto(List<EventTypeResponseDto> eventTypeResponseDtos) {
        this.eventTypeResponseDtos = eventTypeResponseDtos;
    }

    public List<EventTypeResponseDto> getEventTypeResponseDtos() {
        return eventTypeResponseDtos;
    }

    public void setEventTypeResponseDtos(List<EventTypeResponseDto> eventTypeResponseDtos) {
        this.eventTypeResponseDtos = eventTypeResponseDtos;
    }
}
