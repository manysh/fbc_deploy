/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.dto.test;

/**
 *
 * @author Puzan Sakya < puzan at puzansakya@gmail.com >
 */
public class _bookResponseDto {
      
    private int id;   
    private String name;
    private _authorResponseDto author;

    public _bookResponseDto(int id, String name) {
        this.id = id;
        this.name = name;        
    }

    public _bookResponseDto(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public _authorResponseDto getAuthor() {
        return author;
    }

    public void setAuthor(_authorResponseDto author) {
        this.author = author;
    }
    
    
}
