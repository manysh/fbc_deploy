package np.com.cscpl.AuthorizationServer.dto.resourcetype;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResourceTypeResourceDto {

    public static  final String ID = "id";
    public static final String NAME = "name";
    public static final String IS_REQUIRED = "is_required";

    @ApiModelProperty(hidden = true)
    private int id;

    @ApiModelProperty(notes=NAME+IS_REQUIRED,required = true)
    @NotNull(message = NAME+IS_REQUIRED)
    private String name;

    public ResourceTypeResourceDto(ResourceTypeResourceDto resourceTypeResourceDto) {
    }

    public ResourceTypeResourceDto(int id, @NotNull(message = NAME + IS_REQUIRED) String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
