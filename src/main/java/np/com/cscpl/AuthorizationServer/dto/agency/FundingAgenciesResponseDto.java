package np.com.cscpl.AuthorizationServer.dto.agency;

import java.util.ArrayList;
import java.util.List;

public class FundingAgenciesResponseDto {

    public List<FundingAgencyResponseDto> fundingAgencyResponseDtos = new ArrayList<>();

    public FundingAgenciesResponseDto() {
    }

    public FundingAgenciesResponseDto(FundingAgencyResponseDto fundingAgencyResponseDto) {
        this.getFundingAgencyResponseDtos().add(fundingAgencyResponseDto);
    }

    public FundingAgenciesResponseDto(List<FundingAgencyResponseDto> fundingAgencyResponseDtos) {
        this.fundingAgencyResponseDtos = fundingAgencyResponseDtos;
    }

    public List<FundingAgencyResponseDto> getFundingAgencyResponseDtos() {
        return fundingAgencyResponseDtos;
    }

    public void setFundingAgencyResponseDtos(List<FundingAgencyResponseDto> fundingAgencyResponseDtos) {
        this.fundingAgencyResponseDtos = fundingAgencyResponseDtos;
    }
}
