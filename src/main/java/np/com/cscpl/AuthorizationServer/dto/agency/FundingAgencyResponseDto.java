package np.com.cscpl.AuthorizationServer.dto.agency;

import io.swagger.annotations.ApiModelProperty;

public class FundingAgencyResponseDto {
    public static final String AGENCY_ID="agencyId";
    public static final String AGENCY_NAME="agencyName";
    public static final String AGENCY_DESCRIPTION="agencyDescription";
    public static final String IS_REQUIRED=" is required";
    public static final String IS_OPTIONAL=" is optional";

    @ApiModelProperty(hidden = true)
    private int agencyId;

    @ApiModelProperty(notes = AGENCY_NAME+IS_REQUIRED,required = true)
    private String agencyName;

    @ApiModelProperty(notes = AGENCY_DESCRIPTION+IS_OPTIONAL)
    private String agencyDescription;

    public FundingAgencyResponseDto() {
    }

    public FundingAgencyResponseDto(int agencyId, String agencyName, String agencyDescription) {
        this.agencyId = agencyId;
        this.agencyName = agencyName;
        this.agencyDescription = agencyDescription;
    }

    public int getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(int agencyId) {
        this.agencyId = agencyId;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getAgencyDescription() {
        return agencyDescription;
    }

    public void setAgencyDescription(String agencyDescription) {
        this.agencyDescription = agencyDescription;
    }

    @Override
    public String toString() {
        return "FundingAgencyResponseDto{" +
                "agencyId=" + agencyId +
                ", agencyName='" + agencyName + '\'' +
                ", agencyDescription='" + agencyDescription + '\'' +
                '}';
    }
}
