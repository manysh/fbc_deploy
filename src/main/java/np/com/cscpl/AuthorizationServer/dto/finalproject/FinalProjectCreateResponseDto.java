package np.com.cscpl.AuthorizationServer.dto.finalproject;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.List;

public class FinalProjectCreateResponseDto {

    public static final String PROJECT_ID = "projectId";
    public static final String TITLE = "title";
    public static final String ASSIGNMENT_TITLE = "assignmentTitle";
    public static final String DESCRIPTION = "description";
    public static final String WORK_DESCRIPTION = "workDescription";
    public static final String CREATED_DATE = "createdDate";
    public static final String ESTIMATED_COMPLETION_DATE = "estimatedCompletionDate";
    public static final String CLIENT1_ID = "client1Id";
    public static final String CLIENT2_ID = "client2Id";
    public static final String SECTOR1_ID = "sector1Id";
    public static final String SECTOR2_IDS = "sector2Ids";
    public static final String SECTOR3_IDS = "sector3Ids";
    public static final String TAG_IDS = "tagIds";
    public static final String DISTRIC_IDS = "districtIds";
    public static final String FUNDING_AGENCY_IDS = "fundingAgenciesIds";
    public static final String PROJECT_STATUS = "projectStatus";
    public static final String IMAGE_PATH = "imagePath";
    public static final String FILE_PATH = "filePath";
    public static final String IS_REQUIRED = " is required";
    public static final String IS_OPTIONAL = " is optional";

    @ApiModelProperty(hidden = true)
    private int projectId;

    @ApiModelProperty(hidden = false, required = true, notes = TITLE + IS_REQUIRED)
    @NotNull(message = TITLE + IS_REQUIRED)
    private String title;

    @ApiModelProperty(hidden = false, required = true, notes = ASSIGNMENT_TITLE + IS_REQUIRED)
    @NotNull(message = ASSIGNMENT_TITLE + IS_REQUIRED)
    private String assignmentTitle;

    @ApiModelProperty(hidden = false, required = true, notes = DESCRIPTION + IS_REQUIRED)
    @NotNull(message = DESCRIPTION + IS_REQUIRED)
    private String description;

    @ApiModelProperty(hidden = false, required = true, notes = WORK_DESCRIPTION + IS_REQUIRED)
    @NotNull(message = WORK_DESCRIPTION + IS_REQUIRED)
    private String workDescription;

    @ApiModelProperty(hidden = false, required = true, notes = CREATED_DATE + IS_REQUIRED)
    @NotNull(message = CREATED_DATE + IS_REQUIRED)
    private String createdDate;

    @ApiModelProperty(hidden = false, required = true, notes = ESTIMATED_COMPLETION_DATE + IS_REQUIRED)
    @NotNull(message = ESTIMATED_COMPLETION_DATE + IS_REQUIRED)
    private String estimatedCompletionDate;

    @ApiModelProperty(hidden = false, required = true, notes = CLIENT1_ID + IS_REQUIRED)
    @NotNull(message = CLIENT1_ID + IS_REQUIRED)
    private int client1Id;

    @ApiModelProperty(hidden = false, required = true, notes = CLIENT2_ID + IS_REQUIRED)
    @NotNull(message = CLIENT2_ID + IS_REQUIRED)
    private int client2Id;

    @ApiModelProperty(hidden = false, required = true, notes = SECTOR1_ID + IS_REQUIRED)
    @NotNull(message = SECTOR1_ID + IS_REQUIRED)
    private int sector1Id;

    @ApiModelProperty(hidden = false, required = true, notes = SECTOR2_IDS + IS_REQUIRED)
    @NotNull(message = SECTOR2_IDS + IS_REQUIRED)
    private List<Integer> sector2Ids;

    @ApiModelProperty(hidden = false, required = true, notes = SECTOR3_IDS + IS_REQUIRED)
    @NotNull(message = SECTOR3_IDS + IS_REQUIRED)
    private List<Integer> sector3Ids;

    @ApiModelProperty(hidden = false, notes = TAG_IDS + IS_REQUIRED)
    @NotNull(message = TAG_IDS + IS_REQUIRED)
    private List<Integer> tagIds;

    @ApiModelProperty(hidden = false, notes = FUNDING_AGENCY_IDS + IS_REQUIRED)
    @NotNull(message = FUNDING_AGENCY_IDS + IS_REQUIRED)
    private List<Integer> fundingAgenciesIds;

    @ApiModelProperty(notes = DISTRIC_IDS + IS_REQUIRED, required = true)
    @NotNull(message = DISTRIC_IDS + IS_REQUIRED)
    private List<Integer> districtIds;

    @ApiModelProperty(notes = PROJECT_STATUS + IS_REQUIRED, required = true)
    @NotNull(message = PROJECT_STATUS + IS_REQUIRED)
    private String projectStatus;

    @ApiModelProperty(notes = IMAGE_PATH + " is opetional")
    private String imagePath;

    @ApiModelProperty(notes = FILE_PATH + " is opetional")
    private String filePath;

    public FinalProjectCreateResponseDto() {
    }

    public FinalProjectCreateResponseDto(
            int projectId,
            String title,
            String assignmentTitle,
            String description,
            String workDescription,
            String createdDate,
            String estimatedCompletionDate,
            int client1Id,
            int client2Id,
            int sector1Id,
            List<Integer> sector2Ids,
            List<Integer> sector3Ids,
            List<Integer> tagIds,
            List<Integer> fundingAgenciesIds,
            List<Integer> districtIds
    ) {
        this.projectId = projectId;
        this.title = title;
        this.assignmentTitle = assignmentTitle;
        this.description = description;
        this.workDescription = workDescription;
        this.createdDate = createdDate;
        this.estimatedCompletionDate = estimatedCompletionDate;
        this.client1Id = client1Id;
        this.client2Id = client2Id;
        this.sector1Id = sector1Id;
        this.sector2Ids = sector2Ids;
        this.sector3Ids = sector3Ids;
        this.tagIds = tagIds;
        this.fundingAgenciesIds = fundingAgenciesIds;
        this.districtIds = districtIds;
    }

    public FinalProjectCreateResponseDto(
            int projectId,
            @NotNull(message = TITLE + IS_REQUIRED) String title,
            @NotNull(message = ASSIGNMENT_TITLE + IS_REQUIRED) String assignmentTitle,
            @NotNull(message = DESCRIPTION + IS_REQUIRED) String description,
            @NotNull(message = WORK_DESCRIPTION + IS_REQUIRED) String workDescription,
            @NotNull(message = CREATED_DATE + IS_REQUIRED) String createdDate,
            @NotNull(message = ESTIMATED_COMPLETION_DATE + IS_REQUIRED) String estimatedCompletionDate,
            String projectStatus,
            String imagePath,
            String filePath) {
        this.projectId = projectId;
        this.title = title;
        this.assignmentTitle = assignmentTitle;
        this.description = description;
        this.workDescription = workDescription;
        this.createdDate = createdDate;
        this.estimatedCompletionDate = estimatedCompletionDate;
        this.projectStatus = projectStatus;
        this.imagePath = imagePath;
        this.filePath = filePath;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWorkDescription() {
        return workDescription;
    }

    public void setWorkDescription(String workDescription) {
        this.workDescription = workDescription;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getEstimatedCompletionDate() {
        return estimatedCompletionDate;
    }

    public void setEstimatedCompletionDate(String estimatedCompletionDate) {
        this.estimatedCompletionDate = estimatedCompletionDate;
    }

    public int getClient1Id() {
        return client1Id;
    }

    public void setClient1Id(int client1Id) {
        this.client1Id = client1Id;
    }

    public int getClient2Id() {
        return client2Id;
    }

    public void setClient2Id(int client2Id) {
        this.client2Id = client2Id;
    }

    public int getSector1Id() {
        return sector1Id;
    }

    public void setSector1Id(int sector1Id) {
        this.sector1Id = sector1Id;
    }

    public List<Integer> getSector2Ids() {
        return sector2Ids;
    }

    public void setSector2Ids(List<Integer> sector2Ids) {
        this.sector2Ids = sector2Ids;
    }

    public List<Integer> getSector3Ids() {
        return sector3Ids;
    }

    public void setSector3Ids(List<Integer> sector3Ids) {
        this.sector3Ids = sector3Ids;
    }

    public List<Integer> getTagIds() {
        return tagIds;
    }

    public void setTagIds(List<Integer> tagIds) {
        this.tagIds = tagIds;
    }

    public List<Integer> getFundingAgenciesIds() {
        return fundingAgenciesIds;
    }

    public void setFundingAgenciesIds(List<Integer> fundingAgenciesIds) {
        this.fundingAgenciesIds = fundingAgenciesIds;
    }

    public List<Integer> getDistrictIds() {
        return districtIds;
    }

    public void setDistrictIds(List<Integer> districtIds) {
        this.districtIds = districtIds;
    }

    public String getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getAssignmentTitle() {
        return assignmentTitle;
    }

    public void setAssignmentTitle(String assignmentTitle) {
        this.assignmentTitle = assignmentTitle;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

}
