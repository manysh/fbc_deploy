package np.com.cscpl.AuthorizationServer.dto.filedownload;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class FileDownloadResponseDto {

    public static final String FILE_DOWNLOAD_ID="fileDownloadId";
    public static final String FILE_NAME="fileName";
    public static final String FILE_DESCRIPTION="fileDescription";
    public static final String IMAGE_PATH="imagePath";
    public static final String IS_REQUIRED=" is required ";


    @ApiModelProperty(hidden = true)
    private int fileDownlaodId;

    @ApiModelProperty(notes = FILE_NAME+IS_REQUIRED,required = true)
    @NotNull(message = FILE_NAME+IS_REQUIRED)
    private String fileName;

    @NotNull(message = FILE_DESCRIPTION+IS_REQUIRED)
    @ApiModelProperty(notes = FILE_DESCRIPTION+IS_REQUIRED,required = true)
    private String fileDescription;

    @NotNull(message = IMAGE_PATH+IS_REQUIRED)
    @ApiModelProperty(notes = IMAGE_PATH+IS_REQUIRED,required = true)
    private String imagePath;

    @ApiModelProperty(hidden = true)
    private String createdAt;


    public FileDownloadResponseDto() {
        this.createdAt=LocalDateTime.now().toString();
    }

    public FileDownloadResponseDto(
            int fileDownlaodId,
            @NotNull(message = FILE_NAME + IS_REQUIRED) String fileName,
            @NotNull(message = FILE_DESCRIPTION + IS_REQUIRED) String fileDescription,
            @NotNull(message = IMAGE_PATH + IS_REQUIRED) String imagePath,
            String createdAt) {
        this.fileDownlaodId = fileDownlaodId;
        this.fileName = fileName;
        this.fileDescription = fileDescription;
        this.imagePath = imagePath;
        this.createdAt= LocalDateTime.now().toString();
    }

    public int getFileDownlaodId() {
        return fileDownlaodId;
    }

    public void setFileDownlaodId(int fileDownlaodId) {
        this.fileDownlaodId = fileDownlaodId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileDescription() {
        return fileDescription;
    }

    public void setFileDescription(String fileDescription) {
        this.fileDescription = fileDescription;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
