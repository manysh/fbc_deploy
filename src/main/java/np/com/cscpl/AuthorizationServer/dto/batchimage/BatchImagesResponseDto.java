package np.com.cscpl.AuthorizationServer.dto.batchimage;

import np.com.cscpl.AuthorizationServer.model.BatchImage;

import java.util.ArrayList;
import java.util.List;

public class BatchImagesResponseDto {
    private List<BatchImageResponseDto> batchImages = new ArrayList<>();

    public BatchImagesResponseDto() {
    }

    public BatchImagesResponseDto(BatchImageResponseDto batchImageResponseDto) {
        this.getBatchImages().add(batchImageResponseDto);
    }

    public BatchImagesResponseDto(List<BatchImageResponseDto> batchImages) {
        this.batchImages = batchImages;
    }

    public List<BatchImageResponseDto> getBatchImages() {
        return batchImages;
    }

    public void setBatchImages(List<BatchImageResponseDto> batchImages) {
        this.batchImages = batchImages;
    }
}
