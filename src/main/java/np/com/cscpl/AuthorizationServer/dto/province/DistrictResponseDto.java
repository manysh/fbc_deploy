package np.com.cscpl.AuthorizationServer.dto.province;

import io.swagger.annotations.ApiModelProperty;
import np.com.cscpl.AuthorizationServer.dto.geography.GeographyResponseDto;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class DistrictResponseDto {
    public static final String NAME = "name";
    public static final String IS_REQUIRED = "is required";
    @ApiModelProperty(hidden = true)
    private int districtId;

    @ApiModelProperty(notes = NAME+IS_REQUIRED,required = true)
    @NotNull(message = NAME+IS_REQUIRED)
    private String name;

    @ApiModelProperty(hidden = true)
    private int provienceId;

    @ApiModelProperty(hidden = true)
    private String provienceName;

    private List<GeographyResponseDto> geographyList = new ArrayList<>();


    public DistrictResponseDto() {
    }

    public DistrictResponseDto(int districtId, String name, List<GeographyResponseDto> geographyList) {
        this.districtId = districtId;
        this.name = name;
        this.geographyList = geographyList;
    }

    public DistrictResponseDto(int districtId, String name, int provienceId, String provienceName, List<GeographyResponseDto> geographyList) {
        this.districtId = districtId;
        this.name = name;
        this.provienceId = provienceId;
        this.provienceName = provienceName;
        this.geographyList = geographyList;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProvienceId() {
        return provienceId;
    }

    public void setProvienceId(int provienceId) {
        this.provienceId = provienceId;
    }

    public String getProvienceName() {
        return provienceName;
    }

    public void setProvienceName(String provienceName) {
        this.provienceName = provienceName;
    }

    public List<GeographyResponseDto> getGeographyList() {
        return geographyList;
    }

    public void setGeographyList(List<GeographyResponseDto> geographyList) {
        this.geographyList = geographyList;
    }
}