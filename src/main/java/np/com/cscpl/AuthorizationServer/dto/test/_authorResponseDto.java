/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.dto.test;

import java.util.List;

/**
 *
 * @author Puzan Sakya < puzan at puzansakya@gmail.com >
 */
public class _authorResponseDto {

    private int id;
    private String name;
    private List<_bookResponseDto> bookList;

    public _authorResponseDto(int id, String name, List<_bookResponseDto> bookList) {
        this.id = id;
        this.name = name;
        this.bookList = bookList;
    }

    public _authorResponseDto(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<_bookResponseDto> getBookList() {
        return bookList;
    }

    public void setBookList(List<_bookResponseDto> bookList) {
        this.bookList = bookList;
    }
    
    

}
