package np.com.cscpl.AuthorizationServer.dto.degree;

import java.util.List;

public class DegreesReponseDto {
    private List<DegreeResponseDto> degreeResponseDtoList;

    public DegreesReponseDto() {
    }

    public DegreesReponseDto(DegreeResponseDto degreeResponseDto) {
    }

    public DegreesReponseDto(List<DegreeResponseDto> degreeResponseDtoList) {
        this.degreeResponseDtoList = degreeResponseDtoList;
    }

    public List<DegreeResponseDto> getDegreeResponseDtoList() {
        return degreeResponseDtoList;
    }

    public void setDegreeResponseDtoList(List<DegreeResponseDto> degreeResponseDtoList) {
        this.degreeResponseDtoList = degreeResponseDtoList;
    }
}
