package np.com.cscpl.AuthorizationServer.dto.finalproject;

import java.util.ArrayList;
import java.util.List;

public class ProjectDisplaysResponseDto {

    private List<ProjectDisplayResponseDto> projectDisplayResponseDtos= new ArrayList<>();


    public ProjectDisplaysResponseDto() {
    }

    public ProjectDisplaysResponseDto(ProjectDisplayResponseDto projectDisplayResponseDto) {
        this.getProjectDisplayResponseDtos().add(projectDisplayResponseDto);
    }

    public ProjectDisplaysResponseDto(List<ProjectDisplayResponseDto> projectDisplayResponseDtos) {
        this.projectDisplayResponseDtos = projectDisplayResponseDtos;
    }

    public List<ProjectDisplayResponseDto> getProjectDisplayResponseDtos() {
        return projectDisplayResponseDtos;
    }

    public void setProjectDisplayResponseDtos(List<ProjectDisplayResponseDto> projectDisplayResponseDtos) {
        this.projectDisplayResponseDtos = projectDisplayResponseDtos;
    }
}
