package np.com.cscpl.AuthorizationServer.dto.authority;


import java.util.ArrayList;
import java.util.List;

public class AuthoritiesResposeDto {
    private List<AuthorityResposeDto> authorityResposeDtos= new ArrayList<>();

    public AuthoritiesResposeDto() {
    }

    public AuthoritiesResposeDto(List<AuthorityResposeDto> authorityResposeDtos) {
        this.authorityResposeDtos = authorityResposeDtos;
    }


    public AuthoritiesResposeDto(AuthorityResposeDto authorityResposeDto) {
        this.getAuthorityResposeDtos().add(authorityResposeDto);
    }



    public List<AuthorityResposeDto> getAuthorityResposeDtos() {
        return authorityResposeDtos;
    }

    public void setAuthorityResposeDtos(List<AuthorityResposeDto> authorityResposeDtos) {
        this.authorityResposeDtos = authorityResposeDtos;
    }
}
