package np.com.cscpl.AuthorizationServer.dto.hrSector;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class HrSectorResponseDto {

    @ApiModelProperty(hidden = true)
    private Integer id;

    @ApiModelProperty(required = true, notes = "hr sector name is required")
    @NotNull(message = "hr sector name is required")
    private String name;

    @ApiModelProperty(notes = "hr sector description is optional")
    private String detail;

    @ApiModelProperty(notes = "this is flag to filter sub sector")
    private boolean isSub;

    @ApiModelProperty(notes = "this is flag to filter other sector")
    private boolean isOther;

    public HrSectorResponseDto() {
    }

    public HrSectorResponseDto(Integer id) {
        this.id = id;
    }

    public HrSectorResponseDto(Integer id, String name, String detail, boolean isSub, boolean isOther) {
        this.id = id;
        this.name = name;
        this.detail = detail;
        this.isSub = isSub;
        this.isOther = isOther;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public boolean isIsSub() {
        return isSub;
    }

    public void setIsSub(boolean isSub) {
        this.isSub = isSub;
    }

    public boolean isIsOther() {
        return isOther;
    }

    public void setIsOther(boolean isOther) {
        this.isOther = isOther;
    }

    @Override
    public String toString() {
        return "HrSectorResponseDto{" + "id=" + id + ", name=" + name + ", detail=" + detail + ", isSub=" + isSub + ", isOther=" + isOther + '}';
    }

}
