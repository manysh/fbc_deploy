package np.com.cscpl.AuthorizationServer.dto.finalproject;

public class ProjectDisplayResponseDto {

    private int displayId;
    private String displayStatus;
    private int projectId;
    private String projectTitle;
    private String projectAssignmentTitle;
    private String projectDescription;
    private String projectWorkDescription;
    private int displayOrder;

    public ProjectDisplayResponseDto() {
    }

    public ProjectDisplayResponseDto(
            int displayId,
            String displayStatus,
            int projectId,
            String projectTitle,
            String projectAssignmentTitle,
            String projectDescription,
            String projectWorkDescription,
            int displayOrder
    ) {
        this.displayId = displayId;
        this.displayStatus = displayStatus;
        this.projectId = projectId;
        this.projectTitle = projectTitle;
        this.projectAssignmentTitle = projectAssignmentTitle;
        this.projectDescription = projectDescription;
        this.projectWorkDescription = projectWorkDescription;
        this.displayOrder = displayOrder;
    }

    public int getDisplayId() {
        return displayId;
    }

    public void setDisplayId(int displayId) {
        this.displayId = displayId;
    }

    public String getDisplayStatus() {
        return displayStatus;
    }

    public void setDisplayStatus(String displayStatus) {
        this.displayStatus = displayStatus;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getProjectWorkDescription() {
        return projectWorkDescription;
    }

    public void setProjectWorkDescription(String projectWorkDescription) {
        this.projectWorkDescription = projectWorkDescription;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getProjectAssignmentTitle() {
        return projectAssignmentTitle;
    }

    public void setProjectAssignmentTitle(String projectAssignmentTitle) {
        this.projectAssignmentTitle = projectAssignmentTitle;
    }

}
