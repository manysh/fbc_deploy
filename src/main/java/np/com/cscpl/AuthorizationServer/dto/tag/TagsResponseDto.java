package np.com.cscpl.AuthorizationServer.dto.tag;

import java.util.ArrayList;
import java.util.List;

public class TagsResponseDto {

    private List<TagResponseDto> sectorResponseDtos = new ArrayList<>();

    public TagsResponseDto() {
    }

    public TagsResponseDto(TagResponseDto sectorResponseDto) {
        this.getTagResponseDtos().add(sectorResponseDto);
    }

    public TagsResponseDto(List<TagResponseDto> sectorResponseDtos) {
        this.sectorResponseDtos = sectorResponseDtos;
    }

    public List<TagResponseDto> getTagResponseDtos() {
        return sectorResponseDtos;
    }

    public void setTagResponseDtos(List<TagResponseDto> sectorResponseDtos) {
        this.sectorResponseDtos = sectorResponseDtos;
    }
}
