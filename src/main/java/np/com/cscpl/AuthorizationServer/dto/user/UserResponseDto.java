package np.com.cscpl.AuthorizationServer.dto.user;

import org.springframework.hateoas.ResourceSupport;

import java.util.List;

public class UserResponseDto extends ResourceSupport {

    public static final String USER_ID="userId";
    public static final String FULL_NAME="fullName";
    public static final String USERNAME="username";
    public static final String PASSWORD="password";

    public static final String IS_REQUIRED=" is required.";
    public static final String IS_OPTIONAL=" is optional.";

    private Long userId;
    private String fullName;
    private String userName;
    private String password;
    private List<Long> authorityIds;
    private List<String> authorityNames;



    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Long> getAuthorityIds() {
        return authorityIds;
    }

    public void setAuthorityIds(List<Long> authorityIds) {
        this.authorityIds = authorityIds;
    }



    public List<String> getAuthorityNames() {
        return authorityNames;
    }

    public void setAuthorityNames(List<String> authorityNames) {
        this.authorityNames = authorityNames;
    }
}
