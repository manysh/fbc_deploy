package np.com.cscpl.AuthorizationServer.dto.consultanttype;

import java.util.List;

public class ConsultantTypesResponseDto {
    private List<ConsultantTypeResponseDto> consultantTypeResponseDtos;

    public ConsultantTypesResponseDto() {
    }

    public ConsultantTypesResponseDto(ConsultantTypeResponseDto consultantTypeResponseDto) {
    }

    public ConsultantTypesResponseDto(List<ConsultantTypeResponseDto> consultantTypeResponseDtos) {
        this.consultantTypeResponseDtos = consultantTypeResponseDtos;
    }

    public List<ConsultantTypeResponseDto> getConsultantTypeResponseDtos() {
        return consultantTypeResponseDtos;
    }

    public void setConsultantTypeResponseDtos(List<ConsultantTypeResponseDto> consultantTypeResponseDtos) {
        this.consultantTypeResponseDtos = consultantTypeResponseDtos;
    }
}
