package np.com.cscpl.AuthorizationServer.dto.sector2;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class Sector2ResponseDto {
    public static final String SECTOR2_ID="sector2Id";
    public static final String SECTOR2_TTILE="sector2Title";
    public static final String SECTOR2_DESCRIPTION="sector2Description";
    public static final String IS_REQUIRED=" is required";
    public static final String IS_OPTIONAL=" is optional";

    @ApiModelProperty(hidden = true)
    private int sector2Id;

    @ApiModelProperty(required = true,notes = SECTOR2_TTILE+IS_REQUIRED)
    @NotNull(message = SECTOR2_TTILE+IS_REQUIRED)
    private String sector2Title;

    @ApiModelProperty(notes = SECTOR2_DESCRIPTION+IS_OPTIONAL)
    private String sector2Description;

    public Sector2ResponseDto() {
    }

    public Sector2ResponseDto(int sector2Id, @NotNull(message = SECTOR2_TTILE + IS_REQUIRED) String sector2Title, String sector2Description) {
        this.sector2Id = sector2Id;
        this.sector2Title = sector2Title;
        this.sector2Description = sector2Description;
    }

    public int getSector2Id() {
        return sector2Id;
    }

    public String getSector2Title() {
        return sector2Title;
    }

    public String getSector2Description() {
        return sector2Description;
    }
}
