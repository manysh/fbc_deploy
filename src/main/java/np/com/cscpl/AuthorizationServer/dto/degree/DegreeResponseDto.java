package np.com.cscpl.AuthorizationServer.dto.degree;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

import javax.validation.constraints.NotNull;
import np.com.cscpl.AuthorizationServer.dto.finalHrUserDegree.FinalHrUserDegreeResponseDto;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DegreeResponseDto {

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String LEVEL_ID = "level_id";
    public static final String LEVEL_NAME = "level_name";
    public static final String IS_REQUIRED = " is required";
    
//    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int id;

    @ApiModelProperty(hidden = false, notes = NAME + IS_REQUIRED, required = true)
    @NotNull(message = NAME + IS_REQUIRED)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String name;
   

    public DegreeResponseDto() {
    }

    public DegreeResponseDto(DegreeResponseDto degreeResponseDto) {
    }

    public DegreeResponseDto(Integer id, @NotNull(message = NAME + IS_REQUIRED) String name) {
        this.id = id;
        this.name = name;
    }

    public DegreeResponseDto(int id, @NotNull(message = NAME + IS_REQUIRED) String name) {
        this.id = id;
        this.name = name;
    }

    public DegreeResponseDto(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
