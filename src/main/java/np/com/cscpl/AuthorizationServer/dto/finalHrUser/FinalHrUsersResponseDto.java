package np.com.cscpl.AuthorizationServer.dto.finalHrUser;

import java.util.List;

public class FinalHrUsersResponseDto {
    private List<FinalHrUserResponseDto> finalHrUserResponseDtoList;

    public FinalHrUsersResponseDto(List<FinalHrUserResponseDto> finalHrUserResponseDtoList) {
        this.finalHrUserResponseDtoList = finalHrUserResponseDtoList;
    }

    public List<FinalHrUserResponseDto> getFinalHrUserResponseDtoList() {
        return finalHrUserResponseDtoList;
    }

    public void setFinalHrUserResponseDtoList(List<FinalHrUserResponseDto> finalHrUserResponseDtoList) {
        this.finalHrUserResponseDtoList = finalHrUserResponseDtoList;
    }
}
