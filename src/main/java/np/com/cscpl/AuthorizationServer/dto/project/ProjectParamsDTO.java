package np.com.cscpl.AuthorizationServer.dto.project;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;

//@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectParamsDTO {

    private Integer id;
    private String description;
    private Date estimatedCompletionDate;
    private Date initiatedDate;
    private String name;
    private String status;
    private int clientId;
    private int sectorId;

    public ProjectParamsDTO() {
    }

    public ProjectParamsDTO(Integer id, String description, Date estimatedCompletionDate, Date initiatedDate, String name, String status, int clientId, int sectorId) {
        this.id = id;
        this.description = description;
        this.estimatedCompletionDate = estimatedCompletionDate;
        this.initiatedDate = initiatedDate;
        this.name = name;
        this.status = status;
        this.clientId = clientId;
        this.sectorId = sectorId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEstimatedCompletionDate() {
        return estimatedCompletionDate;
    }

    public void setEstimatedCompletionDate(Date estimatedCompletionDate) {
        this.estimatedCompletionDate = estimatedCompletionDate;
    }

    public Date getInitiatedDate() {
        return initiatedDate;
    }

    public void setInitiatedDate(Date initiatedDate) {
        this.initiatedDate = initiatedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getSectorId() {
        return sectorId;
    }

    public void setSectorId(int sectorId) {
        this.sectorId = sectorId;
    }

}
