package np.com.cscpl.AuthorizationServer.dto.batchimage;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class BatchImageResponseDto {

    public static final String IMAGE_ID="imageId";
    public static final String IMAGE_LINK="imageLink";
    public static final String CREATED_DATE="createdDate";
    public static final String IS_REQUIRED=" is required.";

    @JsonProperty(IMAGE_ID)
    @NotNull(message = IMAGE_ID+IS_REQUIRED)
    @ApiModelProperty(notes = IMAGE_ID+IS_REQUIRED,required = true)
    private int imageId;

    @NotNull(message = IMAGE_LINK+IS_REQUIRED)
    @JsonProperty(IMAGE_LINK)
    @ApiModelProperty(notes =IMAGE_LINK+IS_REQUIRED ,required = true)
    private String imageLink;

    @NotNull(message = CREATED_DATE+IS_REQUIRED)
    @JsonProperty(CREATED_DATE)
    @ApiModelProperty(notes = CREATED_DATE+IS_REQUIRED,required = true)
    private String createdDate;

    public BatchImageResponseDto() {
    }

    public BatchImageResponseDto(@NotNull(message = IMAGE_ID + IS_REQUIRED) int imageId, @NotNull(message = IMAGE_LINK + IS_REQUIRED) String imageLink, @NotNull(message = CREATED_DATE + IS_REQUIRED) String createdDate) {
        this.imageId = imageId;
        this.imageLink = imageLink;
        this.createdDate = createdDate;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
