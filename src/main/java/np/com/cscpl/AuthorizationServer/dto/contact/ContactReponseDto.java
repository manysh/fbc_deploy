package np.com.cscpl.AuthorizationServer.dto.contact;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContactReponseDto {

    public static final String ID = "contact_id";
    public static final String FULL_NAME = "full_name";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone_no";
    public static  final String CONTACT_REASON = "contact_reason";
    public static  final String MESSAGE = "message";
    public static final String IS_REQUIRED=" is required";

    @ApiModelProperty(hidden = true)
    private int id;

    @ApiModelProperty(notes = FULL_NAME+IS_REQUIRED,required = true)
    @NotNull(message = FULL_NAME+IS_REQUIRED)
    private String fullName;

    @ApiModelProperty(notes = EMAIL+IS_REQUIRED,required = true)
    @NotNull(message = EMAIL+IS_REQUIRED)
    private String email;

    @ApiModelProperty(notes = PHONE+IS_REQUIRED,required = true)
    @NotNull(message = PHONE+IS_REQUIRED)
    private String phoneNo;


    @ApiModelProperty(notes = CONTACT_REASON +IS_REQUIRED,required = true)
    @NotNull(message = CONTACT_REASON+IS_REQUIRED)
    private String contactReason;

    @ApiModelProperty(notes = MESSAGE+IS_REQUIRED,required = true)
    @NotNull(message = MESSAGE+IS_REQUIRED)
    private String message;

    public ContactReponseDto() {
    }

    public ContactReponseDto(@NotNull(message = FULL_NAME + IS_REQUIRED) String fullName, @NotNull(message = EMAIL + IS_REQUIRED) String email, @NotNull(message = PHONE + IS_REQUIRED) String phoneNo, @NotNull(message = CONTACT_REASON + IS_REQUIRED) String contactReason, @NotNull(message = MESSAGE + IS_REQUIRED) String message) {
        this.fullName = fullName;
        this.email = email;
        this.phoneNo = phoneNo;
        this.contactReason = contactReason;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getContactReason() {
        return contactReason;
    }

    public void setContactReason(String contactReason) {
        this.contactReason = contactReason;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

