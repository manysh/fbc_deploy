/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.dto.finalHrUser;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import np.com.cscpl.AuthorizationServer.dto.finalHrUserDegree.FinalHrUserDegreeResponseDto;
import np.com.cscpl.AuthorizationServer.dto.hrSector.HrSectorResponseDto;

/**
 *
 * @author Puzan Sakya < puzan at puzansakya@gmail.com >
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FinalHrUserResponseDto {

    public static final String FIRST_NAME = "firstName";
    public static final String MIDDLE_NAME = "middleName";
    public static final String LAST_NAME = "lastName";
    public static final String SECTORID = "SECTORID";
    public static final String SECTORNAME = "SECTORNAME";
    public static final String SUBSECTORID = "SUBSECTORID";
    public static final String SUBSECTORNAME = "SUBSECTORNAME";
    public static final String OTHERSECTORID = "OTHERSECTORID";
    public static final String OTHERSECTORNAME = "OTHERSECTORNAME";
    public static final String SUB_SECTOR = "subSector";
    public static final String OTHER_SECTOR = "otherSector";
    public static final String IS_CONSULTANT = "isConsultant";
    public static final String CONSULTANT_TYPE = "consultantType";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String EMAIL = "email";
    public static final String JOIN_DATE = "joinDate";
    public static final String DOB = "dob";
    public static final String PROVIENCE = "provience";
    public static final String DISTRICTID = "districtId";
    public static final String DISTRICTNAME = "districtName";
    public static final String RESOURCEID = "resourceId";
    public static final String RESOURCENAME = "resourceName";
    public static final String CONSULTANT_TYPE_ID = "consultantTypeId";
    public static final String CONSULTANT_TYPE_NAME = "consultantTypeName";
    public static final String ADDRESS = "address";

    public static final String IS_REQUIRED = " is required.";
    public static final String IS_OPTIONAL = " is optional.";

    @ApiModelProperty(hidden = true)
    private int id;

    @ApiModelProperty(value = FIRST_NAME + IS_REQUIRED, required = true)
    private String firstName;

    @ApiModelProperty(value = MIDDLE_NAME + IS_REQUIRED, required = true)
    private String middleName;

    @ApiModelProperty(value = LAST_NAME + IS_REQUIRED, required = true)
    private String lastName;

    @ApiModelProperty(value = EMAIL + IS_REQUIRED, required = true)
    private String email;

    @ApiModelProperty(value = PHONE_NUMBER + IS_REQUIRED, required = true)
    private String phoneNumber;

    @ApiModelProperty(value = JOIN_DATE + IS_REQUIRED, required = true)
    private String joinDate;

    @ApiModelProperty(value = DOB + IS_REQUIRED, required = true)
    private String dob;

    @ApiModelProperty(value = ADDRESS + IS_REQUIRED, required = true)
    private String address;

    @ApiModelProperty(value = IS_CONSULTANT + IS_REQUIRED, required = true)
    private boolean isConsultant;

    @ApiModelProperty(value = "filePath " + IS_REQUIRED, required = true)
    private String filePath;

    @ApiModelProperty(value = DISTRICTID + IS_REQUIRED, required = true)
    private int districtId;

    @ApiModelProperty(hidden = true, value = DISTRICTNAME + IS_OPTIONAL)
    private String district;

//    @ApiModelProperty(value = SECTORID + IS_REQUIRED, required = true)
//    private int hrSectorId;
//
//    @ApiModelProperty(hidden = true, value = SECTORNAME + IS_OPTIONAL)
//    private String hrSectorName;
    @JsonProperty("sectorList")
    private List<HrSectorResponseDto> hrSectorDtos;

//    related to sector ids
    private List<Integer> SectorId;

//    @ApiModelProperty(value = SUBSECTORID + IS_REQUIRED, required = true)
//    private int hrSubSectorId;
//
//    @ApiModelProperty(hidden = true, value = SUBSECTORNAME + IS_OPTIONAL)
//    private String hrSubSectorName;
    @JsonProperty("subSectorList")
    private List<HrSectorResponseDto> hrSubSectorDtos;

    //related to subsector ids
    private List<Integer> hrSubSectorId;

//    @ApiModelProperty(value = OTHERSECTORID + IS_REQUIRED, required = true)
//    private int hrOtherSectorId;
//
//    @ApiModelProperty(hidden = true, value = OTHERSECTORNAME + IS_OPTIONAL)
//    private String hrOtherSectorName;
    //related to subsector ids
    @JsonProperty("otherSectorList")
    private List<HrSectorResponseDto> hrOtherSectorDtos;

    private List<Integer> hrOtherSectorId;

    @ApiModelProperty(value = RESOURCEID + IS_REQUIRED, required = true)
    private int resourceTypeId;

    @ApiModelProperty(hidden = true, value = RESOURCENAME + IS_OPTIONAL)
    private String resourceTypeName;

    @ApiModelProperty(value = CONSULTANT_TYPE_ID + IS_REQUIRED, required = true)
    private int consutlanTypeId;

    private String qualification;
    private String sectorformated;
    private String subSectorformated;
    private String otherSectorformated;
    private String phoneFormatted;
    private String emailFormatted;

    @ApiModelProperty(hidden = true, value = CONSULTANT_TYPE_NAME + IS_OPTIONAL)
    private String consultantTypeName;

    @JsonProperty("degreeList")
    private List<FinalHrUserDegreeResponseDto> fhudrd;

    public FinalHrUserResponseDto() {
    }

    public FinalHrUserResponseDto(int id) {
        this.id = id;
    }

    public FinalHrUserResponseDto(
            int id,
            String firstName,
            String middleName,
            String lastName,
            String phoneNumber,
            String email,
            String joinDate,
            String dob,
            String address,
            boolean isConsultant,
            String filePath,
            int districtId,
            String district,
            List<HrSectorResponseDto> hrSectorDtos,
            List<HrSectorResponseDto> hrSubSectorDtos,
            List<HrSectorResponseDto> hrOtherSectorDtos,
            int resourceTypeId,
            String resourceTypeName,
            int consutlanTypeId,
            String consultantTypeName,
            List<FinalHrUserDegreeResponseDto> fhudrd
    ) {
        this.id = id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.joinDate = joinDate;
        this.dob = dob;
        this.address = address;
        this.isConsultant = isConsultant;
        this.filePath = filePath;
        this.districtId = districtId;
        this.district = district;
        this.hrSectorDtos = hrSectorDtos;
        this.hrSubSectorDtos = hrSubSectorDtos;
        this.hrOtherSectorDtos = hrOtherSectorDtos;
        this.resourceTypeId = resourceTypeId;
        this.resourceTypeName = resourceTypeName;
        this.consutlanTypeId = consutlanTypeId;
        this.consultantTypeName = consultantTypeName;
        this.fhudrd = fhudrd;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        if (middleName != null) {
            return firstName + " " + middleName + " " + lastName;
        }
        return firstName + " " + lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isIsConsultant() {
        return isConsultant;
    }

    public void setIsConsultant(boolean isConsultant) {
        this.isConsultant = isConsultant;
    }

    public int getConsutlanTypeId() {
        return consutlanTypeId;
    }

    public void setConsutlanTypeId(int consutlanTypeId) {
        this.consutlanTypeId = consutlanTypeId;
    }

    public String getConsultantTypeName() {
        return consultantTypeName;
    }

    public void setConsultantTypeName(String consultantTypeName) {
        this.consultantTypeName = consultantTypeName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

//    public int getHrSectorId() {
//        return hrSectorId;
//    }
//
//    public void setHrSectorId(int hrSectorId) {
//        this.hrSectorId = hrSectorId;
//    }
//
//    public String getHrSectorName() {
//        return hrSectorName;
//    }
//
//    public void setHrSectorName(String hrSectorName) {
//        this.hrSectorName = hrSectorName;
//    }
//    public int getHrSubSectorId() {
//        return hrSubSectorId;
//    }
//
//    public void setHrSubSectorId(int hrSubSectorId) {
//        this.hrSubSectorId = hrSubSectorId;
//    }
//
//    public String getHrSubSectorName() {
//        return hrSubSectorName;
//    }
//
//    public void setHrSubSectorName(String hrSubSectorName) {
//        this.hrSubSectorName = hrSubSectorName;
//    }
    public List<HrSectorResponseDto> getHrSectorDtos() {
        return hrSectorDtos;
    }

    public void setHrSectorDtos(List<HrSectorResponseDto> hrSectorDtos) {
        this.hrSectorDtos = hrSectorDtos;
    }

    public List<Integer> getSectorId() {
        return SectorId;
    }

    public void setSectorId(List<Integer> SectorId) {
        this.SectorId = SectorId;
    }

    public List<HrSectorResponseDto> getHrSubSectorDtos() {
        return hrSubSectorDtos;
    }

    public void setHrSubSectorDtos(List<HrSectorResponseDto> hrSubSectorDtos) {
        this.hrSubSectorDtos = hrSubSectorDtos;
    }

//    public int getHrOtherSectorId() {
//        return hrOtherSectorId;
//    }
//
//    public void setHrOtherSectorId(int hrOtherSectorId) {
//        this.hrOtherSectorId = hrOtherSectorId;
//    }
//
//    public String getHrOtherSectorName() {
//        return hrOtherSectorName;
//    }
//
//    public void setHrOtherSectorName(String hrOtherSectorName) {
//        this.hrOtherSectorName = hrOtherSectorName;
//    }
    public List<HrSectorResponseDto> getHrOtherSectorDtos() {
        return hrOtherSectorDtos;
    }

    public void setHrOtherSectorDtos(List<HrSectorResponseDto> hrOtherSectorDtos) {
        this.hrOtherSectorDtos = hrOtherSectorDtos;
    }

    public int getResourceTypeId() {
        return resourceTypeId;
    }

    public void setResourceTypeId(int resourceTypeId) {
        this.resourceTypeId = resourceTypeId;
    }

    public String getResourceTypeName() {
        return resourceTypeName;
    }

    public void setResourceTypeName(String resourceTypeName) {
        this.resourceTypeName = resourceTypeName;
    }

    public List<FinalHrUserDegreeResponseDto> getFhudrd() {
        return fhudrd;
    }

    public void setFhudrd(List<FinalHrUserDegreeResponseDto> fhudrd) {
        this.fhudrd = fhudrd;
    }

    public String getQualificaiton() {
        StringBuilder sb = new StringBuilder();
        this.fhudrd.forEach(fhudrd -> {
            sb.append("<p>").append(fhudrd.getDrd().getName()).append(" (").append(fhudrd.getCompletionDate()).append(")").append("</p>");
        });
        return sb.toString();
    }

    public String getSectorformated() {
        StringBuilder sb = new StringBuilder();
        this.hrSectorDtos.forEach(Sector -> {
            sb.append("<p>").append(Sector.getName()).append("</p>");
        });
        return sb.toString();
    }

    public String getSubSectorformated() {
        StringBuilder sb = new StringBuilder();
        this.hrSubSectorDtos.forEach(subSector -> {
            sb.append("<p>").append(subSector.getName()).append("</p>");
        });
        return sb.toString();
    }

    public String getOtherSectorformated() {
        StringBuilder sb = new StringBuilder();
        this.hrOtherSectorDtos.forEach(otherSector -> {
            sb.append("<p>").append(otherSector.getName()).append("</p>");
        });
        return sb.toString();
    }

    public String getPhoneFormatted() {
        String[] phones = phoneNumber.split(",");
        StringBuilder sb = new StringBuilder();
        for (String s : phones) {
            sb.append("<p>").append(s).append("</p>");
        }
        return sb.toString();
    }

    public String getEmailFormatted() {
        String[] emails = email.split(",");
        StringBuilder sb = new StringBuilder();
        for (String s : emails) {
            sb.append("<p>").append(s).append("</p>");
        }
        return sb.toString();
    }

    public void setQualificaiton(String qualification) {
        this.qualification = qualification;
    }

    public List<Integer> getHrSubSectorId() {
        return hrSubSectorId;
    }

    public void setHrSubSectorId(List<Integer> hrSubSectorId) {
        this.hrSubSectorId = hrSubSectorId;
    }

    public List<Integer> getHrOtherSectorId() {
        return hrOtherSectorId;
    }

    public void setHrOtherSectorId(List<Integer> hrOtherSectorId) {
        this.hrOtherSectorId = hrOtherSectorId;
    }

}
