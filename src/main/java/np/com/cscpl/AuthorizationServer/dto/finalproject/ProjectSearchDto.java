package np.com.cscpl.AuthorizationServer.dto.finalproject;

import java.util.List;

public class ProjectSearchDto {
    private String title;
    private List<Integer> tagIds;
    private List<Integer> agencyIDs;
    private int client1Id;
    private int client2Id;
    private int sectorId;
    private List<Integer> sector2Ids;
    private List<Integer> sector3Ids;
    private List<Integer> districtIds;
    private int pageNumber;
    private int pageSize;
    private String fromDate;
    private String toDate;
    private String projectStatus;
    private String shortListed;

    public ProjectSearchDto() {
    }

    public ProjectSearchDto(
            String title,
            List<Integer> tagIds,
            List<Integer> agencyIDs,
            int client1Id,
            int client2Id,
            int sectorId,
            List<Integer> sector2Ids,
            List<Integer> sector3Ids,
            List<Integer> districtIds,
            int pageNumber,
            int pageSize,
            String projectStatus,
            String shortListed) {
        this.title = title;
        this.tagIds = tagIds;
        this.agencyIDs = agencyIDs;
        this.client1Id = client1Id;
        this.client2Id = client2Id;
        this.sectorId = sectorId;
        this.sector2Ids = sector2Ids;
        this.sector3Ids = sector3Ids;
        this.districtIds = districtIds;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.projectStatus=projectStatus;
        this.shortListed=shortListed;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Integer> getTagIds() {
        return tagIds;
    }

    public void setTagIds(List<Integer> tagIds) {
        this.tagIds = tagIds;
    }

    public List<Integer> getAgencyIDs() {
        return agencyIDs;
    }

    public void setAgencyIDs(List<Integer> agencyIDs) {
        this.agencyIDs = agencyIDs;
    }

    public int getClient1Id() {
        return client1Id;
    }

    public void setClient1Id(int client1Id) {
        this.client1Id = client1Id;
    }

    public int getClient2Id() {
        return client2Id;
    }

    public void setClient2Id(int client2Id) {
        this.client2Id = client2Id;
    }

    public int getSectorId() {
        return sectorId;
    }

    public void setSectorId(int sectorId) {
        this.sectorId = sectorId;
    }

    public List<Integer> getSector2Ids() {
        return sector2Ids;
    }

    public void setSector2Ids(List<Integer> sector2Ids) {
        this.sector2Ids = sector2Ids;
    }

    public List<Integer> getSector3Ids() {
        return sector3Ids;
    }

    public void setSector3Ids(List<Integer> sector3Ids) {
        this.sector3Ids = sector3Ids;
    }

    public List<Integer> getDistrictIds() {
        return districtIds;
    }

    public void setDistrictIds(List<Integer> districtIds) {
        this.districtIds = districtIds;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getShortListed() {
        return shortListed;
    }

    public void setShortListed(String shortListed) {
        this.shortListed = shortListed;
    }

    @Override
    public String toString() {
        return "ProjectSearchDto{" + "title=" + title + ", tagIds=" + tagIds + ", agencyIDs=" + agencyIDs + ", client1Id=" + client1Id + ", client2Id=" + client2Id + ", sectorId=" + sectorId + ", sector2Ids=" + sector2Ids + ", sector3Ids=" + sector3Ids + ", districtIds=" + districtIds + ", pageNumber=" + pageNumber + ", pageSize=" + pageSize + ", fromDate=" + fromDate + ", toDate=" + toDate + ", projectStatus=" + projectStatus + ", shortListed=" + shortListed + '}';
    }
    
    
}
