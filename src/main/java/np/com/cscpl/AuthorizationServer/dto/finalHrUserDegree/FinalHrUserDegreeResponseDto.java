/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.dto.finalHrUserDegree;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.util.Date;
import np.com.cscpl.AuthorizationServer.dto.degree.DegreeResponseDto;
import np.com.cscpl.AuthorizationServer.dto.finalHrUser.FinalHrUserResponseDto;

/**
 *
 * @author Puzan Sakya < puzan at puzansakya@gmail.com >
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FinalHrUserDegreeResponseDto {

    @ApiModelProperty(hidden = true)
    private Integer id;

    private LocalDate completionDate;

    @ApiModelProperty(hidden = true)
    private FinalHrUserResponseDto fhurd;

    @JsonProperty("degree")
    private DegreeResponseDto drd;

    public FinalHrUserDegreeResponseDto() {
    }

    public FinalHrUserDegreeResponseDto(Integer id) {
        this.id = id;
    }

    public FinalHrUserDegreeResponseDto(Integer id, LocalDate completionDate, DegreeResponseDto drd) {
        this.id = id;
        this.completionDate = completionDate;
        this.drd = drd;
    }

    public FinalHrUserDegreeResponseDto(Integer id, LocalDate completionDate, FinalHrUserResponseDto fhurd, DegreeResponseDto drd) {
        this.id = id;
        this.completionDate = completionDate;
        this.fhurd = fhurd;
        this.drd = drd;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(LocalDate completionDate) {
        this.completionDate = completionDate;
    }

    public FinalHrUserResponseDto getFhurd() {
        return fhurd;
    }

    public void setFhurd(FinalHrUserResponseDto fhurd) {
        this.fhurd = fhurd;
    }

    public DegreeResponseDto getDrd() {
        return drd;
    }

    public void setDrd(DegreeResponseDto drd) {
        this.drd = drd;
    }

    @Override
    public String toString() {
        return "FinalHrUserDegreeResponseDto{" + "id=" + id + ", createdDate=" + completionDate + ", fhurd=" + fhurd + ", drd=" + drd + '}';
    }

}
