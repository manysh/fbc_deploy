package np.com.cscpl.AuthorizationServer.dto.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import np.com.cscpl.AuthorizationServer.dto.finalproject.FinalProjectResponseDto;
import np.com.cscpl.AuthorizationServer.dto.project.ProjectResponseDto;
import np.com.cscpl.AuthorizationServer.model.FinalProject;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class ClientResponseDto {
    public static final String CLIENT_ID="client_id";
    public static final String CLIENT_NAME="name";
    public static final String DESC ="description";
    public static final String IMAGE_PATH = "image_path";
    public static final String USER_IMAGE = "user_image";

    public static final String IS_REQUIRED=" is required";

    @ApiModelProperty(hidden = true)
    private Integer clientId;

    @ApiModelProperty(notes = CLIENT_NAME+IS_REQUIRED,required = true)
    @NotNull(message = CLIENT_NAME+IS_REQUIRED)
    private String clientName;

    @ApiModelProperty(notes = DESC+IS_REQUIRED,required = true)
    @NotNull(message = DESC+IS_REQUIRED)
    private String description;




    @ApiModelProperty(notes = "OPTIONAL FIELD")
    private List<FinalProjectResponseDto> projectList = new ArrayList<>();

    @ApiModelProperty(notes = "OPTIONAL FIELD")
    private List<FinalProjectResponseDto> projectList2 = new ArrayList<>();


    @ApiModelProperty(notes = "OPTIONAL FIELD")
    private String imagePath;

    public ClientResponseDto() {
    }

    public ClientResponseDto(int id) {
        this.clientId = id;
    }


    public ClientResponseDto(int clientId, String clientName, String description, String imagePath, List<FinalProjectResponseDto> projectList,List<FinalProjectResponseDto> projectList2) {
        this.clientId = clientId;
        this.clientName = clientName;
        this.description = description;

        this.imagePath = imagePath;
//        this.userImage = userImage;
        this.projectList = projectList;
        this.projectList2 = projectList2;


    }

    public ClientResponseDto(int clientId, String clientName, String description) {
        this.clientId = clientId;
        this.clientName = clientName;
        this.description = description;



    }




    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FinalProjectResponseDto> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<FinalProjectResponseDto> projectList) {
        this.projectList = projectList;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public List<FinalProjectResponseDto> getProjectList2() {
        return projectList2;
    }

    public void setProjectList2(List<FinalProjectResponseDto> projectList2) {
        this.projectList2 = projectList2;
    }
}
