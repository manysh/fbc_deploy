package np.com.cscpl.AuthorizationServer.dto.aboutus;

import java.util.List;

public class AboutUsReponsesDto {
    private List<AboutUsReponseDto> aboutUsReponseDtoList;

    public AboutUsReponsesDto(List<AboutUsReponseDto> aboutUsReponseDtoList) {
        this.aboutUsReponseDtoList = aboutUsReponseDtoList;
    }

    public List<AboutUsReponseDto> getAboutUsReponseDtoList() {
        return aboutUsReponseDtoList;
    }

    public void setAboutUsReponseDtoList(List<AboutUsReponseDto> aboutUsReponseDtoList) {
        this.aboutUsReponseDtoList = aboutUsReponseDtoList;
    }
}
