package np.com.cscpl.AuthorizationServer.dto.menu;

import java.util.ArrayList;
import java.util.List;

public class MenuResponsesDto {
    List<MenuResponseDto> menuResponseDtos = new ArrayList<>();

    public MenuResponsesDto() {
    }

    public MenuResponsesDto(List<MenuResponseDto> menuResponseDtos) {
        this.menuResponseDtos = menuResponseDtos;
    }

    public MenuResponsesDto(MenuResponseDto menuResponseDto) {
        this.getMenuResponseDtos().add(menuResponseDto);
    }

    public List<MenuResponseDto> getMenuResponseDtos() {
        return menuResponseDtos;
    }

    public void setMenuResponseDtos(List<MenuResponseDto> menuResponseDtos) {
        this.menuResponseDtos = menuResponseDtos;
    }
}
