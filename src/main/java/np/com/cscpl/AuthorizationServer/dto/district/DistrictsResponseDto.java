package np.com.cscpl.AuthorizationServer.dto.district;

import java.util.ArrayList;
import java.util.List;

public class DistrictsResponseDto {

    private List<DistrictResponseDto> districtResponseDtos = new ArrayList<>();

    public DistrictsResponseDto() {
    }


    public DistrictsResponseDto(DistrictResponseDto districtResponseDto) {
        this.getDistrictResponseDtos().add(districtResponseDto);
    }


    public DistrictsResponseDto(List<DistrictResponseDto> districtResponseDtos) {
        this.districtResponseDtos = districtResponseDtos;
    }

    public List<DistrictResponseDto> getDistrictResponseDtos() {
        return districtResponseDtos;
    }

    public void setDistrictResponseDtos(List<DistrictResponseDto> districtResponseDtos) {
        this.districtResponseDtos = districtResponseDtos;
    }
}
