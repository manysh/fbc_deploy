package np.com.cscpl.AuthorizationServer.dto.menu;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class SubmenuDto {
    public static final String NAME="name";
    public static final String NAV_LINK="navLink";
    public static final String SUB_MENU_ORDER="submenuOrder";
    public static final String IS_REQUIRED=" is required.";

    @ApiModelProperty(hidden = true)
    private int submenuId;

    @ApiModelProperty(notes = IS_REQUIRED,required = true)
    @NotNull(message = NAME+IS_REQUIRED)
    private String name;

    @ApiModelProperty(notes = "Required Field.",required = true)
    @NotNull(message = NAV_LINK+IS_REQUIRED)
    private String navLink;

    @ApiModelProperty(notes = "required Field.",required = true)
    @NotNull(message = SUB_MENU_ORDER+IS_REQUIRED)
    private int submenuOrder;

    @ApiModelProperty(hidden = true)
    private int menuId;

    @ApiModelProperty(hidden = true)
    private String menuName;

    public SubmenuDto() {
    }

    public SubmenuDto(int submenuId, String name,String navLink, int submenuOrder) {
        this.submenuId = submenuId;
        this.name = name;
        this.navLink=navLink;
        this.submenuOrder = submenuOrder;
    }

    public SubmenuDto(int submenuId, String name,String navLink, int submenuOrder, int menuId, String menuName) {
        this.submenuId = submenuId;
        this.name = name;
        this.navLink=navLink;
        this.submenuOrder = submenuOrder;
        this.menuId = menuId;
        this.menuName = menuName;
    }

    public int getSubmenuId() {
        return submenuId;
    }

    public void setSubmenuId(int submenuId) {
        this.submenuId = submenuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSubmenuOrder() {
        return submenuOrder;
    }

    public void setSubmenuOrder(int submenuOrder) {
        this.submenuOrder = submenuOrder;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getNavLink() {
        return navLink;
    }

    public void setNavLink(String navLink) {
        this.navLink = navLink;
    }

    @Override
    public String toString() {
        return "SubmenuDto{" +
                "submenuId=" + submenuId +
                ", name='" + name + '\'' +
                ", navLink='" + navLink + '\'' +
                ", submenuOrder=" + submenuOrder +
                ", menuId=" + menuId +
                ", menuName='" + menuName + '\'' +
                '}';
    }
}
