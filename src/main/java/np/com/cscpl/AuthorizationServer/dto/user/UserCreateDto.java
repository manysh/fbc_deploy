package np.com.cscpl.AuthorizationServer.dto.user;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class UserCreateDto {

    public static final String FULL_NAME="fullName";
    public static final String USERNAME="username";
    public static final String PASSWORD="password";
    public static final String NEWPASSWORD="newpassword";
    public static final String AUTHORITY="authority";

    public static final String IS_REQUIRED=" is required.";
    public static final String IS_OPTIONAL=" is optional.";

    @ApiModelProperty(value = FULL_NAME+IS_REQUIRED,required = true)
    private String fullName;

    @ApiModelProperty(value = USERNAME+IS_REQUIRED,required = true)
    private String userName;

    @ApiModelProperty(value = PASSWORD+IS_REQUIRED,required = true)
    private String password;
    
    @ApiModelProperty(value = NEWPASSWORD+IS_REQUIRED,required = true)
    private String newPassword;

    @ApiModelProperty(value = AUTHORITY+IS_REQUIRED,required = true)
    private List<Long> authorityIds;

    public UserCreateDto() {
    }

    public UserCreateDto(String fullName,  String userName, String password,String newPassword, List<Long> authorityIds) {
        this.fullName = fullName;
        this.userName = userName;
        this.password = password;
        this.newPassword = newPassword;
        this.authorityIds = authorityIds;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Long> getAuthority() {
        return authorityIds;
    }

    public void setAuthority(List<Long> authority) {
        this.authorityIds = authority;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    

}
