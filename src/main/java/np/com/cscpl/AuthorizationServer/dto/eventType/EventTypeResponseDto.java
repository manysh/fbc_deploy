/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.dto.eventType;

/**
 *
 * @author prabhu
 */
public class EventTypeResponseDto {

    private int id;
    private String type;

    public EventTypeResponseDto() {
    }

    public EventTypeResponseDto(int id) {
        this.id = id;
    }

    public EventTypeResponseDto(int id, String type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "EventTypeResponseDto{" + "id=" + id + ", type=" + type + '}';
    }

}
