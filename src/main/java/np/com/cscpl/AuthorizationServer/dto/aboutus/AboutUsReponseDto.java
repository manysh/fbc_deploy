package np.com.cscpl.AuthorizationServer.dto.aboutus;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AboutUsReponseDto {

    public static final String ID = "id";
    public static final String INTRODUCTION1 = "introduction1";
    public static final String INTRODUCTION2 = "introduction2";
    public static final String INTRODUCTION3 = "introduction3";
    public static final String TITLE1 = "title1";
    public static final String TITLE1DESCRIPTION = "title1Description";
    public static final String TITLE2 = "title2";
    public static final String TITLE2Description = "title2Description";
    public static final String IS_REQUIRED=" is required";

    @ApiModelProperty(hidden = true)
    private int id;

    @ApiModelProperty(notes = INTRODUCTION1+IS_REQUIRED,required = true)
    @NotNull(message = INTRODUCTION1+IS_REQUIRED)
    private String introduction1;

    @ApiModelProperty(notes = INTRODUCTION2+IS_REQUIRED,required = true)
    @NotNull(message = INTRODUCTION2+IS_REQUIRED)
    private String introduction2;

    @ApiModelProperty(notes = INTRODUCTION3+IS_REQUIRED,required = true)
    @NotNull(message = INTRODUCTION3+IS_REQUIRED)
    private String introduction3;

    @ApiModelProperty(notes = TITLE1+IS_REQUIRED,required = true)
    @NotNull(message = TITLE1+IS_REQUIRED)
    private String title1;

    @ApiModelProperty(notes = TITLE1DESCRIPTION+IS_REQUIRED,required = true)
    @NotNull(message = TITLE1DESCRIPTION+IS_REQUIRED)
    private String title1Description;

    @ApiModelProperty(notes = TITLE2+IS_REQUIRED,required = true)
    @NotNull(message = TITLE2+IS_REQUIRED)
    private String title2;

    @ApiModelProperty(notes = TITLE2Description+IS_REQUIRED,required = true)
    @NotNull(message = TITLE2Description+IS_REQUIRED)
    private String title2Description;


    public AboutUsReponseDto() {
    }

    public AboutUsReponseDto( @NotNull(message = INTRODUCTION1 + IS_REQUIRED) String introduction1, @NotNull(message = INTRODUCTION2 + IS_REQUIRED) String introduction2, @NotNull(message = INTRODUCTION3 + IS_REQUIRED) String introduction3, @NotNull(message = TITLE1 + IS_REQUIRED) String title1, @NotNull(message = TITLE1DESCRIPTION + IS_REQUIRED) String title1Description, @NotNull(message = TITLE2 + IS_REQUIRED) String title2, @NotNull(message = TITLE2Description + IS_REQUIRED) String title2Description) {
        this.id = id;
        this.introduction1 = introduction1;
        this.introduction2 = introduction2;
        this.introduction3 = introduction3;
        this.title1 = title1;
        this.title1Description = title1Description;
        this.title2 = title2;
        this.title2Description = title2Description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getIntroduction1() {
        return introduction1;
    }

    public void setIntroduction1(String introduction1) {
        this.introduction1 = introduction1;
    }

    public String getIntroduction2() {
        return introduction2;
    }

    public void setIntroduction2(String introduction2) {
        this.introduction2 = introduction2;
    }

    public String getIntroduction3() {
        return introduction3;
    }

    public void setIntroduction3(String introduction3) {
        this.introduction3 = introduction3;
    }

    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    public String getTitle1Description() {
        return title1Description;
    }

    public void setTitle1Description(String title1Description) {
        this.title1Description = title1Description;
    }

    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }

    public String getTitle2Description() {
        return title2Description;
    }

    public void setTitle2Description(String title2Description) {
        this.title2Description = title2Description;
    }
}
