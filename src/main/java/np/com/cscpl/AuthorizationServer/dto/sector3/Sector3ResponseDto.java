package np.com.cscpl.AuthorizationServer.dto.sector3;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class Sector3ResponseDto {
    public static final String SECTOR2_ID="sector3Id";
    public static final String SECTOR2_TTILE="sector3Title";
    public static final String SECTOR2_DESCRIPTION="sector3Description";
    public static final String IS_REQUIRED=" is required";
    public static final String IS_OPTIONAL=" is optional";

    @ApiModelProperty(hidden = true)
    private int sector3Id;

    @ApiModelProperty(required = true,notes = SECTOR2_TTILE+IS_REQUIRED)
    @NotNull(message = SECTOR2_TTILE+IS_REQUIRED)
    private String sector3Title;

    @ApiModelProperty(notes = SECTOR2_DESCRIPTION+IS_OPTIONAL)
    private String sector3Description;

    public Sector3ResponseDto() {
    }

    public Sector3ResponseDto(int sector3Id, @NotNull(message = SECTOR2_TTILE + IS_REQUIRED) String sector3Title, String sector3Description) {
        this.sector3Id = sector3Id;
        this.sector3Title = sector3Title;
        this.sector3Description = sector3Description;
    }

    public int getSector3Id() {
        return sector3Id;
    }

    public void setSector3Id(int sector3Id) {
        this.sector3Id = sector3Id;
    }

    public String getSector3Title() {
        return sector3Title;
    }

    public void setSector3Title(String sector3Title) {
        this.sector3Title = sector3Title;
    }

    public String getSector3Description() {
        return sector3Description;
    }

    public void setSector3Description(String sector3Description) {
        this.sector3Description = sector3Description;
    }
}
