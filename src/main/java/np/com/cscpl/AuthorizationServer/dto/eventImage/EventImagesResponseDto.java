/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.dto.eventImage;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author prabhu
 */
public class EventImagesResponseDto {

    private List<EventImageResponseDto> eventImageResponseDtos = new ArrayList<>();

    public EventImagesResponseDto() {
    }

    public EventImagesResponseDto(EventImageResponseDto eventImageResponseDto) {
        this.getEventImageResponseDtos().add(eventImageResponseDto);
    }

    public EventImagesResponseDto(List<EventImageResponseDto> eventImageResponseDtos) {
        this.eventImageResponseDtos = eventImageResponseDtos;
    }

    public List<EventImageResponseDto> getEventImageResponseDtos() {
        return eventImageResponseDtos;
    }

    public void setEventImageResponseDtos(List<EventImageResponseDto> eventImageResponseDtos) {
        this.eventImageResponseDtos = eventImageResponseDtos;
    }
}
