package np.com.cscpl.AuthorizationServer.dto.resourcetype;

import java.util.List;

public class ResourceTypesResponseDto {
    private List<ResourceTypeResourceDto> resourceTypeResourceDtoList;

    public ResourceTypesResponseDto() {
    }

    public ResourceTypesResponseDto(ResourceTypeResourceDto resourceTypeResourceDto) {
    }

    public ResourceTypesResponseDto(List<ResourceTypeResourceDto> resourceTypeResourceDtoList) {
        this.resourceTypeResourceDtoList = resourceTypeResourceDtoList;
    }

    public List<ResourceTypeResourceDto> getResourceTypeResourceDtoList() {
        return resourceTypeResourceDtoList;
    }

    public void setResourceTypeResourceDtoList(List<ResourceTypeResourceDto> resourceTypeResourceDtoList) {
        this.resourceTypeResourceDtoList = resourceTypeResourceDtoList;
    }
}
