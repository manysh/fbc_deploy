package np.com.cscpl.AuthorizationServer.converter.contact;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.contact.ContactReponseDto;
import np.com.cscpl.AuthorizationServer.model.Contact;
import org.springframework.stereotype.Component;

@Component
public class ContactConverter extends Converter<ContactReponseDto,Contact> {


    public ContactConverter(){
        super(
                contactReponseDto -> new Contact(

                        contactReponseDto.getFullName(),
                        contactReponseDto.getEmail(),
                        contactReponseDto.getPhoneNo(),
                        contactReponseDto.getContactReason(),
                        contactReponseDto.getMessage()
                ),contact -> new ContactReponseDto(
                        contact.getFullName(),
                        contact.getEmail(),
                        contact.getPhoneNo(),
                        contact.getContactReason(),
                        contact.getMessage()
                )
        );
    }
}
