package np.com.cscpl.AuthorizationServer.converter.menu;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.menu.SubmenuDto;
import np.com.cscpl.AuthorizationServer.model.Menu;
import np.com.cscpl.AuthorizationServer.model.SubMenu;
import org.springframework.stereotype.Component;

@Component
public class SubmenuConverter extends Converter<SubmenuDto,SubMenu> {
    public  SubmenuConverter(){
        super(submenuDto -> new SubMenu(
                submenuDto.getMenuId(),
                submenuDto.getName(),
                submenuDto.getNavLink(),
                submenuDto.getSubmenuOrder(),
                new Menu(
                        submenuDto.getMenuId(),
                        submenuDto.getName()
                )
        ),subMenu -> new SubmenuDto(
                subMenu.getId(),
                subMenu.getName(),
                subMenu.getNavLink(),
                subMenu.getOrder(),
                subMenu.getMenu().getId(),
                subMenu.getMenu().getName()
        ));
    }
}
