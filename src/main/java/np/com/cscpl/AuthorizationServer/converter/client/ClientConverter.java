package np.com.cscpl.AuthorizationServer.converter.client;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.client.ClientResponseDto;
import np.com.cscpl.AuthorizationServer.dto.finalproject.FinalProjectResponseDto;
import np.com.cscpl.AuthorizationServer.dto.project.ProjectResponseDto;
import np.com.cscpl.AuthorizationServer.model.Client;
import np.com.cscpl.AuthorizationServer.model.FinalProject;
import np.com.cscpl.AuthorizationServer.model.Project;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ClientConverter extends Converter<ClientResponseDto, Client> {

    public ClientConverter() {
        super(clientResponseDto -> new Client(
                clientResponseDto.getClientName(),
                clientResponseDto.getDescription(),
                clientResponseDto.getImagePath(),
                clientResponseDto.getProjectList().stream().map(finalProjectResponseDto -> {
                    return new FinalProject(
                            finalProjectResponseDto.getProjectId(),
                            finalProjectResponseDto.getTitle(),
                            finalProjectResponseDto.getAssignmentTitle(),
                            finalProjectResponseDto.getDescription(),
                            finalProjectResponseDto.getWorkDescription(),
                            finalProjectResponseDto.getCreatedDate(),
                            finalProjectResponseDto.getEstimatedCompletionDate(),
                            finalProjectResponseDto.getProjectStatus(),
                            finalProjectResponseDto.getImagePath(),
                            finalProjectResponseDto.getFilePath());
                }).collect(Collectors.toList()),
                clientResponseDto.getProjectList2().stream().map(finalProjectResponseDto -> {
                    return new FinalProject(
                            finalProjectResponseDto.getProjectId(),
                            finalProjectResponseDto.getTitle(),
                            finalProjectResponseDto.getAssignmentTitle(),
                            finalProjectResponseDto.getDescription(),
                            finalProjectResponseDto.getWorkDescription(),
                            finalProjectResponseDto.getCreatedDate(),
                            finalProjectResponseDto.getEstimatedCompletionDate(),
                            finalProjectResponseDto.getProjectStatus(),
                            finalProjectResponseDto.getImagePath(),
                            finalProjectResponseDto.getFilePath());
                }).collect(Collectors.toList())
        ), client -> new ClientResponseDto(
                client.getClientId(),
                client.getClientName(),
                client.getDescription(),
                client.getImagePath(),
                client.getProjectList2().stream().map(eachProject -> {
                    return new FinalProjectResponseDto(
                            eachProject.getId(),
                            eachProject.getTitle(),
                            eachProject.getAssignmentTitle(),
                            eachProject.getDescription(),
                            eachProject.getWorkDescription(),
                            eachProject.getCreatedDate(),
                            eachProject.getCompletetionDate(),
                            eachProject.getClient1() != null ? eachProject.getClient1().getClientId() : 0,
                            eachProject.getClient1() != null ? eachProject.getClient1().getClientName() : "",
                            eachProject.getClient2() != null ? eachProject.getClient2().getClientId() : 0,
                            eachProject.getClient2() != null ? eachProject.getClient2().getClientName() : "",
                            eachProject.getSector1() != null ? eachProject.getSector1().getId() : 0,
                            eachProject.getSector1() != null ? eachProject.getSector1().getName() : "",
                            eachProject.getProjectStatus(),
                            eachProject.getImagePath(),
                            eachProject.getFilePath());
                }).collect(Collectors.toList()),
                //                client.getUserImage(),
                client.getProjectList2().stream().map(eachProject -> {
                    return new FinalProjectResponseDto(
                            eachProject.getId(),
                            eachProject.getTitle(),
                            eachProject.getAssignmentTitle(),
                            eachProject.getDescription(),
                            eachProject.getWorkDescription(),
                            eachProject.getCreatedDate(),
                            eachProject.getCompletetionDate(),
                            eachProject.getClient1() != null ? eachProject.getClient1().getClientId() : 0,
                            eachProject.getClient1() != null ? eachProject.getClient1().getClientName() : "",
                            eachProject.getClient2() != null ? eachProject.getClient2().getClientId() : 0,
                            eachProject.getClient2() != null ? eachProject.getClient2().getClientName() : "",
                            eachProject.getSector1() != null ? eachProject.getSector1().getId() : 0,
                            eachProject.getSector1() != null ? eachProject.getSector1().getName() : "",
                            eachProject.getProjectStatus(),
                            eachProject.getImagePath(),
                            eachProject.getFilePath());
                }).collect(Collectors.toList())));

    }
}
