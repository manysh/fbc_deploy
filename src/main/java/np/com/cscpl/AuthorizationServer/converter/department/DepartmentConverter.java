package np.com.cscpl.AuthorizationServer.converter.department;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.department.DepartmentResponseDto;
import np.com.cscpl.AuthorizationServer.model.Department;
import org.springframework.stereotype.Component;

@Component
public class DepartmentConverter extends Converter<DepartmentResponseDto, Department> {

    public DepartmentConverter() {
        super(departmentResponseDto -> new Department(
                departmentResponseDto.getId(),
                departmentResponseDto.getDepartment()
        ), department -> new DepartmentResponseDto(
                department.getId(),
                department.getDepartment()
        ));
    }
}
