package np.com.cscpl.AuthorizationServer.converter.sector;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.sector.SectorResponseDto;
import np.com.cscpl.AuthorizationServer.model.Sector;
import org.springframework.stereotype.Component;

@Component
public class SectorConverter extends Converter<SectorResponseDto, Sector> {

    public SectorConverter() {
        super(sectorResponseDto -> new Sector(
                sectorResponseDto.getId(),
                sectorResponseDto.getName(),
                sectorResponseDto.getDisplayOrder(),
                sectorResponseDto.getDetail()
        ), sector -> new SectorResponseDto()
                .getBuilder()
                .id(sector.getId())
                .name(sector.getName())
                .detail(sector.getDetail())
                .displayOrder(sector.getDisplayOrder())
                .build());
    }
}
