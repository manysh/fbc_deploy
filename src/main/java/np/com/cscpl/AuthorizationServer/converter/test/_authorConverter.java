/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.converter.test;

import java.util.stream.Collectors;
import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.test._authorResponseDto;
import np.com.cscpl.AuthorizationServer.dto.test._bookResponseDto;
import np.com.cscpl.AuthorizationServer.model._author;
import np.com.cscpl.AuthorizationServer.model._book;
import org.springframework.stereotype.Component;

/**
 *
 * @author Puzan Sakya < puzan at puzansakya@gmail.com >
 */
@Component
public class _authorConverter extends Converter<_authorResponseDto, _author> {

    public _authorConverter() {
        super(aDto -> new _author(
                aDto.getId(),
                aDto.getName(),
                aDto.getBookList().stream().map(bDto -> {
                    return new _book(
                            bDto.getId(),
                            bDto.getName()
                    );
                }).collect(Collectors.toList())
        ), a -> new _authorResponseDto(
                a.getId(),
                a.getName(),
                a.getBookList().stream().map(
                        b -> {
                            return new _bookResponseDto(
                                    b.getId(),
                                    b.getName()
                            );
                        }).collect(Collectors.toList())
        ));
    }

}
