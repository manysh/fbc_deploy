package np.com.cscpl.AuthorizationServer.converter.finalproject;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.finalproject.FinalProjectResponseDto;
import np.com.cscpl.AuthorizationServer.model.FinalProject;
import org.springframework.stereotype.Component;

@Component
public class FinalProjectConverter extends Converter<FinalProjectResponseDto, FinalProject> {

    public FinalProjectConverter() {
        super(finalProjectResponseDto -> new FinalProject(
                finalProjectResponseDto.getProjectId(),
                finalProjectResponseDto.getTitle(),
                finalProjectResponseDto.getAssignmentTitle(),
                finalProjectResponseDto.getDescription(),
                finalProjectResponseDto.getWorkDescription(),
                finalProjectResponseDto.getCreatedDate(),
                finalProjectResponseDto.getEstimatedCompletionDate(),
                finalProjectResponseDto.getProjectStatus(),
                finalProjectResponseDto.getImagePath(),
                finalProjectResponseDto.getFilePath()
        ), finalProject -> new FinalProjectResponseDto(
                finalProject.getId(),
                finalProject.getTitle(),
                finalProject.getAssignmentTitle(),
                finalProject.getDescription(),
                finalProject.getWorkDescription(),
                finalProject.getCreatedDate(),
                finalProject.getCompletetionDate(),
                finalProject.getClient1() != null ? finalProject.getClient1().getClientId() : 0,
                finalProject.getClient1() != null ? finalProject.getClient1().getClientName() : "",
                finalProject.getClient2() != null ? finalProject.getClient2().getClientId() : 0,
                finalProject.getClient2() != null ? finalProject.getClient2().getClientName() : "",
                finalProject.getSector1() != null ? finalProject.getSector1().getId() : 0,
                finalProject.getSector1() != null ? finalProject.getSector1().getName() : "",
                finalProject.getProjectStatus(),
                finalProject.getImagePath(),
                finalProject.getFilePath()
        ));
    }
}
