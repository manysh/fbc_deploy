package np.com.cscpl.AuthorizationServer.converter.tag;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.sector.SectorResponseDto;
import np.com.cscpl.AuthorizationServer.dto.tag.TagResponseDto;
import np.com.cscpl.AuthorizationServer.model.Sector;
import np.com.cscpl.AuthorizationServer.model.Tag;
import org.springframework.stereotype.Component;

@Component
public class TagConverter extends Converter<TagResponseDto, Tag> {

    public TagConverter() {
        super(tagResponseDto -> new Tag(
                tagResponseDto.getId(),
                tagResponseDto.getTag()
        ), tag -> new TagResponseDto(
                tag.getId(),
                tag.getTag()
        ));
    }

}
