package np.com.cscpl.AuthorizationServer.converter.batchimage;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.batchimage.BatchImageResponseDto;
import np.com.cscpl.AuthorizationServer.model.BatchImage;
import org.springframework.stereotype.Component;

@Component
public class BatchImageConverter extends Converter<BatchImageResponseDto,BatchImage> {
    public BatchImageConverter(){
        super(batchImageResponseDto -> new BatchImage(
                batchImageResponseDto.getImageId(),
                batchImageResponseDto.getImageLink(),
                batchImageResponseDto.getCreatedDate()
        ),batchImage -> new BatchImageResponseDto(
                batchImage.getId(),
                batchImage.getImageLink(),
                batchImage.getCreatedDate()
        ));
    }
}
