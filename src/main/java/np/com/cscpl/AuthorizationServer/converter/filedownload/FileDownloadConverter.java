package np.com.cscpl.AuthorizationServer.converter.filedownload;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.filedownload.FileDownloadResponseDto;
import np.com.cscpl.AuthorizationServer.model.FileDownload;
import org.springframework.stereotype.Component;

@Component
public class FileDownloadConverter extends Converter<FileDownloadResponseDto,FileDownload>{
    public FileDownloadConverter(){
        super(fileDownloadResponseDto -> new FileDownload(
                        fileDownloadResponseDto.getFileDownlaodId(),
                        fileDownloadResponseDto.getFileName(),
                        fileDownloadResponseDto.getFileDescription(),
                        fileDownloadResponseDto.getImagePath(),
                        fileDownloadResponseDto.getCreatedAt()
                ),fileDownload -> new FileDownloadResponseDto(
                        fileDownload.getId(),
                        fileDownload.getFileName(),
                        fileDownload.getFileDescription(),
                        fileDownload.getDownloadLink(),
                        fileDownload.getCreatedAt()
                )
        );
    }
}
