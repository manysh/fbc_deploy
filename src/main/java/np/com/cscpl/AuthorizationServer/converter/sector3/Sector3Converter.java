package np.com.cscpl.AuthorizationServer.converter.sector3;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.sector3.Sector3ResponseDto;
import np.com.cscpl.AuthorizationServer.model.Sector3;
import org.springframework.stereotype.Component;

@Component
public class Sector3Converter  extends Converter<Sector3ResponseDto,Sector3> {
    public Sector3Converter(){
        super(sector3ResponseDto -> new Sector3(
                sector3ResponseDto.getSector3Id(),
                sector3ResponseDto.getSector3Title(),
                sector3ResponseDto.getSector3Description()
        ),sector3 -> new Sector3ResponseDto(
                sector3.getId(),
                sector3.getTitle(),
                sector3.getDescription()
        ));
    }
}
