package np.com.cscpl.AuthorizationServer.converter.aboutus;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.aboutus.AboutUsReponseDto;
import np.com.cscpl.AuthorizationServer.model.AboutUs;
import org.springframework.stereotype.Component;


@Component
public class AboutUsConverter extends Converter<AboutUsReponseDto,AboutUs> {
    public AboutUsConverter() {
        super(
                aboutUsReponseDto -> new AboutUs(
                        aboutUsReponseDto.getIntroduction1(),
                        aboutUsReponseDto.getIntroduction2(),
                        aboutUsReponseDto.getIntroduction3(),
                        aboutUsReponseDto.getTitle1(),
                        aboutUsReponseDto.getTitle1Description(), aboutUsReponseDto.getTitle2(),
                        aboutUsReponseDto.getTitle2Description()
                ),
                aboutUs -> new AboutUsReponseDto(

                        aboutUs.getIntroduction1(),
                        aboutUs.getIntroduction2(),
                        aboutUs.getIntroduction3(),
                        aboutUs.getTitle1(),
                        aboutUs.getTitle1Description(),
                        aboutUs.getTitle2(),
                        aboutUs.getTitle2Description()


                )
        );

    }
}
