package np.com.cscpl.AuthorizationServer.converter.career;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.career.CareerResponseDto;
import np.com.cscpl.AuthorizationServer.model.Career;
import org.springframework.stereotype.Component;

@Component
public class CareerConverter extends Converter<CareerResponseDto,Career> {

    public CareerConverter(){
        super(
                careerResponseDto -> new Career(
                        careerResponseDto.getId(),
                        careerResponseDto.getFullName(),
                        careerResponseDto.getEmail(),
                        careerResponseDto.getContactNo(),
                        careerResponseDto.getCvLink(),
                        careerResponseDto.getSubject(),
                        careerResponseDto.getQualification(),
                        careerResponseDto.getMessage()
                ),career -> new CareerResponseDto(
                        career.getId(),
                        career.getFullName(),
                        career.getEmail(),
                        career.getContactNo(),
                        career.getCvLink(),
                        career.getSubject(),
                        career.getQualification(),
                        career.getMessage()
                )
        );
    }
}
