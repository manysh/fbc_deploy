package np.com.cscpl.AuthorizationServer.converter.consultanttype;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.consultanttype.ConsultantTypeResponseDto;
import np.com.cscpl.AuthorizationServer.model.ConsultantType;
import org.springframework.stereotype.Component;

@Component
public class ConsultantTypeConverter extends Converter<ConsultantTypeResponseDto,ConsultantType>{

    public ConsultantTypeConverter(){
        super(resourceTypeResourceDto -> new ConsultantType(
                resourceTypeResourceDto.getId(),
                resourceTypeResourceDto.getName()
        ),resourceType -> new ConsultantTypeResponseDto(
                resourceType.getId(),
                resourceType.getConsultantTypeName()
        ));
    }
}
