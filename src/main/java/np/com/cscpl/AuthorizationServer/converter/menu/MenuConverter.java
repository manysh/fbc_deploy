package np.com.cscpl.AuthorizationServer.converter.menu;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.menu.MenuResponseDto;
import np.com.cscpl.AuthorizationServer.dto.menu.SubmenuDto;
import np.com.cscpl.AuthorizationServer.model.Menu;
import np.com.cscpl.AuthorizationServer.model.SubMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;


@Component
public class MenuConverter extends Converter<MenuResponseDto,Menu> {
    @Autowired
    private SubmenuConverter submenuConverter;

    public MenuConverter(){
        super(menuResponseDto -> new Menu(
                menuResponseDto.getMenuId(),
                menuResponseDto.getName(),
                menuResponseDto.getNavLink(),
                menuResponseDto.getMenuOrder(),
                menuResponseDto.getMenuIcon(),
                menuResponseDto.getSubmenuDtos().stream().map(submenuDto -> {
                    return new SubMenu(
                            submenuDto.getMenuId(),
                            submenuDto.getName(),
                            submenuDto.getNavLink(),
                            submenuDto.getSubmenuOrder());
                }).collect(Collectors.toList())
        ),menu -> new MenuResponseDto(
                menu.getId(),
                menu.getName(),
                menu.getNavLink(),
                menu.getOrder(),
                menu.getIcon(),
                menu.getSubMenus().stream().map(eachSubMenu ->
                    new SubmenuDto(
                            eachSubMenu.getId(),
                            eachSubMenu.getName(),
                            eachSubMenu.getNavLink(),
                            eachSubMenu.getOrder(),
                            eachSubMenu.getMenu().getId(),
                            eachSubMenu.getMenu().getName()
                    )
                ).collect(Collectors.toList())
        ));
    }
}
