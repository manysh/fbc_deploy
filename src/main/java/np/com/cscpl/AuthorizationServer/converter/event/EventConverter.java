/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.converter.event;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.event.EventResponseDto;
import np.com.cscpl.AuthorizationServer.model.Event;
import np.com.cscpl.AuthorizationServer.model.EventType;
import org.springframework.stereotype.Component;

/**
 *
 * @author puzansakya
 */
@Component
public class EventConverter extends Converter<EventResponseDto, Event> {

    public EventConverter() {
        super(eventResponseDTO -> new Event(
                eventResponseDTO.getId(),
                eventResponseDTO.getName(),
                eventResponseDTO.getDescription(),
                eventResponseDTO.getEventDate(),
                eventResponseDTO.getImagePath(),
                new EventType(eventResponseDTO.getEventTypeId()),
                eventResponseDTO.getIsShow(),
                eventResponseDTO.getEgIsShow()
        ), event -> new EventResponseDto(
                event.getId(),
                event.getName(),
                event.getDescription(),
                event.getEventDate(),
                event.getImagePath(),
                event.getEventType(),
                event.getIsShow(),
                event.getEgIsShow()
        ));

    }

}
