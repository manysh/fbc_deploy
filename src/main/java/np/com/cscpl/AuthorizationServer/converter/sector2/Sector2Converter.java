package np.com.cscpl.AuthorizationServer.converter.sector2;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.sector2.Sector2ResponseDto;
import np.com.cscpl.AuthorizationServer.model.Sector2;
import org.springframework.stereotype.Component;

@Component
public class Sector2Converter extends Converter<Sector2ResponseDto,Sector2> {

    public Sector2Converter(){
        super(sector2ResponseDto -> new Sector2(
                sector2ResponseDto.getSector2Id(),
                sector2ResponseDto.getSector2Title(),
                sector2ResponseDto.getSector2Description()
        ),sector2 -> new Sector2ResponseDto(
                sector2.getId(),
                sector2.getTitle(),
                sector2.getDescription()
        ));
    }
}
