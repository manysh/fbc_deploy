/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.converter.eventtype;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.eventType.EventTypeResponseDto;
import np.com.cscpl.AuthorizationServer.model.EventType;
import org.springframework.stereotype.Component;

/**
 *
 * @author puzansakya
 */
@Component
public class EventTypeConverter extends Converter<EventTypeResponseDto, EventType> {

    public EventTypeConverter() {
        super(eventTypeResponseDTO -> new EventType(
                eventTypeResponseDTO.getId(),
                eventTypeResponseDTO.getType()
        ), eventType -> new EventTypeResponseDto(
                eventType.getId(),
                eventType.getType()
        ));

    }
}
