package np.com.cscpl.AuthorizationServer.converter.provience;


import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.geography.GeographyResponseDto;
import np.com.cscpl.AuthorizationServer.dto.province.DistrictResponseDto;
import np.com.cscpl.AuthorizationServer.dto.province.ProvienceResponseDto;
import np.com.cscpl.AuthorizationServer.model.District;
import np.com.cscpl.AuthorizationServer.model.Geography;
import np.com.cscpl.AuthorizationServer.model.Provience;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ProvienceConverter extends Converter<ProvienceResponseDto,Provience>{
    public ProvienceConverter(){
        super(provienceResponseDto -> new Provience(
                provienceResponseDto.getProvienceId(),
                provienceResponseDto.getName(),
                provienceResponseDto.getDescription(),
                provienceResponseDto.getDistrictList().stream().map(districtResponseDto -> {

                    return new District(
                            districtResponseDto.getDistrictId(),
                            districtResponseDto.getName(),
                            districtResponseDto.getGeographyList().stream().map(geographyResponseDto -> {

                                return  new Geography(
                                        geographyResponseDto.getGeographyId(),
                                        geographyResponseDto.getGeographyName()
                                );
                            }).collect(Collectors.toList()));
                }).collect(Collectors.toList())
        ),provience -> new ProvienceResponseDto(
                provience.getId(),
                provience.getName(),
                provience.getDescription(),
                provience.getDistrictList().stream().map(eachDistrict ->{

                    return new DistrictResponseDto(
                            eachDistrict.getId(),
                            eachDistrict.getDistrictName(),
                            eachDistrict.getProvience().getId(),
                            eachDistrict.getProvience().getName(),
                            eachDistrict.getGeographyList().stream().map(eachGeography ->
                                    new GeographyResponseDto(
                                            eachGeography.getGeographyId(),
                                            eachGeography.getGeographyName()
                                    )).collect(Collectors.toList()));


                }).collect(Collectors.toList())
        ));


    }
}
