package np.com.cscpl.AuthorizationServer.converter.user;

import np.com.cscpl.AuthorizationServer.dto.user.UserCreateDto;
import np.com.cscpl.AuthorizationServer.model.User;
import np.com.cscpl.AuthorizationServer.repository.authority.AuthorityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserCreateConverter {

    private AuthorityRepository authorityRepository;
    private BCryptPasswordEncoder passwordEncoder= new BCryptPasswordEncoder(8);

    @Autowired
    public UserCreateConverter(AuthorityRepository authorityRepository) {

        this.authorityRepository = authorityRepository;
    }

    public User convertCreateUser(User databaseUser, UserCreateDto userCreateDto) throws Exception{
        if(userCreateDto==null || userCreateDto==new UserCreateDto()){
            return databaseUser;
        }
        databaseUser.setFullName(userCreateDto.getFullName());

        databaseUser.setUsername(userCreateDto.getUserName());

        List<Long> authorities=userCreateDto.getAuthority();

        if(!authorities.isEmpty()){
            databaseUser.setAuthorities(authorityRepository.findByIdIn(authorities));
        }

        String password=userCreateDto.getPassword();
        if(password !=null){
            databaseUser.setPassword(passwordEncoder.encode(password));
        }
        databaseUser.setEnabled(true);



        return databaseUser;
    }
}
