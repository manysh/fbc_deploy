package np.com.cscpl.AuthorizationServer.converter.user;

import np.com.cscpl.AuthorizationServer.dto.user.UserResponseDto;
import np.com.cscpl.AuthorizationServer.model.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class UserConverter {



    public static UserResponseDto convert(User user){
      BCryptPasswordEncoder passwordEncoder= new BCryptPasswordEncoder(8);

        UserResponseDto userResponseDto= new UserResponseDto();
        if(user==null || user==new User()){
            return userResponseDto;
        }
        userResponseDto.setUserId(user.getId());
        userResponseDto.setFullName(user.getFullName());

        userResponseDto.setUserName(user.getUsername());
        userResponseDto.setPassword(passwordEncoder.encode(user.getPassword()));

        userResponseDto.setAuthorityIds(
                user.getAuthorities()
                        .stream()
                        .map(authority -> authority.getId())
                        .collect(Collectors.toList())
        );

        userResponseDto.setAuthorityNames(user.getAuthorities().stream().map(authority -> authority.getName()).collect(Collectors.toList()));


        return userResponseDto;

    }
}
