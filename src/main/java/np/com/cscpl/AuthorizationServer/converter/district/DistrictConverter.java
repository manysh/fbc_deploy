package np.com.cscpl.AuthorizationServer.converter.district;


import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.district.DistrictResponseDto;
import np.com.cscpl.AuthorizationServer.model.District;
import org.springframework.stereotype.Component;

@Component
public class DistrictConverter extends Converter<DistrictResponseDto,District>{
    public DistrictConverter(){
        super(districtResponseDto -> new District(
                districtResponseDto.getDistrictId(),
                districtResponseDto.getName()
        ),district -> new DistrictResponseDto(
                district.getId(),
                district.getDistrictName()
        ));
    }
}
