package np.com.cscpl.AuthorizationServer.converter.project;

import java.util.stream.Collectors;
import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.client.ClientResponseDto;
import np.com.cscpl.AuthorizationServer.dto.district.DistrictResponseDto;
import np.com.cscpl.AuthorizationServer.dto.project.ProjectResponseDto;
import np.com.cscpl.AuthorizationServer.dto.sector.SectorResponseDto;
import np.com.cscpl.AuthorizationServer.dto.tag.TagResponseDto;
import np.com.cscpl.AuthorizationServer.model.Client;
import np.com.cscpl.AuthorizationServer.model.Project;
import np.com.cscpl.AuthorizationServer.model.Sector;
import org.springframework.stereotype.Component;

@Component
public class ProjectConverter extends Converter<ProjectResponseDto, Project> {

    public ProjectConverter() {
        super(projectResponseDto -> new Project(
                projectResponseDto.getId(),
                projectResponseDto.getCreatedAt(),
                projectResponseDto.getDeletedAt(),
                projectResponseDto.getDescription(),
                projectResponseDto.getEstimatedCompletionDate(),
                projectResponseDto.getInitiatedDate(),
                projectResponseDto.getIsDelete(),
                projectResponseDto.getName(),
                projectResponseDto.getStatus(),
                projectResponseDto.getUpdateAt(),
                projectResponseDto.getBackdrop(),
                new Sector(
                        projectResponseDto.getSectorDTO().getId()
                )
        ), project -> new ProjectResponseDto().getBuilder()
                .id(project.getId())
                .createdAt(project.getCreatedAt())
                .description(project.getDescription())
                .estimatedCompletionDate(project.getEstimatedCompletionDate())
                .initiatedDate(project.getInitiatedDate())
                .isDelete(project.getIsDelete())
                .name(project.getName())
                .status(project.getStatus())
                .updateAt(project.getUpdatedAt())
                .backdrop(project.getBackdrop())
                .sectorDTO(new SectorResponseDto()
                        .getBuilder()
                        .id(project.getSector().getId())
                        .name(project.getSector().getName())
                        .detail(project.getSector().getDetail())
                        .build()
                )
                .districtDTOList(project.getDistrictList().stream().map(district
                        -> new DistrictResponseDto(
                        district.getId(),
                        district.getDistrictName()
                )).collect(Collectors.toList()))
                .tagDTOList(project.getTags().stream().map(tag
                        -> new TagResponseDto(
                        tag.getId(),
                        tag.getTag()
                )).collect(Collectors.toList()))
                .build()
        );
    }
}
