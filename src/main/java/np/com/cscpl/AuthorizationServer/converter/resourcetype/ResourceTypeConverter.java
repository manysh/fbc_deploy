package np.com.cscpl.AuthorizationServer.converter.resourcetype;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.resourcetype.ResourceTypeResourceDto;
import np.com.cscpl.AuthorizationServer.model.ResourceType;
import org.springframework.stereotype.Component;

@Component
public class ResourceTypeConverter extends Converter<ResourceTypeResourceDto,ResourceType> {

    public ResourceTypeConverter(){
        super(resourceTypeResourceDto -> new ResourceType(
                resourceTypeResourceDto.getId(),
                resourceTypeResourceDto.getName()
        ),resourceType -> new ResourceTypeResourceDto(
                resourceType.getId(),
                resourceType.getResourceTypeName()
        ));
    }
}
