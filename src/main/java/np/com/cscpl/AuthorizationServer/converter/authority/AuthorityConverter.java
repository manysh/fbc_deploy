package np.com.cscpl.AuthorizationServer.converter.authority;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.authority.AuthorityResposeDto;
import np.com.cscpl.AuthorizationServer.model.Authority;
import org.springframework.stereotype.Component;

@Component
public class AuthorityConverter extends Converter<AuthorityResposeDto,Authority> {
    public AuthorityConverter(){
        super(authorityResposeDto -> new Authority(),
                authority -> new AuthorityResposeDto(
                        authority.getId(),
                        authority.getName()
                ));
    }
}
