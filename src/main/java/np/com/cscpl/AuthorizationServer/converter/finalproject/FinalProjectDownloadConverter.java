package np.com.cscpl.AuthorizationServer.converter.finalproject;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.finalproject.FinalProjectDownloadResponseDto;
import np.com.cscpl.AuthorizationServer.model.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


@Component
public class FinalProjectDownloadConverter extends Converter<FinalProjectDownloadResponseDto,FinalProject> {

    public FinalProjectDownloadConverter(){
        super(finalProjectDownloadResponseDto -> new FinalProject(),finalProject -> new FinalProjectDownloadResponseDto(
                finalProject.getId(),
                finalProject.getTitle(),
                finalProject.getAssignmentTitle(),
                finalProject.getDescription(),
                finalProject.getWorkDescription(),
                finalProject.getCreatedDate(),
                finalProject.getCompletetionDate(),
                finalProject.getProjectStatus(),
                finalProject.getClient1() !=null?finalProject.getClient1().getClientName():"",
                finalProject.getClient2() !=null?finalProject.getClient2().getClientName():"",
                finalProject.getSector1()!=null?finalProject.getSector1().getName():"",
                sector2ToString(finalProject.getSector2s()),
                sector3ToString(finalProject.getSector3s()),
                locationToString(finalProject.getDistricts()),
                agencyToString(finalProject.getFundingAgencies())
        ));
    }

    public static String sector2ToString(List<Sector2> sector2s){
        if(sector2s.isEmpty() || sector2s==null){
            return "";
        }
        else{
            return sector2s.stream().map(sector2 -> sector2.getTitle()).collect(Collectors.joining(","));
        }
    }

    public static String sector3ToString(List<Sector3> sector3s){
        if(sector3s.isEmpty() || sector3s==null){
            return "";
        }
        else{
            return sector3s.stream().map(sector3 -> sector3.getTitle()).collect(Collectors.joining(","));
        }
    }

    public static String locationToString(List<District> districts){
        if(districts.isEmpty()|| districts==null){
            return "";
        }
        else{
            return districts.stream().map(district -> district.getDistrictName()).collect(Collectors.joining(","));
        }
    }

    public static String agencyToString(List<FundingAgency> fundingAgencies){
        if(fundingAgencies.isEmpty() || fundingAgencies ==null){
            return "";
        }
        else{
            return fundingAgencies.stream().map(fundingAgency -> fundingAgency.getName()).collect(Collectors.joining(","));
        }
    }
}
