package np.com.cscpl.AuthorizationServer.converter.agency;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.agency.FundingAgencyResponseDto;
import np.com.cscpl.AuthorizationServer.model.FundingAgency;
import org.springframework.stereotype.Component;

@Component
public class FundingAgencyConverter extends Converter<FundingAgencyResponseDto,FundingAgency>{
    public FundingAgencyConverter(){
        super(fundingAgencyResponseDto -> new FundingAgency(
                fundingAgencyResponseDto.getAgencyId(),
                fundingAgencyResponseDto.getAgencyName(),
                fundingAgencyResponseDto.getAgencyDescription()
        ),fundingAgency -> new FundingAgencyResponseDto(
                fundingAgency.getId(),
                fundingAgency.getName(),
                fundingAgency.getDescription()
        ));
    }
}
