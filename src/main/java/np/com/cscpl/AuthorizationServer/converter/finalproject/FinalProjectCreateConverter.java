package np.com.cscpl.AuthorizationServer.converter.finalproject;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.finalproject.FinalProjectCreateResponseDto;
import np.com.cscpl.AuthorizationServer.model.FinalProject;
import org.springframework.stereotype.Component;

@Component
public class FinalProjectCreateConverter extends Converter<FinalProjectCreateResponseDto, FinalProject> {

    public FinalProjectCreateConverter() {
        super(finalProjectCreateResponseDto -> new FinalProject(
                finalProjectCreateResponseDto.getProjectId(),
                finalProjectCreateResponseDto.getTitle(),
                finalProjectCreateResponseDto.getAssignmentTitle(),
                finalProjectCreateResponseDto.getDescription(),
                finalProjectCreateResponseDto.getWorkDescription(),
                finalProjectCreateResponseDto.getCreatedDate(),
                finalProjectCreateResponseDto.getEstimatedCompletionDate(),
                finalProjectCreateResponseDto.getProjectStatus(),
                finalProjectCreateResponseDto.getImagePath(),
                finalProjectCreateResponseDto.getFilePath()
        ), finalProject -> new FinalProjectCreateResponseDto(
                finalProject.getId(),
                finalProject.getTitle(),
                finalProject.getAssignmentTitle(),
                finalProject.getDescription(),
                finalProject.getWorkDescription(),
                finalProject.getCreatedDate(),
                finalProject.getCompletetionDate(),
                finalProject.getProjectStatus(),
                finalProject.getImagePath(),
                finalProject.getFilePath()
        ));
    }
}
