package np.com.cscpl.AuthorizationServer.converter.finalproject;

import np.com.cscpl.AuthorizationServer.dto.finalproject.FinalProjectResponseDto;
import np.com.cscpl.AuthorizationServer.model.FinalProject;
import org.springframework.data.domain.Page;

import static np.com.cscpl.AuthorizationServer.facade.finalproject.Helper.*;

public class FinalProjectPageListMap {

    public static Page<?> unboundedMap(Page<FinalProject> finalProjects) {
        return finalProjects.map(finalProject -> {
            FinalProjectResponseDto finalProjectResponseDto = new FinalProjectResponseDto(
                    finalProject.getId(),
                    finalProject.getTitle(),
                    finalProject.getAssignmentTitle(),
                    finalProject.getDescription(),
                    finalProject.getWorkDescription(),
                    finalProject.getCreatedDate(),
                    finalProject.getCompletetionDate(),
                    finalProject.getClient1() != null ? finalProject.getClient1().getClientId() : 0,
                    finalProject.getClient1() != null ? finalProject.getClient1().getClientName() : "",
                    finalProject.getClient2() != null ? finalProject.getClient2().getClientId() : 0,
                    finalProject.getClient2() != null ? finalProject.getClient2().getClientName() : "",
                    finalProject.getSector1() != null ? finalProject.getSector1().getId() : 0,
                    finalProject.getSector1() != null ? finalProject.getSector1().getName() : "",
                    finalProject.getProjectStatus(),
                    finalProject.getImagePath(),
                    finalProject.getFilePath()
            );
            try {
                return setLink(finalProjectResponseDto);
            } catch (Exception ex) {
                System.out.println(ex.getLocalizedMessage());
                return finalProjectResponseDto;
            }
        });
    }
}
