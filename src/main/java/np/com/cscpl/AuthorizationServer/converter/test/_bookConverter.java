/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.converter.test;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.test._bookResponseDto;
import np.com.cscpl.AuthorizationServer.model._book;
import org.springframework.stereotype.Component;

/**
 *
 * @author Puzan Sakya < puzan at puzansakya@gmail.com >
 */
@Component
public class _bookConverter extends Converter<_bookResponseDto, _book> {

    public _bookConverter() {
        super(bDto -> new _book(
                bDto.getId(),
                bDto.getName()
        ), b -> new _bookResponseDto(
                b.getId(),
                b.getName()
        ));
    }

}
