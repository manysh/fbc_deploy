package np.com.cscpl.AuthorizationServer.converter.degree;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.degree.DegreeResponseDto;
import np.com.cscpl.AuthorizationServer.model.Degree;
import org.springframework.stereotype.Component;

@Component
public class DegreeConverter extends Converter<DegreeResponseDto, Degree> {

    public DegreeConverter() {
        super(degreeResponseDto -> new Degree(
                degreeResponseDto.getId(),
                degreeResponseDto.getName()
        ), degree -> new DegreeResponseDto(
                degree.getId(),
                degree.getName()
        ));
    }
}
