package np.com.cscpl.AuthorizationServer.converter.finalproject;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.finalproject.ProjectDisplayResponseDto;
import np.com.cscpl.AuthorizationServer.model.ProjectDisplay;
import org.springframework.stereotype.Component;

@Component
public class ProjectDisplayConverter extends Converter<ProjectDisplayResponseDto, ProjectDisplay> {

    public ProjectDisplayConverter() {
        super(projectDisplayResponseDto -> new ProjectDisplay(
                projectDisplayResponseDto.getDisplayId(),
                projectDisplayResponseDto.getDisplayStatus(),
                projectDisplayResponseDto.getDisplayOrder()
        ), projectDisplay -> new ProjectDisplayResponseDto(
                projectDisplay.getId(),
                projectDisplay.getStatus(),
                projectDisplay.getFinalProject() != null ? projectDisplay.getFinalProject().getId() : 0,
                projectDisplay.getFinalProject() != null ? projectDisplay.getFinalProject().getTitle() : "",
                projectDisplay.getFinalProject() != null ? projectDisplay.getFinalProject().getAssignmentTitle() : "",
                projectDisplay.getFinalProject() != null ? projectDisplay.getFinalProject().getDescription() : "",
                projectDisplay.getFinalProject() != null ? projectDisplay.getFinalProject().getWorkDescription() : "",
                projectDisplay.getDisplayOrder()
        ));
    }
}
