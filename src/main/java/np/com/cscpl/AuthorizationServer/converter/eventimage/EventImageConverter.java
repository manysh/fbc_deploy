/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.converter.eventimage;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.eventImage.EventImageResponseDto;
import np.com.cscpl.AuthorizationServer.model.Event;
import np.com.cscpl.AuthorizationServer.model.EventImage;
import org.springframework.stereotype.Component;

/**
 *
 * @author puzansakya
 */
@Component
public class EventImageConverter extends Converter<EventImageResponseDto, EventImage> {

    public EventImageConverter() {
        super(eventImageResponseDTO -> new EventImage(
                eventImageResponseDTO.getId(),
                eventImageResponseDTO.getImagePath(),
                eventImageResponseDTO.getCaption(),
                new Event(eventImageResponseDTO.getEvent())
        ), eventImage -> new EventImageResponseDto(
                eventImage.getId(),
                eventImage.getImagePath(),
                eventImage.getCaption(),
                eventImage.getEvent().getId()
        ));

    }
}
