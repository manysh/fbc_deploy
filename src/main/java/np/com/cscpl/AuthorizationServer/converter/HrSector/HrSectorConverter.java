/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.converter.HrSector;

import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.event.EventResponseDto;
import np.com.cscpl.AuthorizationServer.dto.hrSector.HrSectorResponseDto;
import np.com.cscpl.AuthorizationServer.model.Event;
import np.com.cscpl.AuthorizationServer.model.EventType;
import np.com.cscpl.AuthorizationServer.model.HrSector;
import org.springframework.stereotype.Component;

/**
 *
 * @author puzansakya
 */
@Component
public class HrSectorConverter extends Converter<HrSectorResponseDto, HrSector> {

    public HrSectorConverter() {
        super(hrSectorResponseDto -> new HrSector(
                hrSectorResponseDto.getId(),
                hrSectorResponseDto.getName(),
                hrSectorResponseDto.getDetail(),                
                hrSectorResponseDto.isIsSub(),
                hrSectorResponseDto.isIsOther()
        ), hrSector -> new HrSectorResponseDto(
                hrSector.getId(),
                hrSector.getName(),
                hrSector.getDetail(),                
                hrSector.isIsSub(),
                hrSector.isIsOther()
        ));

    }

}
