/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package np.com.cscpl.AuthorizationServer.converter.finalHrUserConverter;

import java.time.LocalDate;
import java.util.stream.Collectors;
import np.com.cscpl.AuthorizationServer.converter.Converter;
import np.com.cscpl.AuthorizationServer.dto.degree.DegreeResponseDto;
import np.com.cscpl.AuthorizationServer.dto.finalHrUser.FinalHrUserResponseDto;
import np.com.cscpl.AuthorizationServer.dto.finalHrUserDegree.FinalHrUserDegreeResponseDto;
import np.com.cscpl.AuthorizationServer.dto.hrSector.HrSectorResponseDto;
import np.com.cscpl.AuthorizationServer.model.ConsultantType;
import np.com.cscpl.AuthorizationServer.model.Degree;
import np.com.cscpl.AuthorizationServer.model.District;
import np.com.cscpl.AuthorizationServer.model.FinalHrUser;
import np.com.cscpl.AuthorizationServer.model.FinalHrUserDegree;
import np.com.cscpl.AuthorizationServer.model.HrSector;
import np.com.cscpl.AuthorizationServer.model.ResourceType;
import org.springframework.stereotype.Component;

/**
 *
 * @author Puzan Sakya < puzan at puzansakya@gmail.com >
 */
@Component
public class FinalHrUserConverter extends Converter<FinalHrUserResponseDto, FinalHrUser> {

    public FinalHrUserConverter() {
        super(
                finalHrUserResponseDto -> new FinalHrUser(
                        finalHrUserResponseDto.getId(),
                        finalHrUserResponseDto.getFirstName(),
                        finalHrUserResponseDto.getMiddleName(),
                        finalHrUserResponseDto.getLastName(),
                        finalHrUserResponseDto.getPhoneNumber(),
                        finalHrUserResponseDto.getEmail(),
                        finalHrUserResponseDto.getJoinDate() != null ? LocalDate.parse(finalHrUserResponseDto.getJoinDate()) : null,
                        finalHrUserResponseDto.getDob() != null ? LocalDate.parse(finalHrUserResponseDto.getDob()) : null,
                        finalHrUserResponseDto.getAddress() != null ? finalHrUserResponseDto.getAddress() : null,
                        finalHrUserResponseDto.isIsConsultant(),
                        finalHrUserResponseDto.getFilePath(),
                        finalHrUserResponseDto.getConsutlanTypeId() != 0 ? new ConsultantType(finalHrUserResponseDto.getConsutlanTypeId()) : null,
                        new District(finalHrUserResponseDto.getDistrictId()),
                        finalHrUserResponseDto.getSectorId().stream().map(hrSectorid -> {
                            return new HrSector(
                                    hrSectorid
                            );
                        }).collect(Collectors.toList()),
                        finalHrUserResponseDto.getHrSubSectorId().stream().map(hrSubSectorid -> {
                            return new HrSector(
                                    hrSubSectorid
                            );
                        }).collect(Collectors.toList()),
                        finalHrUserResponseDto.getHrOtherSectorId().stream().map(hrOtherSectorid -> {
                            return new HrSector(
                                    hrOtherSectorid
                            );
                        }).collect(Collectors.toList()),
                        new ResourceType(finalHrUserResponseDto.getResourceTypeId()),
                        finalHrUserResponseDto.getFhudrd().stream().map(fhuDto -> {
                            return new FinalHrUserDegree(
                                    fhuDto.getCompletionDate(),
                                    new Degree(fhuDto.getDrd().getId())
                            );
                        }).collect(Collectors.toList())
                ),
                finalHrUser -> new FinalHrUserResponseDto(
                        finalHrUser.getId(),
                        finalHrUser.getFirstName(),
                        finalHrUser.getMiddleName(),
                        finalHrUser.getLastName(),
                        finalHrUser.getPhoneNumber(),
                        finalHrUser.getEmail(),
                        finalHrUser.getJoinDate() != null ? finalHrUser.getJoinDate().toString() : "N/A",
                        finalHrUser.getDob() != null ? finalHrUser.getDob().toString() : "N/A",
                        finalHrUser.getAddress().isEmpty() ? "N/A" : finalHrUser.getAddress(),
                        finalHrUser.isIsConsultant(),
                        finalHrUser.getFilePath(),
                        finalHrUser.getDistrict().getId(),
                        finalHrUser.getDistrict().getDistrictName(),
                        finalHrUser.getHrSectors().stream().map(hrSector -> {
                            return new HrSectorResponseDto(
                                    hrSector.getId(),
                                    hrSector.getName(),
                                    hrSector.getDetail(),
                                    hrSector.isIsSub(),
                                    hrSector.isIsOther()
                            );
                        }).collect(Collectors.toList()),
                        finalHrUser.getHrSubSector().stream().map(hrSubSector -> {
                            return new HrSectorResponseDto(
                                    hrSubSector.getId(),
                                    hrSubSector.getName(),
                                    hrSubSector.getDetail(),
                                    hrSubSector.isIsSub(),
                                    hrSubSector.isIsOther()
                            );
                        }).collect(Collectors.toList()),
                        finalHrUser.getHrOtherSectors().stream().map(hrOtherSector -> {
                            return new HrSectorResponseDto(
                                    hrOtherSector.getId(),
                                    hrOtherSector.getName(),
                                    hrOtherSector.getDetail(),
                                    hrOtherSector.isIsSub(),
                                    hrOtherSector.isIsOther()
                            );
                        }).collect(Collectors.toList()),
                        finalHrUser.getResourceType().getId(),
                        finalHrUser.getResourceType().getResourceTypeName(),
                        finalHrUser.getConsultantType() != null ? finalHrUser.getConsultantType().getId() : 0,
                        finalHrUser.getConsultantType() != null ? finalHrUser.getConsultantType().getConsultantTypeName() : "N/A",
                        finalHrUser.getFinalHrUserDegrees().stream().map(fhud -> {
                            return new FinalHrUserDegreeResponseDto(
                                    fhud.getId(),
                                    fhud.getCompletionDate(),
                                    new DegreeResponseDto(
                                            fhud.getDegree().getId(),
                                            fhud.getDegree().getName()
                                    )
                            );
                        }).collect(Collectors.toList())
                )
        );
    }

}
